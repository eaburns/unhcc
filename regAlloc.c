/*
 * regAlloc.c - register allocator for unhcc
 *
 * target is Intel IA-32
 *
 * (See regAlloc.h for general comments.)
 *
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "defs.h"
#include "encode.h"
#include "message.h"
#include "regAlloc.h"
#include "symtab.h"
#include "tarch.h"

/** \brief The value of the next available integer virtual register */
static unsigned long nextIntNum;

/** \brief The value of the next available float/double virtual register */
static unsigned long nextFloatNum;

/** \brief The value of the next available float/double virtual register */
static unsigned long nextLongDoubleNum;

/**
 * \brief A variable table entry -- just a linked list for now.
 */
struct vartab {
	/*
 	 * these three pieces of information disambiguate *all* local
 	 * variables and parameters.
 	 */

	/* offset on the stack */
	int offset;

	/* block depth */
	int block;

	/* this tag specifies PDECL or LDECL for parameters or locals
 	 * respectivly */
	STDR_TAG tag;

	/* the type of this variable. */
	Type type;

	/* !0 if the variable has its address taken in the current
 	 * function. */
	int addressed;

	/* the register allocated for this variable (or the memory
 	 * location of this variable). */
	REG reg;

	/* next table element */
	struct vartab *next;
};

/**
 * \brief A register descriptor.
 *
 * This is really a misnomer, this struct describes any location that
 * is the result of an expression tree.
 */
struct reg_desc {
	/* set to !0 if this register is not a temporary (it is the
 	 * location of a variable). */
	int notTemp;

	/* the type of this register */
	TypeTag type;

	char string[REG_STRING_MAX];

	/* integer types have other representations too */
	char byte_string[REG_STRING_MAX];
	char word_string[REG_STRING_MAX];
	char dword_string[REG_STRING_MAX];

	unsigned int baseReg;
	char base_string[REG_STRING_MAX];
};

/**
 * \brief The variable table.
 * This table keeps track of which variables are 'addressed' in a given
 * function.  An 'addressed' variable can not be stored in a register.
 */
struct vartab *vartab;

/**
 * \brief Initializes the virtual register allocator.
 */
void initRegAlloc(void)
{
	nextIntNum = 0;
	nextFloatNum = 0;
}

/**
 * \brief Prints the value of the virtual register.
 */
void printReg(REG r)
{
	struct reg_desc *desc;

	desc = r;

	if (!desc)
		msgn("<none>");
	else
		msgn(desc->string);
}

/**
 * \brief Allocates a register handle that describes a new virtual register
 */
static REG allocIntReg(TypeTag type)
{
	struct reg_desc *reg;

	reg = calloc(1, sizeof(*reg));
	if (!reg)
		bug("allocReg: Out of memory");

	reg->type = type;

	snprintf(reg->byte_string, REG_STRING_MAX, "%%vd%lub", nextIntNum);
	snprintf(reg->word_string, REG_STRING_MAX, "%%vd%luw", nextIntNum);
	snprintf(reg->dword_string, REG_STRING_MAX, "%%vd%lud", nextIntNum);
	snprintf(reg->string, REG_STRING_MAX, "%%vd%lu", nextIntNum);

	nextIntNum += 1;

	/* check for overflow */
	if (nextIntNum == 0)
		bug("allocIntReg: nextIntNum wrapped");

	return reg;
}

/**
 * \brief Allocates a new 'float' or 'double' register descriptor
 */
static REG allocFloatReg(TypeTag type)
{
	struct reg_desc *reg;

	reg = allocReg(type, "%%vf%lu", nextFloatNum);

	nextFloatNum += 1;

	/* check for overflow */
	if (nextFloatNum == 0)
		bug("allocFloatReg: nextFloatNum wrapped");

	return reg;
}

/**
 * \brief Allocates a new 'long double' register descriptor
 */
static REG allocLongDoubleReg(TypeTag type)
{
	struct reg_desc *reg;

	reg = allocReg(type, "%%vLf%lu", nextLongDoubleNum);

	nextLongDoubleNum += 1;

	/* check for overflow */
	if (nextLongDoubleNum == 0)
		bug("allocLongDoubleReg: nextLongDoubleNum wrapped");

	return reg;
}

/**
 * \brief Allocates the next register in the appropriate class given the
 *        TagType.
 */
REG allocNextReg(TypeTag type)
{
	switch (type) {
	case TYFLOAT:
	case TYDOUBLE:
		return allocFloatReg(type);

	case TYLONGDOUBLE:
		return allocLongDoubleReg(type);

	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYSIGNEDLONGINT:
	case TYPOINTER:
		return allocIntReg(type);

	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
		/* structs/unions are just pointers really */
		return allocNextReg(TYPOINTER);

	default:
		bug("allocNextReg: called with an invalid type=%d",
		    type);
	}

	/* not reached */
	return NULL;
}

/**
 * \brief Allocates a register handle that describes a constant value
 */
REG allocReg(TypeTag type, const char *format, ...)
{
	struct reg_desc *reg;
	va_list ap;

	reg = calloc(1, sizeof(*reg));
	if (!reg)
		bug("allocReg: Out of memory");

	reg->type = type;

	va_start(ap, format);
	vsnprintf(reg->string, REG_STRING_MAX, format, ap);
	va_end(ap);

	va_start(ap, format);
	vsnprintf(reg->byte_string, REG_STRING_MAX, format, ap);
	va_end(ap);

	va_start(ap, format);
	vsnprintf(reg->word_string, REG_STRING_MAX, format, ap);
	va_end(ap);

	va_start(ap, format);
	vsnprintf(reg->dword_string, REG_STRING_MAX, format, ap);
	va_end(ap);

	return reg;
}

/**
 * \brief Returns true if the given handle refers to a register (as
 *        opposed to a constant or memory location)/.
 */
int isRegister(REG handle)
{
	return handle && ((struct reg_desc *) handle)->string[0] == '%';
}

/**
 * \brief Returns true if the given handle refers to a constant (as
 *        opposed to a register or memory location)/.
 */
int isConstant(REG handle)
{
	return handle && ((struct reg_desc *) handle)->string[0] == '$';
}

/**
 * \brief Returns true if the given handle refers to a memory location (as
 *        opposed to a register or constant value)/.
 */
int isMemory(REG handle)
{
	return handle && !isRegister(handle) && !isConstant(handle);
}

/**
 * \brief Returns true if the given register is a temporary register.
 */
int isTemp(REG handle)
{
	return handle && !((struct reg_desc *) handle)->notTemp;
}

/**
 * \brief Sets the notTemp flag on the given register, this register is
 *        the location of a variable and must not be destroyed in
 *        temporary expression evaluation.
 */
static void setNotTemp(REG handle)
{
	((struct reg_desc *) handle)->notTemp = 1;
}

/**
 * \brief Gives the string that represents a register.  this can be
 *        used in the output code.
 */
const char *regString(REG handle)
{
	if (!handle)
		bug("regString: handle is NULL");

	return regStringAs(((struct reg_desc *) handle)->type, handle);
}

/**
 * \brief Gives the string that represents a register in a given type.
 *        This can be used in the output code.
 */
const char *regStringAs(TypeTag type, REG handle)
{
	struct reg_desc *reg;

	if (!handle)
		bug("regStringAs: handle is NULL");

	reg = handle;

	switch (type) {
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		return reg->byte_string;

	case TYSIGNEDSHORTINT:
	case TYUNSIGNEDSHORTINT:
		return reg->word_string;

	case TYUNSIGNEDINT:
	case TYSIGNEDINT:
		return reg->dword_string;

	default:
		return reg->string;
	}
}

/**
 * \brief Gets the type of the given register.
 */
TypeTag regType(REG handle)
{
	if (!handle)
		bug("regType called with NULL handle");

	return ((struct reg_desc *) handle)->type;
}

/**
 * Set the base register for a descriptor representing a memory
 * location.  Each time this location is used, we need to use the
 * base register.
 *
 * \param handle The register handle.
 * \param fmt The printf() style format string.
 */
void regMemBaseReg(REG handle, const char *fmt, ...)
{
	struct reg_desc *r;
	va_list ap;

	r = handle;

	r->baseReg = 1;

	va_start(ap, fmt);
	vsnprintf(r->base_string, REG_STRING_MAX, fmt, ap);
	va_end(ap);
}

/**
 * \brief Annotates the output code with the use of a given register.
 */
void useReg(REG handle)
{
	struct reg_desc *r;

	if (!handle)
		bug("useReg: called with NULL handle");


	r = handle;

	if (isMemory(r) && r->baseReg != 0)
		emit("!%s %s", USE_STRING, r->base_string);

	/* no annotation needed for long doubles, they just use the fp
 	 * stack. */
	if (regType(handle) == TYLONGDOUBLE)
		return;

	if (isRegister(handle))
		emit("!%s %s", USE_STRING, regString(handle));

}

/**
 * \brief Annotates the output code with the definition of a given register.
 */
void defReg(REG handle)
{
	if (!handle)
		bug("defReg: called with NULL handle");

	/* no annotation needed for long doubles, they just use the fp
 	 * stack. */
	if (regType(handle) == TYLONGDOUBLE)
		return;

	if (isRegister(handle))
		emit("!%s %s", DEF_STRING, regString(handle));
}

/**
 * \brief Gets a new result register, or uses temporary register passed
 *        in the potResult parameter.
 *
 * The point here is: instructions that over-write the value of
 * one of their register operands with their result need to make a copy
 * of the register if it is not an expression temporary register (a
 * variable register for example).  This routine handles that.  If needed
 * a copy of the non-temporary register is put in a new temporary
 * register.
 *
 * Another issue: if the would-be-result register of an instruction is
 * really a constant, or memory, we need to make a copy of it into a
 * temporary register and use that instead.
 *
 * \param potReg The potential result register.
 *
 * \return A temporary register that contains the same value as the
 *         potReg parameter.  This could either be the potReg parameter
 *         itself (if it was a temporary), or a copy (if it was not
 *         temporary or was not a register).
 */
REG getResultReg(REG potReg)
{
	REG reg;

	if (regType(potReg) == TYLONGDOUBLE) {
		/* for long double, use the fp stack top */
		return allocNextReg(TYLONGDOUBLE);
	}

	if (!isTemp(potReg) || !isRegister(potReg)) {
		/* if the register is not temporary, or the reg desc is
 		 * for memory and the type is not long double, get a new
 		 * result register. */
		reg = allocNextReg(regType(potReg));
		comment1("copy non-temporary, %s", regString(potReg));
		useReg(potReg);
		move(potReg, reg);
		defReg(reg);
		return reg;
	}

	return potReg;
}

/**
 * \brief This routine returns a register if the parameter 'r' describes
 *        a memory location or a constant.  It is slightly more basic
 *        than the getResultReg() routine because it will return the
 *        parameter even if it is a non-temporary register.
 *
 *  This function is ment to be used in cases where a register is
 *  required, but it is not going to get clobbered (comparison
 *  instructions that can't be memory vs memory or constant vs constant
 *  or memory vs constant... etc).
 */
REG getRegister(REG r)
{
	REG ret;

	if (!isRegister(r)) {
		ret = allocNextReg(regType(r));
		comment1("copy non-register to temporary register");
		useReg(r);
		move(r, ret);
		defReg(ret);
	} else {
		ret = r;
	}

	return ret;
}

/**
 * \brief Adds a variable to the variable tab.
 */
void vartabAdd(int offset, int block, STDR_TAG tag, Type type)
{
	struct vartab *ent;

	ent = calloc(1, sizeof(*ent));
	if (!ent)
		bug("vartabAdd: Out of memory.");
	ent->offset = offset;
	ent->block = block;
	ent->tag = tag;
	ent->type = type;
	ent->next = vartab;
	vartab = ent;

#if defined(VARTAB_DEBUG)
	fprintf(stderr, "vartab: adding variable "
	        "offset=%d, block=%d,tag=%s\n",
	        offset, block, tag == PDECL ? "parameter" : "local");
#endif				/* VARTAB_DEBUG */
}

/**
 * \brief Returns the given variable table entry, if found, else NULL.
 */
static struct vartab *vartabFind(int offset, int block, STDR_TAG tag)
{
	struct vartab *p;

	p = vartab;
	while (p) {
		if (p->offset == offset && p->block == block
		    && p->tag == tag)
			break;

		p = p->next;
	}

	return p;
}

/**
 * \brief Marks a variable in the vartab as addressed.
 * This function marks the given ST_ID as 'addressed'.  An 'addressed'
 * variable must not be stored in a register in this function.
 */
void vartabAddressed(int offset, int block, STDR_TAG tag)
{
	struct vartab *p;

	p = vartabFind(offset, block, tag);

	if (p) {
#if defined(VARTAB_DEBUG)
		fprintf(stderr, "vartab: marking as addressed "
		        "offset=%d, block=%d, tag=%s\n",
		        offset, block, tag == PDECL ? "parameter" : "local");
#endif				/* VARTAB_DEBUG */
		p->addressed = 1;
	}
}

/*
 * Returns true if the given variable is addressed in the current
 * function.
 */
int isAddressed(int offset, int block, STDR_TAG tag)
{
	struct vartab *p;

	p = vartabFind(offset, block, tag);

	return p && p->addressed;
}

/**
 * \brief Clears the variable table.
 */
void vartabClear(void)
{
	struct vartab *p, *q;

	p = vartab;
	while (p) {
		q = p->next;
		free(p);
		p = q;
	}

	vartab = NULL;
#if defined(VARTAB_DEBUG)
	fprintf(stderr, "vartab: cleared\n");
#endif				/* VARTAB_DEBUG */
}

/**
 * \brief Prints the variable table.
 */
void vartabPrint(void)
{
	struct vartab *p;

	p = vartab;
	while (p) {
		fprintf(stderr, "offset=%d, block=%d, tag=%s %s\n",
		        p->offset, p->block,
		        p->tag == PDECL ? "parameter" : "local",
			p->reg ? regString(p->reg) : "");
		p = p->next;
	}
}

/**
 * \brief Sets the register descriptor for the given variable.
 */
void vartabSetReg(int offset, int block, STDR_TAG tag, REG reg)
{
	struct vartab *p;

	p = vartabFind(offset, block, tag);

	if (!p)
		bug("variable not found");

	if (isRegister(reg) && p->addressed) {
		bug("variable offset=%d block=%d tag=%s "
		    "is addressed and allocated a register",
		    p->offset, p->block,
		    p->tag == PDECL ? "parameter" : "local");
	}

#if defined(VARTAB_DEBUG)
	fprintf(stderr, "vartabSetReg(offset=%d, block=%d, tag=%s, %s)\n",
		p->offset, p->block, p->tag == PDECL ? "parameter" : "local",
	        regString(reg));
#endif /* VARTAB_DEBUG */

	p->reg = reg;
	setNotTemp(reg);
}

/**
 * \brief Allocates a register for any variable that doesn't currently
 *        have one.
 *
 * \note This is crappy, it is tarch specific and shouldn't be in this
 *       file.
 */
void vartabAllocRegs(int sizeOfLocalsArea)
{
	struct vartab *p;
	TypeTag type;

	p = vartab;
	while (p) {
		if (!p->reg) {
			type = typeQuery(p->type);

			if (type == TYSTRUCT || type == TYUNION
			    || type == TYARRAY) {
				p->reg = allocNextReg(type);
				setNotTemp(p->reg);
				emit("\tleaq\t-%d(%%rbp), %s",
				     sizeOfLocalsArea - p->offset,
				     regString(p->reg));
				defReg(p->reg);
			} else if (p->addressed || type == TYLONGDOUBLE) {
				p->reg = allocReg(type, "-%d(%%rbp)",
				                  sizeOfLocalsArea
				                  - p->offset);
				setNotTemp(p->reg);
			} else {
				p->reg = allocNextReg(type);
				setNotTemp(p->reg);
			}
			comment1("offset=%d, block=%d, tag=%s allocated %s",
			         p->offset, p->block,
			         p->tag == PDECL ? "parameter" : "local",
			         regString(p->reg));

		}
		p = p->next;
	}
}

/**
 * \brief Gets the register descriptor for the given variable.
 */
REG vartabGetReg(int offset, int block, STDR_TAG tag)
{
	struct vartab *p;

	p = vartabFind(offset, block, tag);

	if (p)
		return p->reg;

	return NULL;
}
