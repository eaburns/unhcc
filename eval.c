/**
 * \file eval.c
 * \brief Contains functions for performing compile-time evaluation of
 *        constant expressions.
 * \author Ethan Burns
 * \date 2007-09-22
 */

#define _POSIX_C_SOURCE 200112L
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>

#include "message.h"
#include "tree.h"

/**
 * \brief Re-allocates a string using the symbol table string pool.
 *
 * \param strp A pointer to a string that is freeable with free().
 *
 * \note This is to prevent a memory lead when giving floating point
 *       constants new values.  The issue here is that the string pool
 *       in the symbol table is just a big memory leak anyway right now.
 */
static void st_realloc_str(char **strp)
{
	char *str;

	/* this call is fatal if there isn't enough memory. */
	str = st_save_string(*strp);

	free(*strp);
	*strp = str;
}

/**
 * \brief Evaluates a logical not operation on a constant operand.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 * \note This function should never get called.
 */
static ExpTree eval_LNOT_OP(UnopTree unop)
{
	IntConstTree int_const;
	FpConstTree fp_const;
	LongConstTree long_const;
	UIntConstTree uint_const;
	ULongConstTree ulong_const;
	long double ld;

	switch (unop->left->absn.tag) {
	case INT_CONST_TAG:
		int_const = (IntConstTree) unop->left;
		int_const->value = !int_const->value;
		break;

	case FP_CONST_TAG:
		fp_const = (FpConstTree) unop->left;
		sscanf(fp_const->value, "%Lf", &ld);
		int_const = (IntConstTree) newIntConst(!ld);
		break;

	case LONG_CONST_TAG:
		long_const = (LongConstTree) unop->left;
		int_const = (IntConstTree) newIntConst(!long_const->value);
		break;
	case UINT_CONST_TAG:
		uint_const = (UIntConstTree) unop->left;
		int_const = (IntConstTree) newIntConst(!uint_const->value);
		break;
	case ULONG_CONST_TAG:
		ulong_const = (ULongConstTree) unop->left;
		int_const = (IntConstTree) newIntConst(!ulong_const->value);
		break;
	default:
		bugT((Tree) unop,
		     "eval_LNOT_OP called with a non-const operand");
	}

	return (ExpTree) int_const;
}

/**
 * \brief Evaluates a convert operation on a constant operand.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_CONVERT_OP(UnopTree unop)
{
	TypeTag dest_type;
	IntConstTree int_const;
	FpConstTree fp_const;
	LongConstTree long_const;
	UIntConstTree uint_const;
	ULongConstTree ulong_const;
	char *buf;
	long double ld;

	dest_type = typeQuery(unop->expn.type);

	switch (unop->left->absn.tag) {
	case INT_CONST_TAG:
		int_const = (IntConstTree) unop->left;
		switch (dest_type) {
		case TYFLOAT:
		case TYDOUBLE:
		case TYLONGDOUBLE:
			asprintf(&buf, "%d", int_const->value);
			return newFpConst(buf, unop->expn.type);
		case TYSIGNEDCHAR:
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
			/* no actual conversion needed */
			return (ExpTree) int_const;
		case TYSIGNEDLONGINT:
			return newLongConst(int_const->value);
		case TYUNSIGNEDCHAR:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
			return newUIntConst(int_const->value);
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			return newULongConst(int_const->value);
		default:
			bugT((Tree) unop,
			     "Invalid constant conversion");
		}
	case FP_CONST_TAG:
		fp_const = (FpConstTree) unop->left;
		/* scan the float value into a long double */
		sscanf(fp_const->value, "%Lf", &ld);
		switch (dest_type) {
		case TYFLOAT:
		case TYDOUBLE:
		case TYLONGDOUBLE:
			fp_const->expn.type = unop->expn.type;
			return (ExpTree) fp_const;
		case TYSIGNEDCHAR:
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
			return newIntConst(ld);
		case TYSIGNEDLONGINT:
			return newLongConst(ld);
		case TYUNSIGNEDCHAR:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
			return newUIntConst(ld);
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			return newULongConst(ld);
		default:
			bugT((Tree) unop,
			     "Invalid constant conversion");
		}
	case LONG_CONST_TAG:
		long_const = (LongConstTree) unop->left;
		switch (dest_type) {
		case TYFLOAT:
		case TYDOUBLE:
		case TYLONGDOUBLE:
			asprintf(&buf, "%ld", long_const->value);
			return newFpConst(buf, unop->expn.type);
		case TYSIGNEDCHAR:
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
			return newIntConst(long_const->value);
		case TYSIGNEDLONGINT:
			/* no conversion needed */
			return (ExpTree) long_const;
		case TYUNSIGNEDCHAR:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
			return newUIntConst(long_const->value);
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			return newULongConst(long_const->value);
		default:
			bugT((Tree) unop,
			     "Invalid constant conversion");
		}
	case UINT_CONST_TAG:
		uint_const = (UIntConstTree) unop->left;
		switch (dest_type) {
		case TYFLOAT:
		case TYDOUBLE:
		case TYLONGDOUBLE:
			asprintf(&buf, "%u", uint_const->value);
			return newFpConst(buf, unop->expn.type);
		case TYSIGNEDCHAR:
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
			return newIntConst(uint_const->value);
		case TYSIGNEDLONGINT:
			return newLongConst(uint_const->value);
		case TYUNSIGNEDCHAR:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
			/* no conversion needed */
			return (ExpTree) uint_const;
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			return newULongConst(uint_const->value);
		default:
			bugT((Tree) unop,
			     "Invalid constant conversion");
		}
	case ULONG_CONST_TAG:
		ulong_const = (ULongConstTree) unop->left;
		switch (dest_type) {
		case TYFLOAT:
		case TYDOUBLE:
		case TYLONGDOUBLE:
			asprintf(&buf, "%lu", ulong_const->value);
			return newFpConst(buf, unop->expn.type);
		case TYSIGNEDCHAR:
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
			return newIntConst(ulong_const->value);
		case TYSIGNEDLONGINT:
			return newLongConst(ulong_const->value);
		case TYUNSIGNEDCHAR:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
			return newUIntConst(ulong_const->value);
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			/* no conversion needed */
			return (ExpTree) ulong_const;
		default:
			bugT((Tree) unop,
			     "Invalid constant conversion");
		}
	default:
		bugT((Tree) unop,
		     "eval_CONVERT_OP called with a non-const operand");
	}

	/* not reachable */
	return NULL;
}

/**
 * \brief Evaluates a unary add operation on a constant operand.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_UADD_OP(UnopTree unop)
{
	/* nothing to do, just get rid of this node. */
	return unop->left;
}

/**
 * \brief Evaluates a unary subtraction operation on a constant operand.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_USUB_OP(UnopTree unop)
{
	IntConstTree int_const;
	FpConstTree fp_const;
	LongConstTree long_const;
	UIntConstTree uint_const;
	ULongConstTree ulong_const;
	char *buf;

	switch (unop->left->absn.tag) {
	case INT_CONST_TAG:
		int_const = (IntConstTree) unop->left;
		int_const->value = -int_const->value;
		break;
	case FP_CONST_TAG:
		fp_const = (FpConstTree) unop->left;
		if (fp_const->value[0] != '-') {
			asprintf(&buf, "-%s", fp_const->value);
			st_realloc_str(&buf);
		} else {
			asprintf(&buf, "%s", fp_const->value + 1);
			st_realloc_str(&buf);
		}
		fp_const->value = buf;
		break;
	case LONG_CONST_TAG:
		long_const = (LongConstTree) unop->left;
		long_const->value = -long_const->value;
		break;
	case UINT_CONST_TAG:
		uint_const = (UIntConstTree) unop->left;
		uint_const->value = -uint_const->value;
		break;
	case ULONG_CONST_TAG:
		ulong_const = (ULongConstTree) unop->left;
		ulong_const->value = -ulong_const->value;
		break;
	default:
		bugT((Tree) unop,
		     "eval_USUB_OP called with a non-const operand");
	}

	return unop->left;
}

/**
 * \brief Evaluates a unary compliment operation on a constant operand.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_COMPLMT_OP(UnopTree unop)
{
	IntConstTree int_const;
	LongConstTree long_const;
	UIntConstTree uint_const;
	ULongConstTree ulong_const;

	switch (unop->left->absn.tag) {
	case INT_CONST_TAG:
		int_const = (IntConstTree) unop->left;
		int_const->value = ~int_const->value;
		break;
	case FP_CONST_TAG:
		bugT((Tree) unop, "Non-integral operand to one's "
		     "compliment operator\n");
	case LONG_CONST_TAG:
		long_const = (LongConstTree) unop->left;
		long_const->value = ~long_const->value;
		break;
	case UINT_CONST_TAG:
		uint_const = (UIntConstTree) unop->left;
		uint_const->value = ~uint_const->value;
		break;
	case ULONG_CONST_TAG:
		ulong_const = (ULongConstTree) unop->left;
		ulong_const->value = ~ulong_const->value;
		break;
	default:
		bugT((Tree) unop,
		     "eval_COMPLMT_OP called with a non-const operand");
	}

	return unop->left;
}

/**
 * \brief Performs compile-time evaluation on a constant unary expression.
 * \return A CONST expression node.
 */
ExpTree evalUnopTree(UnopTree unop)
{
	ExpTree ret;

	if (unop->expn.absn.tag != UNOP_TAG)
		bugT((Tree) unop,
		     "evalUnopTree called with non-UNOP_TAG");

	if (!isCompileTimeEvalable(unop->op)) {
		bugT((Tree) unop, "evalUnopTree called with a "
		     "non-compile-time evaluatable operation");
	}

	if (!isConstTag(unop->left->absn.tag)) {
		bugT((Tree) unop,
		     "evalUnopTree called with a non-const operand");
	}

	switch (unop->op) {
	case LNOT_OP:
		ret = eval_LNOT_OP(unop);
		break;
	case CONVERT_OP:
		ret = eval_CONVERT_OP(unop);
		break;
	case UADD_OP:
		ret = eval_UADD_OP(unop);
		break;
	case USUB_OP:
		ret = eval_USUB_OP(unop);
		break;
	case COMPLMT_OP:
		ret = eval_COMPLMT_OP(unop);
		break;
	default:
		bugT((Tree) unop, "evalUnopTree called with a "
		     "non-compile-time evaluatable operation");
	}

	return ret;
}

/**
 * \brief Evaluates a multiplication operation that has two constant
 *        opreands.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_MULT_OP(BinopTree binop)
{
	IntConstTree int_left, int_right;
	LongConstTree long_left, long_right;
	UIntConstTree uint_left, uint_right;
	ULongConstTree ulong_left, ulong_right;
	FpConstTree fp_left, fp_right;
	long double ld_left, ld_right;
	char *buf;

	switch (binop->left->absn.tag) {
	case INT_CONST_TAG:
		int_left = (IntConstTree) binop->left;
		int_right = (IntConstTree) binop->right;
		int_left->value *= int_right->value;
		return binop->left;
	case LONG_CONST_TAG:
		long_left = (LongConstTree) binop->left;
		long_right = (LongConstTree) binop->right;
		long_left->value *= long_right->value;
		return binop->left;
	case UINT_CONST_TAG:
		uint_left = (UIntConstTree) binop->left;
		uint_right = (UIntConstTree) binop->right;
		uint_left->value *= uint_right->value;
		return binop->left;
	case ULONG_CONST_TAG:
		ulong_left = (ULongConstTree) binop->left;
		ulong_right = (ULongConstTree) binop->right;
		ulong_left->value *= ulong_right->value;
		return binop->left;
	case FP_CONST_TAG:
		fp_left = (FpConstTree) binop->left;
		fp_right = (FpConstTree) binop->right;
		sscanf(fp_left->value, "%Lf", &ld_left);
		sscanf(fp_right->value, "%Lf", &ld_right);
		ld_left *= ld_right;
		asprintf(&buf, "%Lf", ld_left);
		st_realloc_str(&buf);
		fp_left->value = buf;
		return binop->left;
	default:
		bugT((Tree) binop,
		     "eval_MULT_OP called with non-const operands");
	}

	/* not reached */
	return NULL;
}

/**
 * \brief Evaluates a division operation that has two constant
 *        opreands.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_DIV_OP(BinopTree binop)
{
	IntConstTree int_left, int_right;
	LongConstTree long_left, long_right;
	UIntConstTree uint_left, uint_right;
	ULongConstTree ulong_left, ulong_right;
	FpConstTree fp_left, fp_right;
	long double ld_left, ld_right;
	char *buf;

	switch (binop->left->absn.tag) {
	case INT_CONST_TAG:
		int_left = (IntConstTree) binop->left;
		int_right = (IntConstTree) binop->right;
		if (int_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		int_left->value /= int_right->value;
		return binop->left;
	case LONG_CONST_TAG:
		long_left = (LongConstTree) binop->left;
		long_right = (LongConstTree) binop->right;
		if (long_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		long_left->value /= long_right->value;
		return binop->left;
	case UINT_CONST_TAG:
		uint_left = (UIntConstTree) binop->left;
		uint_right = (UIntConstTree) binop->right;
		if (uint_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		uint_left->value /= uint_right->value;
		return binop->left;
	case ULONG_CONST_TAG:
		ulong_left = (ULongConstTree) binop->left;
		ulong_right = (ULongConstTree) binop->right;
		if (ulong_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		ulong_left->value /= ulong_right->value;
		return binop->left;
	case FP_CONST_TAG:
		fp_left = (FpConstTree) binop->left;
		fp_right = (FpConstTree) binop->right;
		sscanf(fp_left->value, "%Lf", &ld_left);
		sscanf(fp_right->value, "%Lf", &ld_right);
		if (ld_right == 0)
			bugT((Tree) binop, "Division by zero");
		ld_left /= ld_right;
		asprintf(&buf, "%Lf", ld_left);
		st_realloc_str(&buf);
		fp_left->value = buf;
		return binop->left;
	default:
		bugT((Tree) binop,
		     "eval_DIV_OP called with non-const operands");
	}

	/* not reached */
	return NULL;
}

/**
 * \brief Evaluates a modulus operation that has two constant
 *        opreands.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_MOD_OP(BinopTree binop)
{
	IntConstTree int_left, int_right;
	LongConstTree long_left, long_right;
	UIntConstTree uint_left, uint_right;
	ULongConstTree ulong_left, ulong_right;

	switch (binop->left->absn.tag) {
	case INT_CONST_TAG:
		int_left = (IntConstTree) binop->left;
		int_right = (IntConstTree) binop->right;
		if (int_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		int_left->value %= int_right->value;
		return binop->left;
	case LONG_CONST_TAG:
		long_left = (LongConstTree) binop->left;
		long_right = (LongConstTree) binop->right;
		if (long_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		long_left->value %= long_right->value;
		return binop->left;
	case UINT_CONST_TAG:
		uint_left = (UIntConstTree) binop->left;
		uint_right = (UIntConstTree) binop->right;
		if (uint_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		uint_left->value %= uint_right->value;
		return binop->left;
	case ULONG_CONST_TAG:
		ulong_left = (ULongConstTree) binop->left;
		ulong_right = (ULongConstTree) binop->right;
		if (ulong_right->value == 0)
			bugT((Tree) binop, "Division by zero");
		ulong_left->value %= ulong_right->value;
		return binop->left;
	case FP_CONST_TAG:
		bugT((Tree) binop, "eval_MOD_OP: Invalid operands "
		     "to % operator");
	default:
		bugT((Tree) binop,
		     "eval_MOD_OP called with non-const operands");
	}

	/* not reached */
	return NULL;
}

/**
 * \brief Evaluates an add operation that has two constant
 *        opreands.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_ADD_OP(BinopTree binop)
{
	IntConstTree int_left, int_right;
	LongConstTree long_left, long_right;
	UIntConstTree uint_left, uint_right;
	ULongConstTree ulong_left, ulong_right;
	FpConstTree fp_left, fp_right;
	long double ld_left, ld_right;
	char *buf;

	switch (binop->left->absn.tag) {
	case INT_CONST_TAG:
		int_left = (IntConstTree) binop->left;
		int_right = (IntConstTree) binop->right;
		int_left->value += int_right->value;
		return binop->left;
	case LONG_CONST_TAG:
		long_left = (LongConstTree) binop->left;
		long_right = (LongConstTree) binop->right;
		long_left->value += long_right->value;
		return binop->left;
	case UINT_CONST_TAG:
		uint_left = (UIntConstTree) binop->left;
		uint_right = (UIntConstTree) binop->right;
		uint_left->value += uint_right->value;
		return binop->left;
	case ULONG_CONST_TAG:
		ulong_left = (ULongConstTree) binop->left;
		ulong_right = (ULongConstTree) binop->right;
		ulong_left->value += ulong_right->value;
		return binop->left;
	case FP_CONST_TAG:
		fp_left = (FpConstTree) binop->left;
		fp_right = (FpConstTree) binop->right;
		sscanf(fp_left->value, "%Lf", &ld_left);
		sscanf(fp_right->value, "%Lf", &ld_right);
		ld_left += ld_right;
		asprintf(&buf, "%Lf", ld_left);
		st_realloc_str(&buf);
		fp_left->value = buf;
		return binop->left;
	default:
		bugT((Tree) binop,
		     "eval_ADD_OP called with non-const operands");
	}

	/* not reached */
	return NULL;
}

/**
 * \brief Evaluates a subtraction operation that has two constant
 *        opreands.
 * \return A CONST tree node.
 * \note This function does not free the evaluated and discarded
 *       UnopTree.
 */
static ExpTree eval_SUB_OP(BinopTree binop)
{
	IntConstTree int_left, int_right;
	LongConstTree long_left, long_right;
	UIntConstTree uint_left, uint_right;
	ULongConstTree ulong_left, ulong_right;
	FpConstTree fp_left, fp_right;
	long double ld_left, ld_right;
	char *buf;

	switch (binop->left->absn.tag) {
	case INT_CONST_TAG:
		int_left = (IntConstTree) binop->left;
		int_right = (IntConstTree) binop->right;
		int_left->value -= int_right->value;
		return binop->left;
	case LONG_CONST_TAG:
		long_left = (LongConstTree) binop->left;
		long_right = (LongConstTree) binop->right;
		long_left->value -= long_right->value;
		return binop->left;
	case UINT_CONST_TAG:
		uint_left = (UIntConstTree) binop->left;
		uint_right = (UIntConstTree) binop->right;
		uint_left->value -= uint_right->value;
		return binop->left;
	case ULONG_CONST_TAG:
		ulong_left = (ULongConstTree) binop->left;
		ulong_right = (ULongConstTree) binop->right;
		ulong_left->value -= ulong_right->value;
		return binop->left;
	case FP_CONST_TAG:
		fp_left = (FpConstTree) binop->left;
		fp_right = (FpConstTree) binop->right;
		sscanf(fp_left->value, "%Lf", &ld_left);
		sscanf(fp_right->value, "%Lf", &ld_right);
		ld_left -= ld_right;
		asprintf(&buf, "%Lf", ld_left);
		st_realloc_str(&buf);
		fp_left->value = buf;
		return binop->left;
	default:
		bugT((Tree) binop,
		     "eval_SUB_OP called with non-const operands");
	}

	/* not reached */
	return NULL;
}

ExpTree evalBinopTree(BinopTree binop)
{
	ExpTree ret;

	if (binop->expn.absn.tag != BINOP_TAG)
		bugT((Tree) binop,
		     "evalBinopTree called with non-BINOP_TAG");

	if (!isCompileTimeEvalable(binop->op)) {
		bugT((Tree) binop, "evalBinopTree called with a "
		     "non-compile-time evaluatable operation");
	}
	if (!isConstTag(binop->left->absn.tag)) {
		bugT((Tree) binop,
		     "evalBinopTree called with a non-const left operand");
	}
	if (!isConstTag(binop->right->absn.tag)) {
		bugT((Tree) binop,
		     "evalBinopTree called with a non-const right operand");
	}
	if (binop->left->absn.tag != binop->right->absn.tag) {
		/* both operands should have the same tag, because
		   conversion nodes are added during semantic analysis
		   and constant conversions are done in-line at the
		   creation of a conversion node */
		bugT((Tree) binop, "evalBinopTree called when both "
		     "operands have different tags");
	}

	switch (binop->op) {
	case INDEX_OP:
		ret = (ExpTree) binop;
		break;
	case LSHIFT_OP:
		ret = (ExpTree) binop;
		break;
	case RSHIFT_OP:
		ret = (ExpTree) binop;
		break;
	case ANDD_OP:
		ret = (ExpTree) binop;
		break;
	case XORR_OP:
		ret = (ExpTree) binop;
		break;
	case ORR_OP:
		ret = (ExpTree) binop;
		break;
	case MULT_OP:
		ret = eval_MULT_OP(binop);
		break;
	case DIV_OP:
		ret = eval_DIV_OP(binop);
		break;
	case MOD_OP:
		ret = eval_MOD_OP(binop);
		break;
	case ADD_OP:
		ret = eval_ADD_OP(binop);
		break;
	case SUB_OP:
		ret = eval_SUB_OP(binop);
		break;
	case LT_OP:
		ret = (ExpTree) binop;
		break;
	case GT_OP:
		ret = (ExpTree) binop;
		break;
	case LTE_OP:
		ret = (ExpTree) binop;
		break;
	case GTE_OP:
		ret = (ExpTree) binop;
		break;
	case EQL_OP:
		ret = (ExpTree) binop;
		break;
	case NEQL_OP:
		ret = (ExpTree) binop;
		break;
	case LAND_OP:
		ret = (ExpTree) binop;
		break;
	case LOR_OP:
		ret = (ExpTree) binop;
		break;
	default:
		bugT((Tree) binop, "evalBinopTree called with a "
		     "non-compile-time evaluatable operation");
	}

	return ret;
}

/* vi: set tabstop=8 textwidth=72: */
