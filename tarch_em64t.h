/**
 * \file tarch_em64t.h
 * \brief Contains declarations for the target architecture em64t.
 * \author Ethan Burns
 * \date 2007-09-10
 */
#ifndef _TARCH_EM64T_H_
#define _TARCH_EM64T_H_

#include "types.h"

/* data type sizes in bytes */
#define TARCH_SIZEOF_SCHAR	1	/* signed char */
#define TARCH_SIZEOF_UCHAR	1	/* unsigned char */
#define TARCH_SIZEOF_SSHORT	2	/* signed short */
#define TARCH_SIZEOF_USHORT	2	/* unsigned short */
#define TARCH_SIZEOF_SINT	4	/* signed int */
#define TARCH_SIZEOF_UINT	4	/* unsigned int */
#define TARCH_SIZEOF_SLONG	8	/* signed long */
#define TARCH_SIZEOF_ULONG	8	/* unsigned long and pointer
					   (see tarch.h) */
#define TARCH_SIZEOF_FLOAT	4	/* float */
#define TARCH_SIZEOF_DOUBLE	8	/* double */
#define TARCH_SIZEOF_LDOUBLE	16	/* long double */

#define TARCH_SIZEOF_STACKWORD	8	/* stack word size */

/* intel opcode suffixes */
#define TARCH_ISUFFIX_SCHAR	'b'	/* signed char */
#define TARCH_ISUFFIX_UCHAR	'b'	/* unsigned char */
#define TARCH_ISUFFIX_SSHORT	'w'	/* signed short */
#define TARCH_ISUFFIX_USHORT	'w'	/* unsigned short */
#define TARCH_ISUFFIX_SINT	'l'	/* signed int */
#define TARCH_ISUFFIX_UINT	'l'	/* unsigned int */
#define TARCH_ISUFFIX_SLONG	'q'	/* signed long */
#define TARCH_ISUFFIX_ULONG	'q'	/* unsigned long and pointer */
#define TARCH_ISUFFIX_FLOAT	's'	/* float */
#define TARCH_ISUFFIX_DOUBLE	'd'	/* double */
#define TARCH_ISUFFIX_LDOUBLE	't'	/* long double */

#define TARCH_INT_REGCLASS_SIZE	6	/* 6 registers in the int class 
					 */
#define TARCH_SSE_REGCLASS_SIZE	8	/* 8 registers in the SSE class 
					 */
char determineIntelSuffix(Type t);
char intelSuffix(TypeTag t);
void emitFst(REG dreg);
void emitFld(REG sreg);

#endif /* !_TARCH_EM64T_H_ */
/* vi: set tabstop=8 textwidth=72: */
