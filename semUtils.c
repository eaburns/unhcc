/*  semUtils.c
 *  Written by Denise Mitchell for CS-812 Compilers
 *  UNH Graduate School - Spring 2001
 *  Contains the functions for providing integral promotions,
 *  usual arithmetic conversions, and default arguments
 *
 *  Lots of edits and additions from pjh and Spring 2001 students.
 */

#include "semUtils.h"
#include "message.h"
#include "tree.h"
#include "constExpr.h"
#include "encode.h"

ExpTree integralPromotion(ExpTree t)
{

	/* This function will check the type and will prefix a
	   conversion node based on the following rules: A char, a
	   short int, an int bit-field or enum will be converted to an
	   int.  A null or any other type will be left alone and no
	   action will take place. */
	ExpTree newtree;

	newtree = NULL;

	if (!t) {
		bugT((Tree) t,
		     "null tree pointer in integralPromotion\n");
		return (ExpTree) t;
	}

	switch (typeQuery(t->type)) {
	case TYUNSIGNEDCHAR:
	case TYUNSIGNEDSHORTINT:
	case TYSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
		newtree = prefixConvert(t, TYSIGNEDINT);
		break;
	default:
		newtree = t;	/* we'll return the original tree */
	}

	return (ExpTree) newtree;
}

ExpTree defaultArgumentPromotion(ExpTree t)
{

	ExpTree newtree;

	newtree = 0;
	/* check to see if we have node defined */
	if (!t) {
		bugT((Tree) t,
		     "NULL tree pointer in defaultArgumentPromotion()\n");
		/* bug message also aborts so program will exit here */
	}

	switch (typeQuery(t->type)) {
	case TYFLOAT:
		newtree = prefixConvert(t, TYDOUBLE);
		break;
	case TYVOID:
		errorT((Tree) t, "argument has void type");
		t->type = typeBuildBasic(TYERROR, NO_QUAL);
		newtree = t;
		break;
	default:
		newtree = integralPromotion(t);
		break;
	}
	return newtree;
}


BinopTree arithmeticConversion(BinopTree t)
{
	/* This function will check both operands and add conversion
	   tree per ANSI Standard 3.2.1.5 */

	ExpTree lop;		/* left operand */
	ExpTree rop;		/* right operand */

	/* check to see if we have node defined */
	if (!t) {
		bugT((Tree) t,
		     "NULL tree pointer in arithmeticConversion()\n");
		/* bug message also aborts so program will exit here */
	}

	/* check to see if there are left and right operands */
	if (!(t->left && t->right)) {
		bugT((Tree) t,
		     "Node must have both left and right ops in arithmeticConversion()\n");
		/* bug message also aborts so program will exit here */
	}

	/* pick off the left/right operands and their types for easier
	   references */
	lop = t->left;
	rop = t->right;

	/* both subtrees should have arithmetic type else do nothing */
	if (!
	    (isArithmeticType(lop->type)
	     && isArithmeticType(rop->type))) {
		return t;
	}

	/* handle error types in either operand */
	if (typeQuery(lop->type) == TYERROR) {
		/* set the type rather than add a conversion node */
		rop->type = typeBuildBasic(TYERROR, NO_QUAL);
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYERROR) {
		/* set the type rather than add a conversion node */
		lop->type = typeBuildBasic(TYERROR, NO_QUAL);
		return (BinopTree) t;
	}

	/* handle long double types in either operand */
	if (typeQuery(lop->type) == TYLONGDOUBLE) {
		if (typeQuery(rop->type) != TYLONGDOUBLE) {
			t->right = prefixConvert(rop, TYLONGDOUBLE);
		}
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYLONGDOUBLE) {
		if (typeQuery(lop->type) != TYLONGDOUBLE) {
			t->left = prefixConvert(lop, TYLONGDOUBLE);
		}
		return (BinopTree) t;
	}

	/* handle double types in either operand */
	if (typeQuery(lop->type) == TYDOUBLE) {
		if (typeQuery(rop->type) != TYDOUBLE) {
			t->right = prefixConvert(rop, TYDOUBLE);
		}
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYDOUBLE) {
		if (typeQuery(lop->type) != TYDOUBLE) {
			t->left = prefixConvert(lop, TYDOUBLE);
		}
		return (BinopTree) t;
	}

	/* handle float types in either operand */
	if (typeQuery(lop->type) == TYFLOAT) {
		if (typeQuery(rop->type) != TYFLOAT) {
			t->right = prefixConvert(rop, TYFLOAT);
		}
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYFLOAT) {
		if (typeQuery(lop->type) != TYFLOAT) {
			t->left = prefixConvert(lop, TYFLOAT);
		}
		return (BinopTree) t;
	}

	/* invoke integral promotions */
	t->left = lop = integralPromotion(lop);
	t->right = rop = integralPromotion(rop);


	/* handle unsigned long int */

	if (typeQuery(lop->type) == TYUNSIGNEDLONGINT) {
		if (typeQuery(rop->type) != TYUNSIGNEDLONGINT) {
			t->right =
			    prefixConvert(rop, TYUNSIGNEDLONGINT);
		}
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYUNSIGNEDLONGINT) {
		if (typeQuery(lop->type) != TYUNSIGNEDLONGINT) {
			t->left = prefixConvert(lop, TYUNSIGNEDLONGINT);
		}
		return (BinopTree) t;
	}
	/* handle signed long int */

	/* 
	 *  NOTE: see fine print in 3.2.1.5. This case must consider
	 *  whether "a long int can represent all values of an unsigned int".
	 *  If so, "the operand of type unsigned int is converted to long int".
	 *  If not, "both operands are converted to unsigned long int".
	 */

	if (typeQuery(lop->type) == TYSIGNEDLONGINT) {
		if (typeQuery(rop->type) == TYUNSIGNEDINT) {
			if (computeSize(lop->type) >
			    computeSize(rop->type)) {
				t->right =
				    prefixConvert(rop, TYSIGNEDLONGINT);
			} else {
				t->right =
				    prefixConvert(rop,
						  TYUNSIGNEDLONGINT);
				t->left =
				    prefixConvert(lop,
						  TYUNSIGNEDLONGINT);
			}
		} else if (typeQuery(rop->type) != TYSIGNEDLONGINT) {
			t->right = prefixConvert(rop, TYSIGNEDLONGINT);
		}
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYSIGNEDLONGINT) {
		if (typeQuery(lop->type) == TYUNSIGNEDINT) {
			if (computeSize(rop->type) >
			    computeSize(lop->type)) {
				t->left =
				    prefixConvert(lop, TYSIGNEDLONGINT);
			} else {
				t->right =
				    prefixConvert(rop,
						  TYUNSIGNEDLONGINT);
				t->left =
				    prefixConvert(lop,
						  TYUNSIGNEDLONGINT);
			}
		} else if (typeQuery(lop->type) != TYSIGNEDLONGINT) {
			t->left = prefixConvert(lop, TYSIGNEDLONGINT);
		}
		return (BinopTree) t;
	}

	/* handle unsigned int */

	if (typeQuery(lop->type) == TYUNSIGNEDINT) {
		if (typeQuery(rop->type) != TYUNSIGNEDINT) {
			t->right = prefixConvert(rop, TYUNSIGNEDINT);
		}
		return (BinopTree) t;
	} else if (typeQuery(rop->type) == TYUNSIGNEDINT) {
		if (typeQuery(lop->type) != TYUNSIGNEDINT) {
			t->left = prefixConvert(lop, TYUNSIGNEDINT);
		}
		return (BinopTree) t;
	}

	/* must both be int now */

	return (BinopTree) t;
}


/* utility routine that adds a conversion node to top of tree passed */

ExpTree prefixConvert(ExpTree expTree, TypeTag typeTag)
{
	ExpTree newTree;
	Type ctype;

	ctype = typeBuildBasic(typeTag, NO_QUAL);
	newTree = newUnop(CONVERT_OP, ctype, expTree);	/* create the
							   new node */
	newTree->absn.filename = expTree->absn.filename;
	newTree->absn.lineno = expTree->absn.lineno;
	return (ExpTree) newTree;
}

/*
 * Returns a boolean value (TRUE or FALSE).
 * 
 * Takes an ExpTree node, and determines if it's a complete object.
 *
 * "Complete" means that the size of the object is known.
 *
 * Check arrays for bounds, and structs and unions for member lists. 
 *
 */
BOOLEAN isCompleteObjectType(Type type)
{
	switch (typeQuery(type)) {
	case TYERROR:
		bug("error type in isCompleteObjectType");

	case TYVOID:
		bug("void type in isCompleteObjectType");

		/* if a function, then not an object. */
	case TYFUNCTION:
		return FALSE;

		/* if an array, check if outer dimension is present. */
	case TYARRAY:
		{
			DimFlag dimflag;
			unsigned int dim;

			typeQueryArray(type, &dimflag, &dim);
			if (dimflag == DIMENSION_PRESENT)
				return TRUE;
			else
				return FALSE;
		}

		/* if a struct or union, check if there is a member
		   list */
	case TYSTRUCT:
	case TYUNION:
		if (typeRetrieveMembers(type) != NULL)
			return TRUE;
		else
			return FALSE;

		/* else, it's an object */
	default:
		return TRUE;
	}
	return TRUE;		/* not reached */
}

/*------------------------------------------------------------------------
 * Functions to determine lvalues
 *------------------------------------------------------------------------*/

/*
 * Returns a boolean value (TRUE or FALSE).
 *
 * Takes a ExpTree node, and determines if it is an lvalue.
 *
 * ANSI 3.2.2.1
 */
BOOLEAN isLval(ExpTree t)
{
	/* void type can't be an lval. Neither can function types. */
	if ((typeQuery(t->type) == TYVOID)
	    || (typeQuery(t->type) == TYFUNCTION)) {
		return FALSE;
	} else {
		/* variables are lvals */
		if (((Tree) (t))->tag == VAR_TAG) {
			return TRUE;
		}

		/* DEREF produces an lval unless on top of FieldRef */
		else if (((Tree) (t))->tag == UNOP_TAG &&
			 ((UnopTree) t)->op == DEREF_OP) {
			UnopTree u;

			u = (UnopTree) t;

			/* if child is FieldRef then child must be an
			   lval */
			if (((Tree) (u->left))->tag == FIELDREF_TAG) {
				return isLval(u->left);
			}
			return TRUE;
		}

		/* 
		 *  DEREF might have been turned into NO_OP.
		 *  And this is the only use of NO_OP.
		 */
		else if (((Tree) (t))->tag == UNOP_TAG &&
			 ((UnopTree) t)->op == NO_OP) {
			UnopTree u;

			u = (UnopTree) t;

			/* if child is FieldRef then child must be an
			   lval */
			if (((Tree) (u->left))->tag == FIELDREF_TAG) {
				return isLval(u->left);
			}
			return TRUE;
		}

		/* 
		 *  INDEX produces an lval, but this is probably not needed because we
		 *  turn exp1[exp2] into *(exp1+exp2).
		 */
		else if (((Tree) (t))->tag == BINOP_TAG &&
			 ((BinopTree) t)->op == INDEX_OP) {
			return TRUE;
		}

		/* (exp).m is an lval if (exp) is an lval */
		else if (((Tree) (t))->tag == FIELDREF_TAG) {
			return isLval(((FieldRefTree) t)->left);
		}

		/* (exp)->m is always an lval */
		else if (((Tree) (t))->tag == PTR_TAG) {
			return TRUE;
		}

		else {
			return FALSE;
		}
	}
}

/*
 *  Checks whether a struct/union contains a const-qualified member. This
 *  is applied recursively if the struct/union has members that are structs
 *  or unions.
 *
 *  This can be called for non-struct/union types. In that case it simply
 *  returns false.
 */
BOOLEAN containsConstMember(Type type)
{
	MemberList list;

	if (!isStructUnionType(type))
		return FALSE;

	list = typeRetrieveMembers(type);

	while (list) {
		TypeQualifier qual;

		qual = typeGetQualifier(list->type);
		if (qual == CONST_QUAL || qual == CONST_VOLATILE_QUAL) {
			return TRUE;
		}
		if (containsConstMember(list->type)) {
			return TRUE;
		}
		list = list->next;
	}

	return FALSE;
}

/*
 * Returns a boolean value (TRUE or FALSE).
 *
 * Takes a ExpTree node, and determines if it is a modifiable lvalue.
 *
 * ANSI 3.2.2.1
 *
 * Arrays are *not* modifiable lvals. Neither are incomplete types.
 * Also type cannot be const qualified. And if it is struct/union it
 * cannot contain a member that is const-qualified (including, recursively,
 * any member of all contained structs or unions).
 *
 * Note: Check Lval-ness before using promoteFunctionsOrArrays, since that 
 * will change an array to a pointer type 
 */
BOOLEAN isModifiableLval(ExpTree t)
{
	TypeQualifier qual;

	/* must be an lval (this rules out function types) */
	if (!isLval(t)) {
		return FALSE;
	}

	/* can't be an array */
	if (typeQuery(t->type) == TYARRAY) {
		return FALSE;
	}

	/* can't be an incomplete type */
	if (!isCompleteObjectType(t->type)) {
		return FALSE;
	}

	/* can't be a const-qualified type */
	qual = typeGetQualifier(t->type);
	if (qual == CONST_QUAL || qual == CONST_VOLATILE_QUAL) {
		return FALSE;
	}

	/* if there is a DEREF or NO_OP then can't be const-qualified
	   type below */
	if (((Tree) (t))->tag == UNOP_TAG &&
	    (((UnopTree) t)->op == NO_OP
	     || ((UnopTree) t)->op == DEREF_OP)) {
		qual = typeGetQualifier(((UnopTree) t)->left->type);
		if (qual == CONST_QUAL || qual == CONST_VOLATILE_QUAL) {
			return FALSE;
		}
	}

	/* if it is struct/union, members must be recursively checked
	   for const. */
	if (isStructUnionType(t->type)) {
		if (containsConstMember(t->type)) {	/* does
							   recursive
							   check */
			return FALSE;
		}
	}

	return TRUE;
}

/*------------------------------------------------------------------------
 * Functions to determine arithmetic types.
 *------------------------------------------------------------------------
 */

BOOLEAN isUnsignedType(Type type)
{
	switch (typeQuery(type)) {

	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
		return TRUE;

	case TYERROR:
		info("isArithmeticType: error type passed in");
		return TRUE;

	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDCHAR:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
	case TYPOINTER:
	case TYVOID:
		return FALSE;

	default:
		bug("isUnsignedType: unknown type");
	}

	return FALSE;
/*NOTREACHED*/}

/*
 * Returns a boolean value, TRUE or FALSE.
 *
 * Determines if the Type given is of pointer type.
 */
BOOLEAN isPointerType(Type type)
{
	switch (typeQuery(type)) {
	case TYPOINTER:
		return TRUE;
	default:
		return FALSE;
	}
	/* can't reach */
	return FALSE;
}

/*
 * Returns a boolean value, TRUE or FALSE.
 *
 * Determines if the tree given is a function pointer.
 */
BOOLEAN isFunctionPointerType(Type type)
{
	TypeQualifier qual;
	if (isPointerType(type)) {
		return (typeQuery(typeQueryPointer(type, &qual)) ==
			TYFUNCTION);
	} else {
		return FALSE;
	}
}

/*
 * Returns a boolean value, TRUE or FALSE.
 *
 * Determines if the tree given is an object pointer.
 */
BOOLEAN isObjectPointerType(Type type)
{
	TypeQualifier qual;
	if (isPointerType(type)) {
		return !(typeQuery(typeQueryPointer(type, &qual)) ==
			 TYFUNCTION);
	} else {
		return FALSE;
	}
}

/*
 * Returns a boolean value, TRUE or FALSE.
 * 
 * Determines if the tree given is a null pointer constant.
 *
 * See ANSI 3.2.2.3.
 *
 * Note: reuceConstExpr is dangerous because it changes the Type
 *       field of the tree passed to it. We therefore need to
 *       restore the Type after our use of it.
 */
BOOLEAN isNullPointerConst(ExpTree t)
{
	ExpTree constant;
	Type saveType;

	/* if there is a cast to void* on top then recurse past it */
	if ((t->absn.tag == CASTOP_TAG)
	    && (typeQuery(t->type) == TYPOINTER)
	    && (typeQuery(typeStripModifier(t->type)) == TYVOID)) {
		return isNullPointerConst(((CastopTree) t)->left);
	}

	/* CAST_OP is modified to CONVERT_OP? */
	if ((t->absn.tag == UNOP_TAG) &&
	    (((UnopTree) t)->op == CONVERT_OP) &&
	    (typeQuery(t->type) == TYPOINTER) &&
	    (typeQuery(typeStripModifier(t->type)) == TYVOID)) {
		return isNullPointerConst(((UnopTree) t)->left);
	}

	if (!isIntegralType(t->type)) {
		return FALSE;
	}

	/* see if it is a constant expression */
	saveType = t->type;
	constant = reduceConstExpr(t);

	if (typeQuery(constant->type) == TYERROR) {
		t->type = saveType;
		return FALSE;
	}

	t->type = saveType;
	return getConstExprValue(constant) == 0;
}

/*
 * Returns a boolean value, TRUE or FALSE.
 *
 * Determines if the Type given is a void pointer.
 */
BOOLEAN isVoidPointerType(Type type)
{
	TypeQualifier qual;
	if (isPointerType(type)) {
		return (typeQuery(typeQueryPointer(type, &qual)) ==
			TYVOID);
	} else {
		return FALSE;
	}
}

/*
 * Returns a boolean value, TRUE or FALSE.
 *
 * Determines if the ExpTree given is of struct/union type.
 */
BOOLEAN isStructUnion(ExpTree e)
{
	return ((typeQuery(e->type) == TYSTRUCT) ||
		(typeQuery(e->type) == TYUNION));

}

/*
 * Returns a boolean value, TRUE or FALSE.
 *
 * Determines if the Type given is of struct/union type.
 */
BOOLEAN isStructUnionType(Type type)
{
	return ((typeQuery(type) == TYSTRUCT) ||
		(typeQuery(type) == TYUNION));

}
