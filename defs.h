
/****************************************************************/
/*                                                              */
/*        CS712 - "C" Compiler                                  */
/*                                                              */
/*        --defs.h--                                            */
/*                                                              */
/*        This file contains general definitions for cs712      */
/*        C compiler.                                           */
/*                                                              */
/*                                                              */
/*                                                              */
/****************************************************************/

#ifndef DEFS_H
#define DEFS_H

/*
 *  Put stuff here that is needed from system header files
 */
#ifndef USE_STANDARD_HEADERS

typedef unsigned long size_t;
void *malloc(size_t);
void *calloc(size_t, size_t);
void free(void *);
#define NULL ((void *)0)
typedef struct _IO_FILE FILE;
int fprintf(FILE *, const char *, ...);
FILE *fopen(const char *, const char *);
size_t strlen(const char *);
int strcmp(const char *, const char *);
char *strcpy(char *, const char *);
char *strcat(char *, const char *);
extern struct _IO_FILE *stderr;
#endif

/* support for a BOOLEAN type */
typedef int BOOLEAN;
#define TRUE 1
#define FALSE 0
extern void printBoolean(BOOLEAN);	/* in utils.c */

/* REG is the "register handle" used in register allocator */
typedef void *REG;

typedef void *ST_ID;		/* symbol table identifier abstraction */

/* functions exported by scanner module */
extern int YYlineno(void);
extern int YYcolumnno(void);
extern char *YYfilename(void);
extern void YYinitializeFilename(char *);

/* used in typedef processing */
typedef enum { ON, OFF } GP_SWITCH;

/* compact representation of storage class specifiers */
typedef enum {
	STATIC_SC, EXTERN_SC, AUTO_SC, TYPEDEF_SC, REGISTER_SC, NO_SC
} StorageClass;
void printClass(StorageClass tag);

#endif
