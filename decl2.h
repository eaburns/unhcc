
/*
 *  Header file for phase 2.
 */

#ifndef DECL2_H
#define DECL2_H

#include "defs.h"
#include "decl.h"

/*
 *  Process a single declarator.
 *    First parameter is the tree representation of the declarator.
 *    Second parameter is the base type.
 *    Third parameter is an output parameter through which the id is
 *      returned, if there is one.
 *    Fourth parameter is also an output parameter, indicating whether
 *      an error was detected or not.
 *    Fifth parameter is an optional output parameter. It tracks the
 *      last parameter list seen if function modifiers are processed.
 *      By "last" it is meant "farthest from the base type". This is
 *      used when processing function definitions to keep the parameter
 *      list in order to install the id's into the local symbol table.
 *      Pass NULL if you are not interested in this value.
 *    Return value is the modified type.
 */
extern Type processDeclarator(DECL_TREE, Type, ST_ID *, BOOLEAN *,
			      ParamList *);

/*
 *  Process a declaration consisting of a list of type specifiers
 *  and a list of declarators (could be either global or local).
 */
extern void processDeclaration(BUCKET_PTR specifiers,
			       DECLARATOR_LIST list);

/*
 *  Finish processing a global declaration: semantic checks, access symbol
 *  table, etc.
 */
extern void finishGlobalDeclaration(ST_ID id, Type type,
				    StorageClass class, BOOLEAN err,
				    BOOLEAN isFunctionDefinition);

/*
 *  Finish processing a local declaration: semantic checks, access symbol
 *  table, etc. Also handles parameters.
 */
extern void finishLocalDeclaration(ST_ID id, Type type,
				   StorageClass class, BOOLEAN err,
				   BOOLEAN isParam);

/* support for typedef processing */
void resetEnumItemVal(void);
void setEnumItemVal(long);
int getNextEnumVal(void);
void processEnumItem(ST_ID);
MY_SPECIFIER processEnumIdName(ST_ID);
MY_SPECIFIER verifyEnumId(ST_ID);

#endif
