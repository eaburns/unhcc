/**
 * \file tarch_i386.h
 * \brief Contains declarations for the target architecture i386.
 * \author Ethan Burns
 * \date 2007-09-10
 */
#ifndef _TARCH_I386_H_
#define _TARCH_I386_H_

/* data type sizes in bytes */
#define TARCH_SIZEOF_SCHAR	1	/* signed char */
#define TARCH_SIZEOF_UCHAR	1	/* unsigned char */
#define TARCH_SIZEOF_SSHORT	2	/* signed short */
#define TARCH_SIZEOF_USHORT	2	/* unsigned short */
#define TARCH_SIZEOF_SINT	4	/* signed int */
#define TARCH_SIZEOF_UINT	4	/* unsigned int */
#define TARCH_SIZEOF_SLONG	4	/* signed long */
#define TARCH_SIZEOF_ULONG	4	/* unsigned long and pointer */
#define TARCH_SIZEOF_FLOAT	4	/* float */
#define TARCH_SIZEOF_DOUBLE	8	/* double */
#define TARCH_SIZEOF_LDOUBLE	12	/* long double */

#define TARCH_SIZEOF_STACKWORD	4	/* stack word size */

/* intel opcode suffixes */
#define TARCH_ISUFFIX_SCHAR	'b'	/* signed char */
#define TARCH_ISUFFIX_UCHAR	'b'	/* unsigned char */
#define TARCH_ISUFFIX_SSHORT	'w'	/* signed short */
#define TARCH_ISUFFIX_USHORT	'w'	/* unsigned short */
#define TARCH_ISUFFIX_SINT	'l'	/* signed int */
#define TARCH_ISUFFIX_UINT	'l'	/* unsigned int */
#define TARCH_ISUFFIX_SLONG	'l'	/* signed long */
#define TARCH_ISUFFIX_ULONG	'l'	/* unsigned long and pointer */
#define TARCH_ISUFFIX_FLOAT	's'	/* float */
#define TARCH_ISUFFIX_DOUBLE	'l'	/* double */
#define TARCH_ISUFFIX_LDOUBLE	't'	/* long double */

#endif /* !_TARCH_I386_H_ */
/* vi: set tabstop=8 textwidth=72: */
