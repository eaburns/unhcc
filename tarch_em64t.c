/**
 * \file tarch_em64t.c
 * \brief Contains functions for EM64T.
 * \author Ethan Burns
 * \date 2007-09-28
 */


#include <stdarg.h>
#include <stdio.h>

#include "encode.h"
#include "message.h"
#include "regAlloc.h"
#include "tarch.h"
#include "types.h"

/**
 * \brief Returns the string that represents the given register as the
 *        given type (i.e. rax, eax, ax, al).
 * \note The result of this function is static storage and will be
 *       over written with successive calls.
 */
const char *registerAs(char c, TypeTag t)
{
	static char reg[5];

	reg[0] = '%';

	switch(t) {
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		reg[1] = c;
		reg[2] = 'l';
		reg[3] = '\0';
		break;

	case TYSIGNEDSHORTINT:
	case TYUNSIGNEDSHORTINT:
		reg[1] = c;
		reg[2] = 'x';
		reg[3] = '\0';
		break;

	case TYSIGNEDINT:
	case TYUNSIGNEDINT:
		reg[1] = 'e';
		reg[2] = c;
		reg[3] = 'x';
		reg[4] = '\0';
		break;

	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYPOINTER:
		reg[1] = 'r';
		reg[2] = c;
		reg[3] = 'x';
		reg[4] = '\0';
		break;

	default:
		bug("unexpected type in registerAs");	
	}

	return reg;
}

/*
 * determineIntelSuffix: Type -> Intel single char type spec
 *                               (ie b, w, l, s, etc)
 *
 * NOTE: Do not use this for MOV instructions since there is no routine
 *       to determine which register version to use (%rax, %eax, %ax, %al,
 *       etc.). -- eaburns
 */
char determineIntelSuffix(Type t)
{
	return intelSuffix(typeQuery(t));
}

/*
 * Gets the intel instruction mnemonic suffix from the TypeTag.
 */
char intelSuffix(TypeTag t)
{
	switch (t) {
	case TYFLOAT:
		return TARCH_ISUFFIX_FLOAT;
	case TYDOUBLE:
		return TARCH_ISUFFIX_DOUBLE;
	case TYLONGDOUBLE:
		return TARCH_ISUFFIX_LDOUBLE;
	case TYSIGNEDLONGINT:
		return TARCH_ISUFFIX_SLONG;
	case TYSIGNEDSHORTINT:
		return TARCH_ISUFFIX_SSHORT;
	case TYSIGNEDINT:
		return TARCH_ISUFFIX_SINT;
	case TYUNSIGNEDLONGINT:
		return TARCH_ISUFFIX_ULONG;
	case TYUNSIGNEDSHORTINT:
		return TARCH_ISUFFIX_USHORT;
	case TYUNSIGNEDINT:
		return TARCH_ISUFFIX_UINT;
	case TYUNSIGNEDCHAR:
		return TARCH_ISUFFIX_UCHAR;
	case TYSIGNEDCHAR:
		return TARCH_ISUFFIX_SCHAR;
	case TYPOINTER:
		return TARCH_ISUFFIX_ULONG;
	case TYVOID:
		bug("void type seen in determineIntelSuffix()");
		break;
	case TYERROR:
		bug("error type seen in determineIntelSuffix()");
		break;
	case TYSTRUCT:
		bug("struct type seen in determineIntelSuffix()");
		break;
	case TYUNION:
		bug("union type seen in determineIntelSuffix()");
		break;
	case TYARRAY:
		bug("array type seen in determineIntelSuffix()");
		break;
	case TYFUNCTION:
		bug("function type seen in determineIntelSuffix()");
		break;
	default:
		bug("unknown type seen in determineIntelSuffix()");
	}

	/* not reached */
	return ' ';
}

char *movetab[NUM_TYPES][NUM_TYPES] = {
/* void */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     NULL,   NULL,     NULL,
/* ulong,  ushort,  uint,     uchar,    char,   struct,   union */
   NULL,   NULL,    NULL,     NULL,     NULL,   NULL,     NULL,
/* array   funct,   pointer,  error */
   NULL,   NULL,    NULL,     NULL },

/* float */
/* void,   float,   double,     ldouble,  long,        short,   int */
{  NULL,   "movss", "cvtss2sd", NULL,     "cvttss2siq", NULL,   "cvttss2si",
/* ulong,       ushort,  uint,         uchar,    char,   struct,   union */
   "cvtss2siq", NULL,    "cvttss2si",  NULL,     NULL,   NULL,     NULL,
/* array   funct,   pointer,  error */
   NULL,   NULL,    NULL,     NULL },

/* double */
/* void,   float,      double,   ldouble,  long,         short, int */
{  NULL,   "cvtsd2ss", "movsd",  NULL,     "cvttsd2siq", NULL,  "cvttsd2si",
/* ulong,  ushort,  uint,     uchar,    char,   struct,   union */
   NULL,   NULL,    NULL,     NULL,     NULL,   NULL,     NULL,
/* array   funct,   pointer,  error */
   NULL,   NULL,    NULL,     NULL },

/* long double */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     NULL,   NULL,     NULL,
/* ulong,  ushort,  uint,     uchar,    char,   struct,   union */
   NULL,   NULL,    NULL,     NULL,     NULL,   NULL,     NULL,
/* array   funct,   pointer,  error */
   NULL,   NULL,    NULL,     NULL },

/* long */
/* void,   float,       double,      ldouble,  long,   short,    int */
{  NULL,   "cvtsi2ssq", "cvtsi2sdq", NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,   struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb", "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },

/* short */
/* void,   float,   double,   ldouble,  long,     short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movswq", "movw",   "movswl",
/* ulong,    ushort,  uint,     uchar,    char,     struct,   union */
   "movzwq", "movw",  "movzwl", "movb",   "movb",   "movzwq", "movzwq",
/* array     funct,     pointer,   error */
   "movzwq", "movzwq",  "movzwq",  NULL },

/* int */
/* void,   float,      double,     ldouble,  long,     short,      int */
{  NULL,   "cvtsi2ss", "cvtsi2sd", NULL,     "movslq", "movw",     "movl",
/* ulong,    ushort,    uint,     uchar,  char,    struct,   union */
   "movslq", "movw",    "movl",   "movb", "movb",  "mov",    "mov",
/* array   funct,   pointer,  error */
   "mov",  "mov",   "mov",     NULL },

/* unsigned long */
/* void,   float,       double,      ldouble,  long,   short,    int */
{  NULL,   "cvtsi2ssq", "cvtsi2sdq", NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,     struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb",   "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },

/* unsigned short */
/* void,   float,   double,   ldouble,  long,     short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movzwq", "movw",   "movzwl",
/* ulong,    ushort,   uint,     uchar,    char,   struct,   union */
   "movzwq", "movw",   "movzwl", "movb",   "movb", "movzwq", "movzwq",
/* array      funct,    pointer,   error */
   "movzwq",  "movzwq", "movzwq",  NULL },

/* unsigned int */
/* void,   float,       double,     ldouble,  long,   short,    int */
{  NULL,   "cvtsi2ss",  "cvtsi2sd", NULL,     "mov",  "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,   struct,   union */
   "mov",  "movw",  "movl",   "movb",   "movb", "mov",    "mov",
/* array   funct,   pointer,  error */
   "mov",  "mov",   "mov",     NULL },

/* unsigned char */
/* void,     float,     double,   ldouble,  long,     short,      int */
{  NULL,     NULL,      NULL,     NULL,     "movzbq", "movzbw",   "movzbl",
/* ulong,    ushort,    uint,     uchar,    char,     struct,     union */
   "movzbq", "movzbw",  "movzbl", "movb",   "movb",   "movzbq",   "movzbq",
/* array      funct,    pointer,  error */
   "movzbq",  "movzbq", "movzbq", NULL },

/* char */
/* void,     float,     double,   ldouble,  long,     short,      int */
{  NULL,     NULL,      NULL,     NULL,     "movsbq", "movsbw",   "movsbl",
/* ulong,    ushort,    uint,     uchar,    char,     struct,     union */
   "movzbq", "movzbw",  "movzbl", "movb",   "movb",   "movzbq",   "movzbq",
/* array      funct,    pointer,  error */
   "movzbq",  "movzbq", "movzbq", NULL },

/* struct */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,     struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb",   "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },

/* union */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,     struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb",   "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },

/* array */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,     struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb",   "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },

/* function */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,     struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb",   "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },

/* pointer */
/* void,   float,   double,   ldouble,  long,   short,    int */
{  NULL,   NULL,    NULL,     NULL,     "movq", "movw",   "movl",
/* ulong,  ushort,  uint,     uchar,    char,     struct,   union */
   "movq", "movw",  "movl",   "movb",   "movb",   "movq",   "movq",
/* array   funct,   pointer,  error */
   "movq", "movq",  "movq",   NULL },
};

void emitMove(const char *move, const char *src, const char *dst)
{
	if (!move)
		bug("move string does not exist\n");

	emit("\t%s\t%s, %s", move, src, dst);
}

/**
 * \brief Save the fp control word to the TOS, and set the rounding mode to
 * truncate.
 */
static void setFPTruncateRound(void)
{
	comment("set rounding mode to truncate");
	/* alloc two words on top of stack */
	emit("\tsubq\t$4, %%rsp");
	/* store fp control word in first word */
	emit("\tfnstcw\t(%%rsp)");
	/* put control word in %ax */
	emit("\tmovw\t(%%rsp), %%ax");
	/* set RC bits to "truncate" */
	emit("\tmovb\t$12, %%ah");
	/* put modified cntrl word in 2nd slot */
	emit("\tmovw\t%%ax, 2(%%rsp)");
	/* load modified control word */
	emit("\tfldcw\t2(%%rsp)");
	/* put old control word in %ax */
	emit("\tmovw\t(%%rsp), %%ax");
	/* free the two words */
	emit("\taddq\t$4, %%rsp");
}

/**
 * \brief Restore the fp control word from the TOS.
 */
static void restoreFPRounding(void)
{
	comment("restore rounding mode");
	/* alloc one word on top of stack */
	emit("\tsubq\t$2, %%rsp");
	/* put original cntrl word on stack */
	emit("\tmovw\t%%ax, (%%rsp)");
	/* re-load original control word */
	emit("\tfldcw\t(%%rsp)");
	/* free word on top of stack */
	emit("\taddq\t$2, %%rsp");
}

/**
 * \brief Emits code that stores the top of the floating point stack
 *        into the given register.
 */
void emitFst(REG dreg)
{
	if (regType(dreg) == TYLONGDOUBLE && isRegister(dreg)) {
		/* just leave the value on the fp stack */
		return;
	}

	emit("\tfstp%c\t%s", intelSuffix(regType(dreg)), regString(dreg));
}

/**
 * \brief Loads the value from the given register onto the floating
 *        point stack.
 */
void emitFld(REG sreg)
{
	if (regType(sreg) == TYLONGDOUBLE && isRegister(sreg)) {
		/* just leave the value on the fp stack */
		return;
	}

	emit("\tfld%c\t%s", intelSuffix(regType(sreg)), regString(sreg));
}

/**
 * \brief Special handling for long doubles.
 */
static void moveLongDouble(REG sreg, REG dreg)
{
	TypeTag stype, dtype;
	REG tmp;

	stype = regType(sreg);
	dtype = regType(dreg);

	if (stype != TYLONGDOUBLE && dtype != TYLONGDOUBLE)
		bug("moveLongDouble called with invalid register types");

	if (stype == dtype) {
		/* both long doubles */
		comment1("move long double to long double");
		emitFld(sreg);
		emitFst(dreg);
	} else if (stype == TYLONGDOUBLE) {
		/* source is a long double */
		emitFld(sreg);

		if (dtype == TYFLOAT || dtype == TYDOUBLE) {
			/* destination is float or double, copy it via
 			 * the top of the stack */
			comment1("move long double to float or double");
			if (isMemory(dreg)) {
				emitFst(dreg);
			} else {
				emit_stack_grow(TARCH_SIZEOF_ULONG);
				tmp = allocReg(dtype, "(%%rsp)");
				emitFst(tmp);
				move(tmp, dreg);
				emit_stack_shrink(TARCH_SIZEOF_ULONG);
			}
		} else {
			/* destination is an integer type */
			comment1("move long double to integer");
			if (isMemory(dreg)) {
				setFPTruncateRound();
				emit("\tfistpq\t%s",
				     regStringAs(TYUNSIGNEDLONGINT, dreg));
				restoreFPRounding();
			} else {
				setFPTruncateRound();
				comment1("fp move to stack");
				emit_stack_grow(TARCH_SIZEOF_ULONG);
				tmp = allocReg(dtype, "(%%rsp)");
				emit("\tfistpq\t%s", regString(tmp));
				restoreFPRounding();
				comment1("move stack to destination");
				move(tmp, dreg);
				emit_stack_shrink(TARCH_SIZEOF_ULONG);
			}
		}
	} else if (dtype == TYLONGDOUBLE) {
		/* destination is a long double */
		if (stype == TYFLOAT || stype == TYDOUBLE) {
			/* source is a float or double */
			comment1("move float or double to long double");
			if (isMemory(sreg)) {
				emitFld(sreg);
			} else {
				emit_stack_grow(TARCH_SIZEOF_ULONG);
				tmp = allocReg(stype, "(%%rsp)");
				move(sreg, tmp);
				emitFld(tmp);
				emit_stack_shrink(TARCH_SIZEOF_ULONG);
			}
		} else {
			comment1("move integer to long double");
			if (isMemory(sreg)) {
				emit("\tfildq\t%s", regString(sreg));
			} else {
				emit_stack_grow(TARCH_SIZEOF_ULONG);
				tmp = allocReg(TYUNSIGNEDLONGINT, "(%%rsp)");
				useReg(sreg);
				move(sreg, tmp);
				emit("\tfildq\t(%%rsp)");
				emit_stack_shrink(TARCH_SIZEOF_ULONG);
			}
		}
		emitFst(dreg);
	}
}

/**
 * \brief emits a move from a given register to a given register, the
 *        destination register needs to be for a type of at least the
 *        same size or larger.
 */
void move(REG sreg, REG dreg)
{
	TypeTag stype, dtype;
	REG tmp;

	stype = regType(sreg);
	dtype = regType(dreg);

	if (stype == TYLONGDOUBLE || dtype == TYLONGDOUBLE) {
		/* handle long doubles seperately */
		moveLongDouble(sreg, dreg);
		return;
	}

	if (typeSize(dtype) < typeSize(stype)) {
		/* if the destination is smaller, use the smaller
 		 * version of the source register. */
		emitMove(movetab[stype][dtype], regStringAs(dtype, sreg),
		         regString(dreg));
	} else if (stype == TYUNSIGNEDINT
	           && dtype != TYFLOAT
	           && dtype != TYDOUBLE
	           && dtype != TYLONGDOUBLE
	           && typeSize(dtype) == TARCH_SIZEOF_ULONG) {
		/* do this crazyness for moving unsigned int to a 64-bit
 		 * integer destination */
		comment1("move unsigned int to 64-bit type");
		emit("\txorq\t%s, %s", regString(dreg), regString(dreg));
		emitMove(movetab[stype][dtype], regString(sreg),
		         regStringAs(stype, dreg));
	} else if (typeSize(dtype) > typeSize(stype)
	           && !isConstant(sreg)
	           && (dtype != TYFLOAT && dtype != TYDOUBLE)
	           && (stype != TYFLOAT && stype != TYDOUBLE)) {
		/* for integer types, if the dest is larger than the
 		 * source, we need to move through a register */
		comment1("move with sign extend into temporary register");
		tmp = allocNextReg(dtype);
		emitMove(movetab[stype][dtype], regString(sreg),
		         regString(tmp));
		defReg(tmp);
		useReg(tmp);
		emitMove(moveString(dtype), regString(tmp), regString(dreg));
	} else if (isConstant(sreg)) {
		emitMove(movetab[dtype][dtype], regString(sreg),
		         regString(dreg));
	} else {
		emitMove(movetab[stype][dtype], regString(sreg),
		         regString(dreg));
	}
}

/*
 * Returns the nemonic for a move from a register to a destination.
 * This move does not do any zero-fill or sign extension.
 */
const char *moveString(TypeTag t)
{
	return movetab[t][t];
}

/* vi: set tabstop=8 textwidth=72: */
