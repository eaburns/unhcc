/*
 *
 *  analyze.c - semantic analysis
 *
 *  The driver is a recursive routine called analyze that traverses the AST
 *  for a function definition in (basically) a bottom-up fashion.
 */

#include "regAlloc.h"
#include "defs.h"
#include "tree.h"
#include "symtab.h"
#include "encode.h"
#include "eval.h"
#include "analyze.h"
#include "semUtils.h"
#include "globals.h"
#include "message.h"

static Tree analyzeReturn(ReturnTree r);

/*  Check if the right operand needs to be converted to the left. If it does,
 *  and can be converted, add a convert node. Otherwise, give an error, and
 *  change the type of the top of the tree to TYERROR. Also need to remove the
 *  qualifer, if any.
 *
 *  ANSI 3.3.16
 */
static BinopTree assignmentConversion(BinopTree b)
{
	ExpTree lop;		/* left operand */
	ExpTree rop;		/* right operand */

	/* if NULL tree passed, bug */
	if (!b) {
		bugT((Tree) b,
		     "NULL tree pointer in assignmentConversion()\n");
	}

	/* if not two operands, bad */
	if (!(b->left && b->right)) {
		bugT((Tree) b,
		     "Node must have both left and right ops in assignmentConversion()\n");
	}

	/* pick off the left/right operands and their types for easier
	   references */
	lop = b->left;
	rop = b->right;

	/* subtrees have arithmetic type? */
	if (isArithmeticType(lop->type) && isArithmeticType(rop->type)) {
		/* If they are of the same type, just return. */
		if (typeQuery(lop->type) == typeQuery(rop->type)) {
			b->expn.type =
			    typeUnqualifiedVersion(lop->type);
			return b;
		}

		/* convert the type of the right operand to that of the 
		   left - Using prefixConvert from semUtils.h */
		b->right = prefixConvert(rop, typeQuery(lop->type));

		/* set the top of the tree to that type, as well */
		b->expn.type = typeUnqualifiedVersion(lop->type);
		return b;
	}

	/* left operand is struct/union? */
	else if (isStructUnionType(lop->type)) {
		if (areCompatibleTypes
		    (typeUnqualifiedVersion(lop->type), rop->type)) {
			b->expn.type =
			    typeUnqualifiedVersion(lop->type);
			return b;
		} else {
			errorT((Tree) b,
			       "operands of assignment are not compatible");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return b;
		}
	}

	/* both operands are pointers? */
	else if (isPointerType(lop->type) && isPointerType(rop->type)) {
		/* need to analyze types pointed to */
		Type left;
		Type right;

		left = typeStripModifier(lop->type);
		right = typeStripModifier(rop->type);

		/* 
		 *  These types must be compatible without considering qualifiers or
		 *  one is void and the other is not a function.
		 */
		if (areCompatibleTypes(typeUnqualifiedVersion(left),
				       typeUnqualifiedVersion(right)) ||
		    ((typeQuery(left) == TYVOID)
		     && (typeQuery(right) != TYFUNCTION))
		    || ((typeQuery(right) == TYVOID)
			&& (typeQuery(left) != TYFUNCTION))) {
			/* 
			 *  Now, type pointed to on left must have all the qualifiers that
			 *  type pointed to on right does.
			 */
			if (!typeHasAllQualifiers(left, right)) {
				errorT((Tree) b,
				       "qualifiers dropped in pointer assignment");
				b->expn.type =
				    typeBuildBasic(TYERROR, NO_QUAL);
				return b;
			}

			b->expn.type =
			    typeUnqualifiedVersion(lop->type);
			return b;
		} else {
			errorT((Tree) b,
			       "operands of assignment are not compatible");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return b;
		}
	}

	/* can be pointer on the left and NULL on the right */
	else if (isPointerType(lop->type) && isNullPointerConst(rop)) {
		b->expn.type = typeUnqualifiedVersion(lop->type);
		return b;
	}

	/* no other legal cases */
	errorT((Tree) b, "operands of assignment are not compatible");
	b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
	return b;
}

/*
 * Analyze for the assignment operator consists of doing a conversion, and 
 * checking validity. The final type of the tree is that of * the left node, so 
 * should not be altered.
 *
 * Need to add a convert node if the right operand needs to have a conversion.
 *
 * ANSI 3.3.16
 */
ExpTree analyzeAssign(BinopTree b)
{
	/* the left operand needs to be a modifiable lval. */
	if (!isModifiableLval(b->left)) {
		errorT((Tree) b,
		       "left operand to assignment is not a modifiable lvalue.");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	/* Call conversion - This checks for both to be arithmetic or
	   ptrs, as well as doing the necessary conversion and setting 
	   the top of the tree to the correct type. */
	b = assignmentConversion(b);

	return (ExpTree) b;
}

/*
 *  Functions that return a struct or union are handled by
 *  passing an extra argument that indicates where the struct/union
 *  should be placed.
 *
 *  To do this:
 *    1. Allocate space for the return value in the frame of the caller.
 *    2. Add a VAR node denoting the space as an extra argument. It is passed
 *       as the first argument. Structs and unions are passed by reference
 *       so this will pass a pointer to the space.
 */
static void addExtraArgumentForStructReturn(FcallTree f)
{
	Type retType;
	int offset;
	VarTree v;

	/* allocate space in current frame */
	retType = f->expn.type;
	offset = gCurrentFunctionOffset;
	gCurrentFunctionOffset += computeSize(retType);

	/* create a dummy VAR node denoting that space */
	v = (VarTree) dummyVar();
	v->offset = offset;
	v->block = 1;
	v->stdrTag = LDECL;
	v->expn.type = typeBuildPointer(retType, NO_QUAL);

	/* prepend the dummy VAR node to the argument list */
	if (f->params != NULL) {
		f->params = newEseq(NULL, (ExpTree) v, f->params);
	} else {
		f->params = (ExpTree) v;
	}
}

/*
 *  Do semantic analysis for Fcall tree node, which is an ExpTree node
 *  and has two subtrees: the expression denoting the function to be
 *  called and a sequence (possibly empty) of expressions denoting the
 *  arguments. These two subtree will already be analyzed.
 *
 *  This code must determine the type of the return value and the qualifier
 *  of the return value. It also must process the argument list and add
 *  conversions when required.
 *
 *  The argument list is stored in the following manner:
 *
 *         ESEQ
 *         /  \
 *       arg1  ESEQ
 *             /  \
 *           arg2  .
 *                  .
 *                   ESEQ
 *                   /  \
 *                argN-1 argN
 *
 *  Note: If there is only one argument, there is no ESEQ node.
 *
 *  This makes putting a conversion on top of the last argument a pain,
 *  but such is life.
 *
 *  Returning a struct/union is done by passing an extra argument,
 *  which indicates where the return value should be placed. The
 *  details of this are in addExtraArgumentForStructReturn (above).
 */
static Tree analyzeFcall(FcallTree f)
{
	ParamList retParams;
	ParamList currentParam;
	ParamStyle retStyle;
	ExpTree tempTree;
	ExpTree *treePtrPtr;
	Type funcType;
	Type retType;

	/* could be ptr to func */
	if (typeQuery(f->func->type) == TYPOINTER) {
		funcType = typeStripModifier(f->func->type);
	} else {
		funcType = f->func->type;
	}

	if (typeQuery(funcType) != TYFUNCTION) {
		errorT((Tree) f, "function type expected");
		f->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (Tree) f;
	}

	/* get type of the return value */
	retType = typeQueryFunction(funcType, &retStyle, &retParams);

	/* need to drop the qualifier */
	retType = typeUnqualifiedVersion(retType);

	/* save return type in Fcall node */
	((ExpTree) f)->type = retType;

	/* Next do parameter checking */
	currentParam = retParams;
	tempTree = f->params;
	treePtrPtr = &(f->params);

	/* if no arguments present... */
	if (tempTree == NULL) {
		/* then either must be old-style function or a void
		   parameter list */
		if (currentParam != NULL && retStyle != OLDSTYLE) {
			errorT((Tree) f, "parameter number mismatch");
			f->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return ((Tree) f);
		}
	}

	/* there are arguments... */
	else {
		/* Cycle through all the arguments in the function call 
		 */
		while (((Tree) tempTree)->tag == ESEQ_TAG) {
			/* convert func/ptr type to ptr */
			promoteFunctionOrArrayTree(((EseqTree)
						    tempTree)->left);

			/* no more formal parameters */
			if (currentParam == NULL) {
				/* if function is prototyped, this is
				   an error. */
				if (retStyle == PROTOTYPE) {
					errorT((Tree) f,
					       "parameter number mismatch");
					f->expn.type =
					    typeBuildBasic(TYERROR,
							   NO_QUAL);
					return ((Tree) f);
				}

				/* handle old-style or var-arg
				   parameters in same manner */
				else {
					((EseqTree) tempTree)->left =
					    defaultArgumentPromotion(((EseqTree) tempTree)->left);
				}
			} else {	/* have a formal parameter to
					   check against */

				BinopTree dummy;
				VarTree v;

				/* going to setup a dummy BINOP node
				   and use assignmentConversion */
				v = (VarTree) dummyVar();
				v->expn.type = currentParam->type;
				dummy =
				    (BinopTree) newBinop(ASSIGN_OP,
							 NULL,
							 (ExpTree) v,
							 ((EseqTree)
							  tempTree)->
							 left);
				dummy->expn.absn.lineno =
				    f->expn.absn.lineno;
				dummy->expn.absn.filename =
				    f->expn.absn.filename;
				dummy = assignmentConversion(dummy);

				/* ok, paste possibly updated argument
				   back into argument list */
				((EseqTree) tempTree)->left =
				    dummy->right;
			}

			/* track treenode that points to the last
			   argument */
			treePtrPtr = &(((EseqTree) tempTree)->right);

			/* go to next actual argument */
			tempTree = ((EseqTree) tempTree)->right;

			/* go to next formal parameter, if there is one 
			 */
			if (currentParam != NULL)
				currentParam = currentParam->next;

		}

		/* convert func/ptr type to ptr */
		promoteFunctionOrArrayTree(tempTree);

		/* no more formal parameters */
		if (currentParam == NULL) {
			/* if function is prototyped, this is an error. 
			 */
			if (retStyle == PROTOTYPE) {
				errorT((Tree) f,
				       "parameter number mismatch");
				f->expn.type =
				    typeBuildBasic(TYERROR, NO_QUAL);
				return ((Tree) f);
			}

			/* handle old-style or var-arg parameters in
			   same manner */
			else {
				tempTree =
				    defaultArgumentPromotion(tempTree);
			}
		} else {	/* have a formal parameter to check
				   against */

			BinopTree dummy;
			VarTree v;

			/* going to setup a dummy BINOP node and use
			   assignmentConversion */
			v = (VarTree) dummyVar();
			v->expn.type = currentParam->type;
			dummy =
			    (BinopTree) newBinop(ASSIGN_OP, NULL,
						 (ExpTree) v, tempTree);
			dummy->expn.absn.lineno = f->expn.absn.lineno;
			dummy->expn.absn.filename =
			    f->expn.absn.filename;
			dummy = assignmentConversion(dummy);

			/* ok, paste possibly updated argument back
			   into argument list */
			tempTree = dummy->right;
		}

		/* paste back into node that points to last argument */
		*treePtrPtr = tempTree;

		/* Make sure there aren't too few arguments passed */
		if ((currentParam != NULL)
		    && (currentParam->next != NULL)) {
			errorT((Tree) f, "parameter number mismatch");
			return ((Tree) f);
		}
	}

	/* 
	 *  Now, need to worry about a function that returns struct/union. We
	 *  will prepend an extra argument to handle this. The address of where
	 *  the return value should be placed.
	 */
	if (isStructUnionType(((ExpTree) f)->type)) {
		addExtraArgumentForStructReturn(f);
	}
	return (Tree) f;
}

/*
 *  Usually, in expressions, function and array of T are converted to
 *  pointer to function and pointer to T, respectively. This is used
 *  both when analyzing operator operands and when analyzing arguments
 *  in function calls.
 *
 *  Also need to strip the qualifier here.
 *
 *  ANSI 3.2.2.1
 */
void promoteFunctionOrArrayTree(ExpTree e)
{
	switch (typeQuery(e->type)) {
	case TYARRAY:
	case TYFUNCTION:
		e->type = promoteArrayOrFunction(e->type);
		break;
	default:
		e->type = typeUnqualifiedVersion(e->type);
	}
}

/*
 * analyzeUnop: semantic analysis for unary operators
 *
 */
ExpTree analyzeUnop(UnopTree u)
{
	ExpTree ret;

	switch (u->op) {
	case DEREF_OP:
		/* 
		 *  For good or ill, DEREF is used both for ptr-deref and var-deref.
		 *  This code, however, only needs to worry about ptr-deref, since
		 *  the DEREF node for var-deref is added when analyzing the VAR node.
		 *  For ptr-deref, the type of the subtree must be ptr-to-T. So, type
		 *  of DEREF is set to T.
		 *
		 *  Must check for deref-able type. If not deref-able, turn DEREF into
		 *  NO-OP.
		 * ----------------------------------------------------------------
		 *  This is no longer true (I hope), Wed Oct  3 21:09:43 EST 2007, I
		 *  removed the deref above variables nodes... this is no longer needed
		 *  since the instructions that use the variables do the deref for
		 *  us when passed a memory location as an operand.
		 *  --eaburns
		 *
		 */
		if (typeQuery(u->left->type) != TYPOINTER) {
			errorT((Tree) u, "pointer type expected");
			u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		} else {
			u->expn.type = typeStripModifier(u->left->type);
			if (!isDerefable(u->expn.type)) {
				u->op = NO_OP;
			}
		}
		ret = (ExpTree) u;
		break;

	case INCRA_OP:
		ret = analyzeIncra(u);
		break;

	case DECRA_OP:
		ret = analyzeDecra(u);
		break;

	case INCRB_OP:
		ret = analyzeIncrb(u);
		break;

	case DECRB_OP:
		ret = analyzeDecrb(u);
		break;

	case USUB_OP:
		ret = analyzeUsub(u);
		break;

	case LNOT_OP:
		ret = analyzeLnot(u);
		break;

	case COMPLMT_OP:
		ret = analyzeComplmt(u);
		break;

	case ADDR_OF_OP:
		ret = analyzeAddrOf(u);
		break;

	case UADD_OP:
		ret = analyzeUadd(u);
		break;

	case SIZEOF_OP:
		ret = analyzeSizeof(u);
		break;

	default:
		bugT((Tree) u, "unknown operation in analyzeUnop");
	}

	return ret;
}

/*
 * analyzeBinop: semantic analysis for binary operators
 *
 */
static ExpTree analyzeBinop(BinopTree b)
{
	ExpTree ret;

	switch (b->op) {

	case INDEX_OP:
		ret = analyzeIndex(b);
		break;

	case LSHIFT_OP:
		ret = analyzeLshift(b);
		break;

	case RSHIFT_OP:
		ret = analyzeRshift(b);
		break;

	case ANDD_OP:
		ret = analyzeAndd(b);
		break;

	case XORR_OP:
		ret = analyzeXorr(b);
		break;

	case ORR_OP:
		ret = analyzeOrr(b);
		break;

	case MULT_ASSIGN_OP:
		ret = analyzeMultAssign(b);
		break;

	case DIV_ASSIGN_OP:
		ret = analyzeDivAssign(b);
		break;

	case MOD_ASSIGN_OP:
		ret = analyzeModAssign(b);
		break;

	case ADD_ASSIGN_OP:
		ret = analyzeAddAssign(b);
		break;

	case SUB_ASSIGN_OP:
		ret = analyzeSubAssign(b);
		break;

	case LEFT_ASSIGN_OP:
		ret = analyzeLeftAssign(b);
		break;

	case RIGHT_ASSIGN_OP:
		ret = analyzeRightAssign(b);
		break;

	case AND_ASSIGN_OP:
		ret = analyzeAndAssign(b);
		break;

	case XOR_ASSIGN_OP:
		ret = analyzeXorAssign(b);
		break;

	case OR_ASSIGN_OP:
		ret = analyzeOrAssign(b);
		break;

	case COMMA_OP:
		ret = analyzeComma(b);
		break;

	case MULT_OP:
		ret = analyzeMult(b);
		break;

	case DIV_OP:
		ret = analyzeDiv(b);
		break;

	case MOD_OP:
		ret = analyzeMod(b);
		break;

	case ADD_OP:
		ret = analyzeAdd(b);
		break;

	case SUB_OP:
		ret = analyzeSub(b);
		break;

	case LT_OP:
		ret = analyzeLt(b);
		break;

	case GT_OP:
		ret = analyzeGt(b);
		break;

	case LTE_OP:
		ret = analyzeLte(b);
		break;

	case GTE_OP:
		ret = analyzeGte(b);
		break;

	case EQL_OP:
		ret = analyzeEql(b);
		break;

	case NEQL_OP:
		ret = analyzeNeql(b);
		break;

	case LAND_OP:
		ret = analyzeLand(b);
		break;

	case LOR_OP:
		ret = analyzeLor(b);
		break;

	case ASSIGN_OP:
		ret = analyzeAssign(b);
		break;

	default:
		bugT((Tree) b, "unknown operation in analyzeBinary");
	}

	if (ret->absn.tag == BINOP_TAG
	    && isCompileTimeEvalable(((BinopTree) ret)->op)
	    && isConstTag(((BinopTree) ret)->left->absn.tag)
	    && isConstTag(((BinopTree) ret)->right->absn.tag)) {
		/* this binop is evaluatable at compile-time and both
		   operands are constants then evaluate it now */
		ret = evalBinopTree((BinopTree) ret);
	}

	return ret;
}


Tree analyze(Tree t)
{
	UnopTree u;
	BinopTree b;
	TriopTree tr;
	FcallTree f;
	EseqTree e;
	PtrTree ptr;
	FieldRefTree fr;
	CastopTree c;
	VarTree v;
	LabelTree la;
	CaseTree ca;
	DefaultTree d;
	SeqTree se;
	IfElseTree ie;
	SwitchTree sw;
	WhileTree w;
	DoWhileTree dw;
	ForTree fo;
	GotoTree g;
	ContinueTree co;
	BreakTree br;
	ReturnTree r;

	if (t == NULL) {
		return t;
	}

	switch (t->tag) {

	case UNOP_TAG:
		u = (UnopTree) t;
		u->left = (ExpTree) analyze((Tree) u->left);
		if (u->op != SIZEOF_OP && u->op != ADDR_OF_OP &&
		    !isAssignmentOperator(u->op)) {
			promoteFunctionOrArrayTree(u->left);
		}
		/* avoid cascade of errors */
		if (typeQuery(u->left->type) == TYERROR) {
			u->expn.type = u->left->type;
			return (Tree) u;
		} else {
			return (Tree) analyzeUnop(u);
		}

	case BINOP_TAG:
		b = (BinopTree) t;
		b->left = (ExpTree) analyze((Tree) b->left);
		if (!isAssignmentOperator(b->op)) {
			promoteFunctionOrArrayTree(b->left);
		}
		b->right = (ExpTree) analyze((Tree) b->right);
		promoteFunctionOrArrayTree(b->right);
		/* avoid cascade of errors */
		if (typeQuery(b->left->type) == TYERROR ||
		    typeQuery(b->right->type) == TYERROR) {
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (Tree) b;
		} else {
			return (Tree) analyzeBinop(b);
		}

	case TRIOP_TAG:
		tr = (TriopTree) t;
		tr->left = (ExpTree) analyze((Tree) tr->left);
		promoteFunctionOrArrayTree(tr->left);
		tr->middle = (ExpTree) analyze((Tree) tr->middle);
		promoteFunctionOrArrayTree(tr->middle);
		tr->right = (ExpTree) analyze((Tree) tr->right);
		promoteFunctionOrArrayTree(tr->right);
		/* avoid cascade of errors */
		if (typeQuery(tr->left->type) == TYERROR ||
		    typeQuery(tr->middle->type) == TYERROR ||
		    typeQuery(tr->right->type) == TYERROR) {
			tr->expn.type =
			    typeBuildBasic(TYERROR, NO_QUAL);
			return (Tree) tr;
		}
		if (tr->op == COND_OP) {
			return (Tree) analyzeCond(tr);
		} else {
			bugT(t, "analyze: unknown triop");
		}
		return t;

	case FCALL_TAG:
		f = (FcallTree) t;
		f->func = (ExpTree) analyze((Tree) f->func);
		promoteFunctionOrArrayTree(f->func);
		f->params = (ExpTree) analyze((Tree) f->params);
		/* avoid cascade of errors */
		if (typeQuery(f->func->type) == TYERROR) {
			f->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (Tree) f;
		} else {
			return analyzeFcall(f);
		}

	case ESEQ_TAG:
		e = (EseqTree) t;
		e->left = (ExpTree) analyze((Tree) e->left);
		e->right = (ExpTree) analyze((Tree) e->right);
		return t;

	case PTR_TAG:
		ptr = (PtrTree) t;
		ptr->left = (ExpTree) analyze((Tree) ptr->left);
		promoteFunctionOrArrayTree(ptr->left);
		return (Tree) analyzePtr(ptr);

	case FIELDREF_TAG:
		fr = (FieldRefTree) t;
		fr->left = (ExpTree) analyze((Tree) fr->left);
		promoteFunctionOrArrayTree(fr->left);
		return (Tree) analyzeFieldRef(fr);

	case CASTOP_TAG:
		c = (CastopTree) t;
		c->left = (ExpTree) analyze((Tree) c->left);
		promoteFunctionOrArrayTree(c->left);
		return (Tree) analyzeCast(c);

	case VAR_TAG:
		v = (VarTree) t;

#if 0
		{
			UnopTree u;
			TreeOp op;

			if (isDerefable(v->expn.type))
				op = DEREF_OP;
			else
				op = NO_OP;

			u = (UnopTree) newUnop(op,
					       typeUnqualifiedVersion
					       (v->expn.type),
					       (ExpTree) v);

			t = (Tree) u;
			t->filename = v->expn.absn.filename;
			t->lineno = v->expn.absn.lineno;
		}
#else
    if (v->stdrTag == GDECL && typeQuery(v->expn.type) == TYARRAY) {
      return (Tree) newUnop(ADDR_OF_OP, v->expn.type, &v->expn);
    }
#endif

		return t;

	case INT_CONST_TAG:	/* no work needed for these */
	case FP_CONST_TAG:
	case STRING_LIT_TAG:
	case LONG_CONST_TAG:
	case UINT_CONST_TAG:
	case ULONG_CONST_TAG:
		return t;

	case LABEL_TAG:
		la = (LabelTree) t;
		return analyzeLabel(la);

	case CASE_TAG:
		ca = (CaseTree) t;
		return analyzeCase(ca);

	case DEFAULT_TAG:
		d = (DefaultTree) t;
		return analyzeDefault(d);

	case SEQ_TAG:
		se = (SeqTree) t;
		se->left = analyze(se->left);
		se->right = analyze(se->right);
		return t;

	case IFELSE_TAG:
		ie = (IfElseTree) t;
		return analyzeIfElse(ie);

	case SWITCH_TAG:
		sw = (SwitchTree) t;
		return analyzeSwitch(sw);

	case WHILE_TAG:
		w = (WhileTree) t;
		return analyzeWhile(w);

	case DOWHILE_TAG:
		dw = (DoWhileTree) t;
		return analyzeDoWhile(dw);

	case FOR_TAG:
		fo = (ForTree) t;
		return analyzeFor(fo);

	case GOTO_TAG:
		g = (GotoTree) t;
		return analyzeGoto(g);

	case CONTINUE_TAG:
		co = (ContinueTree) t;
		return analyzeContinue(co);

	case BREAK_TAG:
		br = (BreakTree) t;
		return analyzeBreak(br);

	case RETURN_TAG:
		r = (ReturnTree) t;
		r->expr = (ExpTree) analyze((Tree) r->expr);
		return analyzeReturn(r);

	case ERROR_TAG:
		return t;

	default:
		bugT(t, "Invalid tree tag in analyze()");

	}

	return t;
/*NOTREACHED*/}

/*
 *  ANSI 3.6.6.4
 *
 */
static Tree analyzeReturn(ReturnTree r)
{
	if (typeQuery(gCurrentFunctionReturnType) != TYVOID) {
		if (r->expr != NULL) {
			BinopTree dummy;
			VarTree v;

			/* promote func/array to ptr if necessary */
			promoteFunctionOrArrayTree(r->expr);

			/* going to setup a dummy BINOP node and use
			   assignmentConversion */
			v = (VarTree) dummyVar();
			v->expn.type = gCurrentFunctionReturnType;
			dummy =
			    (BinopTree) newBinop(ASSIGN_OP, NULL,
						 (ExpTree) v, r->expr);
			dummy->expn.absn.lineno = r->absn.lineno;
			dummy->expn.absn.filename = r->absn.filename;
			r->expr =
			    ((BinopTree) assignmentConversion(dummy))->
			    right;
		}

		/* 
		 *  Yes, no else clause: ANSI says it is undefined behavior only for
		 *  the function to fail to produce a value when the caller uses the
		 *  value. So, presumably, it is okay not to produce a value if the
		 *  caller does not use a value.
		 */
	} else {
		if (r->expr != NULL) {
			errorT((Tree) r,
			       "return expression not allowed in void function");
		}
	}

	return (Tree) r;
}

/* vi: set ts=2 expandtab: */
