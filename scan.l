%{
/*
 * scan.l
 * lex/flex input for C scanner
 *
 * modified by shillman in phase 1.
 */

#include <math.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <stddef.h>
#include <errno.h>

/* these are C99 functions: we are compiling with -std=c89 */
float strtof(const char *nptr, char **endptr);
long double strtold(const char *nptr, char **endptr);

/* to quiet gcc warning: for some reason it does not see it in stdio.h */
int fileno(FILE *stream);

#include "defs.h"
#include "decl.h"
#include "tarch.h"
#include "types.h"
#include "symtab.h"
#include "message.h"
#include "bucket.h"
#include "tree.h"
#include "y.tab.h"
#include "decl2.h"

int yylex(void);

#undef yywrap

extern GP_SWITCH g_id_lookup;

static void count (void);
static void comment (void);

static void handleCPPline(void);

/* These return the string if valid, and ERROR if not */
static char * checkDoubleConst (void);
static char * checkLongDoubleConst (void);
static char * checkFloatConst (void);

/* These return the int in question */
static int convertIntConst (int*);
static long convertLongConst (long*);
static unsigned int convertUintConst (int *myError);
static unsigned long convertUlongConst (int *myError);

static unsigned long convertUnsign (char *, unsigned int bytes, int *myError);

static unsigned int convertChrConst (int *myError);

/* this is to determine if there was an error in the functions which return
 * unsigned numbers */
static int myError = 0; /* error if non-zero after function returns */

static unsigned int escChar (char *str, int *myError);
static int digit (char c);

/* current line number in input file */
static int srcLineNo = 1;

/* used with strtol and strtoul */
#define INT_VALID " \n\t\r\f\vlLuU"
/* with strto[d|f|ld] */
#define FLOAT_VALID " \n\t\r\f\vlLfF"

/* following supports the stand-alone scanner program: lexdbg */
#ifdef DEBUG
#       define token(x)        (DisplayToken(# x),  x)
        YYSTYPE yylval;
        GP_SWITCH g_id_lookup = OFF;
        int DisplayToken (char *);

#else

#       define token(x) x

#endif

%}

D      [0-9]
O      [0-7]
L      [a-zA-Z_]
H      [a-fA-F0-9]
E      [Ee][+-]?{D}+

F      [fF]
LO     [lL]
IU     [uU]
IUL    (({LO}[uU])|([uU]{LO}))

FS     ({F}|{LO})?

HC     (0[xX]{H}+)
OC     (0{O}*)
DC     ({D}+)

%%

"/*"        { comment(); }

"auto"      { count(); return(token(AUTO)); }
"break"     { count(); return(token(BREAK)); }
"case"      { count(); return(token(CASE)); }
"char"      { count(); return(token(CHAR)); }
"const"     { count(); return(token(CONST)); }
"continue"  { count(); return(token(CONTINUE)); }
"default"   { count(); return(token(DEFAULT)); }
"do"        { count(); return(token(DO)); }
"double"    { count(); return(token(DOUBLE)); }
"else"      { count(); return(token(ELSE)); }
"enum"      { count(); return(token(ENUM)); }
"extern"    { count(); return(token(EXTERN)); }
"float"     { count(); return(token(FLOAT)); }
"for"       { count(); return(token(FOR)); }
"goto"      { count(); return(token(GOTO)); }
"if"        { count(); return(token(IF)); }
"int"       { count(); return(token(INT)); }
"long"      { count(); return(token(LONG)); }
"register"  { count(); return(token(REGISTER)); }
"return"    { count(); return(token(RETURN)); }
"short"     { count(); return(token(SHORT)); }
"signed"    { count(); return(token(SIGNED)); }
"sizeof"    { count(); return(token(SIZEOF)); }
"static"    { count(); return(token(STATIC)); }
"struct"    { count(); return(token(STRUCT)); }
"switch"    { count(); return(token(SWITCH)); }
"typedef"   { count(); return(token(TYPEDEF)); }
"union"     { count(); return(token(UNION)); }
"unsigned"  { count(); return(token(UNSIGNED)); }
"void"      { count(); return(token(VOID)); }
"volatile"  { count(); return(token(VOLATILE)); }
"while"     { count(); return(token(WHILE)); }

{L}({L}|{D})*           {
        ST_DR tmp = NULL;
        int t1  = 0;

        count(); 
        yylval.y_id = st_enter_id(yytext);
        if (g_id_lookup == ON)
        {
            tmp = st_lookup (yylval.y_id, &t1);

            if ( (tmp != NULL) && (tmp->tag == TDEF) )
            {
                return(token(TYPE_NAME));
            }
        }

        return(token(IDENTIFIER)); 
      }

({HC}|{OC}|{DC})        { /* This looks for signed integer constants */
        int ret;
        count();
        ret = convertIntConst(&yylval.y_int);
        if (ret == 0)
        {
#ifdef DEBUG
          msg("error in int constant: yylval.y_int set to 0.");
#endif
          yylval.y_int = 0;
        }
#ifdef DEBUG
        msg("yylval.y_int contains %d.", yylval.y_int);
#endif
        return(token(INT_CONSTANT));
      }

({HC}|{OC}|{DC}){LO}    { /* This looks for signed long constants */
        int ret;
        count();
        ret = convertLongConst(&yylval.y_long);
        if (ret == 0)
        {
#ifdef DEBUG
          msg("error in long constant: yylval.y_long set to 0.");
#endif
          yylval.y_long = 0;
        }
#ifdef DEBUG
        msg("yylval.y_long contains %ld.", yylval.y_long);
#endif
        return(token(LONG_CONSTANT));
      }

({HC}|{OC}|{DC}){IU}    { /* This looks for unsigned integer constants */
        count(); 
        myError = 0;
        yylval.y_uint = convertUintConst(&myError);
        if (myError)
        {
#ifdef DEBUG
          msg("error in unsigned int constant: yylval.y_uint set to 0.");
#endif
          yylval.y_uint = 0;
        }
#ifdef DEBUG
        msg("yylval.y_uint contains %u.", yylval.y_uint);
#endif
        return(token(UNSIGNED_INT_CONSTANT)); 
      }

({HC}|{OC}|{DC}){IUL}   { /* This looks for unsigned long constants */
        count();
        myError = 0;
        yylval.y_ulong = convertUlongConst(&myError);
        if (myError)
        {
#ifdef DEBUG
          msg("error in unsigned long constant: yylval.y_ulong set to 0.");
#endif
          yylval.y_ulong = 0;
        }
#ifdef DEBUG
        msg("yylval.y_ulong contains %lu.", yylval.y_ulong);
#endif
        return(token(UNSIGNED_LONG_CONSTANT));
      }

'(\\.|[^\\'])+'         {
        count(); 
        myError = 0;
        yylval.y_int = (int)convertChrConst(&myError);
        if (myError)
        {
#ifdef DEBUG
          msg("error in char constant: yylval.y_int set to 0.");
#endif
          yylval.y_int = 0;
        }
#ifdef DEBUG
        msg("yylval.y_int contains %d.", yylval.y_int);
#endif
        return(token(INT_CONSTANT)); 
      }

L'(\\.|[^\\'])+'        { /* This finds the wide char constants */
        count();
        myError = 0;
        yylval.y_uint = convertChrConst(&myError);
        if (myError)
        {
#ifdef DEBUG
          msg("error in wide char constant: yylval.y_uint set to 0.");
#endif
          yylval.y_uint = 0;
        }
#ifdef DEBUG
        msg("yylval.y_uint contains %u.", yylval.y_uint);
#endif
        return(token(UNSIGNED_INT_CONSTANT));
      }

({D}+{E})|({D}*"."{D}+({E})?)|({D}+"."({E})?)        { /* regular double */
        count();
        yylval.y_string = checkDoubleConst();
        return(token(DOUBLE_CONSTANT));
      }

(({D}+{E})|({D}*"."{D}+({E})?)|({D}+"."({E})?)){LO}  { /* long double */
        count();
        yylval.y_string = checkLongDoubleConst();
        return(token(LONG_DOUBLE_CONSTANT));
      }

(({D}+{E})|({D}*"."{D}+({E})?)|({D}+"."({E})?)){F}   { /* float */
        count();
        yylval.y_string = checkFloatConst();
        return(token(FLOAT_CONSTANT));
      }

[L]?\"(\\.|[^\\"])*\"  { 
        int i;
        count(); 
        for(i = 0; yytext[i] != '\0'; i++)
        { /* checking for an embedded newline */
          if(yytext[i] == '\n') 
          {
            error("strings must start and end on the same line.");
            return(token(BAD));
          }
        }
        if(yytext[0] == 'L')
        {
          yytext++;
        }
        yylval.y_string = st_save_string (yytext);
        return(token(STRING_LITERAL)); 
      }

"##"      { 
        count(); 
        error("%s should not appear after pre-processing.", yytext);
        return(token(BAD)); 
      }

^#.*$     {
        handleCPPline();
        count(); 
      }

"..."     { count(); return(token(ELIPSIS)); }
">>="     { count(); return(token(RIGHT_ASSIGN)); }
"<<="     { count(); return(token(LEFT_ASSIGN)); }
"+="      { count(); return(token(ADD_ASSIGN)); }
"-="      { count(); return(token(SUB_ASSIGN)); }
"*="      { count(); return(token(MUL_ASSIGN)); }
"/="      { count(); return(token(DIV_ASSIGN)); }
"%="      { count(); return(token(MOD_ASSIGN)); }
"&="      { count(); return(token(AND_ASSIGN)); }
"^="      { count(); return(token(XOR_ASSIGN)); }
"|="      { count(); return(token(OR_ASSIGN)); }
">>"      { count(); return(token(RIGHT_OP)); }
"<<"      { count(); return(token(LEFT_OP)); }
"++"      { count(); return(token(INC_OP)); }
"--"      { count(); return(token(DEC_OP)); }
"->"      { count(); return(token(PTR_OP)); }
"&&"      { count(); return(token(AND_OP)); }
"||"      { count(); return(token(OR_OP)); }
"<="      { count(); return(token(LE_OP)); }
">="      { count(); return(token(GE_OP)); }
"=="      { count(); return(token(EQ_OP)); }
"!="      { count(); return(token(NE_OP)); }
";"       { count(); return(token(';')); }
"{"       { count(); return(token('{')); }
"}"       { count(); return(token('}')); }
","       { count(); return(token(',')); }
":"       { count(); return(token(':')); }
"="       { count(); return(token('=')); }
"("       { count(); return(token('(')); }
")"       { count(); return(token(')')); }
"["       { count(); return(token('[')); }
"]"       { count(); return(token(']')); }
"."       { count(); return(token('.')); }
"&"       { count(); return(token('&')); }
"!"       { count(); return(token('!')); }
"~"       { count(); return(token('~')); }
"-"       { count(); return(token('-')); }
"+"       { count(); return(token('+')); }
"*"       { count(); return(token('*')); }
"/"       { count(); return(token('/')); }
"%"       { count(); return(token('%')); }
"<"       { count(); return(token('<')); }
">"       { count(); return(token('>')); }
"^"       { count(); return(token('^')); }
"|"       { count(); return(token('|')); }
"?"       { count(); return(token('?')); }

[ \t\v\n\f]    { count(); }

.         { 
        count(); 
        error("%s is not valid.", yytext); 
        return(token(BAD)); 
      }

%%

int yywrap ()
{
  return 1;
}

/* current column number in input file */
static int column = 0;

/*
 * This function parses through a comment, storing the number of lines
 */
static void comment(void)
{
  char c;
  char c1;

  column += 2;   /* for '/' and '*' */

loop:
  while (1)
  {
    c = input();

    if (c == '*' || c == -1)
    {
      break;
    }

    if (c == '\n')
    {
      column = 0;
      srcLineNo++;
    }
    else if (c == '\t')
    {
      column += 8 - (column % 8);
    }
    else
    {
      column++;
    }
  }

  if (c != -1)
  {
    c1 = input();

    if (c1 != '/' && c1 != -1)
    {
      unput(c1);
      goto loop;
    }
  }

  if (c == -1 || c1 == -1) 
  {
    error("open comment at EOF");
  }
  else 
  {
    column += 2;   /* for '*' and '/' */
  }
}

static char* currentFilename = "<no filename available>";

void YYinitializeFilename(char* name)
{
  currentFilename = malloc(strlen(name)+1);
  strcpy(currentFilename, name);
}

char* YYfilename(void)
{
  return currentFilename;
}

/*
 * This processes # lines left by C preprocessor. The lines
 * are of the form: # lineno "filename".
 */
static void handleCPPline(void)
{
  int line;
  char* buf1;
  char* buf2;
  int len;

  buf1 = malloc(strlen(yytext)+1);
  if (sscanf(yytext, "# %d %s", &line, buf1) != 2)
  {
    bug("unexpected cpp line in handleCPPline");
  }

  /* strip off double quotes from filename */
  len = strlen(buf1);
  buf2 = malloc(len-1);
  strncpy(buf2, buf1+1, len-2);
  free(buf1);

  /* now reset the variable holding the current filename */
  free(currentFilename);
  currentFilename = malloc(strlen(buf2)+1);
  strcpy(currentFilename, buf2);

  /* also reset the line number.
   *   subtract one because count will count this line
   */
  srcLineNo = line - 1;
}

/*
 * This counts line numbers and column numbers.
 */
static void count(void)
{
  int i;

  for (i = 0; yytext[i] != '\0'; i++)
  {
    if (yytext[i] == '\n')
    {
      srcLineNo++;
      column = 0;
    }
    else if (yytext[i] == '\t')
    {
      column += 8 - (column % 8);
    }
    else
    {
      column++;
    }
  }
}

/*
 * This function makes sure that the value in the yytext field is a valid signed
 * int, and returns it as an int if so. Otherwise, it returns -1.
 */
static int convertIntConst (int *ret)
{
  int err;
  err = 0;

  *ret = (int)convertUnsign(yytext, TARCH_SIZEOF_SINT, &err);
  if(err)
  { /* the integer given was too large for an int */
    error("%s was too large to be a signed int.", yytext);
    return 0;
  }

  return 1;
}

/*
 * This function makes sure that the value in the yytext field is a valid signed
 * long, and returns it as an int if so. Otherwise, it returns -1.
 */
static long convertLongConst (long *ret)
{
  int err;
  err = 0;

  *ret = convertUnsign(yytext, TARCH_SIZEOF_SLONG, &err);
  if (err)
  {
    error("%s was too large to be a signed long.", yytext);
    return 0;
  }
  return 1;
}

/*
 * This function makes sure that the value in the yytext field is a valid 
 * unsigned int, and returns it as an int if so.
 * If an error, sets *myError to one.
 */
static unsigned int convertUintConst (int *myError)
{
  unsigned int ret = 0;
  ret = (unsigned int)convertUnsign(yytext, TARCH_SIZEOF_UINT, myError);
  if(*myError)
  { /* the integer given was too large for an int */
    error("%s was too large to be an unsigned integer.", yytext);
    *myError = 1;
  }
  return ret;
}

/*
 * This function makes sure that the value in the yytext field is a valid 
 * unsigned int, and returns it as an int if so.
 * If error, *myError will be set to 1.
 */
static unsigned long convertUlongConst (int *myError)
{
  unsigned long ret = 0;
  ret = convertUnsign(yytext, TARCH_SIZEOF_ULONG, myError);
  if (*myError)
  {
    error("%s was too large to be an unsigned long.", yytext);
  }
  return ret;
}

/*
 * This function converts a string to an unsigned integer.
 * Changes *myError to point to 1.
 */
static unsigned long convertUnsign (char * input, unsigned int bytes, int *myError)
{
  unsigned long res = 0;
  unsigned long max;
  char *ptr;
  errno = 0;
  res = strtoul(input, &ptr, 0);
  if(errno !=0 || strspn(ptr, INT_VALID) != strlen(ptr) )
  { /* invalid or too large */
    *myError = 1;
  }

  if (sizeof(unsigned long) > bytes) {
    max = pow(2, bytes * 8);
    if (res > max)
      *myError = 1;
  }

  return res;
}

/* 
 * This function checks that the parameter given is a valid double. If it is 
 * valid, it returns the parameter. Otherwise, it returns the string 'ERROR'.
 */
static char * checkDoubleConst (void)
{
  char *ptr = (char *)NULL;
  char *err_string;

  errno = 0;
  strtod(yytext, &ptr);
  if( (strspn(ptr, FLOAT_VALID) != strlen(ptr)) || errno)
  {
    if(errno)
    {
      error("%s is not a valid double: %s", yytext, strerror(errno));
    }
    else
    {
      error("%s is not a valid double.", yytext);
    }
    if( !(err_string = (char *)malloc(sizeof("ERROR")+1)) )
    {
      fatal("Could not allocate space: %s\n", strerror(errno));
    }
    strncpy(err_string, "ERROR", sizeof("ERROR"));
    return err_string;
  }
  return st_save_string(yytext);
}

/*
 * This function checks that the parameter given is a valid long double. If it 
 * is valid, it returns the parameter. Otherwise, it returns the string 'ERROR'.
 */
static char * checkLongDoubleConst (void)
{
  char *ptr;
  char *err_string;
  int len;

  errno = 0;
  strtold(yytext, &ptr);
  if( (strspn(ptr, FLOAT_VALID) != strlen(ptr)) || errno)
  {
    if(errno)
    {
      error("%s is not a valid long double: %s", yytext, strerror(errno));
    }
    else
    {
      error("%s is not a valid long double.", yytext);
    }
    if( !(err_string = (char *)malloc(sizeof("ERROR")+1)) )
    {
      fatal("Could not allocate space: %s\n", strerror(errno));
    }
    strncpy(err_string, "ERROR", sizeof("ERROR"));
    return err_string;
  }
  for(len = 0; yytext[len] != '\0'; len++);
  yytext[len-1] = '\0';
  return st_save_string(yytext);
}

/*
 * This function checks that the parameter given is a valid float. If it is
 * valid, it returns the parameter. Otherwise, it returns the string 'ERROR'.
 */
static char * checkFloatConst (void)
{
  char *ptr;
  char *err_string;
  int len;

  errno = 0;
  strtof(yytext, &ptr);
  if( (strspn(ptr, FLOAT_VALID) != strlen(ptr)) || errno)
  {
    if(errno)
    {
      error("%s is not a valid float: %s", yytext, strerror(errno));
    }
    else
    {
      error("%s is not a valid float.", yytext);
    }
    if( !(err_string = (char *)malloc(sizeof("ERROR")+1)) )
    {
      fatal("Could not allocate space: %s\n", strerror(errno));
    }
    strncpy(err_string, "ERROR", sizeof("ERROR"));
    return err_string;
  }
  for(len = 0; yytext[len] != '\0'; len++);
  yytext[len-1] = '\0';
  return st_save_string(yytext);
}

/*
 * This function checks that a given character constant is valid, changes it to
 * an int, and returns it. If an error occurs, the *myError is changed to one.
 */
static unsigned int convertChrConst (int *myError)
{
  int temp = 0;
  int i;

  for(i = 0; yytext[i] != '\0'; i++)
  { /* checking for an embedded newline */
    if(yytext[i] == '\n')
    {
      *myError = 1;
      error("Character constants must start and end on the same line.");
      return (unsigned int)0;
    }
  }
  if(yytext[0] == 'L' && yytext[2] == '\\')
  { /* wide character constant w/initial escape character */
      return escChar (yytext+1, myError);
  }
  else if(yytext[1] == '\\')
  { /* not wide, but escape character */
    temp = (int)escChar (yytext, myError);
    if(*myError)
    { /* if an error occured, return */
      return (unsigned int)0;
    }
    else if((unsigned int)temp == escChar (yytext, myError))
    {
      return (unsigned int)temp;
    }
    else
    {
      *myError = 1;
      error("%s is too large to be a valid character constant.", yytext);
      return (unsigned int)0;
    }
  }
  else if(yytext[0] == 'L')
  {
    return (unsigned int)yytext[2];
  }
  else
  {
    return (unsigned int)yytext[1];
  }
}

/* 
 * This function reads an ESC character from the input, checks it against
 * a valid character, and returns it as an int
 */
static unsigned int escChar(char *str, int *myError)
{
  char c;
  char answer;
  char *tmp;
  int intAnswer;
  int lenOfStr;

  tmp = str;
  sscanf (str, "'\\%c", &c);

  switch (c)
  {
    case '\'':
    case '"':
    case '\\':
    case '?':  answer = c; break;
    case 'a': answer = '\a'; break;
    case 'b' : answer = '\b'; break;
    case 'f' : answer = '\f'; break;
    case 'n' : answer = '\n'; break;
    case 'r' : answer = '\r'; break;
    case 't' : answer = '\t'; break;
    case 'v' : answer = '\v'; break;
    case 'x' : intAnswer = 0;
      /* length is strlen minus 4 for the quotes and the \x*/
      lenOfStr = strlen(str)-4; /* so we can make sure no errors occured */
      /* add three to str for the initial quote, and the \x */
      for (str +=3, sscanf(str,"%c",&c);isxdigit(c); lenOfStr--,
           sscanf(++str,"%c", &c))
      {
        intAnswer = intAnswer * 16 + digit(c);
      }

      if(lenOfStr && (str[0] != '\''))
      { /* error */
        *myError = 1;
        error("%s is an invalid hex escape sequence. It failed at %s.", 
               tmp, str);
      }
      answer = (char)intAnswer;
      break;
    default : intAnswer = 0;
      /* length is strlen minus 4 for the quotes and the \x*/
      lenOfStr = strlen(str)-4; /* so we can make sure no errors occured */
      if (c >= '0' && c < '8')
      { /* valid octal character */
        intAnswer = digit(c);
        for (str += 3, sscanf (str, "%c", &c); ((c >= '0') && (c < '8')); 
             lenOfStr--,sscanf (++str, "%c", &c))
        {
          intAnswer = intAnswer * 8 + digit(c);
        }
        if(lenOfStr && (str[0] != '\''))
        { /* error */
          *myError = 1;
          error("%s is an invalid octal escape sequence. It failed at %s.", 
                 tmp, str);
        }
      }

      else
      { /* invalid escape character */
        *myError = 1;
        error("%s is not a valid escape sequence.", str);
      }
      answer = (char)intAnswer;
      break;
  }

  return (unsigned int)answer;
}

/**************************************************************/
/* return the value of a hex digit */
 
static int digit (char c)
{
  if (isdigit(c))
  {
    return c-'0';
  }
  return (toupper(c)-'A'+10);
}

/**************************************************************/
/* return the current line number - exported outside file */

int YYlineno (void)
{
  return srcLineNo;
}

/**************************************************************/
/* return the current column number - exported outside file */

int YYcolumnno (void)
{
  return column;
}

/* following supports the stand-alone scanner program: lexdbg */
#ifdef        DEBUG
FILE *outfp;        /* file to which message.c will write */
FILE *errfp;        /* file to which message.c will write */
  main()
  {

    outfp = stdout;
    errfp = stderr;
    char *p;

    while (yylex());
  }

  int DisplayToken(char *p)
  {
    printf("%-10.30s is \"%s\"\n", p, yytext);
    return 1;
  }
#endif
