/****************************************************************************/
/* 
	definitions to support message.c
*/

#ifndef MESSAGE_H
#define MESSAGE_H

#include "tree.h"

/* Global Count of Compiler Errors */
extern int compiler_errors;

/* Global Count of Compiler Warnings */
extern int compiler_warnings;

/* Global Count of Compiler Messages */
extern int compiler_messages;


/****************************************************************/
/* Available Message routines */

/* Note: All message routine arguments take a "printf" like
	format.
		ex. msg("%d %d \n",x,y)

	NOTE: the routines all print using the FILE variable named
	      "errfp". This variable is assumed to be declared and
	      initialized outside message.c.

	NOTE: the message routines obtain the source file line number
	      by calling the scanner routine YYlineno.
 */


/*********************************************************************/
extern void msgn(char *, ...);

	/* writes a message to errfp file WITH NO NEWLINE */


/*********************************************************************/
extern void msg(char *, ...);

	/* writes a message to errfp file WITH NEWLINE. */

/*********************************************************************/
extern void info(char *, ...);

	/* writes an informational message to errfp file with line
	   number and newline. */

/*********************************************************************/
extern void warning(char *, ...);

	/* this routine issues a formal warning message to errfp file */

/*********************************************************************/
extern void error(char *, ...);

	/* this routine issues a formal error message to errfp file */

/*********************************************************************/
extern void fatal(char *, ...);

	/* this routine writes a fatal error message to errfp file and
	   the program is aborted.

	   "fatal" usually means an internal data structure of the
	   compiler has overflowed. */

/*********************************************************************/
extern void bug(char *, ...);

	/* this routine writes a bug message to errfp file and the
	   program is aborted.

	   "bug" means a problem with the internals of the compiler. */

/*
 *  The following routines are variants of the above routines that
 *  get the filename and line number out of the AST node (rather
 *  than from the scanner). This is for use in post-parse phases,
 *  like the semantic analysis and code generation routines.
 */

extern void infoT(Tree t, char *fmt, ...);
extern void warningT(Tree t, char *fmt, ...);
extern void errorT(Tree t, char *fmt, ...);
extern void fatalT(Tree t, char *fmt, ...);
extern void bugT(Tree t, char *fmt, ...);
#endif
