/*  Fanny Xu, CS812, Phase 3
 *
 *  encodeExpr.c - code generation for expressions
 *
 */

#include "analyze.h"
#include "encode.h"
#include "message.h"
#include "tarch.h"
#include "tree.h"

/* This function does the encoding for the binary '+' operator.
 */
void encodeAdd(BinopTree b)
{
	TypeTag myType;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeAdd\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "NULL child pointer passed to encodeAdd\n");

	myType = typeQuery(b->expn.type);
	if (myType == TYERROR)
		return;		/* should never happen */

	comment("add %s + %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->right->reg);
	useReg(b->left->reg);
	useReg(b->expn.reg);

	switch (myType) {
	case TYLONGDOUBLE:
		useReg(b->right->reg);
		emitFld(b->right->reg);
		useReg(b->left->reg);
		emitFld(b->left->reg);
		emit("\tfaddp");
		emitFst(b->expn.reg);
		break;

	case TYFLOAT:
	case TYDOUBLE:
		emit("\tadds%c\t%s, %s", determineIntelSuffix(b->expn.type),
		     regString(b->left->reg), regString(b->expn.reg));
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		emit("\tadd%c\t%s, %s", determineIntelSuffix(b->expn.type),
		     regStringAs(myType, b->left->reg),
		     regStringAs(myType, b->expn.reg));
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeAdd() %d",
		     myType);
	}

	defReg(b->expn.reg);
	return;
}

/* This function does the encoding for the binary '-' operator.
 */
void encodeSub(BinopTree b)
{
	TypeTag myType;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null pointer passed to encodeSub\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "Null child pointer passed to encodeSub\n");

	myType = typeQuery(b->expn.type);
	if (myType == TYERROR)
		return;

	comment("sub %s - %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->left->reg);
	useReg(b->right->reg);
	useReg(b->expn.reg);

	switch (myType) {
	case TYLONGDOUBLE:
		useReg(b->right->reg);
		emitFld(b->right->reg);
		useReg(b->left->reg);
		emitFld(b->left->reg);
		emit("\tfsubp");
		useReg(b->expn.reg);
		emitFst(b->expn.reg);
		break;

	case TYFLOAT:
	case TYDOUBLE:
		emit("\tsubs%c\t%s, %s", determineIntelSuffix(b->expn.type),
		     regString(b->right->reg), regString(b->expn.reg));
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		emit("\tsub%c\t%s, %s", determineIntelSuffix(b->expn.type),
		     regStringAs(myType, b->right->reg),
		     regStringAs(myType, b->expn.reg));
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeSub()");
	}
	defReg(b->expn.reg);

	return;
}

/* This function does the encoding for the binary operator '/'.
 */
void encodeDiv(BinopTree b)
{
	TypeTag myType;
	REG tmp;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null pointer passed to encodeDiv\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "Null child pointer passed to encodeDiv\n");

	myType = typeQuery(b->expn.type);
	if (myType == TYERROR)
		return;

	comment("div %s / %s", regString(b->left->reg),
	        regString(b->right->reg));

	switch (myType) {
	case TYLONGDOUBLE:
		b->expn.reg = getResultReg(b->right->reg);
		useReg(b->right->reg);
		emitFld(b->right->reg);
		useReg(b->left->reg);
		emitFld(b->left->reg);
		emit("\tfdivp");
		useReg(b->expn.reg);
		emitFst(b->expn.reg);
		break;

	case TYFLOAT:
	case TYDOUBLE:
		b->expn.reg = getResultReg(b->left->reg);
		useReg(b->right->reg);
		useReg(b->expn.reg);

		emit("\tdivs%c\t%s, %s", determineIntelSuffix(b->expn.type),
		     regString(b->right->reg), regString(b->expn.reg));

		defReg(b->expn.reg);
		break;

	case TYSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDLONGINT:
		b->expn.reg = allocNextReg(myType);

		useReg(b->left->reg);
		emit("\t%s\t%s, %s", moveString(myType),
		     regString(b->left->reg), registerAs('a', myType));

		if (myType != TYSIGNEDLONGINT)
			/* cltd clobbers %rdx */
			emit("\tcltd");
		else
			emit("\tcqto");

		tmp = getRegister(b->right->reg);
		useReg(tmp);
		emit("\tidiv%c\t%s", determineIntelSuffix(b->expn.type),
		     regString(tmp));
		emit("\t%s\t%s, %s", moveString(myType),
		     registerAs('a', myType), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	case TYUNSIGNEDCHAR:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDLONGINT:
		b->expn.reg = allocNextReg(myType);

		emit("\txorq\t%%rdx, %%rdx");
		useReg(b->left->reg);
		emit("\t%s\t%s, %s", moveString(myType),
		     regString(b->left->reg), registerAs('a', myType));
		tmp = getRegister(b->right->reg);
		useReg(tmp);
		emit("\tdiv%c\t%s", determineIntelSuffix(b->expn.type),
		     regString(tmp));
		emit("\t%s\t%s, %s", moveString(myType),
		     registerAs('a', myType), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeDiv()");
	}

	return;
}

/* This function does the encoding for the binary operator '*'.
 */
void encodeMult(BinopTree b)
{
	TypeTag myType;
	REG tmp;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null pointer passed to encodeSub\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "Null child pointer passed to encodeSub\n");

	myType = typeQuery(b->expn.type);
	if (myType == TYERROR)
		return;

	comment("mul %s * %s", regString(b->left->reg),
	        regString(b->right->reg));

	switch (myType) {
	case TYLONGDOUBLE:
		b->expn.reg = getResultReg(b->right->reg);
		useReg(b->right->reg);
		emitFld(b->right->reg);
		useReg(b->left->reg);
		emitFld(b->left->reg);
		emit("\tfmulp");
		emitFst(b->expn.reg);
		defReg(b->expn.reg);
		break;

	case TYFLOAT:
	case TYDOUBLE:
		b->expn.reg = getResultReg(b->right->reg);
		useReg(b->left->reg);
		useReg(b->expn.reg);

		emit("\tmuls%c\t%s, %s", determineIntelSuffix(b->expn.type),
		     regString(b->left->reg), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	case TYSIGNEDCHAR:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYUNSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYSIGNEDLONGINT:
		b->expn.reg = allocNextReg(myType);
		useReg(b->left->reg);
		emit("\t%s\t%s, %s", moveString(myType),
		     regString(b->left->reg), registerAs('a', myType));
		tmp = getRegister(b->right->reg);
		useReg(tmp);
		emit("\timul%c\t%s", determineIntelSuffix(b->expn.type),
		     regString(tmp));
		emit("\t%s\t%s, %s", moveString(myType),
		     registerAs('a', myType), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeMult()");
	}

	return;
}

/* This function does the encoding for the binary '%' operator.
 */
void encodeMod(BinopTree b)
{
	TypeTag myType;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null pointer passed to encodeDiv\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "Null child pointer passed to encodeDiv\n");

	myType = typeQuery(b->expn.type);
	if (myType == TYERROR)
		return;

	comment("mod %s %% %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = allocNextReg(myType);

	switch (myType) {
	case TYSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDLONGINT:
		useReg(b->left->reg);
		emit("\t%s\t%s, %s", moveString(myType),
		     regString(b->left->reg), registerAs('a', myType));

		if (myType != TYSIGNEDLONGINT)
			/* cltd clobbers %rdx */
			emit("\tcltd");
		else
			emit("\tcqto");

		useReg(b->right->reg);
		emit("\tidiv%c\t%s", determineIntelSuffix(b->expn.type),
		     regString(b->right->reg));
		emit("\t%s\t%s, %s", moveString(myType),
		     registerAs('d', myType), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	case TYUNSIGNEDCHAR:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDLONGINT:
		emit("\txorq\t%%rdx, %%rdx");
		useReg(b->left->reg);
		emit("\t%s\t%s, %s", moveString(myType),
		     regString(b->left->reg), registerAs('a', myType));
		useReg(b->right->reg);
		emit("\tdiv%c\t%s", determineIntelSuffix(b->expn.type),
		     regString(b->right->reg));
		emit("\t%s\t%s, %s", moveString(myType),
		     registerAs('d', myType), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeMod()");
	}

	return;
}

/* Dummy function for encoding '*='. It is never called.
 */
void encodeMultAssign(BinopTree b)
{
	info("encodeMultAssign: You are not supposed to get here!");
	return;
}

/* Dummy function for encoding '/='. It is never called.
 */
void encodeDivAssign(BinopTree b)
{
	info("encodeDivAssign: You are not supposed to get here!");
	return;
}

/* Dummy function for encoding '%='. It is never called.
 */
void encodeModAssign(BinopTree b)
{
	info("encodeModAssign: You are not supposed to get here!");
	return;
}

/* Dummy function for encoding '+='. It is never called.
 */
void encodeAddAssign(BinopTree b)
{
	info("encodeAddAssign: You are not supposed to get here!");
	return;
}

/* Dummy function for encoding '-='. It is never called.
 */
void encodeSubAssign(BinopTree b)
{
	info("encodeSubAssign: You are not supposed to get here!");
	return;
}

/* This function does the encoding for the unary '-' operator.
 */
void encodeUsub(UnopTree u)
{
	TypeTag myType;
	FpConstTree fpconst;
	BinopTree mul;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null pointer passed to encodeUsub\n");
	if (!u->left)
		bugT((Tree) u,
		     "Null child pointer passed to encodeUsub\n");

	myType = typeQuery(u->expn.type);
	if (myType == TYERROR)
		return;

	comment("negation -%s", regString(u->left->reg));

	switch (myType) {
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
		/* multiply by negative one */
		fpconst = (FpConstTree) newFpConst("-1.0", u->expn.type);
		mul = (BinopTree) newBinop(MULT_OP, u->expn.type,
		                           (ExpTree) fpconst, u->left);
		encode((Tree) mul);
		u->expn.reg = mul->expn.reg;
		break;

	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		u->expn.reg = getResultReg(u->left->reg);

		useReg(u->expn.reg);
		emit("\tneg%c\t%s", determineIntelSuffix(u->expn.type),
		     regString(u->expn.reg));
		defReg(u->expn.reg);
		break;

	default:
		bugT((Tree) u, "Invalid tree type in encodeUsub()\n");
	}
	return;
}


/*  80% level  */

/* Dummy funtion to encode the unary operator '!'.
 * It is never called.
 */
void encodeLnot(UnopTree b)
{
	b = NULL;		/* defeat warning */
	info("encodeLnot: You are not supposed to get here!");
	return;
}

/**
 * \brief Encodes a comparison.  After this the flags registers should
 *        be set properly for set%c instructions.
 */
static void encodeCompare(BinopTree b)
{
	TypeTag myType;
	REG reg;
	/* error checkings */
	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeCompare\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "NULL child pointer passed to encodeCompare\n");

	myType = typeQuery(((ExpTree) b->left)->type);
	if (myType == TYERROR)
		return;

	comment1("encodeCompare");

	switch (myType) {
	case TYFLOAT:
	case TYDOUBLE:
		reg = getRegister(b->left->reg);
		useReg(b->right->reg);
		useReg(reg);
		emit("\tucomis%c\t%s, %s", determineIntelSuffix(b->right->type),
		     regString(b->right->reg), regString(reg));
		break;

	case TYLONGDOUBLE:
		useReg(b->right->reg);
		emit("\tfldt\t%s", regString(b->right->reg));
		useReg(b->left->reg);
		emit("\tfldt\t%s", regString(b->left->reg));
		emit("\tfucomip\t%%st(1), %%st");
		break;

	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		/* we may need to convert the left operand register to a
		 * temporary register (think about comparing two constants,
		 * or memory registers).  This is a slightly different case
		 * from the getResultReg() case because we aren't clobbering
		 * anything. It is ok for this register to be a non-temporary,
		 * it just can't be memory or a constant (incase the left
		 * side is). */
		reg = getRegister(b->left->reg);

		useReg(b->right->reg);
		useReg(reg);
		emit("\tcmp%c\t%s, %s", determineIntelSuffix(b->left->type),
		     regString(b->right->reg), regString(reg));
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeCompare()");
	}

}

/**
 * \brief Emits a set instruction with the destination being a newly
 *        allocated temporary register.  This new register is returned
 *        via the reg argument.
 */
void emitSet(const char *flagSuffix, REG *reg)
{
	*reg = allocNextReg(TYSIGNEDINT);
	emit("\tset%s\t%s", flagSuffix, regStringAs(TYUNSIGNEDCHAR, *reg));
	defReg(*reg);
	useReg(*reg);
	emit("\tmovzbl\t%s, %s", regStringAs(TYUNSIGNEDCHAR, *reg),
	     regString(*reg));
	defReg(*reg);
}

/* This function does the encoding for the binary operator '<'.
 */
void encodeLt(BinopTree b)
{
	comment("less %s < %s", regString(b->left->reg),
	        regString(b->right->reg));

	encodeCompare(b);
	if (typeQuery(b->left->type) == TYDOUBLE
	    || typeQuery(b->left->type) == TYFLOAT
	    || typeQuery(b->left->type) == TYLONGDOUBLE)
		emitSet("b", &b->expn.reg);
	else
		emitSet("l", &b->expn.reg);
}

/* This function does the encoding for the binary operator '>'.
 */
void encodeGt(BinopTree b)
{
	comment("greater %s > %s", regString(b->left->reg),
	        regString(b->right->reg));

	encodeCompare(b);
	if (typeQuery(b->left->type) == TYDOUBLE
	    || typeQuery(b->left->type) == TYFLOAT
	    || typeQuery(b->left->type) == TYLONGDOUBLE)
		emitSet("a", &b->expn.reg);
	else
		emitSet("g", &b->expn.reg);
}

/* This function does the encoding for the binary operator '<='.
 */
void encodeLte(BinopTree b)
{
	comment("less or equal %s <= %s", regString(b->left->reg),
	        regString(b->right->reg));

	encodeCompare(b);
	if (typeQuery(b->left->type) == TYDOUBLE
	    || typeQuery(b->left->type) == TYFLOAT
	    || typeQuery(b->left->type) == TYLONGDOUBLE)
		emitSet("be", &b->expn.reg);
	else
		emitSet("le", &b->expn.reg);
}

/* This function does the encoding for the binary operator '>='.
 */
void encodeGte(BinopTree b)
{
	comment("greater or equal %s >= %s", regString(b->left->reg),
	        regString(b->right->reg));

	encodeCompare(b);
	if (typeQuery(b->left->type) == TYDOUBLE
	    || typeQuery(b->left->type) == TYFLOAT
	    || typeQuery(b->left->type) == TYLONGDOUBLE)
		emitSet("ae", &b->expn.reg);
	else
		emitSet("ge", &b->expn.reg);
}

/* This function does the encoding for the binary operator '=='.
 */
void encodeEql(BinopTree b)
{
	REG tmp;

	comment("equal %s == %s", regString(b->left->reg),
	        regString(b->right->reg));

	encodeCompare(b);

	if (typeQuery(b->left->type) == TYFLOAT
	    || typeQuery(b->left->type) == TYDOUBLE
	    || typeQuery(b->left->type) == TYLONGDOUBLE) {
		b->expn.reg = allocNextReg(TYSIGNEDINT);
		tmp = allocNextReg(TYSIGNEDCHAR);
		emit("\tsete\t%s", regStringAs(TYSIGNEDCHAR, b->expn.reg));
		defReg(b->expn.reg);
		emit("\tsetnp\t%s", regString(tmp));
		defReg(tmp);
		useReg(b->expn.reg);
		useReg(tmp);
		emit("\tandb\t%s, %s",
		     regStringAs(TYSIGNEDCHAR, b->expn.reg),
		     regStringAs(TYSIGNEDCHAR, tmp));
		defReg(tmp);
		/* movzbl because b->expn.type *must* be signed int */
		useReg(tmp);
		emit("\tmovzbl\t%s, %s", regStringAs(TYSIGNEDCHAR, tmp),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
	} else {
		emitSet("e", &b->expn.reg);
	}

}

/* This function does the encoding for the binary operator '!='.
 */
void encodeNeql(BinopTree b)
{
	REG tmp;

	comment("not equal %s != %s", regString(b->left->reg),
	        regString(b->right->reg));

	encodeCompare(b);

	if (typeQuery(b->left->type) == TYFLOAT
	    || typeQuery(b->left->type) == TYDOUBLE
	    || typeQuery(b->left->type) == TYLONGDOUBLE) {
		b->expn.reg = allocNextReg(TYSIGNEDINT);
		tmp = allocNextReg(TYSIGNEDCHAR);
		emit("\tsetne\t%s", regStringAs(TYSIGNEDCHAR, b->expn.reg));
		defReg(b->expn.reg);
		emit("\tsetp\t%s", regString(tmp));
		defReg(tmp);
		useReg(b->expn.reg);
		useReg(tmp);
		emit("\torb\t%s, %s",
		     regStringAs(TYSIGNEDCHAR, b->expn.reg),
		     regStringAs(TYSIGNEDCHAR, tmp));
		defReg(tmp);
		/* movzbl because b->expn.type *must* be signed int */
		useReg(tmp);
		emit("\tmovzbl\t%s, %s", regStringAs(TYSIGNEDCHAR, tmp),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
	} else {
		emitSet("ne", &b->expn.reg);
	}

}

/* This function does the encoding for the binary operator '&&'.
 */
void encodeLand(BinopTree b)
{
	TypeTag myType;
	REG rreg, lreg;
	FpConstTree fpconst;
	BinopTree compare;
	int lab1, lab2;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeLand\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "NULL child pointer passed to encodeLand\n");

	comment("logical and");

	lab1 = genLabel();
	lab2 = genLabel();

	myType = typeQuery(((ExpTree) b->left)->type);
	if (typeQuery(((ExpTree) b->left)->type) == TYERROR ||
	    typeQuery(((ExpTree) b->right)->type) == TYERROR) {
		return;
	}

	b->expn.reg = allocNextReg(typeQuery(b->expn.type));

	encode((Tree) b->left);

	/* encodes the left operand first */
	switch (myType) {
	case TYLONGDOUBLE:
	case TYFLOAT:
	case TYDOUBLE:
		comment1("logical and: left side compare against zero");
		fpconst = (FpConstTree) newFpConst("0.0", b->left->type);
		compare = (BinopTree) newBinop(EQL_OP, b->left->type,
		                               b->left, (ExpTree) fpconst);
		encode((Tree) compare);
		/* result type of compare is signed int */
		useReg(compare->expn.reg);
		emit("\tcmpl\t$1, %s", regString(compare->expn.reg));
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		comment1("logical and: left side compare against zero");
		lreg = getRegister(b->left->reg);
		useReg(lreg);
		emit("\tcmp%c\t$0, %s", determineIntelSuffix(b->left->type),
		     regString(lreg));
		break;

	default:
		bugT((Tree) b,
		     "Invalid left subtree type in encodeLand()");
	}

	emitCondBranch("\tje\t", "L$%d", lab1);
	myType = typeQuery(((ExpTree) b->right)->type);
	encode((Tree) b->right);

	/* encodes the right operand */
	switch (myType) {
	case TYLONGDOUBLE:
	case TYFLOAT:
	case TYDOUBLE:
		comment1("logical and: left side compare against zero");
		fpconst = (FpConstTree) newFpConst("0.0", b->left->type);
		compare = (BinopTree) newBinop(EQL_OP, b->left->type,
		                               b->left, (ExpTree) fpconst);
		encode((Tree) compare);
		/* result type of compare is signed int */
		useReg(compare->expn.reg);
		emit("\tcmpl\t$1, %s", regString(compare->expn.reg));
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		comment1("logical and: right side compare against zero");
		rreg = getRegister(b->right->reg);
		useReg(rreg);
		emit("\tcmp%c\t$0, %s", determineIntelSuffix(b->right->type),
		     regString(rreg));
		break;

	default:
		bugT((Tree) b,
		     "Invalid right subtree type in encodeLand()");
	}

	emitCondBranch("\tje\t", "L$%d", lab1);
	emit("\tmov%c\t$1, %s", determineIntelSuffix(b->expn.type),
	     regString(b->expn.reg));
	defReg(b->expn.reg);
	emitUncondBranch("\tjmp\t", "L$%d", lab2);
	emitLabel("L$%d", lab1);
	emit("\tmov%c\t$0, %s", determineIntelSuffix(b->expn.type),
	     regString(b->expn.reg));
	defReg(b->expn.reg);
	emitLabel("L$%d", lab2);
}

/* This function does the encoding for the binary '||'.
 */
void encodeLor(BinopTree b)
{
	TypeTag myType;
	REG rreg, lreg;
	FpConstTree fpconst;
	BinopTree compare;
	int lab1, lab2;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeLor\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "NULL child pointer passed to encodeLor\n");

	comment("logical or");

	lab1 = genLabel();
	lab2 = genLabel();

	myType = typeQuery(((ExpTree) b->left)->type);
	if (typeQuery(((ExpTree) b->left)->type) == TYERROR ||
	    typeQuery(((ExpTree) b->right)->type) == TYERROR)
		return;

	b->expn.reg = allocNextReg(typeQuery(b->expn.type));

	encode((Tree) b->left);

	switch (myType) {
	case TYLONGDOUBLE:
	case TYFLOAT:
	case TYDOUBLE:
		fpconst = (FpConstTree) newFpConst("0.0", b->left->type);
		compare = (BinopTree) newBinop(EQL_OP, b->left->type,
		                               b->left, (ExpTree) fpconst);
		encode((Tree) compare);
		/* result type of compare is signed int */
		useReg(compare->expn.reg);
		emit("\tcmpl\t$1, %s", regString(compare->expn.reg));
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		comment1("logical or: left side compare against zero");
		lreg = getRegister(b->left->reg);
		useReg(lreg);
		emit("\tcmp%c\t$0, %s", determineIntelSuffix(b->left->type),
		     regString(lreg));
		break;

	default:
		bugT((Tree) b,
		     "Invalid left subtree type in encodeLor()");
	}

	emitCondBranch("\tjne\t", "L$%d", lab1);
	myType = typeQuery(((ExpTree) b->right)->type);
	encode((Tree) b->right);

	switch (myType) {
	case TYLONGDOUBLE:
	case TYFLOAT:
	case TYDOUBLE:
		comment1("logical or: right side compare against zero");
		fpconst = (FpConstTree) newFpConst("0.0", b->right->type);
		compare = (BinopTree) newBinop(EQL_OP, b->right->type,
		                               b->right, (ExpTree) fpconst);
		encode((Tree) compare);
		/* result type of compare is signed int */
		useReg(compare->expn.reg);
		emit("\tcmpl\t$1, %s", regString(compare->expn.reg));
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYUNSIGNEDLONGINT:
	case TYSIGNEDLONGINT:
	case TYPOINTER:
		comment1("logical or: right side compare against zero");
		rreg = getRegister(b->right->reg);
		useReg(rreg);
		emit("\tcmp%c\t$0, %s",
		     determineIntelSuffix(b->right->type),
		     regString(rreg));
		break;

	default:
		bugT((Tree) b,
		     "Invalid right subtree type in encodeLor()");
	}

	emitCondBranch("\tjne\t", "L$%d", lab1);
	emit("\tmov%c\t$0, %s", determineIntelSuffix(b->expn.type),
	     regString(b->expn.reg));
	defReg(b->expn.reg);
	emitUncondBranch("\tjmp\t", "L$%d", lab2);
	emitLabel("L$%d", lab1);
	emit("\tmov%c\t$1, %s", determineIntelSuffix(b->expn.type),
	     regString(b->expn.reg));
	defReg(b->expn.reg);
	emitLabel("L$%d", lab2);
}

/*  90% level  */

/* This function does encoding for the unary operator '~'.
 */
void encodeComplmt(UnopTree u)
{
	TypeTag myType;

	if (!u)
		bugT((Tree) u,
		     "NULL pointer passed to encodeComplmt\n");
	if (!u->left)
		bugT((Tree) u,
		     "NULL child pointer passed to encodeComplmt\n");

	myType = typeQuery(((ExpTree) u->left)->type);
	if (myType == TYERROR)
		return;

	comment("compliment ~%s", regString(u->left->reg));

	u->expn.reg = getResultReg(u->left->reg);

	switch (myType) {
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		useReg(u->expn.reg);
		emit("\tnot%c\t%s", determineIntelSuffix(u->expn.type),
		     regString(u->expn.reg));
		defReg(u->expn.reg);
		break;

	default:
		bugT((Tree) u, "Invalid tree type in encodeComplmt()");
	}
	return;
}

/* This function does encoding for the binary operator '<<'.
 */
void encodeLshift(BinopTree b)
{
	TypeTag myType;

	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeLshift\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "NULL child pointer passed to encodeLshift\n");

	myType = typeQuery(((ExpTree) b->left)->type);
	if (myType == TYERROR)
		return;

	comment("left shift %s << %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->left->reg);

	switch (myType) {
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		useReg(b->right->reg);
		emit("\tmovb\t%s, %%cl", regString(b->right->reg));
		emit("\tsal%c\t%%cl, %s", determineIntelSuffix(b->expn.type),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "invalid tree type in encodeLshift()");
	}
	return;
}

/* This function does encoding for the binary operator '>>'.
 */
void encodeRshift(BinopTree b)
{
	TypeTag myType;

	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeRshift\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "NULL child pointer passed to encodeRshift\n");

	myType = typeQuery(((ExpTree) b->left)->type);
	if (myType == TYERROR)
		return;

	comment("right shift %s >> %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->left->reg);

	switch (myType) {
	case TYUNSIGNEDCHAR:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDLONGINT:
		useReg(b->right->reg);
		emit("\tmovb\t%s, %%cl", regString(b->right->reg));
		emit("\tshr%c\t%%cl, %s", determineIntelSuffix(b->expn.type),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	case TYSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDLONGINT:
		useReg(b->right->reg);
		emit("\tmovb\t%s, %%cl", regString(b->right->reg));
		emit("\tsar%c\t%%cl, %s", determineIntelSuffix(b->expn.type),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeRshift()");
	}
	return;
}

/* This function does encoding for the binary operator '&'.
 */
void encodeAndd(BinopTree b)
{
	TypeTag myType;

	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeAndd\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "NULL child pointer passed to encodeAndd\n");

	myType = typeQuery(((ExpTree) b->right)->type);
	if (myType == TYERROR)
		return;

	comment("binary and %s & %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->right->reg);

	switch (myType) {
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		useReg(b->left->reg);
		useReg(b->expn.reg);
		emit("\tand%c\t%s, %s", determineIntelSuffix(b->right->type),
		     regString(b->left->reg), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeAndd()");
	}
	return;
}

/* This function does encoding for the binary operator '^'.
 */
void encodeXorr(BinopTree b)
{
	TypeTag myType;

	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeXorr\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "NULL child pointer passed to encodeXorr\n");

	myType = typeQuery(((ExpTree) b->right)->type);
	if (myType == TYERROR)
		return;

	comment("xor %s ^ %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->right->reg);

	switch (myType) {
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		useReg(b->left->reg);
		useReg(b->expn.reg);
		emit("\txor%c\t%s, %s", determineIntelSuffix(b->left->type),
		     regString(b->left->reg), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeXorr()\n");
	}
	return;
}

/* This function does encoding for the binary operator '|'.
 */
void encodeOrr(BinopTree b)
{
	TypeTag myType;

	if (!b)
		bugT((Tree) b, "NULL pointer passed to encodeOrr\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "NULL child pointer passed to encodeOrr\n");

	myType = typeQuery(((ExpTree) b->right)->type);
	if (myType == TYERROR)
		return;

	comment("or %s | %s", regString(b->left->reg),
	        regString(b->right->reg));

	b->expn.reg = getResultReg(b->right->reg);

	switch (myType) {
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		useReg(b->left->reg);
		useReg(b->expn.reg);
		emit("\tor%c\t%s, %s", determineIntelSuffix(b->left->type),
		     regString(b->left->reg), regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bugT((Tree) b, "Invalid tree type in encodeOrr()\n");
	}
	return;
}

/* Dummy function to encode leftAssign. It is never called.
 */
void encodeLeftAssign(BinopTree b)
{
	b = NULL;		/* defeat warning */
	info("encodeleftAssign: You are not supposed to get here!");
	return;
}

/* Dummy function to encode righttAssign. It is never called.
 */
void encodeRightAssign(BinopTree b)
{
	b = NULL;		/* defeat warning */
	info("encodeRightAssign: You are not supposed to get here!");
	return;
}

/* Dummy function to encode AndAssign. It is never called.
 */
void encodeAndAssign(BinopTree b)
{
	b = NULL;		/* defeat warning */
	info("encodeAndAssign: You are not supposed to get here!");
	return;
}

/* Dummy function to encode OrAssign. It is never called.
 */
void encodeOrAssign(BinopTree b)
{
	b = NULL;		/* defeat warning */
	info("encodeOrAssign: You are not supposed to get here!");
	return;
}

/* Dummy function to encode XorAssign. It is never called.
 */
void encodeXorAssign(BinopTree b)
{
	b = NULL;		/* defeat warning */
	info("encodeXorAssign: You are not supposed to get here!");
	return;
}

/*  100% level  */

/* This function does the encoding for the postfix '++' operator.
 */
void encodeIncra(UnopTree u)
{
	FpConstTree fpconst;
	BinopTree add;
	BinopTree assign;
	TypeQualifier qual;
	int size;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null pointer passed to encodeIncra\n");
	if (!(u->left))
		bugT((Tree) u,
		     "Null child pointer passed to encodeIncra\n");

	comment("increment after %s++", regString(u->left->reg));

	u->expn.reg = allocNextReg(typeQuery(u->expn.type));
	comment1("get the result value");
	useReg(u->left->reg);
	move(u->left->reg, u->expn.reg);
	defReg(u->expn.reg);

	switch (typeQuery(u->expn.type)) {
	case TYLONGDOUBLE:
	case TYFLOAT:
	case TYDOUBLE:
		/* build a floating point constant 1.0 and get it in a
 		 * register if need be. */
		comment1("add floating point one");
		fpconst = (FpConstTree) newFpConst("1.0", u->left->type);
		add = (BinopTree) newBinop(ADD_OP, u->left->type, u->left,
		                           (ExpTree) fpconst);
		assign = (BinopTree) newBinop(ASSIGN_OP, u->left->type,
		                              u->left, (ExpTree) add);
		encode((Tree) assign);
		break;

	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		comment1("increment");
		useReg(u->left->reg);
		emit("\tinc%c\t%s", determineIntelSuffix(u->left->type),
		     regString(u->left->reg));
		defReg(u->left->reg);
		break;

	case TYPOINTER:
		size = computeSize(typeQueryPointer(u->expn.type, &qual));
		comment1("add size=%d", size);
		useReg(u->left->reg);
		emit("\taddq\t$%d, %s", size,
		     regStringAs(TYUNSIGNEDLONGINT, u->left->reg));
		defReg(u->left->reg);
		break;

	default:
		bugT((Tree) u, "invalid tree type in encodeIncra()\n");
	}
}

/* Dummy function for encoding prefix '++' operator.
 * It never gets called.
 */
void encodeIncrb(UnopTree u)
{
	u = NULL;		/* defeat warning */
	info("encodeIncrb: You are not supposed to get here!\n");
	return;
}

/* This function does the encoding for the postfix '--' operator.
 */
void encodeDecra(UnopTree u)
{
	FpConstTree fpconst;
	BinopTree sub;
	BinopTree assign;
	TypeQualifier qual;
	int size;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null pointer passed to encodeIncra\n");
	if (!(u->left))
		bugT((Tree) u,
		     "Null child pointer passed to encodeIncra\n");

	comment("decrement after %s++", regString(u->left->reg));

	u->expn.reg = allocNextReg(typeQuery(u->expn.type));
	comment1("get the result value");
	useReg(u->left->reg);
	move(u->left->reg, u->expn.reg);
	defReg(u->expn.reg);

	switch (typeQuery(u->expn.type)) {
	case TYLONGDOUBLE:
	case TYFLOAT:
	case TYDOUBLE:
		/* build a floating point constant 1.0 and get it in a
 		 * register if need be. */
		comment1("subtract floating point one");
		fpconst = (FpConstTree) newFpConst("1.0", u->left->type);
		sub = (BinopTree) newBinop(SUB_OP, u->left->type, u->left,
		                           (ExpTree) fpconst);
		assign = (BinopTree) newBinop(ASSIGN_OP, u->left->type,
		                              u->left, (ExpTree) sub);
		encode((Tree) assign);
		break;

	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		comment1("decrement");
		useReg(u->left->reg);
		emit("\tdec%c\t%s", determineIntelSuffix(u->left->type),
		     regString(u->left->reg));
		defReg(u->left->reg);
		break;

	case TYPOINTER:
		size = computeSize(typeQueryPointer(u->expn.type, &qual));
		comment1("subtract size=%d", size);
		useReg(u->left->reg);
		emit("\tsubq\t$%d, %s", size, regString(u->left->reg));
		defReg(u->left->reg);
		break;

	default:
		bugT((Tree) u, "invalid tree type in encodeIncra()\n");
	}
}

/* Dummy function for encoding the prefix '--' operator.
 */
void encodeDecrb(UnopTree u)
{
	u = NULL;		/* defeat warning */
	info("encodeDerb: You are not supposed to get here!\n");
	return;
}

/* Dummy function for encoding the cast operator.
 */
void encodeCast(CastopTree t)
{
	t = NULL;		/* defeat warning */
	info("encodeCast: You are not supposed to get here!\n");
	return;
}

/*  This function encodes the comma operator.
 */
void encodeComma(BinopTree b)
{
	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null pointer passed to encodeComma\n");
	if (!b->left || !b->right)
		bugT((Tree) b,
		     "Null child pointer passed to encodeComma\n");

	/* evaluates the left operand first */
	encode((Tree) (b->left));

	/* pop the stack in order to have the left operand evaluated as 
	   void */
	freeExpressionStatementRegister((Tree) b->left);

	/* evaluates the right operand, leaving the returned value on
	   the stack */
	encode((Tree) (b->right));

	/* pass the right hand side result up */
	b->expn.reg = b->right->reg;
}

/* This function encodes the conditional operator.
 */
void encodeCond(TriopTree t)
{
	TypeTag myType;
	REG lreg;
	int lab1, lab2;

	lab1 = genLabel();
	lab2 = genLabel();

	/* error checkings */
	if (!t)
		bugT((Tree) t, "Null pointer passed to encodeCond\n");
	if (!(t->left && t->middle && t->right))
		bugT((Tree) t,
		     "Null child pointer passed to encodeCond\n");

	comment("triop");

	t->expn.reg = allocNextReg(typeQuery(t->expn.type));

	/* encode the first operand's exp. tree first, which has a Lnot 
	   operator */
	encode((Tree) t->left);
	lreg = getRegister(t->left->reg);

	myType = typeQuery(((ExpTree) t->left)->type);
	switch (myType) {
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		useReg(lreg);
		emit("\tcmp%c\t$0, %s", intelSuffix(myType), regString(lreg));
		emitCondBranch("\tjne\t", "L$%d", lab1);

	default:
		break;
	}
	/* encode the second operand's expression tree */
	encode((Tree) t->middle);
	useReg(t->middle->reg);
	move(t->middle->reg, t->expn.reg);
	defReg(t->expn.reg);
	emitUncondBranch("\tjmp\t", "L$%d", lab2);
	emitLabel("L$%d", lab1);
	/* encode the third operand's expression tree */
	encode((Tree) t->right);
	useReg(t->right->reg);
	move(t->right->reg, t->expn.reg);
	defReg(t->expn.reg);
	emitLabel("L$%d", lab2);
}

/*  phase 5   */

/**
 * Encodes the AddrOf '&' operator. Relies on recursive calls from skeleton
 * to other methods to to encode the guts of it, so it actually does nothing.
**/
void encodeAddrOf(UnopTree u)
{
	comment("address of %s", regString(u->left->reg));

	if (isMemory(u->left->reg)) {
		u->expn.reg = allocNextReg(TYPOINTER);
		useReg(u->left->reg);
		emit("\tleaq\t%s, %s",
		     regString(u->left->reg),
		     regString(u->expn.reg));
		defReg(u->expn.reg);
	} else {
		/* if we have a register here, we can only assume (hope)
 		 * that it contains the address that we are looking for. */
		u->expn.reg = allocNextReg(TYPOINTER);
		useReg(u->left->reg);
		emit("\tmovq\t%s, %s",
		     regStringAs(TYPOINTER, u->left->reg),
		     regStringAs(TYPOINTER, u->expn.reg));
		defReg(u->expn.reg);
	}
}

/**
 * Encodes the Index '[]' operator. Should never be called.
**/

void encodeIndex(BinopTree b)
{
	bugT((Tree) b, "encodeIndex should never be called");
}

/**
 * Encodes the '.' operator.
**/

void encodeFieldRef(FieldRefTree f)
{
	int offset;
	offset = computeMemberOffset(f->left->type, f->id);

	comment("field ref %s + %d", regString(f->left->reg), offset);
	if (offset == 0) {
		comment1("zero offset, do nothing");
		f->expn.reg = f->left->reg;
		return;
	}

	f->expn.reg = getResultReg(f->left->reg);

	useReg(f->expn.reg);
	emit("\taddq\t$%d, %s", offset,
	     regStringAs(TYPOINTER, f->expn.reg));
	defReg(f->expn.reg);
}

/**
 * Encodes the '->' operator. Should never be called.
**/

void encodePtr(PtrTree p)
{
	bugT((Tree) p, "encodePtr should never be called");
}
