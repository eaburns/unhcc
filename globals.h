/*
 *  globals.h: compiler globals, used for processing
 *             function definitions.
 *
 */
#ifndef GLOBALS_H
#define GLOBALS_H

#include "defs.h"
#include "types.h"

/* globals used in compiling functions (defined in main.c) */
extern ST_ID gCurrentFunctionName;
extern int gCurrentFunctionOffset;
extern Type gCurrentFunctionReturnType;
extern StorageClass gCurrentFunctionClass;

/* global used in processing typedefs (defined in decl.c) */
extern GP_SWITCH g_id_lookup;

#endif
