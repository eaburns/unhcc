/*****************************************************************************/
/*                                                                           */
/*          U N I V E R S I T Y    O F    N E W    H A M P S H I R E         */
/*                                                                           */
/*                       COMPUTER  SCIENCE  DEPARTMENT                       */
/*                                                                           */
/*                            CS 812 -- Compilers                            */
/*                                                                           */
/*          --------------------------------------------------------         */
/*                                                                           */
/*  Professor:   Phil Hatcher <pjh@cs.unh.edu>                               */
/*  Programmer:  Andy Foulks <rafoulks@cs.unh.edu>                           */
/*  File:        analyzeStmt.c                                               */
/*  Due:         4-13-03                                                     */
/*                                                                           */
/*****************************************************************************/

#include "regAlloc.h"
#include "analyze.h"
#include "message.h"
#include "encode.h"
#include "semUtils.h"
#include "constExpr.h"

extern int printf(char *, ...);

/* ===============================  GLOBALS  =============================== */
/* 
 *  These globals and functions are all defined in encodeStmt.c.
 *  We use these functions here in this file to build up a list of labels
 *  at "analyze time", so that at "encode time" we don't have to
 *  worry about forward referencing problems (i.e. goto a label that
 *  is not yet defined.)
 */

struct JumpLabelList;

extern int NOT_FOUND_LABEL;

extern struct JumpLabelList *gGotoList;

extern int addJumpLabel(struct JumpLabelList *l, char *jmp, int as);
extern int findAssmLabel(struct JumpLabelList *l, char *label);
extern void clearJumpList(struct JumpLabelList *labelList);
extern struct JumpLabelList *newJumpLabelList(void);

/* ========================================================================= */
/*
 *  These two routines allow any necessary initialization and cleanup to be
 *  performed for the functions in this file. beforeAnalyze is called before
 *  analyze is called for each function body being compiled. afterAnalyze is
 *  called after analyze returns. (These calls are in parse.y.)
 */
void beforeAnalyze(void)
{
	NOT_FOUND_LABEL = -1;

	/* the gotolist has all of the label definitions in it.  we
	   need to clear it before the start of an analyze , but it
	   needs to stay "alive" during the encode phase, so don't
	   clear the goto list in the function afterAnalyze() */

	free(gGotoList);
	gGotoList = newJumpLabelList();
	clearJumpList(gGotoList);
}

/* ========================================================================= */

void afterAnalyze(void)
{
	/* whatever you do, dont call free on gGotoList here */
}

/* ==============================  75% LEVEL  ============================== */
/**
 *  Statements come in trees.  The while tree looks like
 *
 *           while
 *           /   \
 *          /     \
 *        exp     stmt (body of loop, conceptually a 'single statement')
 *        ^
 *        |
 *        +-- control expression
 *
 *  The expression must be a scalar type.  To deal with both ints and
 *  floats easily, we will modify the tree to look like
 *
 *           while
 *           /   \
 *          /     \
 *        !=      stmt 
 *        / \
 *       /   \
 *     exp    0
 */
Tree analyzeWhile(WhileTree w)
{
	w->expr = (ExpTree) analyze((Tree) w->expr);

	w->statement = analyze(w->statement);

	if (w->expr)
		promoteFunctionOrArrayTree(w->expr);

	if (isScalarType(w->expr->type)) {
		IntConstTree zero;
		BinopTree notEquals;

		zero = (IntConstTree) newIntConst(0);

		notEquals =
		    (BinopTree) newBinop(NEQL_OP, NULL, w->expr,
					 (ExpTree) zero);

		notEquals = (BinopTree) analyzeNeql(notEquals);

		w->expr = (ExpTree) notEquals;
	} else {
		w->expr->type = typeBuildBasic(TYERROR, NO_QUAL);
		errorT((Tree) w,
		       "while expression must be scalar type");
	}

	return (Tree) w;
}

/* ========================================================================= */
/**
 *  Same trick as 'while' - change the expression subtree to handle
 *  both ints and floats by doing 'exp != 0' instead of just 'exp'.
 *  Controlling expression must be scalar (int, float, ptr) type.
 */
Tree analyzeIfElse(IfElseTree w)
{
	w->expr = (ExpTree) analyze((Tree) w->expr);

	if (w->expr)
		promoteFunctionOrArrayTree(w->expr);

	w->ifstmt = analyze(w->ifstmt);
	w->elsestmt = analyze(w->elsestmt);

	if (isScalarType(w->expr->type)) {
		IntConstTree zero;
		BinopTree notEquals;

		zero = (IntConstTree) newIntConst(0);

		notEquals =
		    (BinopTree) newBinop(NEQL_OP, NULL, w->expr,
					 (ExpTree) zero);

		notEquals = (BinopTree) analyzeNeql(notEquals);

		w->expr = (ExpTree) notEquals;
	} else {
		w->expr->type = typeBuildBasic(TYERROR, NO_QUAL);
		errorT((Tree) w, "if expression must be scalar type");
	}

	return (Tree) w;
}

/* ========================================================================= */
/**
 *  A break must be inside a loop or a switch.  This 
 *  constraint is detected in the encodeBreak() function 
 */
Tree analyzeBreak(BreakTree br)
{
	return (Tree) br;
}

/* ========================================================================= */
/**
 *  A continue must be inside a loop.  this constraint is 
 *  detected in the encodeContinue() function.
 */
Tree analyzeContinue(ContinueTree co)
{
	return (Tree) co;
}

/* ==============================  85% LEVEL  ============================== */
/**
 *  Use the same trick as while to handle ints and floats easily.
 *  Controlling expression must be scalar type.
 */
Tree analyzeDoWhile(DoWhileTree w)
{
	w->expr = (ExpTree) analyze((Tree) w->expr);

	if (w->expr)
		promoteFunctionOrArrayTree(w->expr);

	w->statement = analyze(w->statement);

	if (isScalarType(w->expr->type)) {
		IntConstTree zero;
		BinopTree notEquals;

		zero = (IntConstTree) newIntConst(0);

		notEquals =
		    (BinopTree) newBinop(NEQL_OP, NULL, w->expr,
					 (ExpTree) zero);

		notEquals = (BinopTree) analyzeNeql(notEquals);

		w->expr = (ExpTree) notEquals;
	} else {
		w->expr->type = typeBuildBasic(TYERROR, NO_QUAL);
		errorT((Tree) w,
		       "do-while expression must be scalar type");
	}

	return (Tree) w;
}

/* ========================================================================= */
/**
 *  Uses the same trick as while to handle ints and floats easily.
 *  Controlling expression must be scalar type.
 *  Since the expressions may be optional, the first or third
 *  expressions may be null.  If the second expression is not there,
 *  we put in a 1 (true) value for the controlling expression. 
 */
Tree analyzeFor(ForTree w)
{
	if (w->expr2 == NULL) {
		IntConstTree one;

		one = (IntConstTree) newIntConst(1);

		w->expr2 = (ExpTree) one;
	}

	w->expr2 = (ExpTree) analyze((Tree) w->expr2);
	w->expr1 = (ExpTree) analyze((Tree) w->expr1);
	w->expr3 = (ExpTree) analyze((Tree) w->expr3);

	if (w->expr1)
		promoteFunctionOrArrayTree(w->expr1);
	if (w->expr2)
		promoteFunctionOrArrayTree(w->expr2);
	if (w->expr3)
		promoteFunctionOrArrayTree(w->expr3);

	w->statement = analyze(w->statement);

	if (isScalarType(w->expr2->type)) {
		BinopTree notEquals;
		IntConstTree zero;

		zero = (IntConstTree) newIntConst(0);

		notEquals =
		    (BinopTree) newBinop(NEQL_OP, NULL, w->expr2,
					 (ExpTree) zero);

		notEquals = (BinopTree) analyzeNeql(notEquals);

		w->expr2 = (ExpTree) notEquals;
	} else {
		w->expr2->type = typeBuildBasic(TYERROR, NO_QUAL);
		errorT((Tree) w,
		       "for loop middle expression must be scalar type");
	}

	return (Tree) w;
}

/* ==============================  100% LEVEL  ============================= */
/**
 *  Here we build up a list of labels and their assembly label pairs.
 *  That way, when we get to the encode stage, all of the labels will
 *  have been accounted for.  We can detect duplicate labels here,
 *  but to check for the 'missing label' error we do the
 *  checking in encodeGoto() -- if the label is not in the table then
 *  we have an error because we need to make sure the label
 *  is somewhere inside a function body.
 */
Tree analyzeLabel(LabelTree l)
{
	int assmLabel;

	assmLabel = findAssmLabel(gGotoList, st_get_id_str(l->label));

	if (assmLabel == NOT_FOUND_LABEL) {
		assmLabel = genLabel();

		addJumpLabel(gGotoList, st_get_id_str(l->label),
			     assmLabel);
	} else {
		errorT((Tree) l, "duplicate label '%s' defined",
		       st_get_id_str(l->label));
	}

	l->left = analyze(l->left);

	return (Tree) l;
}

/* ========================================================================= */
/**
 *  Need to make sure the case value is an integral constant expression. 
 *  Also need to make sure its inside a switch, but we do that check
 *  in encodeCase.
 */
Tree analyzeCase(CaseTree ca)
{
	ca->caseexpr = (ExpTree) analyze((Tree) ca->caseexpr);

	ca->statement = analyze(ca->statement);

	if (!isIntegralType(ca->caseexpr->type)) {
		ca->caseexpr->type = typeBuildBasic(TYERROR, NO_QUAL);
		errorT((Tree) ca,
		       "case label must be integral constant expression");
	}

	return (Tree) ca;
}

/* ========================================================================= */
/**
 *  do the checking in encodeDefault() - need to make sure the
 *  default is somewhere inside a switch.
 */
Tree analyzeDefault(DefaultTree d)
{
	d->statement = analyze(d->statement);

	return (Tree) d;
}

/* ========================================================================= */
/**
 *  The controlling expression must be integral (int) type.
 */
Tree analyzeSwitch(SwitchTree sw)
{
	sw->expr = (ExpTree) analyze((Tree) sw->expr);

	sw->statement = analyze(sw->statement);

	sw->expr = integralPromotion(sw->expr);

	if (!isIntegralType(sw->expr->type)) {
		sw->expr->type = typeBuildBasic(TYERROR, NO_QUAL);
		errorT((Tree) sw,
		       "switch expression must be integral type");
	}

	return (Tree) sw;
}

/* ========================================================================= */
/**
 *  Do the checking in eencodeGoto().  All the labels are being
 *  stored in gGotoList in analyzeLabel(), so theres not much 
 *  to do here.
 */
Tree analyzeGoto(GotoTree g)
{
	return (Tree) g;
}

/* ========================================================================= */
