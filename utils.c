
/****************************************************************/
/*								*/
/*	CS712 - "C" Compiler					*/
/*								*/
/*	--utils.c--						*/
/*								*/
/*	This file contains utility routines for the CS712	*/
/*	C compiler.						*/
/*								*/
/*								*/
/*								*/
/****************************************************************/

#include "defs.h"
#include "types.h"
#include "bucket.h"
#include "symtab.h"
#include "message.h"

/****************************************************************/
/*
	support for symbol table data records
*/

ST_DR stdr_alloc(void)
{
	return (ST_DR) malloc(sizeof(ST_DATA_REC));
}

void stdr_free(ST_DR stdr)
{
	free(stdr);
}

void stdr_print_tag(STDR_TAG tag)
{
	switch (tag) {
	case ECONST:
		msg("ECONST");
		break;
	case LDECL:
		msg("LDECL");
		break;
	case GDECL:
		msg("GDECL");
		break;
	case PDECL:
		msg("PDECL");
		break;
	case FDEF:
		msg("FDEF");
		break;
	case TAG:
		msg("TAG");
		break;
	case TDEF:
		msg("TDEF");	/* lsr, 05/08/01 */
		break;
	default:
		bug("illegal tag in \"print_stdr_tag\"");
	}
}

void stdr_dump(ST_DR stdr)
{

	if (stdr == NULL) {
		msg("<null>");
		return;
	}
	msgn("\t");
	stdr_print_tag(stdr->tag);
	switch (stdr->tag) {
	case ECONST:
		msgn("\t\ttype = ");
		printType(stdr->u.econst.type);
		msg("");
		msg("\t\tvalue = %d", stdr->u.econst.val);
		break;
	case LDECL:
	case GDECL:
	case PDECL:
	case FDEF:
		msgn("\t\ttype = ");
		printType(stdr->u.decl.type);
		msg("");
		msgn("\t\tstorage class = ");
		printClass(stdr->u.decl.sc);
		msg("");
		msg("\t\toffset = %d", stdr->u.decl.offset);
		msg("\t\tstaticLabel = %d", stdr->u.decl.staticLabel);
		msgn("\t\terror = ");
		printBoolean(stdr->u.decl.err);
		msg("");
		break;
	case TAG:
		msgn("\t\ttype = ");
		printType(stdr->u.stag.type);
		msg("");
		break;
	case TDEF:
		msgn("\t\ttype = ");	/* lsr, 05/08/01 */
		printType(stdr->u.tdef.type);
		msg("");
		break;
	}
}


void printBoolean(BOOLEAN b)
{
	if (b)
		msgn("TRUE");

	else
		msgn("FALSE");
}

void printClass(StorageClass tag)
{
	switch (tag) {
	case EXTERN_SC:
		msgn("extern");
		break;
	case STATIC_SC:
		msgn("static");
		break;
	case REGISTER_SC:
		msgn("register");
		break;
	case AUTO_SC:
		msgn("auto");
		break;
	case TYPEDEF_SC:
		msgn("typedef");
		break;
	case NO_SC:
		msgn("<no storage class>");
		break;
	default:
		bug("illegal storage class in printClass");
	}
}
