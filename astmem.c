/* Peter Frontiero, cs812, phase 1 assignment, due March 4
 *
 * astmem.c, defines memory structure used for abstract syntax tree storage
 * and functions to allocate and reset memory
 */

#include "tree.h"
#include "message.h"

#define SIZE 4000

typedef struct memblock {

	void *block;
	struct memblock *next;

} BLOCK;

typedef struct mempool {

	struct memblock *head;
	struct memblock *curr_block;
	int offset;

} POOL;

static struct mempool *astpool;

/*-------------------------- poolInit -------------------------
 * Use this to initialize the memory pool
 */

void poolInit(void)
{
	astpool = (struct mempool *) malloc(sizeof(struct mempool));
	astpool->head = NULL;
	astpool->offset = 0;

}

/*-------------------------- poolAlloc ------------------------
 * Use this to allocate a new abstract syntax tree node
 */

Tree poolAlloc(int size)
{
	Tree p;
	struct memblock *b;
	int oldoffset;

	if (astpool->head == NULL) {	/* first time must allocate new 
					   AST block */
		p = (Tree) malloc(SIZE);

		if (p == NULL) {
			fatal("Memory pool could not be expanded");

		}

		b = (struct memblock *) malloc(sizeof(struct memblock));

		b->block = p;
		astpool->head = b;
		astpool->curr_block = b;
		b->next = NULL;

	}

	while (size % 8 != 0) {	/* return address must be divisible by
				   8 */

		size++;

	}

	if (astpool->offset + size >= SIZE) {	/* AST block all used
						   up */
		if (astpool->curr_block->next == NULL) {
			p = (Tree) malloc(SIZE);

			if (p == NULL) {
				fatal
				    ("Memory pool could not be expanded");

			}

			b = (struct memblock *)
			    malloc(sizeof(struct memblock));

			b->block = p;
			astpool->curr_block->next = b;
			astpool->curr_block = b;
			astpool->offset = 0;
			b->next = NULL;

		} else {
			astpool->curr_block = astpool->curr_block->next;
			astpool->offset = 0;

		}

	}

	oldoffset = astpool->offset;

	astpool->offset = astpool->offset + size;	/* allocate new 
							   Tree node */

	return (Tree) ((char *) astpool->curr_block->block + oldoffset);

}

/*-------------------------- poolReset -----------------------
 * Use this to reset the memory pool
 */

void poolReset(void)
{
	astpool->curr_block = astpool->head;
	astpool->offset = 0;

}
