/*
 *
 * yacc/bison input for C parser
 *
 */

%{

#if 0
#define ST_DUMP
#endif
#if 0
#define PRINTTREE
#endif

#include <string.h>
#include <stdlib.h>

#include "defs.h"
#include "decl.h"
#include "types.h"
#include "bucket.h"
#include "symtab.h"
#include "message.h"
#include "tree.h"
#include "analyze.h"
#include "globals.h"
#include "decl2.h"
#include "constExpr.h"
#include "encode.h"

void yyerror(char *s);
int yylex(void);

extern GP_SWITCH g_id_lookup;   /*  lsr,  05/08/01 */

/* used to concatenate string literals */
static char* concat(char* first, char* second);
%}

%union {
  int             y_int;
  long            y_long;
  unsigned int    y_uint;
  unsigned long   y_ulong;
  double          y_double;
  float           y_float;
  long double     y_ldouble;
  char *          y_string;
  ST_ID           y_id;
  BUCKET_PTR      y_bucket_ptr; 
  Type            y_type; 
  MemberList      y_member_list;
  ParamList       y_param_list;
  PARAM_TYPE_LIST y_param_type_list;
  TYPE_SPECIFIER  y_type_specifier;
  MY_SPECIFIER    y_my_specifier;
  TypeQualifier   y_type_qualifier;
  StorageClass    y_storage_class;
  ST_DR           y_st_dr;
  DECLARATOR_LIST y_declarator_list; 
  DECL_TREE       y_decl_tree;
  Tree            y_tree;
  ExpTree         y_exptree;
  TreeOp          y_treeop;
  }

%token <y_id> IDENTIFIER
%token <y_int> INT_CONSTANT
%token <y_string> DOUBLE_CONSTANT
%token <y_long> LONG_CONSTANT
%token <y_string> FLOAT_CONSTANT
%token <y_uint> UNSIGNED_INT_CONSTANT
%token <y_ulong> UNSIGNED_LONG_CONSTANT
%token <y_string> LONG_DOUBLE_CONSTANT

%token <y_string> STRING_LITERAL

%token SIZEOF
%token PTR_OP INC_OP DEC_OP LEFT_OP RIGHT_OP LE_OP GE_OP EQ_OP NE_OP
%token AND_OP OR_OP MUL_ASSIGN DIV_ASSIGN MOD_ASSIGN ADD_ASSIGN
%token SUB_ASSIGN LEFT_ASSIGN RIGHT_ASSIGN AND_ASSIGN
%token XOR_ASSIGN OR_ASSIGN

%token <y_id> TYPE_NAME   /*  lsr,  05/08/01 */

%token TYPEDEF EXTERN STATIC AUTO REGISTER
%token CHAR SHORT INT LONG SIGNED UNSIGNED FLOAT DOUBLE CONST VOLATILE VOID
%token STRUCT UNION ENUM ELIPSIS

%token CASE DEFAULT IF ELSE SWITCH WHILE DO FOR GOTO CONTINUE BREAK RETURN

%token BAD

%type <y_exptree> primary_expr postfix_expr argument_expr_list
          unary_expr cast_expr multiplicative_expr
          additive_expr shift_expr relational_expr
          equality_expr and_expr exclusive_or_expr
          inclusive_or_expr logical_and_expr logical_or_expr
          expression_statement conditional_expr assignment_expr expr
          constant_expr

%type <y_tree> statement labeled_statement compound_statement
          compound_statement_tail
          statement_list selection_statement
          iteration_statement jump_statement

%type <y_treeop> unary_operator assignment_operator

%type <y_string> string_literal

%type <y_id> identifier 
%type <y_bucket_ptr> declaration_specifiers specifier_qualifier_list  
%type <y_type_specifier> storage_class_specifier struct_or_union
                         type_qualifier
%type <y_my_specifier> type_specifier struct_or_union_specifier

%type <y_my_specifier> enum_specifier  /*  lsr,  05/03/01 */

%type <y_member_list> struct_declaration struct_declaration_list
%type <y_declarator_list> init_declarator init_declarator_list
                          struct_declarator struct_declarator_list
%type <y_decl_tree> declarator direct_declarator abstract_declarator
                    direct_abstract_declarator direct_declarator_func_start
%type <y_decl_tree> pointer
%type <y_type_qualifier> type_qualifier_list

%type <y_param_list> parameter_list parameter_declaration

%type <y_param_type_list> parameter_type_list

%type <y_type> type_name

%start translation_unit 

%%

primary_expr
  : identifier
    {
      $$ = newVar( $1 );
    }
  | INT_CONSTANT
    {
      $$ = newIntConst( $1 );
    }
  | DOUBLE_CONSTANT
    {
      $$ = newFpConst( $1, typeBuildBasic( TYDOUBLE, NO_QUAL ) );
    }
  | string_literal
    {
      $$ = newStringLit( $1,
        typeBuildPointer(typeBuildBasic(TYSIGNEDCHAR, NO_QUAL), NO_QUAL) );
    }
  | LONG_CONSTANT
    {
      $$ = newLongConst( $1 );
    }
  | FLOAT_CONSTANT
    {
      $$ = newFpConst( $1, typeBuildBasic( TYFLOAT, NO_QUAL ) );
    }
  | UNSIGNED_INT_CONSTANT
    {
      $$ = newUIntConst( $1 );
    }
  | UNSIGNED_LONG_CONSTANT
    {
      $$ = newULongConst( $1 );
    }
  | LONG_DOUBLE_CONSTANT
    {
      $$ = newFpConst( $1, typeBuildBasic( TYLONGDOUBLE, NO_QUAL ) );
    }
  | '(' expr ')'
    {
      $$ = $2;
    }
  ;

postfix_expr
  : primary_expr
    {
      $$ = $1;
    }
  | postfix_expr '[' expr ']'
    {
      $$ = newBinop( INDEX_OP, NULL, $1, $3 );
    }
  | postfix_expr '(' ')'
    {
      $$ = newFcall( NULL, $1, NULL );
    }
  | postfix_expr '(' argument_expr_list ')'
    {
      $$ = newFcall( NULL, $1, $3 );
    }
  | postfix_expr '.' identifier
    {
      $$ = newFieldRef( $3, NULL, $1 );
    }
  | postfix_expr PTR_OP identifier
    {
      $$ = newPtr( $3, NULL, $1 );
    } 
  | postfix_expr INC_OP
    {
      $$ = newUnop( INCRA_OP, NULL, $1 );
    }
  | postfix_expr DEC_OP
    {
      $$ = newUnop( DECRA_OP, NULL, $1 );
    }
  ;

argument_expr_list
  : assignment_expr
    {
      $$ = $1;
    }
  | assignment_expr ',' argument_expr_list 
    {
      $$ = newEseq( NULL, $1, $3 );
    }
  ;

unary_expr
  : postfix_expr
    {
      $$ = $1;
    }
  | INC_OP unary_expr
    {
      $$ = newUnop( INCRB_OP, NULL, $2 );
    }
  | DEC_OP unary_expr
    {
      $$ = newUnop( DECRB_OP, NULL, $2 );
    }
  | unary_operator cast_expr
    {
      $$ = newUnop( $1, NULL, $2 );
    }
  | SIZEOF unary_expr
    {
      $$ = newUnop( SIZEOF_OP, NULL, $2 );
    }
  | SIZEOF '(' type_name ')'
    {
      $$ = newIntConst(computeSize($3));
    }  
  ;

unary_operator
  : '&'
    {
      $$ = ADDR_OF_OP;
    }
  | '*'
    {
      $$ = DEREF_OP;
    }
  | '+'
    {
      $$ = UADD_OP;
    }
  | '-'
    {
      $$ = USUB_OP;
    }
  | '~'
    {
      $$ = COMPLMT_OP;
    }
  | '!'
    {
      $$ = LNOT_OP;
    } 
  ;

cast_expr
  : unary_expr
    {
      $$ = $1;
    }
  | '(' type_name ')' cast_expr
    {
      $$ = newCastop( $2, NULL, $4 );
    } 
  ;

multiplicative_expr
  : cast_expr
    {
      $$ = $1;
    }
  | multiplicative_expr '*' cast_expr
    {
      $$ = newBinop( MULT_OP, NULL, $1, $3 );
    }
  | multiplicative_expr '/' cast_expr
    {
      $$ = newBinop( DIV_OP, NULL, $1, $3 );
    }
  | multiplicative_expr '%' cast_expr
    {
      $$ = newBinop( MOD_OP, NULL, $1, $3 );
    }
  ;

additive_expr
  : multiplicative_expr
    {
      $$ = $1;
    }
  | additive_expr '+' multiplicative_expr
    {
      $$ = newBinop( ADD_OP, NULL, $1, $3 );
    }
  | additive_expr '-' multiplicative_expr
    {
      $$ = newBinop( SUB_OP, NULL, $1, $3 );
    }
  ;

shift_expr
  : additive_expr
    {
      $$ = $1;
    }
  | shift_expr LEFT_OP additive_expr
    {
      $$ = newBinop( LSHIFT_OP, NULL, $1, $3 );
    }
  | shift_expr RIGHT_OP additive_expr
    {
      $$ = newBinop( RSHIFT_OP, NULL, $1, $3 );
    } 
  ;

relational_expr
  : shift_expr
    {
      $$ = $1;
    }
  | relational_expr '<' shift_expr
    {
      $$ = newBinop( LT_OP, NULL, $1, $3 );
    }
  | relational_expr '>' shift_expr
    {
      $$ = newBinop( GT_OP, NULL, $1, $3 );
    }
  | relational_expr LE_OP shift_expr
    {
      $$ = newBinop( LTE_OP, NULL, $1, $3 );
    }
  | relational_expr GE_OP shift_expr
    {
      $$ = newBinop( GTE_OP, NULL, $1, $3 );
    }
  ;

equality_expr
  : relational_expr
    {
      $$ = $1;
    }
  | equality_expr EQ_OP relational_expr
    {
      $$ = newBinop( EQL_OP, NULL, $1, $3 );
    }
  | equality_expr NE_OP relational_expr
    {
      $$ = newBinop( NEQL_OP, NULL, $1, $3 );
    }
  ;

and_expr
  : equality_expr
    {
      $$ = $1;
    }
  | and_expr '&' equality_expr
    {
      $$ = newBinop( ANDD_OP, NULL, $1, $3 );
    }
  ;

exclusive_or_expr
  : and_expr
    {
      $$ = $1;
    }
  | exclusive_or_expr '^' and_expr
    {
      $$ = newBinop( XORR_OP, NULL, $1, $3 );
    }
  ;

inclusive_or_expr
  : exclusive_or_expr
    {
      $$ = $1;
    }
  | inclusive_or_expr '|' exclusive_or_expr
    {
      $$ = newBinop( ORR_OP, NULL, $1, $3 );
    }
  ;

logical_and_expr
  : inclusive_or_expr
    {
      $$ = $1;
    }
  | logical_and_expr AND_OP inclusive_or_expr
    {
      $$ = newBinop( LAND_OP, NULL, $1, $3 );
    }
  ;

logical_or_expr
  : logical_and_expr
    {
      $$ = $1;
    }
  | logical_or_expr OR_OP logical_and_expr
    {
      $$ = newBinop( LOR_OP, NULL, $1, $3 );
    }
  ;

conditional_expr
  : logical_or_expr
    {
      $$ = $1;
    }
  | logical_or_expr '?' expr ':' conditional_expr
    {
      $$ = newTriop( COND_OP, NULL, $1, $3, $5 );
    }
  ;

assignment_expr
  : conditional_expr
    {
      $$ = $1;
    }
  | unary_expr assignment_operator assignment_expr
    {
      $$ = newBinop( $2, NULL, $1, $3 );
    }
  ;

assignment_operator
  : '=' 
    {
      $$ = ASSIGN_OP;
    }
  | MUL_ASSIGN
    {
      $$ = MULT_ASSIGN_OP;
    }
  | DIV_ASSIGN
    {
      $$ = DIV_ASSIGN_OP;
    }
  | MOD_ASSIGN
    {
      $$ = MOD_ASSIGN_OP;
    }
  | ADD_ASSIGN
    {
      $$ = ADD_ASSIGN_OP;
    }
  | SUB_ASSIGN
    {
      $$ = SUB_ASSIGN_OP;
    }
  | LEFT_ASSIGN
    {
      $$ = LEFT_ASSIGN_OP;
    }
  | RIGHT_ASSIGN
    {
      $$ = RIGHT_ASSIGN_OP;
    }
  | AND_ASSIGN
    {
      $$ = AND_ASSIGN_OP;
    }
  | XOR_ASSIGN
    {
      $$ = XOR_ASSIGN_OP;
    }
  | OR_ASSIGN
    {
      $$ = OR_ASSIGN_OP;
    }
  ;

expr
  : assignment_expr
    {
      $$ = $1;
    }
  | expr ',' assignment_expr
    {
      $$ = newBinop( COMMA_OP, NULL, $1, $3 );
    }
  ;

constant_expr
  : conditional_expr
    {
      $$ = reduceConstExpr($1);
      if (typeQuery($$->type) == TYERROR)
      {
        errorT((Tree)$$, "malformed constant expression");
      }
    }
  ;

declaration
  : declaration_specifiers ';'
    {
        /* if the specifier is not union/struct/enum, it's an error */
        /* if yes, check whether need to install tag:
         * case: struct a;  yes
         * case: struct a { int  a; }; no
         */
        declarationWithoutDeclarator( $1 );

        g_id_lookup = ON;   /*  lsr,  05/10/01 */
    }
  | declaration_specifiers init_declarator_list ';'
    {
     /* check whether we need to install tag if specifier is struct/union */
     /* case: struct a { int a; } b;    No
      * case: struct a *p, *q;      if struct a defined before NO
      *                             otherwise install
      * case: struct a p;           if struct a defined before NO
      *                             otherwise ERROR
      */
     beforeProcessDeclaration( $1 );

     /****** install the TYPE with ID to symbol table and ******* 
      ****** generate code for the declaration, if necessary *******/
     processDeclaration($1, $2);
     g_id_lookup = ON;   /*  lsr,  05/10/01 */
    }
  ;

declaration_specifiers
  : storage_class_specifier 
   { 
     $$ = update_bucket( NULL, $1, NULL ); 
   }
  | storage_class_specifier declaration_specifiers
   {
     $$ = update_bucket( $2, $1, NULL );
   }
  | type_specifier
   { 
     $$ = update_bucket( NULL, $1.type_spec, $1.type );
     if ($1.err) set_bucket_err($$); 
   }
  | type_specifier declaration_specifiers
   {
     $$ = update_bucket( $2, $1.type_spec, $1.type );
     if ($1.err) set_bucket_err($$); 
   }
  | type_qualifier
   { 
     $$ = update_bucket( NULL, $1, NULL ); 
   }
  | type_qualifier declaration_specifiers
   {
     $$ = update_bucket( $2, $1, NULL );
   }
  ;

init_declarator_list
  : init_declarator
    { 
      $$ = $1;
    }
  | init_declarator_list ',' init_declarator
    {
      /* pjh: this builds list backwards! */
      $3->next = $1;
      $$ = $3;
    }
  ;

init_declarator
  : declarator 
    {
      $$ = newDeclarator($1, NULL);  
    }
  | declarator '='
    {
      g_id_lookup = ON;
    }
    initializer
    {
      info("initializers not yet supported");
      $$ = newDeclarator($1, NULL);
    }
  ;

storage_class_specifier
  : TYPEDEF  { $$ = TYPEDEF_SPEC; }
  | EXTERN   { $$ = EXTERN_SPEC;  }
  | STATIC   { $$ = STATIC_SPEC;  } 
  | AUTO     { $$ = AUTO_SPEC;    }
  | REGISTER { $$ = REGISTER_SPEC;}
  ;

type_specifier
  : VOID 
    {  
      $$ = newSpecifierObject(VOID_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | CHAR 
    {  
      $$ = newSpecifierObject(CHAR_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | SHORT 
    { 
      $$ = newSpecifierObject(SHORT_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | INT  
    {  
      $$ = newSpecifierObject(INT_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | LONG 
    {  
      $$ = newSpecifierObject(LONG_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | FLOAT
    {  
      $$ = newSpecifierObject(FLOAT_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | DOUBLE
    { 
      $$ = newSpecifierObject(DOUBLE_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | SIGNED
    { 
      $$ = newSpecifierObject(SIGNED_SPEC);
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | UNSIGNED
    { 
      $$ = newSpecifierObject(UNSIGNED_SPEC); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | struct_or_union_specifier 
    { 
      $$ = $1; 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  | enum_specifier
    {
      resetEnumItemVal();    /*  lsr,  05/03/01 */
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
      $$ = $1;
    }
  | TYPE_NAME 
    {
      $$ = getTypenameSpecifierObject($1); 
      g_id_lookup = OFF;    /*  lsr,  05/10/01 */
    }
  ;

struct_or_union_specifier
  : struct_or_union '{'
    {
      g_id_lookup=ON;  /*  lsr,  05/10/01 */
    }
    struct_declaration_list '}'
    {
        $$ = * buildStructUnionType( NULL, $1, $4 ); /* lsr $3 to $4 */
    }
  | struct_or_union identifier
    {
        /* install struct tag in symbol table */
        $<y_my_specifier>$ = * installTagWhenMemberListComing( $2, $1 );
    }
   '{'
    {
      g_id_lookup=ON;  /*  lsr  05/10/01 */
    }
    struct_declaration_list '}'

    {
        /* attach member list to the tag in the symbol table */
        /* note: $6 is struct_declaration_list--MEMBER_LIST */
        /* pay attention to how to use $3 */

        attachMemberList( $<y_my_specifier>3, $6 );
        $$ = $<y_my_specifier>3;
    }
  | struct_or_union identifier
   {
        $$ = * buildStructUnionType( $2, $1, NULL );
   }
  ;

struct_or_union
  : STRUCT
    {
      g_id_lookup = OFF;   /*  lsr,  05/10/01 */
      $$ = STRUCT_SPEC;
    }
  | UNION
    {
      g_id_lookup = OFF;   /*  lsr,  05/10/01 */
      $$ = UNION_SPEC;
    }
  ;

struct_declaration_list
  : struct_declaration
    {
        $$ = $1;
    }
  | struct_declaration_list struct_declaration
    {
        $$ = mergeMemberList( $1, $2 );
    }
  ;

struct_declaration
  : specifier_qualifier_list struct_declarator_list ';'
   {
    /* deal with struct tag, if present */
    beforeProcessDeclaration( $1 );
    g_id_lookup = ON;    /*  lsr,  05/10/01 */

    $$ = createMemberList( $1, $2 );
   }
  | error ';'
   {
    g_id_lookup = ON;
    $$ = NULL;
   }
  ;

specifier_qualifier_list
  : type_specifier
    {
       $$ = update_bucket( NULL, $1.type_spec, $1.type );
       if ($1.err) set_bucket_err($$); 
    }
  | type_specifier specifier_qualifier_list
    {
       $$ = update_bucket( $2, $1.type_spec, $1.type );
       if ($1.err) set_bucket_err($$); 
    }
  | type_qualifier
    {
      $$ = update_bucket( NULL, $1, NULL );
    }
  | type_qualifier specifier_qualifier_list
    {
      $$ = update_bucket( $2, $1, NULL );
    }
  ;

struct_declarator_list
  : struct_declarator
    {   /* copied from init_declarator_list, since they are similar*/
        $$ = $1;
    }
  | struct_declarator_list ',' struct_declarator
    {   /* copied from init_declarator_list, since they are similar*/
        $3->next = $1;
        $$ = $3;
    }
  ;

struct_declarator
  : declarator { $$ = newDeclarator( $1, NULL ); }
  | ':' constant_expr { info("bitfields not yet implemented!"); }
  | declarator ':' constant_expr { info("bitfields not yet implemented!"); }
  ;

enum_specifier
  : enum_keyword '{' enumerator_list '}'
    {
      $$ = processEnumIdName(NULL);  /*  lsr,  05/05/01 */
    }
  | enum_keyword identifier '{' enumerator_list '}'
    {
      $$ = processEnumIdName($2);    /*  lsr,  05/05/01 */
    }
  | enum_keyword identifier
    {
      $$ = verifyEnumId($2);         /*  lsr,  05/05/01 */
    }
  ;

enum_keyword
  : ENUM
    {
      g_id_lookup = OFF;
    }
  ;

enumerator_list
  : enumerator
  | enumerator_list ',' enumerator
  ;

enumerator
  : identifier
    {
      processEnumItem($1);    /*  lsr,  05/03/01 */
    }
  | identifier '=' constant_expr
    {
      setEnumItemVal(getConstExprValue($3));   /*  lsr, 05/03/01 */
      processEnumItem($1);  /*  lsr, 05/03/01 */
    }
  ;

type_qualifier
  : CONST  {  $$ = CONST_SPEC; } 
  | VOLATILE{ $$ = VOLATILE_SPEC;}
  ;

type_qualifier_list
  : type_qualifier
    {
      $$ = convertSpecifierToQualifier($1);
    }
  | type_qualifier_list type_qualifier
    {
      $$ = addQualifierToList($1, convertSpecifierToQualifier($2));
    }
  ;

declarator
  : direct_declarator
    {
      $$ = $1;
    }
  | pointer direct_declarator
    {
      $$ = addPointersToDeclTree( $1, $2 );
    }
  ;

direct_declarator
  : identifier
    {
      $$ = newIdDeclNode( $1 );
    }
  | '(' declarator ')'
    {
      $$ = $2;
    }
  | direct_declarator '[' ']'
    {
      $$ = newArrayDeclNode(0, $1);  /* 0 indicates NO_DIM */
    }
  | direct_declarator '[' constant_expr ']'
    {
      /*
       *  Error detection done later, but want to use 0 to indicate
       *  no dimension present, so re-write user 0 (which is an error
       *  anyway) to -1.
       */
      long value = getConstExprValue($3);
      if (value == 0) value = -1;
      $$ = newArrayDeclNode(value, $1);
    }
  | direct_declarator_func_start parameter_type_list ')'
    {
      $$ = newFuncDeclNode($2.style, $2.list, $1);
    }
  | direct_declarator_func_start ')'
    {
      $$ = newFuncDeclNode(PROTOTYPE, NULL, $1);
    }
  | direct_declarator_func_start identifier_list ')'
    {
      /* Note: id list not kept so can't do oldstyle function definitions! */
      $$ = newFuncDeclNode(OLDSTYLE, NULL, $1);
    }
  ;

direct_declarator_func_start
  : direct_declarator '('
    {
      g_id_lookup = ON;
      $$ = $1;
    }
  ;

pointer
  : '*'
    {
      $$ = newPointerDeclNode( NO_QUAL, NULL );
    }
  | '*' type_qualifier_list
    {
      $$ = newPointerDeclNode( $2, NULL );      
    }
  | '*' pointer
    {
      $$ = newPointerDeclNode( NO_QUAL, $2 );
    }
  | '*' type_qualifier_list pointer
    {
      $$ = newPointerDeclNode( $2, $3 );
    }
  ;

parameter_type_list
  : parameter_list
    {
      $$.list = $1;
      $$.style = PROTOTYPE;
    }
  | parameter_list ',' ELIPSIS
    {
      $$.list = $1;
      $$.style = DOT_DOT_DOT;
    }
  ;

parameter_list
  : parameter_declaration
    {
      $$ = $1;
    }
  | parameter_list ',' parameter_declaration
    {
      $$ = appendToParamList($1, $3);
    }
  ;

parameter_declaration
: declaration_specifiers declarator 
  {
    /* deal with struct tag, if present */
    beforeProcessDeclaration( $1 );
    g_id_lookup = ON;

    $$ = makeParamListNode($1, $2);
  }
  | declaration_specifiers 
  {
    /* deal with struct tag, if present */
    beforeProcessDeclaration( $1 );
    g_id_lookup = ON;    /*  lsr,  05/10/01 */

    $$ = makeParamListNode($1, NULL);
  }
  | declaration_specifiers abstract_declarator 
  {
    /* deal with struct tag, if present */
    beforeProcessDeclaration( $1 );
    g_id_lookup = ON;    /*  lsr,  05/10/01 */

    $$ = makeParamListNode($1, $2);
  }
  ;

identifier_list
  : identifier {}
  | identifier_list ',' identifier
  ;

type_name
  : specifier_qualifier_list
  {
    $$ = build_base($1);
    g_id_lookup = ON;    /*  pjh,  04/28/02 */
  }
  | specifier_qualifier_list abstract_declarator
  {
    ST_ID ignoreID;
    BOOLEAN ignoreErr;
    g_id_lookup = ON;    /*  lsr,  05/10/01 */

    $$ = processDeclarator($2, build_base($1), &ignoreID, &ignoreErr, NULL);
  }
  ;

abstract_declarator
  : pointer
    {
      $$ = addPointersToDeclTree($1, NULL);
    }
  | direct_abstract_declarator 
    {
      $$ = $1;
    }
  | pointer direct_abstract_declarator 
    {
      $$ = addPointersToDeclTree($1, $2);
    }
  ;

direct_abstract_declarator
  : '(' abstract_declarator ')' 
    {
      $$ = $2;
    }
  | '[' ']' 
    {
      $$ = newArrayDeclNode(0, NULL);  /* 0 indicates NO_DIM */
    }
  | '[' constant_expr ']' 
    {
      /*
       *  Error detection done later, but want to use 0 to indicate
       *  no dimension present, so re-write user 0 (which is an error
       *  anyway) to -1.
       */
      long value = getConstExprValue($2);
      if (value == 0) value = -1;
      $$ = newArrayDeclNode(value, NULL);
    }
  | direct_abstract_declarator '[' ']' 
    {
      $$ = newArrayDeclNode(0, $1);  /* 0 indicates NO_DIM */
    }
  | direct_abstract_declarator '[' constant_expr ']' 
    {
      /*
       *  Error detection done later, but want to use 0 to indicate
       *  no dimension present, so re-write user 0 (which is an error
       *  anyway) to -1.
       */
      long value = getConstExprValue($3);
      if (value == 0) value = -1;
      $$ = newArrayDeclNode(value, $1);
    }
  | '(' ')' 
    {
      $$ = newFuncDeclNode(OLDSTYLE, NULL, NULL);
    }
  | '(' parameter_type_list ')' 
    {
      $$ = newFuncDeclNode($2.style, $2.list, NULL);
    }
  | direct_abstract_declarator '(' ')' 
    {
      $$ = newFuncDeclNode(OLDSTYLE, NULL, $1);
    }
  | direct_abstract_declarator '(' parameter_type_list ')' 
    {
      $$ = newFuncDeclNode($3.style, $3.list, $1);
    }
  ;

initializer
  : assignment_expr {}
  | '{' initializer_list '}' {}
  | '{' initializer_list ',' '}' {}
  ;

initializer_list
  : initializer
  | initializer_list ',' initializer
  ;

statement
  : labeled_statement
    {
      $$ = $1;
    }
  | compound_statement
    {
      $$ = $1;
    }
  | expression_statement
    {
      $$ = (Tree) $1;
    }
  | selection_statement
    {
      $$ = $1;
    }
  | iteration_statement
    {
      $$ = $1;
    }
  | jump_statement
    {
      $$ = $1;
    }
  | error ';'
    {
      $$ = newError();
    }
  ;

labeled_statement
  : identifier ':' statement
    {
      $$ = newLabel( $1, $3 );
    }
  | CASE constant_expr ':' statement
    {
      $$ = newCase( $2, $4 );
    }
  | DEFAULT ':' statement
    {
      $$ = newDefault( $3 );
    }
  ;

compound_statement
  : '{'
      {
        st_enter_block();
      }
    compound_statement_tail
      {
        $$ = $3;
#ifdef ST_DUMP
        st_dump_block(st_get_cur_block());
#endif
        st_exit_block();
      }
  ;

compound_statement_tail
  : '}'
    {
      $$ = NULL;
    }
  | statement_list '}'
    {
      $$ = $1;
    }
  | declaration_list '}'
    {
      $$ = NULL;
    }
  | declaration_list statement_list '}'
    {
      $$ = $2;
    }
  ;

declaration_list
  : declaration
  | declaration_list declaration
  ;

statement_list
  : statement
    {
      $$ = $1;
    }
  | statement statement_list
    {
      $$ = newSeq( $1, $2 );
    } 
  ;
expression_statement
  : ';'
    {
      $$ = NULL;
    }
  | expr ';'
    {
      $$ = $1;
    }
  ;

selection_statement
  : IF '(' expr ')' statement
    {
      $$ = newIfElse( $3, $5, NULL );
    }
  | IF '(' expr ')' statement ELSE statement
    {
      $$ = newIfElse( $3, $5, $7 );
    }
  | SWITCH '(' expr ')' statement
    {
      $$ = newSwitch( $3, $5 );
    }
  ;

iteration_statement
  : WHILE '(' expr ')' statement
    {
      $$ = newWhile( $3, $5 );
    }
  | DO statement WHILE '(' expr ')' ';'
    {
      $$ = newDoWhile( $2, $5 );
    }
  | FOR '(' ';' ';' ')' statement
    {
      $$ = newFor( NULL, NULL, NULL, $6 );
    }
  | FOR '(' ';' ';' expr ')' statement
    {
      $$ = newFor( NULL, NULL, $5, $7 );
    }
  | FOR '(' ';' expr ';' ')' statement
    {
      $$ = newFor( NULL, $4, NULL, $7 );
    }
  | FOR '(' ';' expr ';' expr ')' statement
    {
      $$ = newFor( NULL, $4, $6, $8 );
    }
  | FOR '(' expr ';' ';' ')' statement
    {
      $$ = newFor( $3, NULL, NULL, $7 );
    }
  | FOR '(' expr ';' ';' expr ')' statement
    {
      $$ = newFor( $3, NULL, $6, $8 );
    }
  | FOR '(' expr ';' expr ';' ')' statement
    {
      $$ = newFor( $3, $5, NULL, $8 );
    }
  | FOR '(' expr ';' expr ';' expr ')' statement
    {
      $$ = newFor( $3, $5, $7, $9 );
    }
  ;

jump_statement
  : GOTO identifier ';'
    {
      $$ = newGoto( $2 );
    }
  | CONTINUE ';'
    {
      $$ = newContinue();
    }
  | BREAK ';'
    {
      $$ = newBreak();
    }
  | RETURN ';'
    {
      $$ = newReturn( NULL );
    }
  | RETURN expr ';'
    {
      $$ = newReturn( $2 );
    }
  ;

translation_unit
  : external_declaration
  | translation_unit external_declaration
  ;

external_declaration
  : function_definition
  | declaration
  | error ';'
    {
      g_id_lookup = ON;
    }
  | error '}'
    {
      g_id_lookup = ON;
    }
  ;

function_definition
  : declarator '{'
    {
      gCurrentFunctionOffset = 0;

      $<y_param_list>$ =
        processFunctionDef(update_bucket(NULL, INT_SPEC, NULL), $1);

      st_enter_block();

      allocateStackSlotsForParameters($<y_param_list>$);

      gCurrentFunctionOffset = adjustFunctionOffset(gCurrentFunctionOffset);
    }
    compound_statement_tail
    {
      beforeAnalyze();
      $4 = analyze($4);
      afterAnalyze();
      encodeFunctionEntry(gCurrentFunctionName, gCurrentFunctionOffset, -1,
                          paramListSize($<y_param_list>3),
                          gCurrentFunctionClass == STATIC_SC);
      encodeMoveParametersToLocalSlots($<y_param_list>3, $4);
      beforeEncode();
      encode($4);
      freeExpressionStatementRegister($4);
      afterEncode();
      encodeFunctionExit(gCurrentFunctionName, -1,
                         paramListSize($<y_param_list>3));
#ifdef PRINTTREE
      printTree($4);
#endif
#ifdef ST_DUMP
      st_dump_block(st_get_cur_block());
#endif
      st_exit_block();
      poolReset();
    }
  | declarator declaration_list '{' compound_statement_tail
    {
      info("old style functions not yet implemented");
    }
  | declaration_specifiers declarator '{'
    {
      gCurrentFunctionOffset = 0;

      $<y_param_list>$ = processFunctionDef($1, $2);

      st_enter_block();

      allocateStackSlotsForParameters($<y_param_list>$);

      gCurrentFunctionOffset = adjustFunctionOffset(gCurrentFunctionOffset);
    }
    compound_statement_tail
    {
      beforeAnalyze();
      $5 = analyze($5);
      afterAnalyze();
      encodeFunctionEntry(gCurrentFunctionName, gCurrentFunctionOffset, -1,
                          paramListSize($<y_param_list>4),
                          gCurrentFunctionClass == STATIC_SC);
      encodeMoveParametersToLocalSlots($<y_param_list>4, $5);
      beforeEncode();
      encode($5);
      freeExpressionStatementRegister($5);
      afterEncode();
      encodeFunctionExit(gCurrentFunctionName, -1,
                         paramListSize($<y_param_list>4));
#ifdef PRINTTREE
      printTree($5);
#endif
#ifdef ST_DUMP
      st_dump_block(st_get_cur_block());
#endif
      st_exit_block();
      poolReset();
    }
  | declaration_specifiers declarator declaration_list '{'
      compound_statement_tail
    {
      info("old style functions not yet implemented");
    }
  ;

identifier
  : IDENTIFIER
   {
     $$ = $1;
   }
  ;

string_literal
  : STRING_LITERAL
    {
      $$ = $1;
    }
  | string_literal STRING_LITERAL
    {
      $$ = concat($1, $2);
    }
  ;

%%

void yyerror(char *s)
{
  error("%s (column %d)", s, YYcolumnno());
}

/* Concatenates the second string literal onto the end of the first string
 * literal, removing the double quotes at the end of first, and the beginning
 * of second.
 *
 * Added by shillman on 2/2/2001. The string_literal yacc code was also added.
 */
static char* concat(char* first, char* second)
{
  char *temp, *temp2;
  size_t length1, length2;

  length1 = strlen(first);
  length2 = strlen(second);

  if( !(temp = (char *)malloc(length1+length2+1)) )
  {
    fatal("Could not allocate space for strings in concat, parse.y\n");
  }
  else
  {
    strcpy(temp, first);
    temp[length1-1] = '\0';
    strncat(temp, second+1, length2);
  }
  temp2 = st_save_string(temp);
  free(temp);
  return temp2;
}

