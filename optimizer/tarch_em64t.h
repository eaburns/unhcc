/**
 * \file optimizer/tarch_em64t.h
 * Contains declarations for EM64T functionality.
 * \author Ethan Burns
 * \date 2007-11-14
 */
#ifndef _TARCH_EM64T_H_
#define _TARCH_EM64T_H_

enum reg_class {
	INTEGER_CLASS = 0,
	FLOAT_CLASS,

	REG_CLASS_NUM,

	REG_CLASS_NONE
};

/**
 * Number of bytes to add to the function frame for spillage.
 *
 * \note We need to keep the stack alligned at a 16-byte boundary for
 *       function calls, so this must be a multiple of 16, this ends up
 *       being a *HUGE* waste of stack space in most cases.  This should
 *       be made smarter, maybe make a function that allocates 8-bytes
 *       for integer class and 16 for floating point... then we can do
 *       another pass over to make sure that the locals are aligned
 *       correctly.
 *
 * \note We will always spill a 'double' when we are spilling a floating
 *       point class register.
 */
#define TARCH_REG_SPILL_SIZE 16

#endif /* !_TARCH_EM64T_H_ */
/* vi: set tabstop=8 textwidth=72: */
