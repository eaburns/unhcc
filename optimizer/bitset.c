/**
 * \file bitset.c
 *
 * \author Ethan Burns
 * \date 2007-10-22
 */

#include <assert.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "bitset.h"

#if 0
#define DEBUG_BITSET
#endif

#if defined(DEBUG_BITSET)
#include <stdio.h>
#define dprintf(...) fprintf(stdout, __VA_ARGS__)
#else
#define dprintf(...)
#endif /* DEBUG_BITSET */

#define BITS_PER_ELEM \
	((unsigned int)(sizeof(*((struct bitset *) 0)->array) * 8))
#define BITSET_FILL_ELEM_MASK 0xFF

#define BITSET_INDEX(e) (((e) + 1) / BITS_PER_ELEM)
#define BITSET_BIT(e) ((e) % BITS_PER_ELEM)
#define BITSET_MASK(e) (1 << BITSET_BIT(e))
#define BITSET_MAX(s) (((s)->size * BITS_PER_ELEM) - 2)

static void bitset_grow(struct bitset *, unsigned int);

/**
 * Creates a new bitset.
 *
 * \return A new bitset.
 *
 * \note Memory allocation failes are fatal.
 */
struct bitset *new_bitset(void)
{
	struct bitset *ret;

	ret = calloc(1, sizeof(*ret));
	if (!ret)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	dprintf("new bitset\n");
	bitset_grow(ret, BITSET_START_SIZE);

	return ret;
}

/**
 * Frees a bitset.
 *
 * \param s The set to free.
 */
void delete_bitset(struct bitset *s)
{
	if (s->array)
		free(s->array);
	free(s);
}

/**
 * Grows the bitset to be large enought to hold the given element.
 *
 * \param s The bitset.
 * \param to The element that needs to be covered by the set.
 */
static void bitset_grow(struct bitset *s, unsigned int to)
{
	unsigned int old_size;

	old_size = s->size;

	dprintf("growing bitset from=%u to=%u\n", s->size, to);
	assert(s->size < to);
	s->size = to;
	s->array = realloc(s->array, s->size);
	memset(s->array + old_size, 0, s->size - old_size);
}

/**
 * Adds an element to a bitset.
 *
 * \param s The set.
 * \param e The element to set.
 */
void bitset_add(struct bitset *s, unsigned int e)
{
	if (BITSET_MAX(s) < e)
		bitset_grow(s, BITSET_INDEX(e) + BITSET_START_SIZE);
	dprintf("setting e=%u -> bit %u of elem %u\n", e,
	        BITSET_BIT(e), BITSET_INDEX(e));
	s->array[BITSET_INDEX(e)] |=  BITSET_MASK(e);
}

/**
 * Deletes an element from the bitset.
 *
 * \param s The set.
 * \param e The element to delete.
 */
void bitset_del(struct bitset *s, unsigned int e)
{
	if (BITSET_MAX(s) < e)
		return;

	dprintf("clearing e=%u -> bit %u of elem %u\n", e,
	        BITSET_BIT(e), BITSET_INDEX(e));
	s->array[BITSET_INDEX(e)] &=  ~BITSET_MASK(e);
}

/**
 * Checks if the set contains the given element.
 *
 * \param s The set.
 * \param e The element.
 * \return True if the set contains the given value, false if not.
 */
int bitset_contains(struct bitset *s, unsigned int e)
{
	if (BITSET_MAX(s) < e)
		return 0;

	return s->array[BITSET_INDEX(e)] & BITSET_MASK(e);
}

/**
 * Creates a new bitset that is a clone of the given set.
 *
 * \param s The given set.
 * \return A new set that is a clone of the given set.
 */
struct bitset *bitset_clone(struct bitset *s)
{
	struct bitset *ret;

	ret = new_bitset();
	bitset_copy(s, ret);
	return ret;
}

/**
 * Copies a bitset into another already created bitset.
 *
 * \param src The source set.
 * \param dst The destination set.
 */
void bitset_copy(struct bitset *src, struct bitset *dst)
{
	if (dst->size < src->size)
		bitset_grow(dst, src->size);

	memcpy(dst->array, src->array, dst->size);

	if (dst->size > src->size)
		memset(dst->array + src->size, 0, dst->size - src->size);
}

/**
 * Creates a new set that is the union of the two given sets.
 *
 * \param a The first set.
 * \param b The second set.
 * \param u Used to return the union.
 */
void bitset_union(struct bitset *a, struct bitset *b, struct bitset *u)
{
	unsigned int i;
	struct bitset *bigger, *smaller;

	if (a->size > b->size) {
		bigger = a;
		smaller = b;
	} else {
		bigger = b;
		smaller = a;
	}

	if (u->size < bigger->size)
		bitset_grow(u, bigger->size);

	for (i = 0; i < smaller->size; i++)
		u->array[i] = bigger->array[i] | smaller->array[i];

	for (i = smaller->size; i < bigger->size; i++)
		u->array[i] = bigger->array[i];
}

/**
 * Computes the intersection of two basic blocks.
 *
 * \param a The first set.
 * \param b The second set.
 * \param n Used to return the intersction.
 */
void bitset_intersection(struct bitset *a, struct bitset *b, struct bitset *n)
{
	unsigned int i;
	struct bitset *bigger, *smaller;

	if (a->size > b->size) {
		bigger = a;
		smaller = b;
	} else {
		bigger = b;
		smaller = a;
	}

	if (n->size < smaller->size)
		bitset_grow(n, smaller->size);

	for (i = 0; i < smaller->size; i++)
		n->array[i] = bigger->array[i] & smaller->array[i];
}

/**
 * Creates a new set that is the subtraction of set b from set a.
 *
 * \param a The first set.
 * \param b The set to subtract from a.
 * \param d Used to return the difference of the two sets.
 */
void bitset_subtract(struct bitset *a, struct bitset *b, struct bitset *d)
{
	unsigned int i;

	if (d->size < a->size)
		bitset_grow(d, a->size);

	for (i = 0; i < a->size; i++)
		d->array[i] = (a->array[i] ^ b->array[i]) & a->array[i];
}

/**
 * Calls the given function for each element in the bitset.
 *
 * \param s The set.
 * \param f The function to call.
 * \param aux Auxiliary data to pass to each call.
 *
 * \note This is *very* expensive.
 */
void bitset_map(struct bitset *s, void (*f)(unsigned int, void *), void *aux)
{
	unsigned int i;

	for (i = 0; i < BITSET_MAX(s); i++) {
		if (bitset_contains(s, i))
			f(i, aux);
	}
}

/**
 * Check if two bitsets are equal
 *
 * \param a The first set.
 * \param b The second set.
 * \return True if the two sets are equal, false if not.
 */
int bitset_eq(struct bitset *a, struct bitset *b)
{
	int i;
	struct bitset *bigger, *smaller;

	if (a->size > b->size) {
		bigger = a;
		smaller = b;
	} else {
		bigger = b;
		smaller = a;
	}


	if (memcmp(smaller->array, bigger->array, smaller->size) != 0)
		return 0;

	for (i = smaller->size; i < bigger->size; i++) {
		if (bigger->array[i] != 0)
			return 0;
	}

	return 1;
}

/**
 * Clears all values out of the give set.
 *
 * \param s The set.
 */
void bitset_clear(struct bitset *s)
{
	memset(s->array, 0, s->size);
}

/**
 * Fills a bitset with all of the values up to the given max.
 *
 * \param s The bitset.
 * \param nelems The number of elements to set (starting from zero)
 */
void bitset_fill(struct bitset *s, unsigned int nelems)
{
	int mask, i;

	dprintf("%s: nelems=%d\n", __func__, nelems);

	bitset_clear(s);

	if (BITSET_MAX(s) < nelems)
		bitset_grow(s, BITSET_INDEX(nelems) + BITSET_START_SIZE);

	if (s->size > 1) {
		dprintf("%s: memset(s->array, %#x, %d)\n",
		        __func__, BITSET_FILL_ELEM_MASK, BITSET_INDEX(nelems));
		memset(s->array, BITSET_FILL_ELEM_MASK, BITSET_INDEX(nelems));
	}

	mask = 0;
	for (i = 0; i < BITSET_BIT(nelems); i += 1)
		mask |= BITSET_MASK(i);

	dprintf("%s: mask=%#x\n", __func__, mask);

	s->array[BITSET_INDEX(nelems)] = mask;
}

/* vi: set tabstop=8 textwidth=72: */
