/**
 * \file annotation.c
 * Defines functions that deal with annotated lines of output
 *        code.
 * \author Ethan Burns
 * \date 2007-10-09
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "annotation.h"

#define WHITESPACE " \t\f"

/**
 * Check if the given line is annotation or not.
 * \return Non-zero if the given line is an annotation line, zero if
 *         not.
 */
int is_annotation(const char *line)
{
	return line && line[0] == ANNOTATION_CHAR;
}

/**
 * Checks if the annotation text is the given string.
 * \param line The annotated line.
 * \param text The text to check for.
 * \return Non-zero if the given annotated line is the given text, zero
 *         if not.
 */
static int annotation_text(const char *line, const char *text)
{
	return is_annotation(line) && strstr(line, text) == &line[1];
}

/**
 * Returns the annotated data for the given line.
 * \param line The annotated line.
 * \param text The annotation text.
 * \return The pointer to the annotation data or NULL if the line is not
 *         annotated or not annotation for the given text.
 */
static const char *get_annotation_data(const char *line, const char *text)
{
	const char *data;
	unsigned int span;

	if (!annotation_text(line, text))
		return NULL;

	data = line + strlen(text) + 1;
	span = strspn(data, WHITESPACE);
	return data + span;
}

/**
 * Check if the given line is a section switch annotation.  If it is,
 * return the name of the new section.
 * \param line The line to check.
 * \param name_buf An output parameter for the function name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a section switch annotation line,
 *         zero if not.
 */
int is_section(const char *line, char *name_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "SECTION");
	if (!data)
		return 0;

	strncpy(name_buf, data, len - 1);
	name_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is a function label.  If it is, return the
 * name of the function.
 * \param line The line to check.
 * \param name_buf An output parameter for the function name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a function annotation line, zero
 *         if not.
 */
int is_function(const char *line, char *name_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "FUNCTION");
	if (!data)
		return 0;

	strncpy(name_buf, data, len - 1);
	name_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is a lable.  If it is, return the
 *        name.
 * \param line The line to check.
 * \param name_buf An output parameter for the label name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a label annotation line, zero
 *         if not.
 */
int is_label(const char *line, char *name_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "LABEL");
	if (!data)
		return 0;

	strncpy(name_buf, data, len - 1);
	name_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is a conditional branch.  If it is,
 *        return the destination name.
 * \param line The line to check.
 * \param name_buf An output parameter for the destination name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a cond branch annotation line, zero
 *         if not.
 */
int is_cond_branch(const char *line, char *name_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "CBRANCH");
	if (!data)
		return 0;

	strncpy(name_buf, data, len - 1);
	name_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is an unconditional branch.  If it is,
 *        return the destination name.
 * \param line The line to check.
 * \param name_buf An output parameter for the destination name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a ucond branch annotation line, zero
 *         if not.
 */
int is_uncond_branch(const char *line, char *name_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "UBRANCH");
	if (!data)
		return 0;

	strncpy(name_buf, data, len - 1);
	name_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is a function return.
 * \param line The line to check.
 * \return Non-zero if the given line is a return annotation line, zero
 *         if not.
 */
int is_return(const char *line)
{
	return annotation_text(line, "RETURN");
}

/**
 * Check if the given line is a register use.  If it is, return
 *        the register name.
 * \param line The line to check.
 * \param reg_buf An output parameter for the register name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a use annotation line, zero
 *         if not.
 */
int is_use(const char *line, char *reg_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "USE");
	if (!data)
		return 0;

	strncpy(reg_buf, data, len - 1);
	reg_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is a register def.  If it is, return
 *        the register name.
 * \param line The line to check.
 * \param reg_buf An output parameter for the register name.
 * \param len The length of the output buffer.
 * \return Non-zero if the given line is a def annotation line, zero
 *         if not.
 */
int is_def(const char *line, char *reg_buf, unsigned int len)
{
	const char *data;

	data = get_annotation_data(line, "DEF");
	if (!data)
		return 0;

	strncpy(reg_buf, data, len - 1);
	reg_buf[len - 1] = '\0';

	return 1;
}

/**
 * Check if the given line is a local variable allocation line.
 *
 * \param line The line to check.
 * \param buf Buffer used to return the number of bytes allocated for
 *            locals.
 * \return Non-zero if the given line is a alocals  annotation line, zero
 *         if not.
 */
int is_alocals(const char *line, unsigned int *buf)
{
	const char *data;
	char *endptr;

	data = get_annotation_data(line, "ALOCALS");
	if (!data)
		return 0;

	*buf = (unsigned int) strtoul(data, &endptr, 10);
	if (*endptr != '\0')
		err(EXIT_FAILURE, "%s: %s", __func__, data);

	return 1;
}

/**
 * Check if the given line is a local variable deallocation line.
 *
 * \param line The line to check.
 * \return Non-zero if the given line is a dlocals  annotation line, zero
 *         if not.
 */
int is_dlocals(const char *line)
{
	const char *data;

	data = get_annotation_data(line, "DLOCALS");
	if (!data)
		return 0;

	return 1;
}

/**
 * Returns true if the given line is a comment.
 *
 * \param line The line to check.
 * \return !0 if the given line is a comment, 0 otherwise.
 */
int is_comment(const char *line)
{
	unsigned int spaces;

	spaces = strspn(line, " \t\v\f\n\r");
	return line[spaces] == '#';
}

/**
 * Builds a use annotation string into the given buffer for the given
 * register.
 *
 * \param r The register.
 * \param buf The buffer.
 * \param len The buffer length.
 */
void get_use(const char *r, char *buf, unsigned int len)
{
        snprintf(buf, len, "!USE %s", r);
}

/**
 * Builds a def annotation string into the given buffer for the given
 * register.
 *
 * \param r The register.
 * \param buf The buffer.
 * \param len The buffer length.
 */
void get_def(const char *r, char *buf, unsigned int len)
{
        snprintf(buf, len, "!DEF %s", r);
}


/* vi: set tabstop=8 textwidth=72: */
