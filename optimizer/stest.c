/**
 * \file bsettest.c
 * Tests a bitset.
 * \author Ethan Burns
 * \date 2007-10-22
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "bitset.h"

void print_elem(unsigned int i, void *ignore)
{
	printf("%u ", i);
}

int main(void)
{
	struct bitset *a, *b, *c, *d, *e, *f;

	printf("BITSET_START_SIZE=%u\n", BITSET_START_SIZE);

	a = new_bitset();
	b = new_bitset();
	c = new_bitset();
	d = new_bitset();
	e = new_bitset();
	f = new_bitset();

	bitset_fill(f, 10);

	bitset_add(a, 0);
	bitset_add(a, 1);
	bitset_add(a, 2);
	bitset_add(a, 3);
	bitset_add(a, 4);
	bitset_add(a, 5);
	bitset_add(a, 7);
	bitset_add(a, 8);
	bitset_add(a, 9);
	bitset_add(a, 10);
	bitset_add(a, 11);
	bitset_add(a, 12);
	bitset_add(a, 13);
	bitset_add(a, 14);
	bitset_add(a, 15);

	bitset_add(b, 2);
	bitset_add(b, 4);
	bitset_add(b, 6);

	bitset_union(a, b, c);
	bitset_subtract(a, b, d);
	bitset_subtract(b, a, e);

	printf("%20s: ", "a");
	bitset_map(a, print_elem, NULL);
	printf("\n%20s: ", "b");
	bitset_map(b, print_elem, NULL);
	printf("\n%20s: ", "c = (a U b): ");
	bitset_map(c, print_elem, NULL);
	printf("\n%20s: ", "d = (a - b)");
	bitset_map(d, print_elem, NULL);
	printf("\n%20s: ", "e = (b - a)");
	bitset_map(e, print_elem, NULL);
	printf("\n");

	printf("\n%20s: ", "f = fill(10)");
	bitset_map(f, print_elem, NULL);
	printf("\n");

	bitset_fill(f, 3);
	printf("\n%20s: ", "f = fill(3)");
	bitset_map(f, print_elem, NULL);
	printf("\n");

	if (!bitset_contains(a, 1))
		errx(EXIT_FAILURE, "a doesn't contain 1");
	if (bitset_contains(d, 2))
		errx(EXIT_FAILURE, "d does contain 2");

	delete_bitset(a);
	delete_bitset(b);
	delete_bitset(c);
	delete_bitset(d);
	delete_bitset(e);
	delete_bitset(f);
}

/* vi: set tabstop=8 textwidth=72: */
