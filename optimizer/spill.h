/**
 * \file optimizer/spill.h
 * Contains declarations for register spilling.
 * \author Ethan Burns
 * \date 2007-11-21
 */


int do_spills(struct list *);

/* vi: set tabstop=8 textwidth=72: */
