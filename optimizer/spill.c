/**
 * \file optimizer/spill.c
 * Contains code for spilling virtual registers when graph coloring
 * fails.
 * \author Ethan Burns
 * \date 2007-11-21
 */

#include <assert.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "annotation.h"
#include "bblock.h"
#include "list.h"
#include "reg.h"

#if 0
#define DEBUG_SPILL
#endif

#if defined(DEBUG_SPILL)
#define dprintf(...) 					\
	do {						\
		fprintf(stdout, "%s:%d, %s: ",		\
		        __FILE__, __LINE__, __func__);	\
		fprintf(stdout, __VA_ARGS__);		\
	} while (0)
#else
#define dprintf(...)
#endif

static void func_spill(struct func *, struct list *);
static unsigned int alloc_spill_space(struct func *);
static void replace_number(struct line *, unsigned int, unsigned int);
static struct list *spill_process_line(struct list *, struct list *,
                                       struct list *);
static void allocate_spills(struct func *, struct list *);
static void spill_process_use(struct reg, struct list *,
                              struct list *, struct list *);
static void spill_process_def(struct reg, struct list *, struct list *);

/**
 * Runs through all of the functions and checks if there needs to be
 * spill code added.  If there is, we add it here.
 *
 * \param funcs The function list.
 * \return 0 if no spills were needed, !0 if spills were needed.
 */
int do_spills(struct list *funcs)
{
	int i, retval;
	struct func *f;
	struct list *node, *next;

	retval = 0;

	list_for_each (node, next, funcs) {
		f = node->data;
		for (i = 0; i < REG_CLASS_NUM; i += 1) {
			if (f->to_spill[i] != NULL) {
				retval = 1;
				func_spill(f, f->to_spill[i]);
			}
		}
	}

	return retval;
}

/**
 * Adds spill code for the registers in the given spill list.
 *
 * \param f The function inwhich to spill the registers.
 * \param spill The list of registers to spill.
 *
 * \note This function frees the spill list.
 */
static void func_spill(struct func *f, struct list *spill)
{
	struct bblock *b;
	struct list *usechain;
	struct list *bnode, *bnext, *lnode, *lnext;

	allocate_spills(f, spill);

	usechain = NULL;
	list_for_each (bnode, bnext, &f->blocks) {
		b = bnode->data;
		list_for_each (lnode, lnext, &b->lines)
			usechain = spill_process_line(lnode, spill, usechain);
	}
}

/**
 * Adds code to the current function to allocate (and deallocate) a new
 * local slot that can be used for register spilling.
 *
 * \param f The function to allocate (and deallocate) space in.
 * \return The base pointer offset of the newly allocated space.
 */
static unsigned int alloc_spill_space(struct func *f)
{
	struct line *l;
	struct bblock *b;
	struct list *bnode, *bnext, *lnode, *lnext;
	unsigned int size;

	/* the list nodes for the lines that alloc and dealloc locals */
	struct list *alloc, *dealloc;

	alloc = dealloc = NULL;

	list_for_each (bnode, bnext, &f->blocks) {
		b = bnode->data;
		list_for_each (lnode, lnext, &b->lines) {
			l = lnode->data;
			if (is_alocals(l->line, &size)) {
				assert(!alloc);
				alloc = lnode;
			} else if (is_dlocals(l->line)) {
				assert(!dealloc);
				assert(alloc);
				dealloc = lnode;
				goto done;
			}
		}
	}

	/*
	 * there should always be annotation for locals allocation and
	 * deallocation, even if there is only zero bytes allocated
	 */
	errx(EXIT_FAILURE, "%s: alloc or dealloc of locals not found",
	     __func__);
 done:
	assert(alloc);
	assert(dealloc);

	replace_number(alloc->data, size, size + TARCH_REG_SPILL_SIZE);
	replace_number(alloc->next->data, size, size + TARCH_REG_SPILL_SIZE);
	replace_number(dealloc->data, size, size + TARCH_REG_SPILL_SIZE);
	replace_number(dealloc->next->data, size, size + TARCH_REG_SPILL_SIZE);

	return size + TARCH_REG_SPILL_SIZE;
}

/**
 * Replaces the first occurrance of the number 'old' with the number
 * 'new' in the given line.
 *
 * \param l The line.
 * \param old The number to replace.
 * \param new The new number.
 */
static void replace_number(struct line *l, unsigned int old, unsigned int new)
{
	long linemax;
	char *linebuf, *old_s, *new_s;
	char *old_pos;
	unsigned int buf_ind, i;

	linemax = get_linemax();

	linebuf = calloc(linemax, sizeof(*linebuf));
	if (!linebuf)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	old_s = calloc(linemax, sizeof(*old_s));
	if (!old_s)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	new_s = calloc(linemax, sizeof(*new_s));
	if (!new_s)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	snprintf(old_s, linemax, "%u", old);
	snprintf(new_s, linemax, "%u", new);

	old_pos = strstr(l->line, old_s);
	if (!old_pos)
		errx(EXIT_FAILURE, "%s: %s not found\n", __func__, old_s);

	buf_ind = 0;
	/* copy the beginning of the line */
	for (i = 0; l->line + i < old_pos; i += 1) {
		linebuf[buf_ind] = l->line[i];
		buf_ind += 1;
	}

	/* replace the old number with our new one */
	for (i = 0; i < strlen(new_s); i += 1) {
		linebuf[buf_ind] = new_s[i];
		buf_ind += 1;
	}

	/* copy the end of the line */
	for (i = (old_pos - l->line) + strlen(old_s)
	     ; i < strlen(l->line)
	     ; i += 1) {
		linebuf[buf_ind] = l->line[i];
		buf_ind += 1;
	}

	dprintf("l->line=[%s] replaced with [%s]\n", l->line, linebuf);

	free((char *) l->line);
	l->line = linebuf;

	free(new_s);
	free(old_s);
}


/**
 * Process a single line looking for registers to spill.
 *
 * \param l The line to check.
 * \param spills The spill registers.
 * \param usechain The list-node for the first USE in a chain of uses,
 *                 or NULL if we are not in a chain of uses.
 * \return The new usechain value.  If we are not in a list of USEs,
 *         NULL is returned.  If this line was a use and the usechain
 *         value passed in was NULL (another we were previously not in a
 *         usechain) then return this list.  If this line was a use and
 *         we were in a use chain (usechain was not NULL), then just
 *         return the usechain head.
 *
 * \note All of this usechain crap is because we need to do register
 *       restores *before* all of the USE annotation for the line using
 *       the registers (otherwise we lose register conflictions on the
 *       next coloring pass).
 */
static struct list *spill_process_line(struct list *llist,
                                       struct list *spills,
                                       struct list *usechain)
{
	char buf[REG_STRING_MAX];
	struct line *l;

	l = llist->data;

	dprintf("%s: [%s]\n", __func__, l->line);

        if (is_use(l->line, buf, REG_STRING_MAX)) {
		if (!usechain)
			usechain = llist;
		spill_process_use(reg_from_string(buf), llist,
		                  spills, usechain);
        } else if (is_def(l->line, buf, REG_STRING_MAX)) {
		usechain = NULL;
		spill_process_def(reg_from_string(buf), llist, spills);
        } else {
		usechain = NULL;
	}

	return usechain;
}

/**
 * Allocates spill space for each register on the list.
 */
static void allocate_spills(struct func *f, struct list *spill)
{
	struct rg_node *n;
	struct list *node, *next;

	list_for_each (node, next, spill) {
		n = node->data;
		n->offs = alloc_spill_space(f);
	}
}

/**
 * Processess a single use annotation.
 *
 * \param r The register being 'use'd.
 * \param llist The line's list node.
 * \param spill The registers to spill.
 * \param usechain The head of the usechain list.
 */
static void spill_process_use(struct reg r,
                              struct list *llist,
                              struct list *spill,
                              struct list *usechain)
{
	char def[1000];
	char comment[1000];
	struct line *defline, *loadline;
	struct list *deflist, *loadlist;
	struct list *node, *next;
	struct rg_node *n;

	list_for_each (node, next, spill) {
		n = node->data;
		if (reg_eq(r, n->reg)) {
			loadline = new_line(alloc_reg_load(&r, n->offs));
			dprintf("%s: reg load [%s]\n", __func__,
			        loadline->line);
			loadlist = new_list(loadline);

			get_def(reg_string(r), def, 1000);
			defline = new_line(def);
			dprintf("%s: new DEF [%s]\n", __func__, def);
			deflist = new_list(defline);

			list_add_tail(usechain, deflist);
			list_add_tail(deflist, loadlist);
			snprintf(comment, 1000,
			         "\t# Loading %s from -%u(%%rbp)",
			         reg_string(r), n->offs);
			list_add_tail(loadlist, new_list(new_line(comment)));
			break;
		}
	}
}

/**
 * Processess a single def annotation.
 *
 * \param r The register being 'def'd.
 * \param llist The line's list node.
 * \param spill The registers to spill.
 */
static void spill_process_def(struct reg r, struct list *llist,
                              struct list *spill)
{
	char use[1000];
	char comment[1000];
	struct line *useline, *saveline;
	struct list *uselist, *savelist, *commentlist;
	struct list *node, *next;
	struct rg_node *n;

	list_for_each (node, next, spill) {
		n = node->data;
		if (reg_eq(r, n->reg)) {
			snprintf(comment, 1000, "\t# Saving %s to -%u(%%rbp)",
			         reg_string(r), n->offs);
			commentlist = new_list(new_line(comment));

			get_use(reg_string(r), use, 1000);
			useline = new_line(use);
			dprintf("%s: new USE [%s]\n", __func__, use);
			uselist = new_list(useline);

			saveline = new_line(alloc_reg_save(&r, n->offs));
			dprintf("%s: reg save [%s]\n", __func__,
			        saveline->line);
			savelist = new_list(saveline);

			list_add(llist, commentlist);
			list_add(commentlist, uselist);
			list_add(uselist, savelist);
			break;
		}
	}
}

/* vi: set tabstop=8 textwidth=72: */
