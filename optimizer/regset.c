/**
 * \file regset.c
 * Definitions for manipulating a virtual register set.
 * \author Ethan Burns
 * \date 2007-10-22
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bitset.h"
#include "reg.h"

#if 0
#define DEBUG_VREGSET
#endif

#if defined(DEBUG_VREGSET)
#include <stdio.h>
#define dprintf(...) fprintf(stdout, __VA_ARGS__)
#else
#define dprintf(...)
#endif /* DEBUG_VREGSET */

static void __print_vreg(unsigned int i, void *void_col);

/**
 * Initializes a virtual register set.
 *
 * \param s The virtual register set to initialize.
 */
void regset_init(struct regset *s)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		s->regs[i] = new_bitset();
}

/**
 * Destroys the memory allocated for the given virtual register set.
 *
 * \param s The set to desetroy.
 */
void regset_destroy(struct regset *s)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		delete_bitset(s->regs[i]);
}

/**
 * Adds the register to the set.
 *
 * \param s The set.
 * \param r The register.
 */
void regset_add(struct regset *s, const char *r)
{
	regset_add_raw(s, reg_from_string(r));
}

/**
 * Adds a register to the set.
 *
 * \param s The set.
 * \param r The register.
 */
void regset_add_raw(struct regset *s, struct reg r)
{
	bitset_add(s->regs[r.class], r.number);
}

/**
 * Removes the register from the set.
 *
 * \param s The set.
 * \param r The register.
 */
void regset_del(struct regset *s, const char *r)
{
	regset_del_raw(s, reg_from_string(r));
}

/**
 * Removes the register from the set.
 *
 * \param s The set.
 * \param r THe register.
 */
void regset_del_raw(struct regset *s, struct reg r)
{
	bitset_del(s->regs[r.class], r.number);
}

/**
 * Clears all of the values in the regset.
 *
 * \param s The set to clear.
 */
void regset_clear(struct regset *s)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		bitset_clear(s->regs[i]);
}

/**
 * Checks if the given set contains the given register.
 *
 * \param s The set.
 * \param r_str The register string.
 * \return True if the register is contained in the set, false if not.
 */
int regset_contains(struct regset *s, const char *r_str)
{
	struct reg r;
	r = reg_from_string(r_str);
	return regset_contains_raw(s, r);
}

/**
 * Checks if the given set contains the given register.
 *
 * \param s The set.
 * \param r The register.
 * \return True if the register is contained in the set, false if not.
 */
int regset_contains_raw(struct regset *s, struct reg r)
{
	return bitset_contains(s->regs[r.class], r.number);
}

/**
 * Copies the virtual register set from src to dst.
 *
 * \param src The source set.
 * \param dst The destination set.
 */
void regset_copy(struct regset *src, struct regset *dst)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		bitset_copy(src->regs[i], dst->regs[i]);
}

/**
 * Computes the union of the sets.
 *
 * \param a The first set.
 * \param b The second set.
 * \param u The union is returned via this parameter, this parameter
 *          must be an uninitialized set, it is initialized by this routine.
 */
void regset_union(struct regset *a, struct regset *b, struct regset *u)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		bitset_union(a->regs[i], b->regs[i], u->regs[i]);
}

/**
 * Computes the subtraction of the two sets.
 *
 * \param a The first set.
 * \param b The second set.
 * \param s The difference is returned via this parameter, this parameter
 *          must be an uninitialized set, it is initialized by this routine.
 */
void regset_subtract(struct regset *a, struct regset *b, struct regset *s)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		bitset_subtract(a->regs[i], b->regs[i], s->regs[i]);
}

/**
 * Auxiliary data to pass around for printing virtual register sets.
 */
struct __print_aux {
	FILE *outfile;
	enum reg_class class;
	unsigned int col;
	unsigned int col0;
};

/**
 * This function can be mapped over the virtual regseter set to print
 * the registers.
 *
 * \param ind The set index.
 * \param void_aux The auxiliary data structure.
 */
static void __print_vreg(unsigned int ind, void *void_aux)
{
	unsigned int i;
	struct __print_aux *aux;
	char *buf;

	aux = void_aux;

	buf = reg_string(REG(aux->class, ind));

	if (strlen(buf) + aux->col > 72) {
		fprintf(aux->outfile, "\n");
		for (i = 1; i < aux->col0; i++)
			fprintf(aux->outfile, " ");
		aux->col = aux->col0;
	}

	fprintf(aux->outfile, "%s ", buf);
	aux->col += strlen(buf) + 1;
}

/**
 * Prints the registers in the regset.
 *
 * \param outfile The output file to print to.
 * \param s The set to print.
 * \param col0 The leftmost column to print to, it is assumed that the
 *             cursor starts on col0 for the first line, subsequent
 *             lines will be alligned on col0.
 *
 * \note Linewrap is at column 72.
 */
void regset_print(FILE *outfile, struct regset *s, unsigned int col0)
{
	int i;
	struct __print_aux aux;

	aux.outfile = outfile;
	aux.col = aux.col0 = col0;
	for (i = 0; i < REG_CLASS_NUM; i += 1) {
		aux.class = i;
		bitset_map(s->regs[i], __print_vreg, &aux);
	}
}

/**
 * Compares two regsets for equality.
 *
 * \param a The first set.
 * \param b The second set.
 * \return True if the sets are equal, false if not.
 */
int regset_eq(struct regset *a, struct regset *b)
{
	int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1) { 
		if (!bitset_eq(a->regs[i], b->regs[i]))
			return 0;
	}

	return 1;
}

/**
 * Aux data passed to __regset_map_helper
 */
struct __regset_map_helper_data {
	void *aux;
	enum reg_class class;
	void (*func)(struct reg, void *);
};

/**
 * Helper function for regset_map().
 *
 * \param i The index.
 * \param aux A void* containing a pointer to a
 *            struct __regset_map_helper_data.
 */
static void __regset_map_helper(unsigned int i, void *aux)
{
	struct __regset_map_helper_data *data;

	data = aux;
	data->func(REG(data->class, i), data->aux);
}

/**
 * Maps a function against all of the register indexes.
 *
 * \param set The register set.
 * \param func The function to map.
 * \param aux Auxiliary data.
 */
void regset_map(struct regset *set, void (*func)(struct reg, void *),
                void *aux)
{
	int i;
	struct __regset_map_helper_data data;

	data.func = func;
	data.aux = aux;

	for (i = 0; i < REG_CLASS_NUM; i += 1) {
		data.class = i;
		bitset_map(set->regs[i], __regset_map_helper, &data);
	}
}

/* vi: set tabstop=8 textwidth=72: */
