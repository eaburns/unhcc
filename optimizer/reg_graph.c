/**
 * \file vreggraph.c
 * Contains definitions for the register graph data structure.
 * \author Ethan Burns
 * \date 2007-11-05
 */

#include <assert.h>
#include <err.h>
#include <stdlib.h>

#include "bblock.h"
#include "bitset.h"
#include "list.h"
#include "reg.h"
#include "tarch.h"

#if 0
#define DEBUG_REG_GRAPH
#endif

#if defined(DEBUG_REG_GRAPH)
#define dprintf(...) 					\
	do {						\
		fprintf(stdout, "%s:%d, %s: ",		\
		        __FILE__, __LINE__, __func__);	\
		fprintf(stdout, __VA_ARGS__);		\
	} while (0)
#else
#define dprintf(...)
#endif

static struct rg_node *rg_node(struct reg_graph *, struct reg);
static struct rg_node *new_rg_node(struct reg);
static void rg_node_add_arc(struct rg_node *, struct reg);
static void rg_node_remove_arc(struct rg_node *, struct reg);
static void print_reg_string(FILE *, struct reg);
static void __print_set(struct reg, void *);
static struct rg_node *rg_remove_node(struct reg_graph *, struct rg_node *);
static struct rg_node *best_spill(struct reg_graph *);
static double spill_factor(struct rg_node *);
static struct rg_node *pull_next_node(struct reg_graph *, unsigned int);
static void rg_node_color(struct reg_graph *, struct rg_node *, unsigned int);

/**
 * Finds a node in the graph given its index.
 *
 * \param g The graph.
 * \param r The register.
 */
static struct rg_node *rg_node(struct reg_graph *g, struct reg r)
{
	struct rg_node *n;
	struct list *node, *next;

	dprintf("%s: looking for %s\n", __func__, reg_string(r));

	list_for_each (node, next, &g->nodes) {
		n = node->data;
		if (reg_eq(n->reg, r))
			return n;
	}

	return NULL;
}

/**
 * Creates a new vreg node.
 *
 * \param r The register.
 * \return A new register graph node.
 */
static struct rg_node *new_rg_node(struct reg r)
{
	struct rg_node *n;

	n = calloc(1, sizeof(*n));
	if (!n)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	n->reg = r;
	n->color = -1;
	regset_init(&n->arcs);

	return n;
}

/**
 * Adds a new node to the graph.
 *
 * \param r The register.
 */
void rg_add_node(struct reg_graph *g, struct rg_node *n)
{
	struct list *node, *next;
	struct rg_node *m;

	list_add(&g->nodes, new_list(n));

	/* re-add arcs to n in other graph nodes */
	list_for_each (node, next, &g->nodes) {
		m = node->data;
		if (regset_contains_raw(&n->arcs, m->reg))
			rg_node_add_arc(m, n->reg);
	}
}

/**
 * Adds a new node to the graph.
 *
 * \param g The graph.
 * \param r The register.
 * \param cost0 The starting cost.
 */
void rg_add_new_node(struct reg_graph *g, struct reg r, float cost0)
{
	struct rg_node *n;

	dprintf("%s: adding node %d, %ld\n", __func__, r.class, r.number);

	n = new_rg_node(r);
	n->cost = cost0;
	rg_add_node(g, n);
}

/**
 * Adds an arc to a register node.
 *
 * \param n The node.
 * \param r The register to arc to.
 */
static void rg_node_add_arc(struct rg_node *n, struct reg r)
{
	if (!regset_contains_raw(&n->arcs, r)) {
		dprintf("%s: adding arc to class=%d, number=%ld\n",
		        __func__, r.class, r.number);
		n->degree += 1;
		regset_add_raw(&n->arcs, r);
	}
}

/**
 * Removes an arc from a register node.
 *
 * \param n The node.
 * \param r The register to remove the arc to.
 */
static void rg_node_remove_arc(struct rg_node *n, struct reg r)
{
	if (n->degree > 0)
		n->degree -= 1;
	regset_del_raw(&n->arcs, r);
}

/**
 * Frees the memory associated with a register graph node.
 *
 * \param n The node.
 */
void delete_rg_node(struct rg_node *n)
{
	regset_destroy(&n->arcs);
	free(n);
}

/**
 * Creates a new register graph with no nodes.
 *
 * \return The newly allocated register graph.
 * \note Memory allocation failure is fatal.
 * \note The caller must explicitly add the nodes.
 */
struct reg_graph *new_reg_graph(void)
{
	struct reg_graph *graph;

	graph = calloc(1, sizeof(*graph));
	if (!graph)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	list_init_head(&graph->nodes);

	return graph;
}

/**
 * Frees the memory associated with the reg_graph.
 *
 * \param graph The graph.
 */
void free_reg_graph(struct reg_graph *graph)
{
	struct list *node, *next;


	list_for_each (node, next, &graph->nodes) {
		delete_rg_node(node->data);
		list_delete(node);
		list_free(node);
	}

	free(graph);
}

/**
 * Adds an arc between between the two registers.
 *
 * \param g The graph.
 * \param a The first register.
 * \param b The second register.
 */
void rg_add_arc(struct reg_graph *g, struct reg a, struct reg b)
{
	struct rg_node *na, *nb;

	na = rg_node(g, a);
	assert(na);
	nb = rg_node(g, b);
	assert(nb);

	rg_node_add_arc(na, b);
	rg_node_add_arc(nb, a);
}

/**
 * Prints the register represented by the given index and dmax to the
 * given outfile.
 *
 * \param outfile The output file.
 * \param r The register.
 */
static void print_reg_string(FILE *outfile, struct reg r)
{
	fprintf(outfile, "%s", reg_string(r));
}

/**
 * Function that can be mapped over a regset to print the registers.
 *
 * \param r The regsiter.
 * \param aux A void* that contains a __reg_print_aux struct.
 */
static void __print_set(struct reg r, void *aux)
{
	FILE *outfile;

	outfile = aux;
	print_reg_string(outfile, r);
	fprintf(outfile, " ");
}

/**
 * Prints the register graph.
 *
 * \param outfile The output file.
 * \param g The graph.
 * \param c The class for this graph.
 */
void rg_print(FILE *outfile, struct reg_graph *g, enum reg_class c)
{
	struct rg_node *n, *min;
	struct list *node, *next;

	min = best_spill(g);

	list_for_each (node, next, &g->nodes) {
		n = node->data;
		print_reg_string(outfile, n->reg);
		fprintf(outfile, " c=%ld deg=%u cost=%7.2f%c: ",
			n->color, n->degree,
			spill_factor(n), n == min ? '*' : ' ');
		regset_map(&n->arcs, __print_set, outfile);
		fprintf(outfile, "\n");
	}
}

/**
 * Removes a node from the graph, and returns a copy of the node.
 *
 * \param g The graph.
 * \param r The register.
 */
static struct rg_node *rg_remove_node(struct reg_graph *g, struct rg_node *n)
{
	struct rg_node *m;
	struct list *node, *next;

	list_for_each (node, next, &g->nodes) {
		if (n == node->data)
			break;
	}

	assert(node->data == n);

	list_delete(node);
	list_free(node);

	list_for_each (node, next, &g->nodes) {
		m = node->data;
		if (regset_contains_raw(&m->arcs, n->reg))
			rg_node_remove_arc(node->data, n->reg);
	}

	return n;
}

/**
 * Adds to a node's spill cost.
 *
 * \param g The graph.
 * \param r The register.
 * \param dcost The change in cost.
 */
void rg_update_node_cost(struct reg_graph *g, struct reg r,
                         unsigned int dcost)
{
	struct rg_node *n;

	n = rg_node(g, r);
	n->cost += dcost;
}

/**
 * Gets the node that is the best spill candidate.
 *
 * \param g The graph.
 * \return The node that is the best spill candidate.
 */
static struct rg_node *best_spill(struct reg_graph *g)
{
	struct list *node, *next;
	struct rg_node *min = NULL;

	list_for_each (node, next, &g->nodes) {
		if (min == NULL
		    || spill_factor(node->data) < spill_factor(min))
			min = node->data;
	}

	return min;
}

/**
 * Get the spill factor for this node.
 *
 * \param n The node.
 * \return cost / degree
 */
static double spill_factor(struct rg_node *n)
{
	return (double) n->cost / (double) n->degree;
}

/**
 * Removes the next node of degree < 'degree' from the graph and
 * returns it.
 *
 * \param g The graph.
 * \param degree The max degree of the node to pull from the graph.
 * \param The node that was removed from the graph, or NULL if there are
 *        no nodes with degree < 'degree'.
 */
static struct rg_node *pull_next_node(struct reg_graph *g, unsigned int degree)
{
	struct list *node, *next;
	struct rg_node *n;

	list_for_each (node, next, &g->nodes) {
		n = node->data;
		if (n->degree < degree)
			return rg_remove_node(g, n);
	}

	return NULL;
}

/**
 * Attempts to find an ncolors-coloring of the given graph.
 *
 * \param g The graph.
 * \param ncolors The number of available colors.
 * \return NULL, or a list of nodes that needed to be spilled inorder to
 *         color the graph.
 * \note If a spill list is returned, the given regs need to have spill
 *       code inserted and the graph needs to be re-build and
 *       re-colored.
 */
struct list *rg_color(struct reg_graph *g, unsigned int ncolors)
{
	struct list *node, *next;
	struct list *spill;
	struct list pulled;
	struct rg_node *n, *s;

	spill = new_list(NULL);
	list_init_head(spill);
	list_init_head(&pulled);

	while (!list_empty(&g->nodes)) {
#if defined(DEBUG_REG_GRAPH)
		rg_print(stdout, g, ((struct rg_node *) g->nodes.next->data)->reg.class);
#endif /* DEBUG_REG_GRAPH */

		n = pull_next_node(g, ncolors);
		if (!n) {
			/* spill a node */
			s = rg_remove_node(g, best_spill(g));
			list_add(spill, new_list(s));
			dprintf("%s: spilled %s\n", __func__,
			        reg_string(s->reg));
		} else {
			list_add(&pulled, new_list(n));
			dprintf("%s: pulled %s degree=%u\n", __func__,
			        reg_string(n->reg), n->degree);
		}
	}

	list_for_each (node, next, &pulled) {
		n = node->data;
		rg_node_color(g, n, ncolors);
		rg_add_node(g, n);
	}

	list_for_each (node, next, &pulled) {
		list_delete(node);
		list_free(node);
	}

	if (!list_empty(spill))
		return spill;

	list_free(spill);

	return NULL;
}

/**
 * Data used for populating the neighboring color bitset.
 */
struct __color_helper_data {
	struct reg_graph *graph;
	struct bitset *colors;
	struct regset *arcs;
	unsigned int ncolors;
};

/**
 * Mapped over a rg_node's arcs to populate the set of its neighboring
 * colors.
 */
void __populate_neighbor_colors(struct reg r, void *aux)
{
	struct rg_node *neigh;
	struct __color_helper_data *data;

	data = aux;

	neigh = rg_node(data->graph, r);

	if (!neigh) {
		/* the neighbor was spilled, remove the arc */
		regset_del_raw(data->arcs, r);
	} else {
		assert(neigh->color <= data->ncolors);
		bitset_add(data->colors, neigh->color);
	}
}

/**
 * Attempts to color a node.
 *
 * \param n The node.
 * \param ncolors The number of possible colors.
 * \note For this to be useful, n must have some arcs.
 */
static void rg_node_color(struct reg_graph *g, struct rg_node *n,
                          unsigned int ncolors)
{
	unsigned int i;
	struct __color_helper_data data;
	struct bitset *colors;

	colors = new_bitset();

	data.graph = g;
	data.colors = colors;
	data.ncolors = ncolors;
	data.arcs = &n->arcs;
	dprintf("%s: populating neighbors for %s\n", __func__,
	        reg_string(n->reg));
	regset_map(&n->arcs, __populate_neighbor_colors, &data);

	for (i = 0; i < ncolors; i += 1) {
		if (!bitset_contains(colors, i))
			break;
	}

	assert(i < ncolors);
	assert(!bitset_contains(colors, i));
	n->color = i;
	dprintf("%s: coloring %s %ld\n", __func__,
	        reg_string(n->reg), n->color);

	delete_bitset(colors);
}

/**
 * Maps the colored virtual registers to real registers and does
 * replacements in the given line.
 *
 * \param g The graph.
 * \param l The line.
 */
void rg_map_regs(struct reg_graph *g, struct line *l)
{
	char *replaced;
	struct rg_node *n;
	struct list *node, *next;

	list_for_each (node, next, &g->nodes) {
		n = node->data;

		replaced = vreg_replace(l->line, n->reg,
		                        color_to_reg_str(n->reg.class,
		                                         n->color));
		if (replaced != l->line) {
			free((char *) l->line);
			l->line = replaced;
		}
	}
}

/* vi: set tabstop=8 textwidth=72: */
