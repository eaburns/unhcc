/**
 * \file loops.c
 * Contains function declarations for finding loops.
 * \author Ethan Burns
 * \date 2007-11-15
 */


#include <assert.h>
#include <err.h>
#include <stdlib.h>

#include "bblock.h"
#include "bitset.h"
#include "list.h"

#if 0
#define DEBUG_LOOPS
#endif

#if defined(DEBUG_LOOPS)
#define dprintf(...) 					\
	do {						\
		fprintf(stdout, "%s:%d, %s: ",		\
		        __FILE__, __LINE__, __func__);	\
		fprintf(stdout, __VA_ARGS__);		\
	} while (0)
#else
#define dprintf(...)
#endif

static struct loop *new_loop(struct func *, unsigned int );
static int loop_insert(struct loop *, struct bblock *);
static int is_backedge(struct bblock *, struct bblock *);
static void add_natural_loop_blocks(struct loop *, struct bblock *,
                                    struct bblock *);
static void find_backedges(struct bblock *);
static void fill_loop(struct loop *);
static void func_find_loops(struct func *);
static void find_dominators(struct list *);
static void func_find_dominators(struct func *);
static int bblock_find_dominators(struct bblock *);

/**
 * Creates a new loop structure.
 *
 * \param id The id of this loop.
 * \return The newly allocated loop.
 */
static struct loop *new_loop(struct func *f, unsigned int id)
{
	struct loop *l;
	struct list *node, *next;

	list_for_each (node, next, &f->loops) {
		l = node->data;
		if (l->id == id)
			return l;
	}

	l = calloc(1, sizeof(*l));
	if (!l)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	l->id = id;
	l->blocks = new_bitset();
	l->func = f;

	list_add(&f->loops, new_list(l));

	return l;
}

/**
 * Frees the memory assocated with the given loop structure.
 *
 * \param l The loop.
 */
void free_loop(struct loop *l)
{
	delete_bitset(l->blocks);
	free(l);
}

/**
 * Marks a basic block as being in the given loop.
 *
 * \param b The basic block.
 * \param l The loop.
 * \return 0 if the block was already in the loop, !0 if the block was
 *           added.
 */
static int loop_insert(struct loop *l, struct bblock *b)
{
	if (bitset_contains(l->blocks, b->id))
		return 0;

	dprintf("%s: adding %d to loop %d\n", __func__, b->id, l->id);
	bitset_add(l->blocks, b->id);
	b->loopdepth += 1;

	return 1;
}

/**
 * Checks if a given edge is a backedge.
 *
 * \param cur The current basic block.
 * \param suc The successor basic block.
 * \return !0 if the edge is a backedge, 0 if not.
 */
static int is_backedge(struct bblock *cur, struct bblock *suc)
{
	return cur->id > suc->id && bitset_contains(cur->dominators, suc->id);
}

/**
 * Finds any back-edges in the give basic block and creates a loop
 * structure for them.
 */
static void find_backedges(struct bblock *b)
{
	struct bblock *suc;
	struct list *node, *next;

	list_for_each(node, next, &b->outarcs) {
		suc = node->data;
		if (is_backedge(b, suc))
			new_loop(b->func, suc->id);
	}
}

/**
 * Adds all of the blocks in the natural loop defined by the given
 * back-edge to the given loop.
 *
 * \param l The loop.
 * \param header The loop header.
 * \param edge The block containing the backedge.
 */
static void add_natural_loop_blocks(struct loop *l,
                                    struct bblock *header,
                                    struct bblock *edge)
{
	struct list stack, *entry;
	struct list *node, *next;
	struct bblock *b;

	list_init_head(&stack);
	loop_insert(l, header);
	loop_insert(l, edge);
	list_add(&stack, new_list(edge));

	while (!list_empty(&stack)) {
		/* pop */
		assert(stack.next);
		entry = list_delete(stack.next);
		b = entry->data;
		list_free(entry);

		list_for_each (node, next, &b->inarcs) {
			if (loop_insert(l, node->data))
				list_add(&stack, new_list(node->data));
		}
	}
}

/**
 * Fills in the loop with the blocks that are contained within it.
 *
 * \param l The loop to fill in.
 */
static void fill_loop(struct loop *l)
{
	struct bblock *header, *pred;
	struct list *node, *next;

	header = bblock_lookup_by_id(&l->func->blocks, l->id);
	assert(header);

	/* find all back edges for this loop */
	list_for_each (node, next, &header->inarcs) {
		pred = node->data;
		if (is_backedge(pred, header))
			add_natural_loop_blocks(l, header, pred);
	}
}

/**
 * Finds all of the loops in the given function.
 *
 * \param f The function.
 */
static void func_find_loops(struct func *f)
{
	struct list *node, *next;

	/* find loop backedges */
	list_for_each(node, next, &f->blocks)
		find_backedges(node->data);

	/* fill in loops with their respective blocks */
	list_for_each(node, next, &f->loops)
		fill_loop(node->data);
}

/**
 * Finds all of the loops in all of the functions.
 *
 * \param funcs The list of functions
 */
void build_loops(struct list *funcs)
{
	struct list *node, *next;

	find_dominators(funcs);

	list_for_each(node, next, funcs)
		func_find_loops(node->data);
}

/**
 * Mapped over the loop's bitset of blocks.
 *
 * \param i The block ID for this entry.
 * \param aux A void* containing the the output FILE*.
 */
static void __print_loop_block(unsigned int i, void *aux)
{
	FILE *outfile;

	outfile = aux;
	fprintf(outfile, "%d ", i);
}

/**
 * Prints all of the basic blocks in the given loop.
 *
 * \param outfile The output file to print to.
 * \param l The loop.
 */
void print_loop(FILE *outfile, struct loop *l)
{
	assert(l != NULL);
	fprintf(outfile, "%d: ", l->id);
	bitset_map(l->blocks, __print_loop_block, outfile);
	fprintf(outfile, "\n");
}

/**
 * Finds all of the dominator relationships for every function.
 *
 * \param funcs The list of functions.
 */
static void find_dominators(struct list *funcs)
{
	struct list *node, *next;

	list_for_each (node, next, funcs)
		func_find_dominators(node->data);
}

/**
 * Finds all the dominator relationships for each BB in the given
 * function.
 *
 * \param f The function.
 */
static void func_find_dominators(struct func *f)
{
	int change;
	struct bblock *b;
	struct list *node, *next;

	/* Initialize n in N - {N_0}: D(n) = N */
	list_for_each (node, next, &f->blocks) {
		b = node->data;
		if (b->id == 0) {
			bitset_clear(b->dominators);
			bitset_add(b->dominators, b->id);
		} else {
			bitset_fill(b->dominators, f->nextbblock);
		}
	}

	do {
		list_for_each (node, next, &f->blocks) {
			b = node->data;
			if (b->id != 0)
				change = bblock_find_dominators(node->data);
		}
	} while (change);
}

/**
 * Finds the dominators for a given basic block.  This needs to be done
 * in an iteration untill there are no more changes.
 *
 * \param b The basic block.
 * \return !0 if there was a change made, 0 if not.
 */
static int bblock_find_dominators(struct bblock *b)
{
	int change;
	struct bblock *pred;
	struct bitset *copy;
	struct list *node, *next;

	copy = bitset_clone(b->dominators);
	bitset_fill(b->dominators, b->func->nextbblock);

	list_for_each (node, next, &b->inarcs) {
		pred = node->data;

		bitset_intersection(b->dominators, pred->dominators,
		                    b->dominators);
	}

	bitset_add(b->dominators, b->id);

	if (!bitset_eq(b->dominators, copy))
		change = 1;
	else
		change = 0;

	delete_bitset(copy);

	return change;
}

/* vi: set tabstop=8 textwidth=72: */
