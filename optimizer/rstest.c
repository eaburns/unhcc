/**
 * \file vrtest.c
 * Contains tests for the virtual register sets.
 * \author Ethan Burns
 * \date 2007-10-22
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "reg.h"

int main(void)
{
	struct regset a, b, u, s;

	regset_init(&a);
	regset_init(&b);
	regset_init(&u);
	regset_init(&s);

	regset_add(&a, "%vd5b");
	regset_add(&a, "%vd0");
	regset_add(&a, "%vd0d");
	regset_add(&a, "%vd10b");
	regset_add(&a, "%vf4");

	regset_add(&b, "%vd5");
	regset_add(&b, "%vd8");
	regset_add(&b, "%vf0");
	regset_add(&b, "%vf5");

	regset_union(&a, &b, &u);
	regset_subtract(&a, &b, &s);

	printf("%60s: ", "a");
	regset_print(stdout, &a, 63);
	printf("\n%60s: ", "b");
	regset_print(stdout, &b, 63);
	printf("\n%60s: ", "u");
	regset_print(stdout, &u, 63);
	printf("\n%60s: ", "s");
	regset_print(stdout, &s, 63);
	printf("\n");

	regset_destroy(&a);
	regset_destroy(&b);
	regset_destroy(&u);
	regset_destroy(&s);

	return EXIT_SUCCESS;
}

/* vi: set tabstop=8 textwidth=72: */
