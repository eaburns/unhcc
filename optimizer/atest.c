/**
 * \file atest.c
 *
 * Tests the annotation functions.
 *
 * \author Ethan Burns
 * \date 2007-10-09
 */


#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "annotation.h"

#define BUF_MAX 255

int main(void)
{
	char buf[BUF_MAX];

	if (!is_label("!LABEL lname", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_label false negative");
	printf("is_label [%s]\n", buf);
	if (is_label("!CBRANCH lname", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_label false positive");

	if (!is_cond_branch("!CBRANCH lname", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_cond_branch false negative");
	printf("is_cond_branch [%s]\n", buf);
	if (is_cond_branch("!UBRANCH lname", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_cond_branch false positive");

	if (!is_uncond_branch("!UBRANCH lname", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_uncond_branch false negative");
	printf("is_uncond_branch [%s]\n", buf);
	if (is_uncond_branch("!CBRANCH lname", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_uncond_branch false positive");

	if (!is_return("!RETURN"))
		errx(EXIT_FAILURE, "is_return false negative");
	printf("is_return\n");
	if (is_return("!CBRANCH"))
		errx(EXIT_FAILURE, "is_return false positive");

	if (!is_use("!USE %vd0", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_use false negative");
	printf("is_use [%s]\n", buf);
	if (is_use("!UBRANCH %vd0", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_use false positive");

	if (!is_def("!DEF %vd0", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_def false negative");
	printf("is_def [%s]\n", buf);
	if (is_def("!USE %vd0", buf, BUF_MAX))
		errx(EXIT_FAILURE, "is_def false positive");
}

/* vi: set tabstop=8 textwidth=72: */
