/**
 * \file vreg.h
 * Functions for manipulating virtual register strings.
 * \author Ethan Burns
 * \date 2007-10-22
 */
#ifndef _VREG_H_
#define _VREG_H_

#include <stdio.h>

#include "list.h"
#include "tarch.h"

struct bitset;
struct line;

/**
 * A register.
 */
struct reg {
	/** The register class */
	enum reg_class class;
	/** The register number */
	long number;
};

/**
 * Check if two registers are equal.
 * \param a The first register.
 * \param b The second register.
 * \return 0 if they are not equal, !0 if they are equal.
 */
static inline int reg_eq(struct reg a, struct reg b)
{
	return a.class == b.class && a.number == b.number;
}

/** Creates a static constant register structure */
#define REG(c, n) ((struct reg) { c, n })

/**
 * A register set.
 */
struct regset {
	/** The register set */
	struct bitset *regs[REG_CLASS_NUM];
};

/**
 * A node in the register graph.
 */
struct rg_node {
	/** the register */
	struct reg reg;
	/** the node's color */
	long color;
	/** node degree (number of arcs) */
	unsigned int degree;
	/** spill cost estimate */
	float cost;
	/** node arcs */
	struct regset arcs;
	/** spill location (offset from %rbp) */
	unsigned int offs;
};

/**
 * A virtual register graph.
 */
struct reg_graph {
	/** The list of nodes */
	struct list nodes;
};

/*
 * regset functions.
 */
void regset_init(struct regset *);
void regset_destroy(struct regset *);
void regset_add(struct regset *, const char *);
void regset_add_raw(struct regset *, struct reg);
void regset_del(struct regset *, const char *);
void regset_del_raw(struct regset *, struct reg);
void regset_clear(struct regset *);
int regset_contains(struct regset *, const char *);
int regset_contains_raw(struct regset *, struct reg);
void regset_copy(struct regset *, struct regset *);
void regset_union(struct regset *, struct regset *, struct regset *);
void regset_subtract(struct regset *, struct regset *, struct regset *);
void regset_print(FILE *outfile, struct regset *, unsigned int);
int regset_eq(struct regset *, struct regset *);
void regset_map(struct regset *, void (*)(struct reg, void*), void *);

/*
 * virtual register graph functions.
 */

struct reg_graph *new_reg_graph(void);
void free_reg_graph(struct reg_graph *);
void delete_rg_node(struct rg_node *);
void rg_add_new_node(struct reg_graph *, struct reg, float cost0);
void rg_add_arc(struct reg_graph *, struct reg, struct reg);
void rg_print(FILE *, struct reg_graph *, enum reg_class);
void rg_update_node_cost(struct reg_graph *, struct reg, unsigned int);
struct list *rg_color(struct reg_graph *, unsigned int);
void rg_map_regs(struct reg_graph *, struct line *);

/*
 * virtual register string manipulation functions.
 */

/** A generous maximum register string length */
#define REG_STRING_MAX 100

long get_linemax(void);
char *vreg_replace(const char *, struct reg, const char *);

/*
 * architecture specific functions (defined in the tarch_*.c files)
 */
const char *reg_getsized(const char *, char);
char reg_suffix(const char *);
char *reg_string(struct reg);
struct reg reg_from_string(const char *);

#endif /* !_VREG_H_ */
/* vi: set tabstop=8 textwidth=72: */
