/**
 * \file registerize.c
 *
 * Contains the main function for the flow control graph generation.
 *
 * \author Ethan Burns
 * \date 2007-11-19
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bblock.h"

#if 0
#define DEBUG_REGISTERIZE
#endif

int main(int argc, char *argv[])
{
	int spills;
	FILE *infile;
	DEFINE_LIST_HEAD(funcs);

	if (argc > 2) {
		fprintf(stderr, "usage: registerize [<asm file>]\n");
		fprintf(stderr, "       If no input file is specified, stdin is used\n");
		return EXIT_FAILURE;
	}

	if (argc == 1) {
		infile = stdin;
	} else {
		infile = fopen(argv[1], "r");
		if (!infile)
			err(EXIT_FAILURE, "%s: fopen failed", __func__);
	}

	build_bblocks(infile, &funcs);

	do {
		spills = build_graph(&funcs);
#if defined(DEBUG_REGISTERIZE)
		print_bblocks(stdout, &funcs);
#endif /* DEBUG_REGISTERIZE */
		if (spills)
			wipe_slate(&funcs);
	} while (spills);

	map_registers(&funcs);

#if defined(DEBUG_REGISTERIZE)
	print_bblocks(stdout, &funcs);
#endif /* DEBUG_REGISTERIZE */

	dump_code(stdout, &funcs);

	free_bblocks(&funcs);

	/* get rid of any reference to this datastructure so that
 	 * valgrind may tell us if we missed any free()s. */
	funcs.data = NULL;
	funcs.prev = NULL;
	funcs.next = NULL;

	fclose(infile);

	return EXIT_SUCCESS;
}

/* vi: set tabstop=8 textwidth=72: */
