/**
 * \file bitset.h
 * Contains the definition of a bitarray set.
 * \author Ethan Burns
 * \date 2007-10-22
 */
#ifndef _BITSET_H_
#define _BITSET_H_

/**
 * The starting size for a bitset.  This is the number of elements that
 * the array is allocated to.
 */
#define BITSET_START_SIZE 10

/**
 * A dynamically allocated bit array set.
 */
struct bitset {
	/**
 	 * The number of elements in the bit array.
 	 */
	unsigned int size;

	/**
	 * The dynamically allocated bit array.
	 */
	char *array;
};

struct bitset *new_bitset(void);
void delete_bitset(struct bitset *);
void bitset_add(struct bitset *, unsigned int);
void bitset_del(struct bitset *, unsigned int);
int bitset_contains(struct bitset *, unsigned int);
struct bitset *bitset_clone(struct bitset *);
void bitset_copy(struct bitset *, struct bitset *);
void bitset_union(struct bitset *, struct bitset *, struct bitset *);
void bitset_intersection(struct bitset *, struct bitset *, struct bitset *);
void bitset_subtract(struct bitset *, struct bitset *, struct bitset *);
void bitset_map(struct bitset *, void (*)(unsigned int, void *), void *);
int bitset_eq(struct bitset *, struct bitset *);
void bitset_clear(struct bitset *s);
void bitset_fill(struct bitset *s, unsigned int);

#endif /* !_BITSET_H_ */
/* vi: set tabstop=8 textwidth=72: */
