/**
 * \file list.c
 *
 * Contains definitions a circular doubly linked list.
 *
 * \author Ethan Burns
 * \date 2007-10-09
 */

#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"

/**
 * Adds a new node inbetween two other nodes.
 */
static void __list_add(struct list *prev, struct list *l, struct list *next)
{
	prev->next = l;
	l->prev = prev;
	l->next = next;
	next->prev = l;
}

/**
 * Initialize the list head.
 *
 * \param head The dummy head node to initialize.
 */
void list_init_head(struct list *head)
{
	head->data = NULL;
	head->next = head;
	head->prev = head;
}

/**
 * Creates a new list element.
 *
 * \param data The data to fill in for this element.
 * \return The newly created element
 * \note Out of memory is considered a fatal error, so it is safe to
 *       assume that if this function returns it is a non-NULL value.
 */
struct list *new_list(void *data)
{
	struct list *l;

	l = calloc(1, sizeof(*l));
	if (!l)
		err(EXIT_FAILURE, "%s calloc failed", __func__);
	l->data = data;
	return l;
}

/**
 * Frees the list node.
 *
 * \param l The list node to free.
 */
void list_free(struct list *l)
{
	free(l);
}

/**
 * Adds a list to the beginning of the given list.
 *
 * \param head The head of the list.
 * \param l The new list to add.
 * \note head is assumed to be a dummy element.
 */
void list_add(struct list *head, struct list *l)
{
	if (!head)
		errx(EXIT_FAILURE, "%s: passed a NULL head", __func__);
	__list_add(head, l, head->next);
}

/**
 * Adds a list to the end of the given list.
 *
 * \param head The head of the list.
 * \param l The new list to add.
 * \note head is assumed to be a dummy element.
 */
void list_add_tail(struct list *head, struct list *l)
{
	if (!head)
		errx(EXIT_FAILURE, "%s: passed a NULL head", __func__);

	__list_add(head->prev, l, head);
}

/**
 * Deletes the list node from the list that it is in.
 *
 * \param l The list node to delete.
 * \return The deleted element.
 * \note If the given element is not in a list then a warning message is
 *       printed.
 */
struct list *list_delete(struct list *l)
{
	if (!l->prev)
		warnx("%s: list entry is not in a list", __func__);
	if (!l->next)
		warnx("%s: list entry is not in a list", __func__);
	l->prev->next = l->next;
	l->next->prev = l->prev;
	return l;
}

/**
 * Check if the given list is empty.
 *
 * \param head The list head.
 * \return Non-zero if the list is empty, zero if it is not empty.
 */
int list_empty(struct list *head)
{
	return head->prev == head;
}

/**
 * Splices two lists: back is added to the tail of front.
 *
 * \param front The front list.
 * \param back The back list that is added to the end of front.
 */
void list_splice(struct list *front, struct list *back)
{
	back->next->prev = front->prev;
	front->prev->next = back->next;
	front->prev = back->prev;
	back->prev->next = front;

	back->prev = back;
	back->next = back;
}

/* vi: set tabstop=8 textwidth=72: */
