/**
 * \file tarch.h
 * Target architecture declarations.
 * \author Ethan Burns
 * \date 2007-11-16
 */
#ifndef _TARCH_H_
#define _TARCH_H_

#include "tarch_em64t.h"

struct reg;

const char *alloc_reg_save(struct reg *, unsigned int);
const char *alloc_reg_load(struct reg *, unsigned int);

unsigned int phys_regs_in_class(enum reg_class);
const char *color_to_reg_str(enum reg_class, long color);

#endif /* !_TARCH_H_ */
/* vi: set tabstop=8 textwidth=72: */
