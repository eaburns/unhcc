/**
 * \file list.h
 *
 * Contains declarations for a circular doubly linked list with a
 * dummy node head.
 *
 * \note This is mildly based on the Linux kernel list implementation.
 * \author Ethan Burns
 * \date 2007-10-09
 */
#ifndef _LIST_H_
#define _LIST_H_

/**
 * A list node.
 */
struct list {
	void *data;
	struct list *prev;
	struct list *next;
};

struct list *new_list(void *data);
void list_free(struct list *l);
void list_init_head(struct list *head);
void list_add(struct list *head, struct list *l);
void list_add_tail(struct list *head, struct list *l);
struct list *list_delete(struct list *l);
int list_empty(struct list *head);
void list_splice(struct list *front, struct list *back);

/**
 * Defines a new static list head node called '_name'.
 *
 * \param _name The name of the list head variable to define.
 */
#define DEFINE_LIST_HEAD(_name) struct list _name = { NULL, &_name, &_name }

/**
 * The beginning of a for-each loop that safely walks through a list
 * with the param '_p'.  Nodes can be removed from the list and it
 * should work just fine.
 *
 * \param _p A struct list * that is used to walk down the list.
 * \param _next A struct list * that is used to keep track of the next
 *             node so that '_p' can be safely deleted from the list.
 * \param _head The head node of the list.
 */
#define list_for_each(_p, _next, _head)				\
	for ((_p) = (_head)->next, (_next) = (_p)->next;	\
	     (_p) != (_head);					\
	     (_p) = (_next), (_next) = (_p)->next)

/**
 * The beginning of a for-each loop that safely walks through a list
 * in reverse with the param '_p'.  Nodes can be removed from the
 * list and it should work just fine.
 *
 * \param _p A struct list * that is used to walk down the list.
 * \param _prev A struct list * that is used to keep track of the prev
 *             node so that '_p' can be safely deleted from the list.
 * \param _head The head node of the list.
 */
#define list_for_each_reverse(_p, _prev, _head)			\
	for ((_p) = (_head)->prev, (_prev) = (_p)->prev;	\
	     (_p) != (_head);					\
	     (_p) = (_prev), (_prev) = (_p)->prev)

#endif /* !_LIST_H_ */
/* vi: set tabstop=8 textwidth=72: */
