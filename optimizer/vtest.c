/**
 * \file vtest.c
 * Tests the virtual register functions.
 * \author Ethan Burns
 * \date 2007-10-22
 */


#include <stdio.h>
#include <stdlib.h>

#include "reg.h"

int main(void)
{
	char *str, *reg, *new;
	struct reg r;

	str = "%vd1000b";
	printf("vstr=%s, type=%c, num=%ld, suffix=%c\n", str,
	       reg_from_string(str).class, reg_from_string(str).number,
	       reg_suffix(str));

	str = "%vf18";
	printf("vstr=%s, type=%c, num=%ld, suffix=%c\n", str,
	       reg_from_string(str).class, reg_from_string(str).number,
	       reg_suffix(str));

	str = "	movl %vd11b, %vf10";
	reg = "%rax";
	r = REG(INTEGER_CLASS, 10);
	printf("%20s %s in [%s] with %s\n", "Replacing",
	       reg_string(r), str, reg);
	new = vreg_replace(str, r, reg);
	printf("%20s [%s]\n", "", new);
	if (new != str)
		free(new);

	str = "%vd10b, %vd10w, %vd10d, %vd10";
	reg = "%rax";
	r = REG(INTEGER_CLASS, 10);
	printf("%20s %s in [%s] with %s\n", "Replacing",
	       reg_string(r), str, reg);
	new = vreg_replace(str, r, reg);
	printf("%20s [%s]\n", "", new);
	if (new != str)
		free(new);

	str = "%vd10b, %vd10w, %vd10d, %vd10";
	reg = "%r8";
	r = REG(INTEGER_CLASS, 10);
	printf("%20s %s in [%s] with %s\n", "Replacing",
	       reg_string(r), str, reg);
	new = vreg_replace(str, r, reg);
	printf("%20s [%s]\n", "", new);
	if (new != str)
		free(new);

	str = "%vd10b, %vd10w, %vd10d, %vd10";
	reg = "%rsi";
	r = REG(INTEGER_CLASS, 10);
	printf("%20s %s in [%s] with %s\n", "Replacing",
	       reg_string(r), str, reg);
	new = vreg_replace(str, r, reg);
	printf("%20s [%s]\n", "", new);
	if (new != str)
		free(new);

	str = "(%vd10b), (%vd10w), (%vd10d), (%vd10)";
	reg = "%r8";
	r = REG(INTEGER_CLASS, 10);
	printf("%20s %s in [%s] with %s\n", "Replacing",
	       reg_string(r), str, reg);
	new = vreg_replace(str, r, reg);
	printf("%20s [%s]\n", "", new);
	if (new != str)
		free(new);

	str = "	movl %vf12, %vf12";
	reg = "%xmm0";
	r = REG(FLOAT_CLASS, 12);
	printf("%20s %s in [%s] with %s\n", "Replacing",
	       reg_string(r), str, reg);
	new = vreg_replace(str, r, reg);
	printf("%20s [%s]\n", "", new);
	if (new != str)
		free(new);

	return EXIT_SUCCESS;
}

/* vi: set tabstop=8 textwidth=72: */
