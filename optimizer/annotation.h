/**
 * \file annotation.h
 *
 * Declares functions to deal with annotated lines of compiler output.
 *
 * \author Ethan Burns
 * \date 2007-10-09
 */
#ifndef _ANNOTATION_H_
#define _ANNOTATION_H_

/** Character that distinguishes annotation lines from asm lines */
#define ANNOTATION_CHAR '!'

int is_annotation(const char *line);
int is_section(const char *line, char *name_buf, unsigned int len);
int is_function(const char *line, char *name_buf, unsigned int len);
int is_label(const char *line, char *name_buf, unsigned int len);
int is_cond_branch(const char *line, char *name_buf, unsigned int len);
int is_uncond_branch(const char *line, char *name_buf, unsigned int len);
int is_return(const char *line);
int is_use(const char *line, char *reg_buf, unsigned int len);
int is_def(const char *line, char *reg_buf, unsigned int len);
int is_alocals(const char *line, unsigned int *);
int is_dlocals(const char *line);
int is_comment(const char *line);

void get_use(const char *r, char *buf, unsigned int len);
void get_def(const char *r, char *buf, unsigned int len);

#endif /* !_ANNOTATION_H_ */
/* vi: set tabstop=8 textwidth=72: */
