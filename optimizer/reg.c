/**
 * \file vreg.c
 * Contains definitions for manipulating registers.
 * \author Ethan Burns
 * \date 2007-10-22
 */

#define _GNU_SOURCE

#include <err.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>

#include "reg.h"

/**
 * Returns the (soft) maximum size of a line of text.
 *
 * \return The max size of a line of text.  This is clearly the *soft*
 *         max, but that's not our problem.
 */
long get_linemax(void)
{
	long linemax;

	linemax = sysconf(_SC_LINE_MAX);
	if (linemax <= 0) {
		if (linemax < 0)
			warn("%s: sysconf(_SC_LINE_MAX) failed", __func__);
#if defined(LINE_MAX)
		warnx("%s: using LINE_MAX=%d", __func__, LINE_MAX);
		linemax = LINE_MAX;
#elif defined(_POSIX2_LINE_MAX)
		warnx("%s: using _POSIX2_LINE_MAX=%d",
		      __func__, _POSIX2_LINE_MAX);
		linemax = _POSIX2_LINE_MAX;
#else
		warnx("%s: using arbitrary value=255", __func__);
		linemax = 255;
#endif /* LINE_MAX */
	}

	return linemax;
}

/**
 * Allocates a new string that is the result of replacing the virtual
 * register (defined by its type and number) in the line string with
 * the register string.
 *
 * \param line The line string.
 * \param type The vreg type to replace.
 * \param num The vreg number to replace.
 * \param reg The register string.
 * \return A newly allocated string (to be free()d by the caller) that
 *         contains the result of the replacement.  If there are no
 *         changes, line is returned instead.
 *
 * \note This function calls itself recursivly on the line so that it
 *       can replace all instances of the given register.
 */
char *vreg_replace(const char *line,
                   struct reg r,
                   const char *reg)
{
	char s;
	char *copy, *nextret;
	char *buf;
	long linemax;
	unsigned int i, endind, bufind;
	struct reg tmp;

	linemax = get_linemax();
	buf = calloc(linemax, sizeof(*buf));
	if (!buf)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	/* make a copy so that we can destroy our string with no
 	 * worrying. */
	copy = strdup(line);
	if (!copy)
		err(EXIT_FAILURE, "%s: strdup failed", __func__);

	for (bufind = i = 0; i < strlen(line); i++) {
		if (line[i] == '%' && line[i+1] == 'v') {
			endind = strcspn(copy + i, " \t\f\r\n,)") + i;
			tmp = reg_from_string(copy + i);
			if (reg_eq(tmp, r)) {
				/* found a match, change it in our
 				 * buffer and returns on the new line */
				s = reg_suffix(copy + i);
				strcat(buf + bufind, reg_getsized(reg, s));
				strcat(buf + bufind, line + endind);
				nextret = vreg_replace(buf, r, reg);
				if (nextret != buf)
					free(buf);
				free(copy);
				return nextret;
			} else {
				/* not a match, copy it over and keep
 				 * looking */
				buf[bufind] = '%';
				bufind += 1;
			}
		} else {
			buf[bufind] = line[i];
			bufind += 1;
		}
	}
	free(copy);
	free(buf);

	/* no matches found, just return what we were given */
	return (char*) line;
}

/* vi: set tabstop=8 textwidth=72: */
