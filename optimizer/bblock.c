/**
 * \file bblock.c
 * Contains declarations for the basic block list datastructure.
 * \author Ethan Burns
 * \date 2007-10-09
 */
#define _GNU_SOURCE

#include <err.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "annotation.h"
#include "bblock.h"
#include "bitset.h"
#include "list.h"
#include "reg.h"
#include "spill.h"

#if 0
#define DEBUG_BBLOCKS
#endif

#if defined(DEBUG_BBLOCKS)
#define dprintf(...) 					\
	do {						\
		fprintf(stdout, "%s:%d, %s: ",		\
		        __FILE__, __LINE__, __func__);	\
		fprintf(stdout, __VA_ARGS__);		\
	} while (0)
#else
#define dprintf(...)
#endif

static struct func *new_func(struct list *);
static void free_func(struct func *);
static struct bblock *new_bblock(struct func *);
static void free_bblock(struct bblock *);
static void free_line(struct line *);
static void bblock_set_name(struct bblock *, const char *);
static void bblock_add_line(struct bblock *, const char *);
static void bblock_add_arc(struct bblock *, struct bblock *);
static struct bblock *bblock_lookup_by_name(struct list *, const char *);
static void print_bblock(FILE *, struct bblock *);
static void link_bblock(struct bblock *);
static void track_reg_maxes(struct func *, const char *);
static void link_bblocks(struct list *);
static unsigned int block_cost(struct bblock *);
static void bblock_compute_local(struct bblock *, char *, long,
                                 struct regset *);
static int bblock_sets_step(struct bblock *, struct regset *);
static void bblock_compute_liveout(struct bblock *);
void reg_graph_update(struct reg_graph *g, struct regset *in,
                       const char *reg);
static void func_wipe_slate(struct func *);
static void bblock_wipe_slate(struct bblock *);
static void func_map_regs_on_line(struct func *, struct line *);
static int reg_spilled(struct func *, struct reg);

/**
 * Allocates a few function and adds it to the given list
 *
 * \param head The head of the function list.
 *
 * \return The newly allocated function.
 */
static struct func *new_func(struct list *head)
{
	int i;
	struct func *f;

	f = calloc(1, sizeof(*f));
	if (!f)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	list_init_head(&f->blocks);
	list_init_head(&f->loops);

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		f->reg_maxes[i] = -1;

	if (head)
		list_add_tail(head, new_list(f));

	return f;
}

/**
 * Frees the memory associated with a function structure.
 *
 * \param f The function to free.
 */
static void free_func(struct func *f)
{
	int i;
	struct list *node, *next;

	if (f->name)
		free((char *) f->name);

	list_for_each (node, next, &f->loops) {
		free_loop(node->data);
		list_free(list_delete(node));
	}

	for (i = 0; i < REG_CLASS_NUM; i += 1) {
		if (f->i_graphs[i])
			free_reg_graph(f->i_graphs[i]);
		if (f->to_spill[i]) {
			list_for_each (node, next, f->to_spill[i]) {
				list_delete(node);
				delete_rg_node(node->data);
				list_free(node);
			}
		}
		if (f->spilled[i]) {
			list_for_each (node, next, f->spilled[i]) {
				list_delete(node);
				delete_rg_node(node->data);
				list_free(node);
			}
		}
	}
	free(f);
}

/**
 * Sets the name of this function.
 *
 * \param f The function.
 * \param name The name.
 */
void func_set_name(struct func *f, const char *name)
{
	if (f->name) {
		errx(EXIT_FAILURE, "%s: function already has a name",
		     __func__);
	}

	f->name = strdup(name);
	if (!f->name)
		err(EXIT_FAILURE, "%s: strdup failed", __func__);
}

/**
 * Creates a new basic block and gives it a unique ID and adds it to the
 * given function.
 *
 * \param f The function this basic block belongs to.
 *
 * \return Returns the new basic block structure.
 */
static struct bblock *new_bblock(struct func *f)
{
	struct bblock *b;

	b = calloc(1, sizeof(*b));
	if (!b)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	if (f->nextbblock + 1 == 0)
		errx(EXIT_FAILURE, "%s: nextid overflow", __func__);
	b->id = f->nextbblock;
	b->func = f;
	dprintf("new basic block %d\n", b->id);
	f->nextbblock += 1;
	list_init_head(&b->lines);
	list_init_head(&b->inarcs);
	list_init_head(&b->outarcs);
	regset_init(&b->in);
	regset_init(&b->kill);
	regset_init(&b->live);
	regset_init(&b->liveout);
	b->dominators = new_bitset();

	list_add_tail(&f->blocks, new_list(b));

	return b;
}

/**
 * Frees the memory allocated for a basic block
 *
 * \param block The block to free.
 */
static void free_bblock(struct bblock *block)
{
	struct list *node, *next;

	delete_bitset(block->dominators);

	list_for_each(node, next, &block->lines) {
		list_delete(node);
		free_line(node->data);
		list_free(node);
	}

	list_for_each(node, next, &block->inarcs) {
		list_delete(node);
		list_free(node);
	}

	list_for_each(node, next, &block->outarcs) {
		list_delete(node);
		list_free(node);
	}

	regset_destroy(&block->in);
	regset_destroy(&block->kill);
	regset_destroy(&block->live);
	regset_destroy(&block->liveout);

	free((void *) block->name);
	free(block);
}

/**
 * Frees a line.
 *
 * \param l The line to free.
 */
static void free_line(struct line *l)
{
	free((char*) l->line);
	regset_destroy(&l->live);
	free(l);
}

/**
 * Allocates a new line.
 *
 * \param text The text of the line.
 * \return The newly allocated line.
 */
struct line *new_line(const char *text)
{
	struct line *l;

	l = calloc(1, sizeof(*l));
	if (!l)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);
	regset_init(&l->live);
	l->line = strdup(text);
	if (!l->line)
		err(EXIT_FAILURE, "%s: strdup failed", __func__);
	return l;
}

/**
 * Frees all basic blocks for all functions.
 */
void free_bblocks(struct list *funcs)
{
	struct list *fnode, *fnext;
	struct list *bnode, *bnext;
	struct func *f;

	list_for_each(fnode, fnext, funcs) {
		f = fnode->data;
		list_for_each(bnode, bnext, &f->blocks) {
			list_delete(bnode);
			free_bblock(bnode->data);
			list_free(bnode);
		}
		list_delete(fnode);
		free_func(fnode->data);
		list_free(fnode);
	}
}

/**
 * Sets the name of the given basic block.
 * \param block The block to set the name of.
 * \param name The name to give the basic block.
 */
static void bblock_set_name(struct bblock *block, const char *name)
{
	if (!block)
		errx(EXIT_FAILURE, "%s: passed a NULL block", __func__);
	block->name = strdup(name);
	dprintf("basic block %d name set to [%s]\n", block->id, name);
	if (!block->name)
		err(EXIT_FAILURE, "%s: strdup failed", __func__);
}

/**
 * Adds a line of text to the basic block.
 * \param block The basic block to add the line to.
 * \param text The text-line to add to the block.
 */
static void bblock_add_line(struct bblock *block, const char *text)
{
	struct line *l;

	if (!block)
		errx(EXIT_FAILURE, "%s: passed a NULL block", __func__);
	l = new_line(text);
	list_add_tail(&block->lines, new_list(l));
}

/**
 * Links two basic blocks with an arc.
 *
 * \param out The basic block needing an outgoing arc.
 * \param in The basic block needing an incoming arc.
 * \note If the arc already exists a warning message is printed and no
 *       arcs are added.
 */
static void bblock_add_arc(struct bblock *out, struct bblock *in)
{
	struct list *node, *next;

	list_for_each(node, next, &out->outarcs) {
		if (node->data == in) {
			warnx("%s: arc %d --> %d already exists\n",
			      __func__, out->id, in->id);
			return;
		}
	}

	list_for_each(node, next, &in->inarcs) {
		if (node->data == out) {
			warnx("%s: arc %d <-- %d already exists\n",
			      __func__, in->id, out->id);
			return;
		}
	}

	list_add_tail(&out->outarcs, new_list(in));
	list_add_tail(&in->inarcs, new_list(out));
}

/**
 * Looks up a basic block by its unique ID.
 * \param head The head of the list of basic blocks for the lookup.
 * \param id The ID to use for the lookup.
 * \return The basic block with the given ID, NULL if not found.
 */
struct bblock *bblock_lookup_by_id(struct list *head, unsigned int id)
{
	struct list *node, *next;
	struct bblock *block;

	list_for_each(node, next, head) {
		block = node->data;
		if (block->id == id)
			return block;
	}

	return NULL;
}

/**
 * Looks up a basic block by its name.
 * \param head The head of the list of basic blocks for the lookup.
 * \param name The name to use for the lookup.
 * \return The basic block with the given name, NULL if not found.
 */
static struct bblock *bblock_lookup_by_name(struct list *head, const char *name)
{
	struct list *node, *next;
	struct bblock *block;

	list_for_each(node, next, head) {
		block = node->data;
		if (block->name && strcmp(block->name, name) == 0)
			return block;
	}

	return NULL;
}

/**
 * Process a single line of input.
 *
 * \param func The fucntion being processed.
 * \param block The current basic block.
 * \param line The line of text to process.
 * \param buf A pre-allocated buffer that can be used to read annotation
 *            data into.
 * \param buf_len The length of buf.
 * \return Returns the current basic block being read.  This basic block
 *         may be different than the one passed in as a param.  NULL is
 *         returned when the last line of the given function has been
 *         processed.
 * \note Branch annotation lines are left in the basic block so that
 *       the graph can be linked later on.
 * \note This step links basic blocks that are adjacent via fall-thrus
 *       (conditional branches and labels).
 */
struct bblock *process_line(struct func *func, struct bblock *block,
                            const char *line, char *buf, long buf_len)
{
	struct bblock *retval;

	retval = block;
	if (is_function(line, buf, buf_len)) {
		func_set_name(func, buf);
	} else if (is_label(line, buf, buf_len)) {
		dprintf("label [%s]\n", buf);
		if (!list_empty(&block->lines)) {
			retval = new_bblock(func);
			bblock_set_name(retval, buf);
			/* add fall thru arc */
			bblock_add_arc(block, retval);
		} else {
			/*
 			 * this is already a fresh, new basic block.
 			 * this happens if we have a branch followed by
 			 * a label, or two labels in a row somehow.
 			 */
			bblock_set_name(retval, buf);
		}
	} else if (is_cond_branch(line, buf, buf_len)) {
		dprintf("conditional branch [%s]\n", buf);
		bblock_add_line(block, line);
		retval = new_bblock(func);
		/* add fall thru arc */
		bblock_add_arc(block, retval);
	} else if (is_uncond_branch(line, buf, buf_len)) {
		dprintf("unconditional branch [%s]\n", buf);
		bblock_add_line(block, line);
		retval = new_bblock(func);
	} else if (is_return(line)) {
		dprintf("return\n");
		retval = NULL;
	} else {
		if (is_use(line, buf, buf_len))
			track_reg_maxes(block->func, buf);
		else if (is_def(line, buf, buf_len))
			track_reg_maxes(block->func, buf);
		dprintf("line processed [%s]\n", line);
		bblock_add_line(block, line);
	}

	return retval;
}

/**
 * Reads the given file and builds a list of basic blocks using the
 * annotation added by the compiler.
 *
 * \param infile The input file stream.
 */
void build_bblocks(FILE *infile, struct list *funcs)
{
	struct bblock *block;
	struct func *func;
	long linemax;
	char *line, *buf;

	linemax = get_linemax() + 1;
	line = calloc(linemax, sizeof(*line));
	if (!line)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	buf = calloc(linemax, sizeof(*buf));
	if (!buf)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	func = new_func(funcs);
	block = new_bblock(func);
	do {
		if (!fgets(line, linemax, infile)) {
			if (ferror(infile))
				err(EXIT_FAILURE, "%s: fgets failed",
			            __func__);
			else if (feof(infile))
				break;
		}
		line[strlen(line) - 1] = '\0';
		block = process_line(func, block, line, buf, linemax);
		if (!block) {
			func = new_func(funcs);
			block = new_bblock(func);
		}
	} while (!feof(infile));

	free(line);

	link_bblocks(funcs);
	build_loops(funcs);

	free(buf);
}

/**
 * Mapped over a bitset to print the dominators for the given set.
 *
 * \param i The basic block ID that dominates the current block.
 * \param aux A void* containing the FILE*  for the output file.
 */
static void __print_dominators(unsigned int i, void *aux)
{
	FILE *outfile;

	outfile = aux;
	fprintf(outfile, "%d ", i);
}

/**
 * Prints a single basic block to the given output file.
 *
 * \param block The basic block to print.
 * \param outfile The output file to print to.
 */
static void print_bblock(FILE *outfile, struct bblock *block)
{
	struct line *l;
	struct list *node, *next;

	fprintf(outfile, "-------------------- %d --------------------\n",
	        block->id);
	if (block->name)
		fprintf(outfile, "%15s: %s\n", "name", block->name);

	fprintf(outfile, "%15s: ", "in-bound arcs");
	list_for_each(node, next, &block->inarcs)
		fprintf(outfile, "%d ", ((struct bblock *) node->data)->id);
	fprintf(outfile, "\n%15s: ", "out-bound arcs");

	list_for_each(node, next, &block->outarcs)
		fprintf(outfile, "%d ", ((struct bblock *) node->data)->id);

#if defined(DEBUG_BBLOCKS)
	fprintf(outfile, "\n%15s: ", "IN");
	regset_print(outfile, &block->in, 18);

	fprintf(outfile, "\n%15s: ", "KILL");
	regset_print(outfile, &block->kill, 18);

	fprintf(outfile, "\n%15s: ", "LIVE");
	regset_print(outfile, &block->live, 18);

	fprintf(outfile, "\n%15s: ", "LIVEOUT");
	regset_print(outfile, &block->liveout, 18);
#endif /* DEBUG_BBLOCKS */

	fprintf(outfile, "\n%15s: ", "dominators");
	bitset_map(block->dominators, __print_dominators, outfile);

	fprintf(outfile, "\n%15s: %d", "loop depth", block->loopdepth);

	fprintf(outfile, "\n\n");
	list_for_each(node, next, &block->lines) {
		l = node->data;
		fprintf(outfile, "->");
		regset_print(outfile, &l->live, 3);
		fprintf(outfile, "\n  %s\n", l->line);
	}
	fprintf(outfile, "->");
	regset_print(outfile, &block->liveout, 3);
	fprintf(outfile, "\n");
}

/**
 * Prints the given function to the given output file.
 *
 * \param outfile The output file to print to.
 * \param f The function to print.
 */
void print_func(FILE *outfile, struct func *f)
{
	int i;
	struct list *node, *next;

	fprintf(outfile, "==================== %s() "
	        "====================\n",
	        f->name ? f->name : "<unknown>");

	fprintf(outfile, "\nInteference graph:\n");
	fprintf(outfile, "------------------\n");

	for (i = 0; i < REG_CLASS_NUM; i += 1) {
		if (f->i_graphs[i])
			rg_print(outfile, f->i_graphs[i], i);

		printf("%s() Spills:\n", f->name);
		if (f->spilled[i]) {
			list_for_each (node, next, f->spilled[i]) {
				struct rg_node *n;
				n = node->data;
				printf("%s ", reg_string(n->reg));
			}
		}
		printf("\n");
	}
	fprintf(outfile, "\n");

	fprintf(outfile, "loops:\n");
	list_for_each (node, next, &f->loops)
		print_loop(outfile, node->data);

	list_for_each (node, next, &f->blocks)
		print_bblock(outfile, node->data);
}

/**
 * Prints the list of basic blocks.
 *
 * \param outfile The output file to print to.
 */
void print_bblocks(FILE *outfile, struct list *funcs)
{
	struct list *node, *next;

	list_for_each(node, next, funcs)
		print_func(outfile, node->data);
}

/**
 * Finds all branches in the given block and adds arcs to each of these
 * adjacent block.  The branch annotations are then removed.
 *
 * \param block The block to link.
 *
 * \note This function does not link fall-thrus, these are done already
 *       when the basic block list is built.
 */
static void link_bblock(struct bblock *block)
{
	struct line *l;
	struct list *node, *next;
	struct bblock *b;
	long linemax;
	char *buf;

	linemax = get_linemax();
	buf = calloc(linemax, sizeof(*buf));
	if (!buf)
		err(EXIT_FAILURE, "%s calloc failed", __func__);

	list_for_each(node, next, &block->lines) {
		l = node->data;
		if (is_uncond_branch(l->line, buf, linemax)
		    || is_cond_branch(l->line, buf, linemax)) {
			b = bblock_lookup_by_name(&block->func->blocks, buf);
			if (!b) {
				warnx("%s: branch destination [%s] not found",
				      __func__, buf);
			} else {
				bblock_add_arc(block, b);
			}
		}
	}

	free(buf);
}

/**
 * Tracks the maximum virtual register for each class in the source
 * file.
 *
 * \param reg The register string.
 */
static void track_reg_maxes(struct func *f, const char *reg)
{
	struct reg r;
	long max;

	r = reg_from_string(reg);
	max = f->reg_maxes[r.class];

	if (r.number > max) {
		dprintf("%s(): reg_maxes[%d] %ld->%ld\n", f->name,
		        r.class, f->reg_maxes[r.class], r.number);
		f->reg_maxes[r.class] = r.number;
	}
}

/**
 * Builds the flow control graph by linking all of the basic blocks.
 *
 * \note The BRANCH and RETURN annotation lines are removed by this
 *       function.
 */
static void link_bblocks(struct list *funcs)
{
	struct list *fnode, *fnext;
	struct func *f;
	struct list *bnode, *bnext;

	list_for_each(fnode, fnext, funcs) {
		f = fnode->data;
		list_for_each(bnode, bnext, &f->blocks)
			link_bblock(bnode->data);
	}
}

/**
 * Computes the sets associated with a basic block.
 *
 * \param buf A temporary buffer big enough for a virtual register name.
 * \param buflen The length of the temporary buffer.
 * \return 0 on success, !0 on if we were unsuccessful at coloring the
 *         graph, in which case spill code was inserted and we need to
 *         re-build the graph.
 */
int build_graph(struct list *funcs)
{
	int i, j;
	int change;
	long linemax;
	struct list *fnode, *fnext;
	struct list *bnode, *bnext;
	struct func *f;
	struct bblock *b;
	struct regset tmp;

	char *buf;

	linemax = get_linemax();
	buf = calloc(linemax, sizeof(*buf));
	if (!buf)
		err(EXIT_FAILURE, "%s calloc failed", __func__);

	list_for_each(fnode, fnext, funcs) {
		f = fnode->data;

		/* make a temporary set to pass around for intermediate
		 * set computation */
		regset_init(&tmp);

		/* init the sets and locally compute IN and KILL */
		list_for_each(bnode, bnext, &f->blocks) {
			b = bnode->data;
			bblock_compute_local(b, buf, linemax, NULL);
		}

		/* initialize LIVE to IN */
		list_for_each(bnode, bnext, &f->blocks) {
			b = bnode->data;
			regset_copy(&b->in, &b->live);
		}

		/* iteratively compute LIVE sets */
#if defined(DEBUG_BBLOCKS)
		i = 0;
#endif /* DEBUG_BBLOCKS */
		do {
			change = 0;
			list_for_each(bnode, bnext, &f->blocks)
				change |= bblock_sets_step(bnode->data, &tmp);
#if defined(DEBUG_BBLOCKS)
			i += 1;
			dprintf("after iteration(%d) change=%d\n", i, change);
#endif /* DEBUG_BBLOCKS */

		} while (change);

		/* compute LIVEOUT sets */
		list_for_each(bnode, bnext, &f->blocks) {
			b = bnode->data;
			bblock_compute_liveout(b);
		}

		/* allocate the inteference graph */
		for (i = 0; i < REG_CLASS_NUM; i += 1) {
			f->i_graphs[i] = new_reg_graph();
			for (j = 0; j <= f->reg_maxes[i]; j += 1) {
				dprintf("Adding reg to graph %s\n",
				         reg_string(REG(i, j)));
				rg_add_new_node(f->i_graphs[i], REG(i, j),
				  reg_spilled(f, REG(i,j)) ? INFINITY : 0.0);
			}
		}

		/* re-compute the LIVE set for *every* line in each
 		 * block using LIVEOUT */
		list_for_each(bnode, bnext, &f->blocks) {
			b = bnode->data;
			bblock_compute_local(b, buf, linemax, &b->liveout);
		}

		regset_destroy(&tmp);

		for (i = 0; i < REG_CLASS_NUM; i += 1) {
			f->to_spill[i] = rg_color(f->i_graphs[i],
			                       phys_regs_in_class(i));
		}
	}

	free(buf);

	return do_spills(funcs);
}

/**
 * Computes the spill cost of any given line in a basic block.
 *
 * \param b The block.
 * \return The estimated spill cost.
 */
static unsigned int block_cost(struct bblock *b)
{
	if (b->loopdepth > 0)
		return ITER_CNT_EST * b->loopdepth;
	else
		return 1;
}

/**
 * Computes the IN set of a basic block, also computes the KILL set.  If
 * the function that this basic block belongs to has a non-NULL i_graph
 * field, the inteference graph is updated with each IN set update.
 *
 * \param b The basic block.
 * \param buf A temporary buffer that can hold register names from
 *            annotation lines.
 * \param buflen The length of the buffer.
 * \param liveout The liveout set for this basic block, or NULL if the
 *                liveout set was not yet computed.
 */
static void bblock_compute_local(struct bblock *b, char *buf, long buflen,
                                 struct regset *liveout)
{
	struct reg r;
	struct line *l;
	struct regset *last_live;
	struct list *node, *prev;

	if (liveout)
		regset_copy(liveout, &b->in);
	else
		regset_clear(&b->in);

	regset_clear(&b->kill);
	last_live = NULL;

	list_for_each_reverse(node, prev, &b->lines) {
		l = node->data;
		if (is_use(l->line, buf, buflen)) {
			regset_add(&b->in, buf);
			r = reg_from_string(buf);
			if (b->func->i_graphs[r.class]) {
				reg_graph_update(b->func->i_graphs[r.class],
						  &b->in, buf);
				rg_update_node_cost(b->func->i_graphs[r.class],
				                    r, block_cost(b));
			}
			if (last_live) {
				/* if we have a last_live, add to it */
				regset_add(last_live, buf);
			}
		} else if (is_def(l->line, buf, buflen)) {
			if (liveout && !regset_contains(&b->in, buf)) {
				/* def of a register never used. */
				list_delete(node);
				free_line(node->data);
				list_free(node);
				dprintf("%s: removing line [%s]\n", __func__,
				        ((struct line *) prev->data)->line);
				prev = prev->prev;
				list_delete(node->prev);
				free_line(node->prev->data);
				list_free(node->prev);
				continue;
			}
			regset_add(&b->kill, buf);
			regset_del(&b->in, buf);
			r = reg_from_string(buf);
			if (b->func->i_graphs[r.class]) {
				rg_update_node_cost(b->func->i_graphs[r.class],
				                    r, block_cost(b));
			}
		} else if (!is_annotation(l->line)) {
			/* start the live set with the currnet IN */
			regset_copy(&b->in, &l->live);
			last_live = &l->live;
		}
	}
}

/**
 * Computes a single step in the iterative reverse dataflow analysis for
 * the basic block's LIVE set.
 *
 * \param b The basic block.
 * \param tmp A scratch regset.
 * \return Returns true if there was a change made, or else it returns
 *         false.
 */
static int bblock_sets_step(struct bblock *b, struct regset *tmp)
{
	int change;
	struct list *node, *next;
	struct bblock *successor;

	change = 0;

	/* LIVE(b) = IN(b) U (LIVE(x) - KILL(b)), x in successor(b) */
	list_for_each(node, next, &b->outarcs) {
		successor = node->data;

		/* LIVE(x) - KILL(b) */
		regset_subtract(&successor->live, &b->kill, tmp);
		regset_union(tmp, &b->live, tmp);

		if (!regset_eq(&b->live, tmp)) {
			regset_copy(tmp, &b->live);
			dprintf("setting change\n");
			change |= 1;
		}
	}

	return change;
}

/**
 * Computes the LIVEOUT set for a basic block using the LIVE sets for
 * the successor blocks.
 *
 * \param b The basic block.
 */
static void bblock_compute_liveout(struct bblock *b)
{
	struct list *node, *next;
	struct bblock *successor;

	list_for_each(node, next, &b->outarcs) {
		successor = node->data;
		regset_union(&b->liveout, &successor->live, &b->liveout);
	}
}

/**
 * This block is used to add arcs to all registers in the IN set for a
 * given line.
 */
struct __reg_arc_aux {
	/** The register */
	struct reg reg;
	/** The graph used for adding arcs */
	struct reg_graph *graph;
};

/**
 * Function that is mapped across IN sets to add an arc to each register
 * in the IN set to a new register.
 *
 * \param r The register.
 * \param aux A void* containing a struct __reg_arc_aux data.
 */
static void __reg_arc_update(struct reg r, void *aux)
{
	struct __reg_arc_aux *data;

	data = aux;
	if (data->reg.number != r.number && data->reg.class == r.class)
		rg_add_arc(data->graph, r, data->reg);
}

/**
 * Updates the inteference graph by adding an arc for everything in the
 * IN set with the newly added register.
 *
 * \param g The graph.
 * \param in The current IN set.
 * \param reg The register string.
 */
void reg_graph_update(struct reg_graph *g, struct regset *in,
                       const char *reg)
{
	struct __reg_arc_aux data;

	data.reg = reg_from_string(reg);
	data.graph = g;
	regset_map(in, __reg_arc_update, &data);
}

/**
 * Clears all sets, and graphs.  Leaves only the list of
 * functions/blocks/lines.  This can be used so that we can add spill
 * code, wipe_slate() and then re-compute the inteference graph.
 *
 * \param funcs The list of functions.
 */
void wipe_slate(struct list *funcs)
{
	struct list *node, *next;

	list_for_each (node, next, funcs)
		func_wipe_slate(node->data);
}

/**
 * Clears all sets, and graphs.  Leaves only the list of
 * functions/blocks/lines.  This can be used so that we can add spill
 * code, wipe_slate() and then re-compute the inteference graph.
 *
 * \param f The function.
 */
static void func_wipe_slate(struct func *f)
{
	unsigned int i;
	struct list *node, *next;

	for (i = 0; i < REG_CLASS_NUM; i += 1) {
		if (!f->to_spill[i])
			continue;

		if (f->spilled[i])
			list_splice(f->spilled[i], f->to_spill[i]);
		else
			f->spilled[i] = f->to_spill[i];

		f->to_spill[i] = NULL;
		free_reg_graph(f->i_graphs[i]);
		f->i_graphs[i] = NULL;
	}

	list_for_each (node, next, &f->blocks)
		bblock_wipe_slate(node->data);
}

/**
 * Clears all sets, and graphs.  Leaves only the list of
 * functions/blocks/lines.  This can be used so that we can add spill
 * code, wipe_slate() and then re-compute the inteference graph.
 *
 * \param b The block.
 */
static void bblock_wipe_slate(struct bblock *b)
{
	struct line *l;
	struct list *node, *next;

	regset_destroy(&b->kill);
	regset_init(&b->kill);
	regset_destroy(&b->in);
	regset_init(&b->in);
	regset_destroy(&b->live);
	regset_init(&b->live);
	regset_destroy(&b->liveout);
	regset_init(&b->liveout);

	list_for_each (node, next, &b->lines) {
		l = node->data;
		regset_destroy(&l->live);
		regset_init(&l->live);
	}
}

/**
 * Maps the virtual registers to physical registers and does register
 * replacement.
 *
 * \param funcs The list of functions.
 */
void map_registers(struct list *funcs)
{
	long linemax;
	char *buf;
	struct line *l;
	struct bblock *b;
	struct func *f;
	struct list *lnode, *lnext;
	struct list *bnode, *bnext;
	struct list *fnode, *fnext;
	unsigned int text;

	linemax = get_linemax();
	buf = calloc(linemax, sizeof(*buf));
	if (!buf)
		err(EXIT_FAILURE, "%s calloc failed", __func__);

	text = 0;
	list_for_each (fnode, fnext, funcs) {
		f = fnode->data;
		list_for_each (bnode, bnext, &f->blocks) {
			b = bnode->data;
			list_for_each (lnode, lnext, &b->lines) {
				l = lnode->data;
				if (is_section(l->line, buf, linemax)) {
					if (strcmp(buf, "text") == 0)
						text = 1;
					else
						text = 0;
				} else if (text
				           && !is_annotation(l->line)
				           && !is_comment(l->line)) {
					func_map_regs_on_line(f, l);
				}
			}
		}
	}

	free(buf);
}

/**
 * Maps the virtual registers to real registers on the given line.
 *
 * \param f The function.
 * \param l The line.
 */
static void func_map_regs_on_line(struct func *f, struct line *l)
{
	unsigned int i;

	for (i = 0; i < REG_CLASS_NUM; i += 1)
		rg_map_regs(f->i_graphs[i], l);
}

/**
 * Dumps the assembly code out to a file.
 *
 * \param outfile The output file.
 * \param funcs The function list.
 */
void dump_code(FILE *outfile, struct list *funcs)
{
	struct func *f;
	struct bblock *b;
	struct line *l;
	struct list *fnode, *fnext, *bnode, *bnext, *lnode, *lnext;

	list_for_each (fnode, fnext, funcs) {
		f = fnode->data;
		list_for_each (bnode, bnext, &f->blocks) {
			b = bnode->data;
			list_for_each (lnode, lnext, &b->lines) {
				l = lnode->data;
				if (is_annotation(l->line))
					continue;
				fprintf(outfile, "%s\n", l->line);
			}
		}
	}
}

/**
 * Check if a register has already been spilled.  We can't spill it
 * again.
 *
 * \param f The function.
 * \param r The register.
 * \return !0 if the register has been spilled, 0 if it has not.
 */
static int reg_spilled(struct func *f, struct reg r)
{
	struct rg_node *n;
	struct list *node, *next;

	if (!f->spilled[r.class])
		return 0;

	list_for_each (node, next, f->spilled[r.class]) {
		n = node->data;
		if (reg_eq(r, n->reg))
			return 1;
	}

	return 0;
}

/* vi: set tabstop=8 textwidth=72: */
