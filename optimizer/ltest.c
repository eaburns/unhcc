/**
 * \file ltest.c
 *
 * A basic driver to test the list implementation.
 *
 * \author Ethan Burns
 * \date 2007-10-09
 */

#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#include "list.h"

int main(void)
{
	struct list head, back;
	struct list *node, *next;

	list_init_head(&head);
	list_init_head(&back);

	list_add(&head, new_list("Two"));
	list_add(&head, new_list("One"));
	list_add_tail(&head, new_list("Three"));

	list_for_each(node, next, &head)
		printf("%s\n", (const char *) node->data);

	printf("\n");
	printf("\n");
	list_for_each_reverse(node, next, &head)
		printf("%s\n", (const char *) node->data);

	/* delete the second element */
	list_free(list_delete(head.next->next));
	printf("\n");
	printf("\n");
	list_for_each(node, next, &head)
		printf("%s\n", (const char *) node->data);

	list_add(&back, new_list("back1"));
	list_add_tail(&back, new_list("back2"));
	list_add_tail(&back, new_list("back3"));

	list_splice(&head, &back);
	printf("\n");
	printf("\nSplice:\n");
	list_for_each(node, next, &head)
		printf("%s\n", (const char *) node->data);

	list_for_each(node, next, &head) {
		list_delete(node);
		list_free(node);
	}

	if (!list_empty(&head)) {
		errx(EXIT_FAILURE, "List is not empty after "
		     "deleting everything");
	}

	return EXIT_SUCCESS;
}

/* vi: set tabstop=8 textwidth=72: */
