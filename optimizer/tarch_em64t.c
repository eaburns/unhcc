/**
 * \file optimizer/tarch_em64t.c
 * Contains function and data definitions for the EM64T architecture.
 * \author Ethan Burns
 * \date 2007-11-14
 */

#include <assert.h>
#include <ctype.h>
#include <err.h>
#include <stdlib.h>
#include <string.h>

#include "reg.h"

static int is_em64t_int(const char *reg, struct reg *buf);
static int is_em64t_float(const char *reg, struct reg *buf);
static const char *em64t_reg_to_string(struct reg r);

/**
 * Table of integer class register names.
 */
const char *int_reg_tab[][4] = {
/*
	{ "%rax", "%eax", "%ax", "%al" },
*/
	{ "%rbx", "%ebx", "%bx", "%bl" },
/*
	{ "%rcx", "%ecx", "%cx", "%cl" },
	{ "%rdx", "%edx", "%dx", "%dl" },
	{ "%rsi", "%esi", "%si", "%sil" },
	{ "%rdi", "%edi", "%di", "%dil" },
	{ "%r8", "%r8d", "%r8w", "%r8b" },
	{ "%r9", "%r9d", "%r9w", "%r9b" },
	{ "%r10", "%r10d", "%r10w", "%r10b" },
*/
	{ "%r11", "%r11d", "%r11w", "%r11b" },
	{ "%r12", "%r12d", "%r12w", "%r12b" },
/*
	{ "%r13", "%r13d", "%r13w", "%r13b" },
	{ "%r14", "%r14d", "%r14w", "%r14b" },
	{ "%r15", "%r15d", "%r15w", "%r15b" },
*/
};

/**
 * The number of elements in the integer register table.
 */
#define INT_REG_TAB_SIZE \
	(sizeof(int_reg_tab)/sizeof(int_reg_tab[0]))

/**
 * The names of the floating point registers.
 */
const char *float_reg_tab[] = {
/*
	"%xmm0",
	"%xmm1",
	"%xmm2",
	"%xmm3",
	"%xmm4",
	"%xmm5",
	"%xmm6",
	"%xmm7",
*/
	"%xmm8",
	"%xmm9",
	"%xmm10",
	"%xmm11",
	"%xmm12",
	"%xmm13",
	"%xmm14",
	"%xmm15",
};

/**
 * The number of elements in the floating point register table.
 */
#define FLOAT_REG_TAB_SIZE \
	(sizeof(float_reg_tab)/sizeof(float_reg_tab[0]))


/**
 * Returns the register string for the size of the given register based
 * on the given suffix.
 *
 * \param reg The given reg.
 * \param suffix The suffix.
 * \return A constant string representing the given register of the
 *         given size.
 * \note This function *can* return static memory.
 */
const char *reg_getsized(const char *reg, char suffix)
{
	static char buf[REG_STRING_MAX];

	if (suffix == '\0') {
		return reg;
	} else if (strcmp(reg, "%rax") == 0) {
		switch (suffix) {
		case 'b': return "%al";
		case 'w': return "%ax";
		case 'd': return "%eax";
		default: errx(EXIT_FAILURE, "%s: Invalid suffix [%c]\n",
		              __func__, suffix);
		}
	} else if (strcmp(reg, "%rbx") == 0) {
		switch (suffix) {
		case 'b': return "%bl";
		case 'w': return "%bx";
		case 'd': return "%ebx";
		default: errx(EXIT_FAILURE, "%s: Invalid suffix [%c]\n",
		              __func__, suffix);
		}
	} else if (strcmp(reg, "%rcx") == 0) {
		switch (suffix) {
		case 'b': return "%cl";
		case 'w': return "%cx";
		case 'd': return "%ecx";
		default: errx(EXIT_FAILURE, "%s: Invalid suffix [%c]\n",
		              __func__, suffix);
		}
	} else if (strcmp(reg, "%rdx") == 0) {
		switch (suffix) {
		case 'b': return "%dl";
		case 'w': return "%dx";
		case 'd': return "%edx";
		default: errx(EXIT_FAILURE, "%s: Invalid suffix [%c]\n",
		              __func__, suffix);
		}
	} else if (strcmp(reg, "%rsi") == 0) {
		switch (suffix) {
		case 'b': return "%sil";
		case 'w': return "%si";
		case 'd': return "%esi";
		default: errx(EXIT_FAILURE, "%s: Invalid suffix [%c]\n",
		              __func__, suffix);
		}
	} else if (strcmp(reg, "%rdi") == 0) {
		switch (suffix) {
		case 'b': return "%dil";
		case 'w': return "%di";
		case 'd': return "%edi";
		default: errx(EXIT_FAILURE, "%s: Invalid suffix [%c]\n",
		              __func__, suffix);
		}
	} else if (strcmp(reg, "%r8") == 0
	           || strcmp(reg, "%r9") == 0
	           || strcmp(reg, "%r10") == 0
	           || strcmp(reg, "%r11") == 0
	           || strcmp(reg, "%r12") == 0
	           || strcmp(reg, "%r13") == 0
	           || strcmp(reg, "%r14") == 0
	           || strcmp(reg, "%r15") == 0) {
		snprintf(buf, REG_STRING_MAX, "%s%c", reg, suffix);
		return buf;
	} else {
		errx(EXIT_FAILURE, "%s: Invalid register", __func__);
	}

	return NULL;
}

/**
 * Gets the suffix of the virtual regiseter.
 *
 * \param r The virtual register.
 * \return The suffix of the virtual register or '\0' if there is no suffix.
 */
char reg_suffix(const char *r)
{
	unsigned int i;

	for (i = 3; isdigit(r[i]); i++)
		;

	if (r[i] == 'd' || r[i] == 'w' || r[i] == 'b')
		return r[i];

	return '\0';
}

/**
 * Returns the string representation for the given register.
 *
 * \param r The register.
 * \return A static buffer containing the printable register string.
 * \note Not re-entrant.
 */
char *reg_string(struct reg r)
{
	static char buf[REG_STRING_MAX];

	if (r.number < 0) {
		snprintf(buf, REG_STRING_MAX, "%s", em64t_reg_to_string(r));
	} else {
		char c;

		if (r.class == INTEGER_CLASS) {
			c = 'd';
		} else if (r.class == FLOAT_CLASS) {
			c = 'f';
		} else {
			errx(EXIT_FAILURE, "%s: invalid register struct",
			    __func__);
		}

		snprintf(buf, REG_STRING_MAX, "%%v%c%ld", c, r.number);
	}

	return buf;
}

/**
 * Check if a given register is an em64t integer register.
 *
 * \param reg The register string.
 * \param buf The buffer used to return the register if successful.
 * \return !0 on success and buf contains the register structure, 0 if
 *         the given string is not an em64t integer register.
 */
static int is_em64t_int(const char *reg, struct reg *buf)
{
	unsigned int i, size;

	for (i = 0; i < INT_REG_TAB_SIZE; i += 1) {
		for (size = 0; size < 4; size += 1) {
			if (strcmp(reg, int_reg_tab[i][size]) == 0) {
				buf->class = INTEGER_CLASS;
				buf->number = -i;
				return 1;
			}
		}
	}

	return 0;
}

/**
 * Check if a given register is an em64t floating point register.
 *
 * \param reg The register string.
 * \param buf The buffer used to return the register if successful.
 * \return !0 on success and buf contains the register structure, 0 if
 *         the given string is not an em64t floating point register.
 */
static int is_em64t_float(const char *reg, struct reg *buf)
{
	unsigned int i;

	for (i = 0; i < FLOAT_REG_TAB_SIZE; i+=1) {
		if (strcmp(reg, float_reg_tab[i]) == 0) {
			buf->class = FLOAT_CLASS;
			buf->number = -i;
			return 1;
		}
	}

	return 0;
}

/**
 * Gets the string value of the given real register.
 *
 * \param r The register structure.
 * \return A constant string that represents the register.
 */
static const char *em64t_reg_to_string(struct reg r)
{
	if (r.class == INTEGER_CLASS) {
		assert(r.number < 0);
		assert(-r.number < INT_REG_TAB_SIZE);
		return int_reg_tab[-r.number][0];
	} else if (r.class == FLOAT_CLASS) {
		assert(r.number < 0);
		assert(-r.number < FLOAT_REG_TAB_SIZE);
		return float_reg_tab[-r.number];
	} else {
		return "<invalid class>";
	}
}

/**
 * Builds a register structure given a string version of a register.
 *
 * \param r_str The register string representation.
 * \return The register structure.
 */
struct reg reg_from_string(const char *r_str)
{
	struct reg r;

	if (is_em64t_int(r_str, &r) || is_em64t_float(r_str, &r)) {
		return r;
	} else {
		unsigned int i;

		/* must be a virtual register */
		if (r_str[2] == 'd') {
			r.class = INTEGER_CLASS;
		} else if (r_str[2] == 'f') {
			r.class = FLOAT_CLASS;
		} else {
			errx(EXIT_FAILURE, "%s: invalid register string",
			     __func__);
		}

		r.number = 0;
		for (i = 3; i < strlen(r_str) && isdigit(r_str[i]); i++) {
			r.number *= 10;
			r.number += r_str[i] - '0';
		}

		return r;
	}
}

/**
 * Allocates a string for a register load from the locals area at the
 * given offset from the base pointer.
 *
 * \param r The register.
 * \param bp_offs The base pointer offset.
 * \return A string that contains the register load instruction, this
 *         string must be free()d by the caller.
 */
const char *alloc_reg_load(struct reg *r, unsigned int bp_offs)
{
	long linemax;
	char *retval;

	linemax = get_linemax();
	retval = calloc(linemax, sizeof(*retval));
	if (!retval)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	if (r->class == INTEGER_CLASS) {
		snprintf(retval, linemax, "\tmovq\t-%u(%%rbp), %s",
		         bp_offs, reg_string(*r));
	} else if (r->class == FLOAT_CLASS) {
		snprintf(retval, linemax, "\tmovsd\t-%u(%%rbp), %s",
		         bp_offs, reg_string(*r));
	} else {
		errx(EXIT_FAILURE, "%s: invalid register", __func__);
	}

	return retval;
}

/**
 * Allocates a string for a register save to the locals area at the
 * given offset from the base pointer.
 *
 * \param r The register.
 * \param bp_offs The base pointer offset.
 * \return A string that contains the register save instruction, this
 *         string must be free()d by the caller.
 */
const char *alloc_reg_save(struct reg *r, unsigned int bp_offs)
{
	long linemax;
	char *retval;

	linemax = get_linemax();
	retval = calloc(linemax, sizeof(*retval));
	if (!retval)
		err(EXIT_FAILURE, "%s: calloc failed", __func__);

	if (r->class == INTEGER_CLASS) {
		snprintf(retval, linemax, "\tmovq\t%s, -%u(%%rbp)",
		         reg_string(*r), bp_offs);
	} else if (r->class == FLOAT_CLASS) {
		snprintf(retval, linemax, "\tmovsd\t%s, -%u(%%rbp)",
		         reg_string(*r), bp_offs);
	} else {
		errx(EXIT_FAILURE, "%s: invalid register", __func__);
	}

	return retval;
}

/**
 * Get the number of physical registers in the given class.
 *
 * \param c The class.
 * \return The number of physical registers in the given class.
 */
unsigned int phys_regs_in_class(enum reg_class c)
{
	if (c == INTEGER_CLASS)
		return INT_REG_TAB_SIZE;
	else if (c == FLOAT_CLASS)
		return FLOAT_REG_TAB_SIZE;
	else
		errx(EXIT_FAILURE, "%s: invalid class %d\n", __func__, c);

	/* never reached */
	return 0;
}

/**
 * Returns the string of the register name given the class and the
 * color.
 *
 * \param class The register class.
 * \param color The register color.
 * \return A constant that is the physical register string.
 */
const char *color_to_reg_str(enum reg_class class, long color)
{
	assert(color >= 0);

	if (class == INTEGER_CLASS) {
		assert(color < INT_REG_TAB_SIZE);
		return int_reg_tab[color][0];
	} else if (class == FLOAT_CLASS) {
		assert(color < FLOAT_REG_TAB_SIZE);
		return float_reg_tab[color];
	} else {
		errx(EXIT_FAILURE, "%s: invalid register class %d\n",
		     __func__, class);
	}

	return "<invalid register class>";
}

/* vi: set tabstop=8 textwidth=72: */
