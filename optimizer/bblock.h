/**
 * \file bblock.h
 *
 * A data structure for a basic block of ASM code.
 *
 * \author Ethan Burns
 * \date 2007-10-09
 */
#ifndef _BBLOCK_H_
#define _BBLOCK_H_

#include <stdio.h>

#include "list.h"
#include "reg.h"

/**
 * Estimated number of times a loop will itterate.  Used for estimating
 * register spill costs.
 */
#define ITER_CNT_EST 10

/**
 * A function structure, contains a list of basic blocks.
 */
struct func {
	/** The name of this function */
	const char *name;

	/** List of basic blocks */
	struct list blocks;

	/** Id of the next basic block in this function */
	unsigned int nextbblock;

	/** List of loops in this function */
	struct list loops;

	/** Virtual register class maxes */
	long reg_maxes[REG_CLASS_NUM];

	/** List of registers that have been spilled */
	struct list *spilled[REG_CLASS_NUM];

	/** List of registers that will need to be spilled */
	struct list *to_spill[REG_CLASS_NUM];

	/** The virtual register inteference graph */
	struct reg_graph *i_graphs[REG_CLASS_NUM];
};

/**
 * A basic block structure that contains a list of lines, a name and an id.
 *
 * A basic block can be addressed by either its name or its id.
 */
struct bblock {
	/** The basic block id number */
	unsigned int id;

	/** The function containing this block */
	struct func *func;

	/** The name of this basic block */
	const char *name;

	/** List of lines in this block */
	struct list lines;

	/** Incoming arcs on the flow graph */
	struct list inarcs;

	/** Outgoing arcs on the flow graph */
	struct list outarcs;

	/** The KILL register set for this block */
	struct regset kill;

	/** The IN register set for this block */
	struct regset in;

	/** The LIVE register set for this block */
	struct regset live;

	/** The LIVEOUT register set for this block */
	struct regset liveout;

	/** The set of basic blocks that dominate this bblock */
	struct bitset *dominators;

	/** Number of loops that this block resides in */
	unsigned int loopdepth;
};

/**
 * An input file line/live register set tuple.
 */
struct line {
	/** The registers live exactly before the line */
	struct regset live;
	/** The line from the input file */
	const char *line;
};

/**
 * Contains information about loops.
 */
struct loop {
	/** The ID of a loop (the id of the bblock that is the header) */
	unsigned int id;

	/** The function containing this loop */
	struct func *func;

	/** Set of blocks in this loop */
	struct bitset *blocks;
};

extern struct list bblocks;

void free_bblocks(struct list *);
void build_bblocks(FILE *, struct list *);
void build_loops(struct list *);
void free_loop(struct loop *);
void print_bblocks(FILE *, struct list *);
struct bblock *bblock_lookup_by_id(struct list *, unsigned int);
void print_loop(FILE *, struct loop *);
int build_graph(struct list *);
void wipe_slate(struct list *);
struct line *new_line(const char *);
void map_registers(struct list *);
void dump_code(FILE *, struct list *);

#endif /* _BBLOCK_H_ */
/* vi: set tabstop=8 textwidth=72: */
