/*
 * regAlloc.h - header file for IA-32 Intel register allocator
 *
 * Actually there is no register allocator. So this module is simply
 * used to track whether a subtree has been encoded or not. This
 * is important for implementing the DUP nodes. This file defines
 * two REG values: 0 (null register) and 1 (subtree visited). Remember
 * that REG is defined in defs.h and is just an int.
 */
#ifndef REGALLOC_H
#define REGALLOC_H

#if 0
#define VARTAB_DEBUG
#endif

#include "defs.h"
#include "tree.h"
#include "types.h"

#define DEF_STRING "DEF"
#define USE_STRING "USE"

/* max size of the string that represents the value of a register
 * descriptor */
#define REG_STRING_MAX	255

/* initialize a REG to "none" */
#define nullReg() (0)

/* initialize the register allocator: does nothing actually */
void initRegAlloc(void);

/* displays a register for debugging purposes. */
void printReg(REG r);

/* allocates the appropriate register type for the given tag type */
REG allocNextReg(TypeTag);

/* allocates a register handle that describes given format */
REG allocReg(TypeTag, const char *format, ...);

/* returns the string that represents a register.  this can be used in
 * the output code. */
const char *regString(REG handle);

const char *regStringAs(TypeTag type, REG handle);

/* returns true if the given handle describes an actual register. */
int isRegister(REG handle);

/* returns true if the given handle describes a constant value. */
int isConstant(REG handle);

/* returns true if the given handle describes a memory location. */
int isMemory(REG handle);

/* returns true if the given register is a temporary register.  */
int isTemp(REG handle);

/* returns the type of the given register. */
TypeTag regType(REG);

/* annotates the output code with the use of a given register. */
void useReg(REG);

/* annotates the output code with the definition of a given register. */
void defReg(REG);

/* get a temporary register if potReg is not temporary already */
REG getResultReg(REG potReg);

/* get a register if r is not a register already */
REG getRegister(REG r);

/* Set the regdesc's base register.  Used for memory registers that
 * 'use' a register as their base address. */
void regMemBaseReg(REG r, const char *, ...);

/*
 *
 * The variable table keeps track of which variables can be stored in
 * registers and which need to be left in memory.
 *
 * This requires some explanation:  the only disambiguating information
 * for varaibles is their offset and their block number and whether or
 * not they are a local or a parameter.  We *can not* use the symbol
 * table here since it is dead after the analysis phase. The symbol
 * table info is transient since it is pushed and poped as blocks are
 * analyzed.
 *
 */

/* adds a variable to the variable tab */
void vartabAdd(int offset, int block, STDR_TAG tag, Type type);

/*
 * marks a variable in the vartab as addressed.  This means that the
 * current function uses the address of this variable and it must not be
 * cached in a register.
 */
void vartabAddressed(int offset, int block, STDR_TAG tag);

/*
 * Returns true if the given variable is addressed in the current
 * function.
 */
int isAddressed(int offset, int block, STDR_TAG tag);

/* clears the variable table */
void vartabClear(void);

/* prints the variable table */
void vartabPrint(void);

/* sets the register descriptor for the given variable, also the notTemp
 * flag on the given register is set. */
void vartabSetReg(int offset, int block, STDR_TAG tag, REG);

/* allocates a register for all variables with out one */
void vartabAllocRegs(int sizeOfLocalsArea);

/*
 * Returns the register for the given variable.
 */
REG vartabGetReg(int offset, int block, STDR_TAG tag);

#endif
