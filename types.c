/*
 *
 *  Type module: manipulation of objects representing C types
 *
 *  Does not support bitfield types. Also does not support oldstyle
 *  function types that include parameter lists. (This means it does
 *  not work for oldstyle function definitions.)
 *
 *  Assumes that enum types will be mapped to an integer type outside
 *  of this module, so this module does not support enum types.
 */

#include "defs.h"

#include "types.h"

#include "message.h"

/*
 *  The following structs provide the details of the Type objects.
 *  The user view is simply a "void *" but these structs provide
 *  the internal view.
 */

typedef struct InternalType {	/* These fields are in all Type records 
				 */

	TypeTag tag;

	TypeQualifier qualifiers;

} *InternalType;

typedef struct InternalArrayType {

	struct InternalType base;

	DimFlag flag;

	unsigned int size;

	Type elementType;

} *InternalArrayType;

typedef struct InternalPointerType {

	struct InternalType base;

	Type baseType;

} *InternalPointerType;

typedef struct InternalFunctionType {

	struct InternalType base;

	ParamStyle style;

	ParamList list;

	Type returnType;

} *InternalFunctionType;

typedef struct InternalStructUnionType {

	struct InternalType base;

	struct IndirectStructUnionType *inner;

} *InternalStructUnionType;

/*
 *  struct/union types are complicated because they use
 *  name equivalence. A level of indirection is necessary
 *  in order to support qualified struct/union types. Need
 *  another node in which to store the qualifiers so that
 *  the struct/union details can be stored in one common
 *  node, as required by name equivalence.
 */
typedef struct IndirectStructUnionType {

	ST_ID tagname;

	MemberList list;

} *IndirectStructUnionType;

/*
 *  Allocate memory for a Type node.
 *
 *  Right now we just use malloc and we never free. (Types are forever.)
 *  This may need to be changed later.
 */
static
void *typeAllocate(size_t size)
{
	return malloc(size);
}

/*
 *  The following functions validate various enumeration types
 *  used by the Type module.
 *
 *  Should these functions be exported?
 */
static
int isValidQualifier(TypeQualifier qualifiers)
{
	switch (qualifiers) {
	case NO_QUAL:
	case CONST_QUAL:
	case VOLATILE_QUAL:
	case CONST_VOLATILE_QUAL:
		return 1;
	default:
		return 0;
	}
}

static
int isValidDimFlag(DimFlag flag)
{
	switch (flag) {
	case NO_DIMENSION:
	case DIMENSION_PRESENT:
		return 1;
	default:
		return 0;
	}
}

static
int isValidTypeTag(TypeTag tag)
{
	switch (tag) {
	case TYVOID:
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYERROR:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
	case TYPOINTER:
		return 1;

	default:
		return 0;
	}
}

static
int isValidParamStyle(ParamStyle style)
{
	switch (style) {
	case PROTOTYPE:
	case OLDSTYLE:
	case DOT_DOT_DOT:
		return 1;
	default:
		return 0;
	}
}

/***********************************************************************/

/*
 *  Initializes the Type module.
 */
void typeInitialize(void)
{
	/* nothing to do */
}

/*
 *  Returns the type category of a type: the "outermost" TypeTag.
 */
TypeTag typeQuery(Type type)
{
	InternalType t;

	t = type;

	if (t == NULL)
		bug("NULL Type passed to typeQuery");

	return t->tag;
}

/*
 *  This function is a helper function for areCompatibleTypes.
 *
 *  It checks a prototype parameter list to be sure the types could
 *  result from the default argument promotion.
 */
static
BOOLEAN checkOldstyleCompatibility(ParamList plist)
{
	while (plist != NULL) {
		switch (typeQuery(plist->type)) {
		case TYFLOAT:
		case TYSIGNEDSHORTINT:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDCHAR:
		case TYSIGNEDCHAR:
			return FALSE;

		case TYDOUBLE:
		case TYLONGDOUBLE:
		case TYSIGNEDLONGINT:
		case TYSIGNEDINT:
		case TYUNSIGNEDLONGINT:
		case TYUNSIGNEDINT:
		case TYSTRUCT:
		case TYUNION:
		case TYARRAY:
		case TYPOINTER:
		case TYFUNCTION:
			break;

		case TYVOID:
			bug("void type seen in checkOldstyleCompatibility");

		case TYERROR:
			bug("error type seen in checkOldstyleCompatibility");

		default:
			bug("unknown type seen in checkOldstyleCompatibility");
		}
		plist = plist->next;
	}
	return TRUE;
}

/*
 *  This function is a helper function for areCompatibleTypes. It
 *  is also used by typeFormComposite. It is non-static as it is
 *  also used in semantic analysis of functions.
 *
 *  It simulates argument promotion (ANSI 3.7.1) for array and function
 *  types. "array of T" becomes "pointer to T" and "function returning T"
 *  becomes "pointer to function returning T".
 */
Type promoteArrayOrFunction(Type type)
{
	if (typeQuery(type) == TYARRAY) {
		return typeBuildPointer(typeStripModifier(type),
					NO_QUAL);
	}

	if (typeQuery(type) == TYFUNCTION) {
		return typeBuildPointer(type, NO_QUAL);
	}

	/* leave other Types alone */
	return type;
}


/*
 *  Returns TRUE if the two Types are compatible and FALSE otherwise.
 */
BOOLEAN areCompatibleTypes(Type type1, Type type2)
{
	if (type1 == NULL || type2 == NULL)
		bug("NULL Type passed to areCompatibleTypes");

	if (typeQuery(type1) != typeQuery(type2))
		return FALSE;

	switch (typeQuery(type1)) {
	case TYVOID:
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYERROR:
		{
			InternalType t1;
			InternalType t2;

			t1 = type1;
			t2 = type2;
			return (t1->qualifiers == t2->qualifiers);
		}

	case TYSTRUCT:
	case TYUNION:
		{
			InternalStructUnionType t1;
			InternalStructUnionType t2;

			t1 = type1;
			t2 = type2;

			if (t1->base.qualifiers != t2->base.qualifiers)
				return FALSE;
			else
				return (t1->inner == t2->inner);
		}

	case TYARRAY:
		{
			InternalArrayType t1;
			InternalArrayType t2;

			t1 = type1;
			t2 = type2;

			if (!areCompatibleTypes
			    (t1->elementType, t2->elementType))
				return FALSE;

			if (t1->flag == DIMENSION_PRESENT
			    && t2->flag == DIMENSION_PRESENT)
				return (t1->size == t2->size);

			return TRUE;
		}

	case TYPOINTER:
		{
			InternalPointerType t1;
			InternalPointerType t2;

			t1 = type1;
			t2 = type2;

			if (t1->base.qualifiers != t2->base.qualifiers)
				return FALSE;

			return areCompatibleTypes(t1->baseType,
						  t2->baseType);
		}

	case TYFUNCTION:
		{
			InternalFunctionType t1;
			InternalFunctionType t2;

			t1 = type1;
			t2 = type2;

			if (!areCompatibleTypes
			    (t1->returnType, t2->returnType))
				return FALSE;

			if (t1->style == t2->style) {
				ParamList plist1;
				ParamList plist2;

				plist1 = t1->list;
				plist2 = t2->list;

				while ((plist1 != NULL)
				       && (plist2 != NULL)) {
					if (!areCompatibleTypes
					    (promoteArrayOrFunction
					     (typeUnqualifiedVersion
					      (plist1->type)),
					     promoteArrayOrFunction
					     (typeUnqualifiedVersion
					      (plist2->type)))) {
						return FALSE;
					}
					plist1 = plist1->next;
					plist2 = plist2->next;
				}

				/* ParamLists must be same length */
				return (plist1 == plist2);
			}

			if (t1->style == PROTOTYPE
			    && t2->style == DOT_DOT_DOT)
				return FALSE;

			if (t1->style == DOT_DOT_DOT
			    && t2->style == PROTOTYPE)
				return FALSE;

			if (t1->style == OLDSTYLE
			    && t2->style == DOT_DOT_DOT)
				return FALSE;

			if (t1->style == DOT_DOT_DOT
			    && t2->style == OLDSTYLE)
				return FALSE;

			if (t1->style == OLDSTYLE)
				return checkOldstyleCompatibility(t2->
								  list);

			if (t2->style == OLDSTYLE)
				return checkOldstyleCompatibility(t1->
								  list);

			bug("areCompatibleTypes: should not reach this point!");
		}

	default:
		bug("unknown types in areCompatibleTypes");
	}
	return FALSE;		/* won't reach */
}

/*
 *  If input type is a derived declarator type, then remove the "outermost"
 *  modifier and return the underlying type. If the input type
 *  is not a derived type, then return NULL.
 */
Type typeStripModifier(Type type)
{
	InternalType t;

	t = type;

	if (t == NULL)
		bug("NULL Type passed to typeStripModifier");

	switch (t->tag) {

	case TYARRAY:
		{
			DimFlag flag;
			unsigned int dim;

			return typeQueryArray(t, &flag, &dim);
		}

	case TYPOINTER:
		{
			TypeQualifier qual;

			return typeQueryPointer(t, &qual);
		}

	case TYFUNCTION:
		{
			ParamStyle style;
			ParamList list;

			return typeQueryFunction(t, &style, &list);
		}

	default:
		return NULL;

	}

	return NULL;
}

/*
 *  Builds a Type object for the given TypeTag. As well as the
 *  "basic" types, this routine also can be used to construct
 *  "void" type and an "error" type. Note that ANSI C says that
 *  "char" is a basic type, but this module assumes "char" will
 *  be mapped to either "unsigned char" or "signed char".
 */
Type typeBuildBasic(TypeTag tag, TypeQualifier qualifiers)
{
	if (!isValidQualifier(qualifiers))
		bug("illegal type qualifier in typeBuildBasic");

	switch (tag) {
	case TYVOID:
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYERROR:
		{
			InternalType ret;
			ret = typeAllocate(sizeof(struct InternalType));
			if (ret == NULL)
				fatal
				    ("out of memory in typeBuildBasic");
			ret->tag = tag;
			ret->qualifiers = qualifiers;
			return ret;
		}

	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
	case TYPOINTER:
		bug("illegal type in typeBuildBasic");

	default:
		bug("unknown type in typeBuildBasic");
	}
	return NULL;		/* won't reach */
}

/*
 *  Builds a pointer Type given the Type pointed to and the type
 *  qualifiers, if any.
 */
Type typeBuildPointer(Type base, TypeQualifier qual)
{
	if (!isValidQualifier(qual))
		bug("illegal qualifier passed to typeBuildPointer");

	if (base == NULL)
		bug("illegal base type passed to typeBuildPointer");

	{
		InternalPointerType ret;
		ret = typeAllocate(sizeof(struct InternalPointerType));
		if (ret == NULL)
			fatal("out of memory in typeBuildPointer");
		ret->base.tag = TYPOINTER;
		ret->base.qualifiers = qual;
		ret->baseType = base;
		return ret;
	}
}

/*
 *  Builds an array Type given three parameters: the element Type,
 *  a flag indicating whether a dimension is available, and the
 *  dimension of the array.
 */
Type typeBuildArray(Type element, DimFlag flag, unsigned int dimension)
{
	if (!isValidDimFlag(flag))
		bug("illegal dimension flag passed to typeBuildArray");

	if (element == NULL)
		bug("illegal element type passed to typeBuildArray");

	if (flag == DIMENSION_PRESENT && dimension == 0) {
		bug("illegal dimension passed to typeBuildArray");
	}

	{
		InternalArrayType ret;
		ret = typeAllocate(sizeof(struct InternalArrayType));
		if (ret == NULL)
			fatal("out of memory in typeBuildArray");
		ret->base.tag = TYARRAY;
		ret->base.qualifiers = NO_QUAL;
		ret->flag = flag;
		ret->size = dimension;
		ret->elementType = element;
		return ret;
	}
}

/*
 *  Builds a unqualified struct Type given two parameters: the struct tag name
 *  and a member list, which can both be NULL.
 */
Type typeBuildStruct(ST_ID tagname, MemberList members)
{
	InternalStructUnionType ret;
	ret = typeAllocate(sizeof(struct InternalStructUnionType));
	if (ret == NULL)
		fatal("out of memory in typeBuildStruct");
	ret->base.tag = TYSTRUCT;
	ret->base.qualifiers = NO_QUAL;
	{
		IndirectStructUnionType inner;
		inner =
		    typeAllocate(sizeof
				 (struct IndirectStructUnionType));
		if (inner == NULL)
			fatal("out of memory in typeBuildStruct");
		inner->tagname = tagname;
		inner->list = members;
		ret->inner = inner;
	}
	return ret;
}

/*
 *  Creates a new type by qualifying an existing struct type.
 *  The qualifiers attached to the given struct type are ignored and
 *  a new struct type is created using the given qualifiers.
 */
Type typeQualifyStruct(Type type, TypeQualifier qual)
{
	if (!isValidQualifier(qual))
		bug("illegal qualifier passed to typeQualifyStruct");

	if (type == NULL)
		bug("NULL Type passed to typeQualifyStruct");

	if (typeQuery(type) != TYSTRUCT)
		bug("non-struct Type passed to typeQualifyStruct");

	{
		InternalStructUnionType in;
		InternalStructUnionType ret;
		in = type;
		ret =
		    typeAllocate(sizeof
				 (struct InternalStructUnionType));
		if (ret == NULL)
			fatal("out of memory in typeQualifyStruct");
		ret->base.tag = TYSTRUCT;
		ret->base.qualifiers = qual;
		ret->inner = in->inner;
		return ret;
	}
}

/*
 *  Builds a unqualified union Type given two parameters: the union tag name
 *  and a member list, which can both be NULL.
 */
Type typeBuildUnion(ST_ID tagname, MemberList members)
{
	InternalStructUnionType ret;
	ret = typeAllocate(sizeof(struct InternalStructUnionType));
	if (ret == NULL)
		fatal("out of memory in typeBuildUnion");
	ret->base.tag = TYUNION;
	ret->base.qualifiers = NO_QUAL;
	{
		IndirectStructUnionType inner;
		inner =
		    typeAllocate(sizeof
				 (struct IndirectStructUnionType));
		if (inner == NULL)
			fatal("out of memory in typeBuildUnion");
		inner->tagname = tagname;
		inner->list = members;
		ret->inner = inner;
	}
	return ret;
}

/*
 *  Creates a new type by qualifying an existing union type.
 *  The qualifiers attached to the given union type are ignored and
 *  a new union type is created using the given qualifiers.
 */
Type typeQualifyUnion(Type type, TypeQualifier qual)
{
	if (!isValidQualifier(qual))
		bug("illegal qualifier passed to typeQualifyUnion");

	if (type == NULL)
		bug("NULL Type passed to typeQualifyUnion");

	if (typeQuery(type) != TYUNION)
		bug("non-union Type passed to typeQualifyUnion");

	{
		InternalStructUnionType in;
		InternalStructUnionType ret;
		in = type;
		ret =
		    typeAllocate(sizeof
				 (struct InternalStructUnionType));
		if (ret == NULL)
			fatal("out of memory in typeQualifyUnion");
		ret->base.tag = TYUNION;
		ret->base.qualifiers = qual;
		ret->inner = in->inner;
		return ret;
	}
}

/*
 *  Builds a function Type given three parameters: the return Type,
 *  a flag indicating what kind of parameter list is present, and the
 *  parameter list.
 */
Type typeBuildFunction(Type returnType, ParamStyle style,
		       ParamList params)
{
	if (!isValidParamStyle(style))
		bug("illegal ParamStyle value passed to typeBuildFunction");

	if (returnType == NULL)
		bug("illegal element type passed to typeBuildFunction");

	if (style == OLDSTYLE && params != NULL)
		bug("parameter list provided for old style function in typeBuildFunction");

	if (style == DOT_DOT_DOT && params == NULL)
		bug("null parameter list for elipsis function in typeBuildFunction");

	switch (typeQuery(returnType)) {
	case TYFUNCTION:
	case TYARRAY:
		bug("illegal return type passed to typeBuildFunction");
	default:
		break;
	}

	{
		InternalFunctionType ret;
		ret = typeAllocate(sizeof(struct InternalFunctionType));
		if (ret == NULL)
			fatal("out of memory in typeBuildFunction");
		ret->base.tag = TYFUNCTION;
		ret->base.qualifiers = NO_QUAL;
		ret->returnType = returnType;
		ret->style = style;
		ret->list = params;
		return ret;
	}
}

/*
 *  Retrieves qualifiers for a given type. Doesn't search for
 *  qualifiers, rather returns qualifier field from "outermost"
 *  type record. For array and function types, it returns NO_QUAL.
 */
TypeQualifier typeGetQualifier(Type type)
{
	InternalType t;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeGetQualifier");

	return t->qualifiers;
}

/*  
 *  If the first parameter is an array Type, then it returns that
 *  array Type's DimFlag and dimension through the second and
 *  third parameter and the array Type's element Type is returned.
 *  If the first parameter is not an array Type, the "bug" function
 *  is called.
 */
Type typeQueryArray(Type type, DimFlag * dimflag, unsigned int *dim)
{
	InternalArrayType t;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeQueryArray");

	if (typeQuery(type) != TYARRAY)
		bug("non-array type passed to typeQueryArray");

	*dimflag = t->flag;
	*dim = t->size;

	return t->elementType;
}

/*
 *  If the first parameter is a function Type, then it returns that
 *  function Type's ParamStyle and ParamList through the second and
 *  third parameter and the function Type's return Type is returned.
 *  If the first parameter is not a function Type, then the "bug" function
 *  is called.
 */
Type typeQueryFunction(Type type, ParamStyle * paramstyle,
		       ParamList * params)
{
	InternalFunctionType t;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeQueryFunction");

	if (typeQuery(type) != TYFUNCTION)
		bug("non-function type passed to typeQueryFunction");

	*paramstyle = t->style;
	*params = t->list;

	return t->returnType;
}

/*
 *  If the first parameter is a pointer Type, then it returns that
 *  pointer Type's TypeQualifier through the second parameter and the
 *  Type pointed to is returned.  If the first parameter is not a pointer
 *  Type, the "bug" function is called.
 */
Type typeQueryPointer(Type type, TypeQualifier * qual)
{
	InternalPointerType t;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeQueryPointer");

	if (typeQuery(type) != TYPOINTER)
		bug("non-pointer type passed to typeQueryPointer");

	*qual = t->base.qualifiers;

	return t->baseType;
}

/*
 *  Takes two parameters: a previously constructed Type
 *  and a struct or union MemberList. If the input Type
 *  is a struct or union Type, the routine assigns the member
 *  list to the input Type. The modified input Type is then
 *  returned. If the input Type is not struct or union then
 *  the "bug" function is called.
 */
Type typeAssignMembers(Type type, MemberList members)
{
	InternalStructUnionType t;
	IndirectStructUnionType inner;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeAssignMembers");

	switch (typeQuery(type)) {
	case TYSTRUCT:
	case TYUNION:
		break;
	default:
		bug("illegal type passed to typeAssignMembers");
	}

	inner = t->inner;

	if (inner->list != NULL)
		bug("typeAssignMembers: struct/union type already has member list");

	if (members == NULL)
		bug("NULL member list passed to typeAssignMembers");

	inner->list = members;

	return t;
}

/*
 *  If the single parameter is a struct or union Type, then its
 *  MemberList is returned. If the parameter is not struct or union,
 *  then the "bug" function is called.
 */
MemberList typeRetrieveMembers(Type type)
{
	InternalStructUnionType t;
	IndirectStructUnionType inner;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeRetrieveMembers");

	switch (typeQuery(type)) {
	case TYSTRUCT:
	case TYUNION:
		break;
	default:
		bug("illegal type passed to typeRetrieveMembers");
	}

	inner = t->inner;
	return inner->list;
}

/*
 *  This routine takes one parameter which should be either struct or union
 *  Type. If so, it returns the tagname of the given struct or union.
 *  If not, the "bug" function is called.
 */
ST_ID typeRetrieveTagname(Type type)
{
	InternalStructUnionType t;
	IndirectStructUnionType inner;

	t = type;

	if (type == NULL)
		bug("NULL Type passed to typeRetrieveMembers");

	switch (typeQuery(type)) {
	case TYSTRUCT:
	case TYUNION:
		break;
	default:
		bug("illegal type passed to typeRetrieveMembers");
	}

	inner = t->inner;
	return inner->tagname;
}

/*
 *  Tests whether the input type is a Function Type.
 */
BOOLEAN isFunctionType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isFunctionType");

	return (typeQuery(type) == TYFUNCTION);
}

/*
 *  Tests whether the input type is an Incomplete Type.
 */
BOOLEAN isIncompleteType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isObjectType");

	switch (typeQuery(type)) {
	case TYVOID:
		return TRUE;

	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYERROR:
	case TYPOINTER:
		return FALSE;

	case TYSTRUCT:
	case TYUNION:
		{
			InternalStructUnionType t;

			t = type;
			return (t->inner->list == NULL);
		}

	case TYARRAY:
		{
			InternalArrayType t;

			t = type;
			return (t->flag == NO_DIMENSION);
		}

	case TYFUNCTION:
		return FALSE;

	default:
		bug("unknown type in isIncompleteType");
	}
	return FALSE;		/* won't reach */
}

/*
 *  Tests whether the input type is an Object Type.
 */
BOOLEAN isObjectType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isObjectType");

	if (isIncompleteType(type))
		return FALSE;

	if (isFunctionType(type))
		return FALSE;

	/* should we issue bug message instead? */
	if (typeQuery(type) == TYERROR)
		return FALSE;

	return TRUE;
}

/*
 *  Tests whether the input type is a signed integer Type.
 */
BOOLEAN isSignedIntegerType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isSignedIntegerType");

	switch (typeQuery(type)) {
	case TYVOID:
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYERROR:
	case TYPOINTER:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
		return FALSE;

	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDCHAR:
		return TRUE;

	default:
		bug("unknown type in isSignedIntegerType");
	}
	return FALSE;		/* won't reach */
}

/*
 *  Tests whether the input type is a unsigned integer Type.
 */
BOOLEAN isUnsignedIntegerType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isUnsignedIntegerType");

	switch (typeQuery(type)) {
	case TYVOID:
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYERROR:
	case TYPOINTER:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDCHAR:
		return FALSE;

	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
		return TRUE;

	default:
		bug("unknown type in isUnsignedIntegerType");
	}
	return FALSE;		/* won't reach */
}

/*
 *  Tests whether the input type is a floating point Type.
 */
BOOLEAN isFloatingType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isFloatingType");

	switch (typeQuery(type)) {
	case TYVOID:
	case TYERROR:
	case TYPOINTER:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYSIGNEDCHAR:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
		return FALSE;

	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
		return TRUE;

	default:
		bug("unknown type in isFloatingType");
	}
	return FALSE;		/* won't reach */
}

/*
 *  Tests whether the input type is a basic Type.
 */
BOOLEAN isBasicType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isBasicType");

	if (isSignedIntegerType(type))
		return TRUE;

	if (isUnsignedIntegerType(type))
		return TRUE;

	if (isFloatingType(type))
		return TRUE;

	return FALSE;
}

/*
 *  This function takes a Type parameter and returns the unqualified version
 *  of that Type. Only manipulates the outermost type record.
 */

Type typeUnqualifiedVersion(Type type)
{
	InternalType t;

	if (type == NULL)
		bug("NULL Type passed to typeUnqualifiedVersion");

	if (!isValidTypeTag(typeQuery(type)))
		bug("unknown Type passed to typeUnqualifiedVersion");

	t = type;

	/* return the input type if it is not qualified */
	if (t->qualifiers == NO_QUAL)
		return type;

	/* otherwise we need to build a new type */
	switch (typeQuery(type)) {

	case TYPOINTER:
		return typeBuildPointer(typeStripModifier(type),
					NO_QUAL);

	case TYARRAY:
		bug("typeUnqualifiedVersion: array should not be qualified!");

	case TYFUNCTION:
		bug("typeUnqualifiedVersion: function should not be qualified!");

	case TYSTRUCT:
		{
			InternalStructUnionType in;
			InternalStructUnionType ret;

			in = type;
			ret =
			    typeAllocate(sizeof
					 (struct
					  InternalStructUnionType));
			if (ret == NULL)
				fatal
				    ("out of memory in typeUnqualifiedVersion");
			ret->base.tag = TYSTRUCT;
			ret->base.qualifiers = NO_QUAL;
			ret->inner = in->inner;
			return ret;
		}

	case TYUNION:
		{
			InternalStructUnionType in;
			InternalStructUnionType ret;

			in = type;
			ret =
			    typeAllocate(sizeof
					 (struct
					  InternalStructUnionType));
			if (ret == NULL)
				fatal
				    ("out of memory in typeUnqualifiedVersion");
			ret->base.tag = TYUNION;
			ret->base.qualifiers = NO_QUAL;
			ret->inner = in->inner;
			return ret;
		}

	default:
		return typeBuildBasic(typeQuery(type), NO_QUAL);

	}
}

/*
 *  Tests whether the input type is a character Type.
 */
BOOLEAN isCharacterType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isCharacterType");

	switch (typeQuery(type)) {
	case TYVOID:
	case TYERROR:
	case TYPOINTER:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYFUNCTION:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
		return FALSE;

	case TYSIGNEDCHAR:
	case TYUNSIGNEDCHAR:
		return TRUE;

	default:
		bug("unknown type in isCharacterType");
	}
	return FALSE;		/* won't reach */
}

/*
 *  Tests whether the input type is an integral Type.
 */
BOOLEAN isIntegralType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isIntegralType");

	if (isSignedIntegerType(type))
		return TRUE;

	if (isUnsignedIntegerType(type))
		return TRUE;

	return FALSE;
}

/*
 *  Tests whether the input type is an arithmetic Type.
 */
BOOLEAN isArithmeticType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isArithmeticType");

	if (isIntegralType(type))
		return TRUE;

	if (isFloatingType(type))
		return TRUE;

	return FALSE;
}

/*
 *  Tests whether the input type is a scalar Type.
 */
BOOLEAN isScalarType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isScalarType");

	if (isArithmeticType(type))
		return TRUE;

	if (typeQuery(type) == TYPOINTER)
		return TRUE;

	return FALSE;
}

/*
 *  Tests whether the input type is an aggregate Type.
 */
BOOLEAN isAggregateType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isAggregateType");

	if (typeQuery(type) == TYARRAY)
		return TRUE;

	if (typeQuery(type) == TYSTRUCT)
		return TRUE;

	return FALSE;
}

/*
 *  Tests whether the input type is a derived declarator Type.
 */
BOOLEAN isDerivedDeclaratorType(Type type)
{
	if (type == NULL)
		bug("NULL Type passed to isDerivedDeclaratorType");

	return (typeStripModifier(type) != NULL);
}

/*
 *  Given two types, forms the composite type. If it is
 *  not possible then the bug function is called.
 */
Type typeFormComposite(Type type1, Type type2)
{
	if (type1 == NULL || type2 == NULL)
		bug("NULL type passed to typeFormComposite");

	if (!areCompatibleTypes(type1, type2))
		bug("types passed to typeFormComposite are not compatible");

	if (typeQuery(type1) == TYARRAY) {
		InternalArrayType t1;
		InternalArrayType t2;

		t1 = type1;
		t2 = type2;

		if (t1->flag == DIMENSION_PRESENT) {
			return
			    typeBuildArray(typeFormComposite
					   (t1->elementType,
					    t2->elementType), t1->flag,
					   t1->size);
		}
		return
		    typeBuildArray(typeFormComposite
				   (t1->elementType, t2->elementType),
				   t2->flag, t2->size);
	}

	if (typeQuery(type1) == TYPOINTER) {
		InternalPointerType t1;
		InternalPointerType t2;

		t1 = type1;
		t2 = type2;

		return
		    typeBuildPointer(typeFormComposite
				     (t1->baseType, t2->baseType),
				     t1->base.qualifiers);
	}

	if (typeQuery(type1) == TYFUNCTION) {
		InternalFunctionType t1;
		InternalFunctionType t2;

		t1 = type1;
		t2 = type2;

		if (t1->style == PROTOTYPE && t2->style == OLDSTYLE)
			return type1;

		if (t2->style == PROTOTYPE && t1->style == OLDSTYLE)
			return type2;

		if (t1->style == OLDSTYLE && t2->style == OLDSTYLE)
			return type1;

		/* 
		 *  Note that DOT_DOT_DOT is never compatible with PROTOTYPE or OLDSTYLE,
		 *  so only remaining cases are two PROTOTYPEs or two DOT_DOT_DOTs. Need
		 *  to form composite types for each parameter.
		 */
		{
			ParamList plist1;
			ParamList plist2;
			ParamList head;
			ParamList tail;

			plist1 = t1->list;
			plist2 = t2->list;
			head = tail = NULL;

			while ((plist1 != NULL) && (plist2 != NULL)) {
				Type new;
				ParamList param;

				new =
				    typeFormComposite
				    (promoteArrayOrFunction
				     (typeUnqualifiedVersion
				      (plist1->type)),
				     promoteArrayOrFunction
				     (typeUnqualifiedVersion
				      (plist2->type)));

				param =
				    typeAllocate(sizeof(struct param));
				if (param == NULL)
					bug("out of memory in typeFormComposite");

				param->id = NULL;
				param->type = new;
				param->sc = NO_SC;
				param->err = FALSE;
				param->next = NULL;
				param->prev = NULL;	/* unused */

				if (head == NULL) {
					head = tail = param;
				} else {
					tail->next = param;
					tail = param;
				}

				plist1 = plist1->next;
				plist2 = plist2->next;
			}
			return
			    typeBuildFunction(typeFormComposite
					      (t1->returnType,
					       t2->returnType),
					      t1->style, head);
		}
	}

	/* otherwise the types must be identical so either one will do */
	return type1;
}

/*
 *  Checks to see if type1 has all the qualifiers of type2.
 *  Only checks the outermost type record.
 */
BOOLEAN typeHasAllQualifiers(Type type1, Type type2)
{
	InternalType t1;
	InternalType t2;

	if (type1 == NULL || type2 == NULL)
		bug("NULL type passed to typeHasAllQualifiers");

	t1 = type1;
	t2 = type2;

	if (t1->qualifiers == NO_QUAL && t2->qualifiers != NO_QUAL)
		return FALSE;

	if (t1->qualifiers == CONST_QUAL &&
	    (t2->qualifiers == VOLATILE_QUAL ||
	     t2->qualifiers == CONST_VOLATILE_QUAL))
		return FALSE;

	if (t1->qualifiers == VOLATILE_QUAL &&
	    (t2->qualifiers == CONST_QUAL ||
	     t2->qualifiers == CONST_VOLATILE_QUAL))
		return FALSE;

	return TRUE;
}

/*
 *  The following routines are used to print in English the meaning of a type.
 *  The printing is done via the msg/msgn functions. To avoid infinite
 *  recursion, struct/union member lists are not printed once a struct/union
 *  member list is entered.
 */

static
void internalPrintType(Type type, int recursionFlag)
{
	InternalType t;
	unsigned int dim;
	DimFlag dimflag;
	ParamStyle paramstyle;
	ParamList params;
	TypeQualifier qualifier;
	Type *object;
	TypeTag tag;

	t = type;

	if (type == NULL) {
		msgn("<null>");
		return;
	}

	tag = t->tag;
	qualifier = typeGetQualifier(type);

	switch (tag) {
	case TYARRAY:
		object = typeQueryArray(type, &dimflag, &dim);
		if (dimflag == DIMENSION_PRESENT)
			msgn("array of %d ", dim);
		else
			msgn("array of ");
		internalPrintType(object, recursionFlag);
		break;

	case TYPOINTER:
		object = typeQueryPointer(type, &qualifier);
		printQualifier(qualifier);
		msgn("pointer to ");
		internalPrintType(object, recursionFlag);
		break;

	case TYSTRUCT:
		printQualifier(qualifier);
		msg("structure %s",
		    st_get_id_str(typeRetrieveTagname(type)));
		if (recursionFlag)
			printMemberList(typeRetrieveMembers(type));
		break;

	case TYUNION:
		printQualifier(qualifier);
		msg("union %s",
		    st_get_id_str(typeRetrieveTagname(type)));
		if (recursionFlag)
			printMemberList(typeRetrieveMembers(type));
		break;

	case TYFUNCTION:
		object = typeQueryFunction(type, &paramstyle, &params);
		if (paramstyle == OLDSTYLE) {
			msgn("function returning ");
			internalPrintType(object, recursionFlag);
		} else {
			msgn("function prototype");
			if (paramstyle == DOT_DOT_DOT)
				msgn(" (elipsis)");
			printParamList(params);
			msgn("\t\treturning ");
			internalPrintType(object, recursionFlag);
		}
		break;

	case TYFLOAT:
		printQualifier(qualifier);
		msgn("float");
		break;

	case TYDOUBLE:
		printQualifier(qualifier);
		msgn("double");
		break;

	case TYLONGDOUBLE:
		printQualifier(qualifier);
		msgn("long double");
		break;

	case TYUNSIGNEDINT:
		printQualifier(qualifier);
		msgn("unsigned int");
		break;

	case TYUNSIGNEDCHAR:
		printQualifier(qualifier);
		msgn("unsigned char");
		break;

	case TYUNSIGNEDSHORTINT:
		printQualifier(qualifier);
		msgn("unsigned short int");
		break;

	case TYUNSIGNEDLONGINT:
		printQualifier(qualifier);
		msgn("unsigned long int");
		break;

	case TYSIGNEDCHAR:
		printQualifier(qualifier);
		msgn("signed char");
		break;

	case TYSIGNEDINT:
		printQualifier(qualifier);
		msgn("signed int");
		break;

	case TYSIGNEDLONGINT:
		printQualifier(qualifier);
		msgn("signed long int");
		break;

	case TYSIGNEDSHORTINT:
		printQualifier(qualifier);
		msgn("signed short int");
		break;

	case TYVOID:
		printQualifier(qualifier);
		msgn("void");
		break;

	case TYERROR:
		msgn("error");
		break;

	default:
		bug("illegal typetag (%d) in internalPrintType", tag);
	}
}

void printType(Type type)
{
	internalPrintType(type, 1);
}

void printTypetag(TypeTag tag)
{
	switch (tag) {
	case TYARRAY:
		msgn("array");
		break;

	case TYPOINTER:
		msgn("pointer to");
		break;

	case TYSTRUCT:
		msgn("struct");
		break;

	case TYUNION:
		msgn("union");
		break;

	case TYFUNCTION:
		msgn("function returning");
		break;

	case TYFLOAT:
		msgn("float");
		break;

	case TYDOUBLE:
		msgn("double");
		break;

	case TYLONGDOUBLE:
		msgn("long double");
		break;

	case TYUNSIGNEDINT:
		msgn("unsigned int");
		break;

	case TYUNSIGNEDCHAR:
		msgn("unsigned char");
		break;

	case TYUNSIGNEDSHORTINT:
		msgn("unsigned short int");
		break;

	case TYUNSIGNEDLONGINT:
		msgn("unsigned long int");
		break;

	case TYSIGNEDCHAR:
		msgn("signed char");
		break;

	case TYSIGNEDINT:
		msgn("signed int");
		break;

	case TYSIGNEDLONGINT:
		msgn("signed long int");
		break;

	case TYSIGNEDSHORTINT:
		msgn("signed short int");
		break;

	case TYVOID:
		msgn("void");
		break;

	case TYERROR:
		msgn("error");
		break;

	default:
		bug("illegal tag in printTypetag");
	}
}

void printQualifier(TypeQualifier tag)
{
	switch (tag) {
	case CONST_QUAL:
		msgn("const ");
		break;
	case VOLATILE_QUAL:
		msgn("volatile ");
		break;
	case CONST_VOLATILE_QUAL:
		msgn("const volatile ");
		break;
	case NO_QUAL:
		msgn("<no qual> ");
		break;
	default:
		bug("illegal qualifier in printQualifier");
		break;
	}
}

void printDimflag(DimFlag tag)
{
	switch (tag) {
	case NO_DIMENSION:
		msgn("no dimension");
		break;
	case DIMENSION_PRESENT:
		msgn("dimension present");
		break;
	default:
		bug("illegal flag in printDimflag");
	}
}

void printParamStyle(ParamStyle tag)
{
	switch (tag) {
	case OLDSTYLE:
		msgn("oldstyle");
		break;
	case PROTOTYPE:
		msgn("prototype");
		break;
	case DOT_DOT_DOT:
		msgn("elipsis prototype");
		break;
	default:
		bug("illegal tag in printParamStyle");
	}
}

void printParamList(ParamList params)
{
	if (params == NULL) {
		msg("");
		msg("\t<parameter list is null>");
		return;
	}
	while (params) {
		char *idStr;

		msg("");

		/* id might be NULL! */
		idStr = st_get_id_str(params->id);
		msgn("\tparam: id %s; type ",
		     (idStr ? idStr : "<null>"));

		/* type might be NULL, but printType handles NULL */
		printType(params->type);

		params = params->next;
	}
	msg("");
}

void printMemberList(MemberList members)
{
	if (members == NULL) {
		msg("");
		msg("\tmember list is <null>");
		return;
	}

	while (members) {
		msg("");
		msgn("\tmember: id %s; type ",
		     st_get_id_str(members->id));
		internalPrintType(members->type, FALSE);	/* don't 
								   recurse 
								 */
		members = members->next;
	}

	msg("");
}
