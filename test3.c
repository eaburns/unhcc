int i, j, k;

void f1(void)
{
  while (i < j)
  {
    if (j > k)
    {
      if (k > 25)
      {
        k = k * i;
        continue;
      }
      else
      {
        k = k - i;
      }
    }
    j = j  + 1;
  }
  i = i - 1;
}

void f2(void)
{
  for (i = 0; i < 10; i++)
  {
    j = j * k;
    if (j > 1000) break;
  }
}

void f3(void)
{
  switch (i)
  {
  case 0:
    k = j;
    break;
  case 1:
    k = j * j;
  case 2:
    k = k * j;
    break;
  default:
    k = 100;
  }
}

void f4(void)
{
  if (i > j)
  {
    goto L1;
  }
  do
  {
    k = k * j;
    L1:
    j = j - 1;
  }
  while (j > 0);
}
    
