/*  semUtils.h
 *  Written by Denise Mitchell for CS-812 Compilers
 *  UNH Graduate School - Spring 2001
 *  Supports semUtils.h
 */
#ifndef SEMUTILS_H
#define SEMUTILS_H

#include "tree.h"
#include "types.h"

/* publish prototype for functions */

ExpTree integralPromotion(ExpTree);
ExpTree defaultArgumentPromotion(ExpTree);
BinopTree arithmeticConversion(BinopTree);
ExpTree prefixConvert(ExpTree, TypeTag);
BOOLEAN isCompleteObjectType(Type type);
BOOLEAN isLval(ExpTree t);
BOOLEAN isConstQualifiedType(Type type, TypeQualifier qual);
BOOLEAN containsConstMember(Type type);
BOOLEAN isModifiableLval(ExpTree t);
BOOLEAN isUnsignedType(Type type);
BOOLEAN isPointerType(Type t);
BOOLEAN isFunctionPointerType(Type type);
BOOLEAN isObjectPointerType(Type type);
BOOLEAN isNullPointerConst(ExpTree t);
BOOLEAN isFloatType(Type t);
BOOLEAN isVoidPointerType(Type type);
BOOLEAN isStructUnion(ExpTree e);
BOOLEAN isStructUnionType(Type type);

#endif
