/*
 *  constExpr.h - constant expressions
 *
 *  Unfortunately this code is incomplete and buggy. But it seems
 *  to do the simple cases correctly.
 *
 */
#ifndef CONSTEXPR_H
#define CONSTEXPR_H

#include "tree.h"
#include "globals.h"
#include "analyze.h"

/* to get the value from a constant node (eg used for newArrayDeclNode) */
long getConstExprValue(ExpTree e);

/* this is the main entry point: ExpTree is reduced to a constant node. */
ExpTree reduceConstExpr(ExpTree e);

#endif
