/*
 *  decl.h: header for the code that processes declarations
 *
 */

#ifndef DECL_H
#define DECL_H

#include "defs.h"
#include "types.h"
#include "bucket.h"
#include "symtab.h"

/*
 *  DECL_TREE represents the pieces of a single declarator.
 */
typedef enum { DECL_PTR, DECL_ARR, DECL_FUNC, DECL_ID } DECL_TAG;

typedef struct decl_tree {
	DECL_TAG tag;
	union {
		int dim;	/* if tag is array, store the dimension */
		TypeQualifier qual;	/* for pointer qualifier */
		ST_ID id;	/* if tag is ID, store the id */
		struct {	/* if tag of function, store paramstyle 
				   & params */
			ParamStyle paramstyle;
			ParamList params;
		} paramInfo;
	} u;
	struct decl_tree *next;
} DECL_NODE, *DECL_TREE;

/* DECL_NODE constructors */
extern DECL_TREE newPointerDeclNode(TypeQualifier, DECL_TREE);
extern DECL_TREE newArrayDeclNode(int, DECL_TREE);
extern DECL_TREE newFuncDeclNode(ParamStyle, ParamList, DECL_TREE);
extern DECL_TREE newIdDeclNode(ST_ID);

/* DECL_NODE destructor */
extern void deleteDeclNode(DECL_NODE *);

/* add string of pointer nodes to top of a declarator tree */
extern DECL_TREE addPointersToDeclTree(DECL_TREE pointers,
				       DECL_TREE subtree);

/* routines for processing qualifiers attached to a pointer */
extern TypeQualifier convertSpecifierToQualifier(TYPE_SPECIFIER);
extern TypeQualifier addQualifierToList(TypeQualifier, TypeQualifier);

/* define link list for declarator list */
typedef struct declarator {
	DECL_TREE tree;
	struct declarator *next;
} DECLARATOR, *DECLARATOR_LIST;

/* DECLARATOR constructor */
extern DECLARATOR_LIST newDeclarator(DECL_TREE tree,
				     DECLARATOR_LIST list);

/* DECLARATOR constructor */
extern void deleteDeclarator(DECLARATOR *);

/*
 *  Process declarator for function definition and return its param list.
 */
extern ParamList processFunctionDef(BUCKET_PTR bucket, DECL_TREE tree);

/*
 *  used for tracking the kind of parameter list as well as the list itself
 *  ie OLDSTYLE, PROTOTYPE or DOT_DOT_DOT
 */
typedef struct param_type_list {
	ParamList list;
	ParamStyle style;
} PARAM_TYPE_LIST;

/*
 *  Define a struct in order to hold struct/union type in type_specifier
 *  on parser's semantic stack.
 */
typedef struct my_specifier {
	TYPE_SPECIFIER type_spec;
	Type type;		/* for normal type this field is NULL */
	BOOLEAN err;		/* only necessary for struct/union
				   types */
} MY_SPECIFIER;

/*
 *  Given a type specifier, build the MY_SPECIFIER.
 */
extern MY_SPECIFIER newSpecifierObject(TYPE_SPECIFIER);

/*
 *  Given a bucket and a list of declarators, build a list of
 *  struct members and their types.
 */
extern MemberList createMemberList(BUCKET_PTR spec_qual_list,
				   DECLARATOR_LIST decl_list);

/*
 *  Merge two member lists.
 */
extern MemberList mergeMemberList(MemberList list1, MemberList list2);

/*
 *  Build a struct/union type and then place it in a MY_SPECIFIER.
 *
 *  Note: tag is NOT installed now.
 */
extern MY_SPECIFIER *buildStructUnionType(ST_ID id,
					  TYPE_SPECIFIER type_spec,
					  MemberList mem_list);

/*
 *  Attach a member list to a struct/union type.
 *
 *  It is an error if there is already a member list attached.
 */
extern void attachMemberList(MY_SPECIFIER my_spec, MemberList mem_list);

/*
 *  Called when a declaration has no declarator.
 *
 *  This is legal if there is a side-effect, such as a struct/union tag
 *  being declared.
 *
 *  As well as doing error checking, the appropriate processing of the
 *  type bucket must be performed.
 */
extern void declarationWithoutDeclarator(BUCKET_PTR bucket);

/*
 *  struct/union declarations need special processing after bucket
 *  is ready but before declarators are processed IF member list
 *  is not given in the declaration. If no member list is present
 *  with tag in the symbol table, then error checking must be done
 *  to be sure this is a legal declaration.
 *
 *  Note: this function is called for all declarations so it must
 *  ignore types other than struct/unions.
 */
void beforeProcessDeclaration(BUCKET_PTR bucket);

/* 
 *  This function is called for struct declarations when there is both
 *  a tag and a member list given. It is called before the member list
 *  is processed, however, and it must produce a MY_SPECIFIER that
 *  contains the specifier and the struct/union TYPE. Processing is
 *  complicated because the tag may already be installed. If so, the
 *  block numbers must be checked. If they are the same, then both
 *  types must be struct or they both must be union, and the previously
 *  defined type must not have a member list already attached.
 */
MY_SPECIFIER *installTagWhenMemberListComing(ST_ID id,
					     TYPE_SPECIFIER t_spec);

/*
 *  Compute the size of a union type.
 *
 *  Note: keep this code machine-independent by using computeSize from
 *  the encode module.
 */
int computeUnionSize(Type type);

/*
 *  Compute the size of a struct type.
 *
 *  Note: keep this code machine-independent by using computeSize from
 *  the encode module and computeMemberOffset from this module.
 */
int computeStructSize(Type type);

/*
 *  Compute offset for a member of a struct/union type.
 *
 *  Should return -1 if member name not found in struct/union type.
 *
 *  Note: keep this code machine-independent by using computeSize and
 *  determineAlignment from the encode module.
 */
int computeMemberOffset(Type type, ST_ID memberName);

/*
 *  Given a bucket and a declarator for a parameter, produce type,
 *  qualifiers, storage class and identifier for the parameter and
 *  place these items in a PARAM_LIST node, which is then returned.
 *  Of course, appropriate error checking must also be performed.
 */
ParamList makeParamListNode(BUCKET_PTR bucket, DECL_TREE tree);

/*
 *  Append one param list node on the end of a param list.
 *
 *  The second parameter is the single node to be appended to the
 *  list which is the first parameter.
 *
 *  Returned the updated list.
 *
 *  Appropriate error checking must be done.
 */
ParamList appendToParamList(ParamList list1, ParamList list2);

/*
 *  Need to allocate stack slots for parameters.
 */
void allocateStackSlotsForParameters(ParamList list);

/*
 * Function completes the processing of a type definition indicated
 * by the ST_ID and Type parms.  It checks to see
 * if the id has been previously used at this scope to define a
 * type.  If so, it issues an error and returns.  Otherwise it
 * installs the type definition into the symbol table.
 */
void finishTypedefDeclaration(ST_ID, Type);

/*
 * Function builds and returns a MY_SPECIFIER object from the info
 * obtained by retrieving an id's stdr from the symbol table.
 */
MY_SPECIFIER getTypenameSpecifierObject(ST_ID);

#endif
