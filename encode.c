/*
 * encode.c - write target code for unhcc compiler
 *
 * The target is the EM64T Architecture.
 *
 * This code also assumes the compiler will run on an EM64T machine.
 *
 * All operands are left on top of the run-time stack so no register
 * allocation is performed. Registers are only used as temporaries
 * while doing individual operations.
 */

#include <stdio.h>

#include "defs.h"
#include "types.h"
#include "tree.h"
#include "symtab.h"
#include "encode.h"
#include "globals.h"
#include "message.h"
#include "semUtils.h"
#include "tarch.h"
#include "regAlloc.h"

/*
 * Static functions.
 */
static int roundEven16(int val);
static const char *int_reg_arg(unsigned int num);
static const char *float_reg_arg(unsigned int num);
static void encodeUnop(UnopTree u);
static void encodeBinop(BinopTree b);
static void encodeFloatConversion(UnopTree u);
static void encodeIntegerConversion(UnopTree u);
static void checkConvertDstType(Tree t, TypeTag type);
static void encodeConvert(UnopTree u);
static void copyStructUnionToTopOfStack(REG src, int size, int startOffs);
static void encodeFcallHelper(ExpTree e, Type retType);
static int analyzeOneArg(Type t, int *intRegCnt, int *floatRegCnt);
static int bytesPassedViaStack(ExpTree e, Type retType);
static void moveOneArg(Type t, int *intRegCnt, int *floatRegCnt,
		       REG reg, int *em64tOffset);
static int moveArguments(ExpTree e, Type retType);
static void encodeFcall(FcallTree b);
static void paramToREG(ParamList p, int *intRegs, int *floatRegs,
                       int *em64tOffs);
static void encodeStructUnionAssign(BinopTree b);
static void encodeAssign(BinopTree b);
static void encodeReturn(ReturnTree r);

/*
 * If the given size is less than one stack-word, returns one stack-word
 * else returns the given size... essintially aligns a size to a
 * stack-word.
 *
 * NOTE: this doesn't align things bigger than one stack-word in size,
 *       those are all assumed to be in multiples of stackwords anyway.
 */
#define SIZE_ON_STACK(x) \
	((x) < TARCH_SIZEOF_STACKWORD ? TARCH_SIZEOF_STACKWORD : (x))

/*
 *  Track size of the locals area for current function. Local variables
 *  are allocated in the locals area from the low addresses back to the
 *  high addresses. That is, first local variable is at the lowest address.
 *  This may appear backwards as the variables asigned the smaller addresses
 *  will appear at the higher negative offsets in the generated code.
 */
int sizeOfLocalsArea;

/*
 *  Track stack depth from entry point to the function currently being
 *  compiled. The issue is that the stack must be aligned on a 16-byte
 *  boundary when an fcall is executed. The stack is on a 16-byte boundary
 *  after the function entry is complete: fcall saves %rsp, then %rbp
 *  is saved. So the code generator must increment this variable every
 *  time there is a push and decrement every time there is a pop. When
 *  code is being generated for fcall, this variable is checked to see
 *  if a dummy quadword needs to be placed on the stack between the IA-32
 *  style arg block and the EM64T style arg block.
 */
int encodeStackDepth;

/*
 *  genLabel: generate a unique label number
 *            prepend L$ to get full label name
 *            e.g.: L$5
 */
unsigned int genLabel(void)
{
	static unsigned int nextLabel = 0;

	return nextLabel++;
}


/**
 * \brief Emits a push instruction with the given constants onto
 *        the stack.
 */
void emit_push_const(unsigned long int val)
{
	encodeStackDepth += 8;
	emit("\tpushq\t$%d\t# depth %d", val, encodeStackDepth);
}

/**
 * \brief Emits a push instruction from the given reigster onto the
 *        stack.
 */
void emit_push_reg(const char *reg)
{
	encodeStackDepth += 8;
	emit("\tpushq\t%s\t# depth %d", reg, encodeStackDepth);
}

/**
 * \brief Emits a pop instruction into the given reigster from the
 *        stack.
 */
void emit_pop_reg(const char *reg)
{
	encodeStackDepth -= 8;
	emit("\tpopq\t%s\t# depth %d", reg, encodeStackDepth);
}

/**
 * \brief Emits a stack growing instruction.  This function allocates space
 *        on top of the stack.
 */
void emit_stack_grow(unsigned int size)
{
	encodeStackDepth += size;
	emit("\tsubq\t$%u, %%rsp\t# depth %d", size, encodeStackDepth);
}

/**
 * \brief Emits a stack shrinking instruction.  This function deallocates space
 *        on top of the stack.
 */
void emit_stack_shrink(unsigned int size)
{
	encodeStackDepth -= size;
	emit("\taddq\t$%u, %%rsp\t# depth %d", size, encodeStackDepth);
}

/**
 * \brief output output file prologue
 */
void encode_init(void)
{				/* beginning of program */
	/* global variables will be first */
	emitSection("data");

	encodeStackDepth = 0;
}


/**
 * \brief encode_global_var_decl
 */
void encode_global_var_decl(ST_ID id, Type type)
{
	int size;

	size = computeSize(type);
	comment("global variable declaration");
	emit("\t.comm\t%s, %d", st_get_id_str(id), size);
}


/**
 * \brief encode_static_var_decl
 */
void encode_static_var_decl(ST_ID id, Type type)
{
	int size;

	size = computeSize(type);
	comment("static variable declaration");
	emit("\t.lcomm\t%s, %d", st_get_id_str(id), size);
}

/*
 * function to generate code for local static variables
 * defferent from global static for the following case:
 * fun1() { static int a; }
 * fun2() { static float a; }
 */
void encode_local_static(ST_ID id, unsigned int label, Type type)
{
	int size;

	size = computeSize(type);
	comment("local static variable declaration");
	emitSection("data");
	emit("\t.lcomm\t%s$L%d, %d", st_get_id_str(id), label, size);
	emitSection("text");
}

/**
 * Puts a floating point register (XMM) value onto the top of the stack.
 */
static void emit_push_fp(const char *r)
{
	emit_stack_grow(16);
	emit("\tmovsd\t%s, (%%rsp)", r);
}

/**
 * Removes a floating point (XMM) register from the top of the stack
 * into the given register.
 */
static void emit_pop_fp(const char *r)
{
	emit("\tmovsd\t(%%rsp), %s", r);
	emit_stack_shrink(16);
}

/**
 * Saves all callee-saved registers onto the stack.
 */
static void encodeSaveCallee(void)
{
	emit_push_reg("%rbx");
	emit_push_reg("%r12");
	emit_push_reg("%r13");
	emit_push_reg("%r14");
	emit_push_reg("%r15");
	emit_push_fp("%xmm8");
	emit_push_fp("%xmm9");
	emit_push_fp("%xmm10");
	emit_push_fp("%xmm11");
	emit_push_fp("%xmm12");
	emit_push_fp("%xmm13");
	emit_push_fp("%xmm14");
	emit_push_fp("%xmm15");
}

/**
 * Restores all callee-saved registers from the stack.
 */
static void encodeRestoreCallee(void)
{
	emit_pop_fp("%xmm15");
	emit_pop_fp("%xmm14");
	emit_pop_fp("%xmm13");
	emit_pop_fp("%xmm12");
	emit_pop_fp("%xmm11");
	emit_pop_fp("%xmm10");
	emit_pop_fp("%xmm9");
	emit_pop_fp("%xmm8");
	emit_pop_reg("%r15");
	emit_pop_reg("%r14");
	emit_pop_reg("%r13");
	emit_pop_reg("%r12");
	emit_pop_reg("%rbx");
}

/**
 * Saves all caller-saved registers onto the stack.
 */
static void encodeSaveCaller(void)
{
	emit_push_reg("%r11");
}

/**
 * Restores all caller-saved registers from the stack.
 */
static void encodeRestoreCaller(void)
{
	emit_pop_reg("%r11");
}

/*
 * compute the size in bytes for the given Type
 */
int computeSize(Type type)
{
	switch (typeQuery(type)) {

	case TYFLOAT:
		return TARCH_SIZEOF_FLOAT;

	case TYDOUBLE:
		return TARCH_SIZEOF_DOUBLE;

	case TYLONGDOUBLE:
		return TARCH_SIZEOF_LDOUBLE;

	case TYUNSIGNEDINT:
		return TARCH_SIZEOF_UINT;

	case TYUNSIGNEDCHAR:
		return TARCH_SIZEOF_UCHAR;

	case TYUNSIGNEDSHORTINT:
		return TARCH_SIZEOF_USHORT;

	case TYUNSIGNEDLONGINT:
		return TARCH_SIZEOF_ULONG;

	case TYSIGNEDCHAR:
		return TARCH_SIZEOF_SCHAR;

	case TYSIGNEDINT:
		return TARCH_SIZEOF_SINT;

	case TYSIGNEDLONGINT:
		return TARCH_SIZEOF_SLONG;

	case TYSIGNEDSHORTINT:
		return TARCH_SIZEOF_SSHORT;

	case TYVOID:
		bug("TYVOID in computeSize()");
		return 0;
	 /*NOTREACHED*/ case TYPOINTER:	/* all pointers are of
						   the same size */
		return TARCH_SIZEOF_ULONG;

	case TYARRAY:
		{
			DimFlag dimflag;
			unsigned int dim;
			Type elementType;

			elementType =
			    typeQueryArray(type, &dimflag, &dim);
			if (dimflag == DIMENSION_PRESENT) {
				return computeSize(elementType) * dim;
			} else {
				bug("array with no dimension in computeSize()");
				return 0;
			 /*NOTREACHED*/}
		}		/* end of case array */

	case TYERROR:
		return 0;

	case TYSTRUCT:
		return computeStructSize(type);

	case TYUNION:
		return computeUnionSize(type);

	default:
		bug("unsupported type in computeSize()\n");
		return 0;
	 /*NOTREACHED*/}	/* end of switch */
	return 0;
 /*NOTREACHED*/}		/* end of computeSize */

int typeSize(TypeTag type)
{
	switch (type) {

	case TYFLOAT:
		return TARCH_SIZEOF_FLOAT;

	case TYDOUBLE:
		return TARCH_SIZEOF_DOUBLE;

	case TYLONGDOUBLE:
		return TARCH_SIZEOF_LDOUBLE;

	case TYUNSIGNEDINT:
		return TARCH_SIZEOF_UINT;

	case TYUNSIGNEDCHAR:
		return TARCH_SIZEOF_UCHAR;

	case TYUNSIGNEDSHORTINT:
		return TARCH_SIZEOF_USHORT;

	case TYUNSIGNEDLONGINT:
		return TARCH_SIZEOF_ULONG;

	case TYSIGNEDCHAR:
		return TARCH_SIZEOF_SCHAR;

	case TYSIGNEDINT:
		return TARCH_SIZEOF_SINT;

	case TYSIGNEDLONGINT:
		return TARCH_SIZEOF_SLONG;

	case TYSIGNEDSHORTINT:
		return TARCH_SIZEOF_SSHORT;

	case TYPOINTER:
	case TYARRAY:
	case TYSTRUCT:
	case TYUNION:
		return TARCH_SIZEOF_POINTER;

	case TYVOID:
		bug("TYVOID in computeSize()");
	case TYERROR:
		return 0;
		return 0;

	default:
		bug("unsupported type in computeSize()\n");
		return 0;
	 /*NOTREACHED*/}	/* end of switch */
}


/*
 * Compute the alignment of the given Type
 * TODO: these values should be abstracted to constants in a tarch.h
 * file.
 */
int determineAlignment(Type type)
{
	switch (typeQuery(type)) {
	case TYFLOAT:
		return 4;
	case TYDOUBLE:
		return 4;
	case TYLONGDOUBLE:
		return 16;
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
		return 1;
	case TYUNSIGNEDSHORTINT:
	case TYSIGNEDSHORTINT:
		return 2;
	case TYUNSIGNEDINT:
	case TYSIGNEDINT:
		return 4;
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		return 8;

	case TYVOID:
		bug("TYVOID in determineAlignment()");
		return 0;
	 /*NOTREACHED*/ case TYERROR:
		return 0;

	case TYARRAY:		/* equal to its element type alignment */
		{
			DimFlag dimflag;
			unsigned int dim;
			Type elementType;

			elementType =
			    typeQueryArray(type, &dimflag, &dim);
			return determineAlignment(elementType);
		}

	case TYSTRUCT:
	case TYUNION:
		return 16;	/* worst case alignment */

	default:
		bug("unsupported type in determineAlignment()\n");
		return 0;
	 /*NOTREACHED*/}	/* end of switch */

}				/* end of determineAlignment */

/**
 * \brief An alignment that is good for *any* Type.
 */
int worstCaseAlignment(void)
{
	return 16;
}

/*
 *  Used to round frame sizes up to even multiple of 16.
 *
 */
static int roundEven16(int val)
{
	if ((val & 0xF) == 0)
		return val;

	return (val | 0xF) + 1;
}

/**
 * \brief Returns the name of the register to use for the given integer
 *        argument number.
 */
static const char *int_reg_arg(unsigned int num)
{
	char *intRegs[6];

	intRegs[0] = "%rdi";
	intRegs[1] = "%rsi";
	intRegs[2] = "%rdx";
	intRegs[3] = "%rcx";
	intRegs[4] = "%r8";
	intRegs[5] = "%r9";

	if (num > 6)
		bug("int_reg_arg called with num > 6\n");

	return intRegs[num - 1];
}

/**
 * \brief Returns the name of the register to use for the given floating
 *        point argument number.
 */
static const char *float_reg_arg(unsigned int num)
{
	char *floatRegs[8];

	floatRegs[0] = "%xmm0";
	floatRegs[1] = "%xmm1";
	floatRegs[2] = "%xmm2";
	floatRegs[3] = "%xmm3";
	floatRegs[4] = "%xmm4";
	floatRegs[5] = "%xmm5";
	floatRegs[6] = "%xmm6";
	floatRegs[7] = "%xmm7";

	if (num > 8)
		bug("float_reg_arg called with num > 6\n");

	return floatRegs[num - 1];
}

/* is the given Type derer-able on the target architecture? */
BOOLEAN isDerefable(Type t)
{
	switch (typeQuery(t)) {

	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYPOINTER:
		return TRUE;

	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYERROR:
	case TYVOID:
		return FALSE;

	case TYFUNCTION:
		return FALSE;

	default:
		bug("unknown type in isDerefable");
	}

	/* not reached */
	return FALSE;
}

/*
 *  encodeAssignDetail: make details of encoding ASSIGN generally
 *                     available. Used, for example, in compound
 *                     assignment operators. This only supports
 *                     the "basic" types (ie not struct/union).
 *
 *  type - Type of value to be assigned.
 *
 *  t - Tree being encoded.
 *
 */
void encodeAssignDetail(Type type, Tree t)
{

	BinopTree bin;
	REG sreg, dreg, tmp;

	type = type;

	bin = (BinopTree) t;
	sreg = bin->right->reg;
	dreg = bin->left->reg;

	comment("assign %s to %s", regString(sreg), regString(dreg));

	if ((isMemory(sreg) && isMemory(dreg))
	    && (regType(sreg) != TYLONGDOUBLE
	        && regType(dreg) != TYLONGDOUBLE)) {
		/* long doubles are handled differently,
 		 * memory-to-memory is ok for them */
		comment1("memory-to-memory assign, needs tmp reg");
		tmp = allocNextReg(typeQuery(bin->right->type));
		/* zero fill or sign extend to fill the register */
		useReg(sreg);
		move(sreg, tmp);
		defReg(tmp);
		sreg = tmp;
	}

	/* just move the exact size into the memory location */
	useReg(sreg);

	/* we may need to mark a USE of the base register if this is a
 	 * move into a memory location */
	if (isMemory(dreg))
		useReg(dreg);

	move(sreg, dreg);
	defReg(dreg);

	if (isConstant(sreg)) {
		/* if the assignment was from a constant, just keep
 		 * using that constant for future assignments. */
		bin->expn.reg = sreg;
	} else {
		bin->expn.reg = dreg;
	}
}

/*
 *  encodeDerefDetail: make details of encoding DEREF generally
 *                     available. Used, for example, in compound
 *                     assignment operators.
 *
 *  type - Type of value to be loaded.
 *
 *  t - Tree being encoded.
 *
 */
void encodeDerefDetail(Type type, Tree t)
{
	UnopTree un;
	REG tmp;

	un = (UnopTree) t;

	if (isMemory(un->left->reg)) {
		comment("deref memory location via temporary register");
		tmp = allocNextReg(TYPOINTER);
		comment1("deref with memory source, move to register");
		useReg(un->left->reg);
		move(un->left->reg, tmp);
		defReg(tmp);
	} else {
		tmp = un->left->reg;
	}

	un->expn.reg = allocReg(typeQuery(type), "(%s)",
	                        regStringAs(TYPOINTER, tmp));

	if (isRegister(tmp)) {
		/* need to flag that this register emits use annotation */
		regMemBaseReg(un->expn.reg, "%s", regString(tmp));
	}
}

/*
 * encodeUnop: code generation for unary operators
 *
 * Note: No support for SIZEOF as it will be fully handled at compile time
 *       (during semantic analysis).
 */
static void encodeUnop(UnopTree u)
{
	switch (u->op) {
	case NO_OP:
	case DUP_OP:
		/* copy the register up the tree */
		u->expn.reg = u->left->reg;
		break;
	case DEREF_OP:
		encodeDerefDetail(u->expn.type, (Tree) u);
		break;
	case INCRA_OP:
		encodeIncra(u);
		break;
	case DECRA_OP:
		encodeDecra(u);
		break;
	case INCRB_OP:
		encodeIncrb(u);
		break;
	case DECRB_OP:
		encodeDecrb(u);
		break;
	case USUB_OP:
		encodeUsub(u);
		break;
	case LNOT_OP:
		encodeLnot(u);
		break;
	case CONVERT_OP:
		encodeConvert(u);
		break;
	case SIZEOF_OP:
		bugT((Tree) u, "SIZEOF not expected in encodeUnary");
		break;
	case ADDR_OF_OP:
		encodeAddrOf(u);
		break;
	case COMPLMT_OP:
		encodeComplmt(u);
		break;
	case UADD_OP:
		bugT((Tree) u, "UADD seen in encodeUnary!");
		break;
	default:
		bugT((Tree) u, "unknown operation in encodeUnary");
	}
}

/*
 * encodeBinop: code generation for binary operators
 *
 */
static void encodeBinop(BinopTree b)
{
	switch (b->op) {

	case INDEX_OP:
		encodeIndex(b);
		break;

	case LSHIFT_OP:
		encodeLshift(b);
		break;

	case RSHIFT_OP:
		encodeRshift(b);
		break;

	case ANDD_OP:
		encodeAndd(b);
		break;

	case XORR_OP:
		encodeXorr(b);
		break;

	case ORR_OP:
		encodeOrr(b);
		break;

	case MULT_ASSIGN_OP:
		encodeMultAssign(b);
		break;

	case DIV_ASSIGN_OP:
		encodeDivAssign(b);
		break;

	case MOD_ASSIGN_OP:
		encodeModAssign(b);
		break;

	case ADD_ASSIGN_OP:
		encodeAddAssign(b);
		break;

	case SUB_ASSIGN_OP:
		encodeSubAssign(b);
		break;

	case LEFT_ASSIGN_OP:
		encodeLeftAssign(b);
		break;

	case RIGHT_ASSIGN_OP:
		encodeRightAssign(b);
		break;

	case AND_ASSIGN_OP:
		encodeAndAssign(b);
		break;

	case XOR_ASSIGN_OP:
		encodeXorAssign(b);
		break;

	case OR_ASSIGN_OP:
		encodeOrAssign(b);
		break;

	case MULT_OP:
		encodeMult(b);
		break;

	case DIV_OP:
		encodeDiv(b);
		break;

	case MOD_OP:
		encodeMod(b);
		break;

	case ADD_OP:
		encodeAdd(b);
		break;

	case SUB_OP:
		encodeSub(b);
		break;

	case LT_OP:
		encodeLt(b);
		break;

	case GT_OP:
		encodeGt(b);
		break;

	case LTE_OP:
		encodeLte(b);
		break;

	case GTE_OP:
		encodeGte(b);
		break;

	case EQL_OP:
		encodeEql(b);
		break;

	case NEQL_OP:
		encodeNeql(b);
		break;

	case ASSIGN_OP:
		encodeAssign(b);
		break;

	case LOR_OP:
	case LAND_OP:
	case COMMA_OP:
		bugT((Tree) b,
		     "unexpected LOR, LAND or COMMA seen in encodeBinary");

	default:
		bugT((Tree) b, "unknown operation in encodeBinary");
	}
}

/*
 *  encode: traverses an AST and generates code in a (basically)
 *          bottom-up fashion.
 */

void encode(Tree t)
{
	UnopTree u;
	BinopTree b;
	TriopTree tr;
	FcallTree f;
	EseqTree e;
	PtrTree ptr;
	FieldRefTree fr;
	CastopTree c;
	VarTree v;
	IntConstTree i;
	LongConstTree l;
	FpConstTree fp;
	StringLitTree s;
	LabelTree la;
	CaseTree ca;
	DefaultTree d;
	SeqTree se;
	IfElseTree ie;
	SwitchTree sw;
	WhileTree w;
	DoWhileTree dw;
	ForTree fo;
	GotoTree g;
	ContinueTree co;
	BreakTree br;
	ReturnTree r;
	int label;
	char *typeStr;

	if (t == NULL) {
		return;
	}

	switch (t->tag) {

	case UNOP_TAG:
		u = (UnopTree) t;
		if (typeQuery(u->expn.type) == TYERROR)
			break;
		if (u->expn.reg)
			break;
		encode((Tree) u->left);
		encodeUnop(u);
		break;

	case BINOP_TAG:
		b = (BinopTree) t;

		if (typeQuery(b->expn.type) == TYERROR)
			break;
		if (b->expn.reg)
			break;

		/* logical operators cannot do default evaluation order
		 */
		if (b->op == LAND_OP) {
			encodeLand(b);
		} else if (b->op == LOR_OP) {
			encodeLor(b);
		}
		/* convenient to override default evaluation for comma,
		   too */
		else if (b->op == COMMA_OP) {
			encodeComma(b);
		} else {
			encode((Tree) b->left);
			encode((Tree) b->right);
			encodeBinop(b);
		}
		break;

	case TRIOP_TAG:
		tr = (TriopTree) t;
		if (typeQuery(tr->expn.type) == TYERROR)
			break;
		if (tr->expn.reg)
			break;
		if (tr->op == COND_OP) {
			encodeCond(tr);
		} else {
			bugT(t, "encode: unknown triop");
		}
		break;

	case FCALL_TAG:
		f = (FcallTree) t;
		if (typeQuery(f->expn.type) == TYERROR)
			break;
		if (f->expn.reg)
			break;
		encodeFcall(f);
		break;

	case ESEQ_TAG:
		e = (EseqTree) t;
		if (e->expn.reg)
			break;
		encode((Tree) e->left);
		encode((Tree) e->right);
		/* pass up the register of the left tree */
		e->expn.reg = e->left->reg;
		break;

	case PTR_TAG:
		ptr = (PtrTree) t;
		if (typeQuery(ptr->expn.type) == TYERROR)
			break;
		if (ptr->expn.reg)
			break;
		encode((Tree) ptr->left);
		encodePtr(ptr);
		break;

	case FIELDREF_TAG:
		fr = (FieldRefTree) t;
		if (typeQuery(fr->expn.type) == TYERROR)
			break;
		if (fr->expn.reg)
			break;
		encode((Tree) fr->left);
		encodeFieldRef(fr);
		break;

	case CASTOP_TAG:
		c = (CastopTree) t;
		if (typeQuery(c->expn.type) == TYERROR)
			break;
		if (c->expn.reg)
			break;
		encode((Tree) c->left);
		encodeCast(c);
		/* Can't get here */
		break;

	case VAR_TAG:
		v = (VarTree) t;
		if (typeQuery(v->expn.type) == TYERROR)
			break;
		if (v->expn.reg)
			break;

		if ((v->block == 0) || (v->class == EXTERN_SC)
		    || typeQuery(v->expn.type) == TYFUNCTION
		    || (typeStripModifier(v->expn.type)
		        && (typeQuery(typeStripModifier(v->expn.type))
		                                      == TYFUNCTION)
		        && v->stdrTag == GDECL)) {

			/* global variable, local extern, or function */
			v->expn.reg = allocReg(typeQuery(v->expn.type),
			                       "%s", st_get_id_str(v->name));

		} else {
			/* local variable or parameter */
			if (v->class == STATIC_SC) {
				/* for static locals use a munged name */
				v->expn.reg = allocReg(typeQuery(v->expn.type),
				                       "$%s$L%u",
				                       st_get_id_str(v->name),
				                       v->staticLabel);
			} else {
				/* non-static vars: get the register
 				 * descriptor from the vartab */
				v->expn.reg = vartabGetReg(v->offset,
				                           v->block,
				                           v->stdrTag);
				if (!v->expn.reg
				    && typeQuery(v->expn.type) == TYPOINTER) {
					/*
 					 * if there is not a variable
 					 * allocated yet, this is
 					 * probably the anonymous
 					 * struct space used for a
 					 * function return, lets load
 					 * its address into a register.
 					 */
					v->expn.reg = allocNextReg(TYPOINTER);
					comment("struct used for function "
					        "return at %d(%%rbp)",
					        v->offset);
					emit("\tleaq\t-%d(%%rbp), %s",
					     sizeOfLocalsArea - v->offset,
					     regString(v->expn.reg));
				}

				if (!v->expn.reg) {
					bug("unregistered variable "
					    "offset=%d, block=%d, stdrTag=%s\n",
					    v->offset, v->block,
					    v->stdrTag == LDECL ? "LDECL"
					                        : "PDECL");
				}
			}
		}
		break;

	case INT_CONST_TAG:
	case UINT_CONST_TAG:
		i = (IntConstTree) t;
		if (i->expn.reg)
			break;
		i->expn.reg = allocReg(typeQuery(i->expn.type),
		                       "$%u", i->value);
		break;

	case LONG_CONST_TAG:
	case ULONG_CONST_TAG:
		l = (LongConstTree) t;
		if (l->expn.reg)
			break;
		l->expn.reg = allocReg(typeQuery(l->expn.type),
		                       "$%lu", l->value);
		break;

	case FP_CONST_TAG:
		fp = (FpConstTree) t;
		if (fp->expn.reg)
			break;

		label = genLabel();

		switch (typeQuery(fp->expn.type)) {
		case TYFLOAT:
			typeStr = "float";
			break;
		case TYDOUBLE:
			typeStr = "double";
			break;
		case TYLONGDOUBLE:
			typeStr = "tfloat";
			break;
		default:
			bugT(t,
			     "encode: unexpected type in FP_CONST_TAG");
		}
		comment("floating point constant");
		emitSection("data");
		/* this is a label, but its for data not instructions */
		emit("FPC%d:\t.%s %s", label, typeStr, fp->value);
		emitSection("text");

		fp->expn.reg = allocReg(typeQuery(fp->expn.type),
		                        "FPC%d", label);
		break;

	case STRING_LIT_TAG:
		s = (StringLitTree) t;
		if (s->expn.reg)
			break;

		label = genLabel();
		s->expn.reg = allocNextReg(typeQuery(s->expn.type));

		comment("string literal");
		emitSection("data");
		/* this is a label for data not instructions */
		emit("L$%d:\t.asciz\t%s", label, s->string);
		emitSection("text");

		emit("\t%s\t$L$%d, %s",
		     moveString(typeQuery(s->expn.type)),
		     label,
		     regString(s->expn.reg));
		defReg(s->expn.reg);
		break;

	case LABEL_TAG:
		la = (LabelTree) t;
		encodeLabel(la);
		break;

	case CASE_TAG:
		ca = (CaseTree) t;
		encodeCase(ca);
		break;

	case DEFAULT_TAG:
		d = (DefaultTree) t;
		encodeDefault(d);
		break;

	case SEQ_TAG:
		se = (SeqTree) t;
		encode(se->left);
		freeExpressionStatementRegister(se->left);
		encode(se->right);
		freeExpressionStatementRegister(se->right);
		break;

	case IFELSE_TAG:
		ie = (IfElseTree) t;
		encodeIfElse(ie);
		break;

	case SWITCH_TAG:
		sw = (SwitchTree) t;
		encodeSwitch(sw);
		break;

	case WHILE_TAG:
		w = (WhileTree) t;
		encodeWhile(w);
		break;

	case DOWHILE_TAG:
		dw = (DoWhileTree) t;
		encodeDoWhile(dw);
		break;

	case FOR_TAG:
		fo = (ForTree) t;
		encodeFor(fo);
		break;

	case GOTO_TAG:
		g = (GotoTree) t;
		encodeGoto(g);
		break;

	case CONTINUE_TAG:
		co = (ContinueTree) t;
		encodeContinue(co);
		break;

	case BREAK_TAG:
		br = (BreakTree) t;
		encodeBreak(br);
		break;

	case RETURN_TAG:
		r = (ReturnTree) t;
		encode((Tree) r->expr);
		encodeReturn(r);
		break;

	case ERROR_TAG:
		break;

	default:
		bugT(t, "Invalid tree tag in encode()");

	}

	return;
}

/**
 * \brief Computes the size in bytes of all of the parameters in the
 *        given ParamList.
 */
unsigned int paramListSize(ParamList params)
{
	unsigned int size;
	ParamList p;

	size = 0;

	if (params == NULL)
		return 0;

	for (p = params; p; p = p->next) {
		switch (typeQuery(p->type)) {
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
		case TYUNSIGNEDCHAR:
		case TYSIGNEDCHAR:
		case TYSIGNEDLONGINT:
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			size += 8;
			break;
		case TYFLOAT:
		case TYDOUBLE:
			size += 8;
			break;
		case TYLONGDOUBLE:
			size += 16;
			break;
		case TYSTRUCT:
		case TYUNION:
			/* TODO: small structs passed in registers? --
			   pjh */
			size += computeSize(p->type);
			break;
		case TYVOID:
			return 0;
		case TYERROR:
			bug("error type in paramListSize");
		case TYARRAY:
			bug("array type in paramListSize");
		case TYFUNCTION:
			bug("function type in paramListSize");
		default:
			bug("unknown type in paramListSize");
		}
	}

	return size;
}

/*
 *  encodeFunctionEntry: generates code for function entry.
 *
 *  We don't use the savedRegsSize argument.
 *
 *  argBuildSize is used to pass the size of the parameters for this
 *  function.  We use this to allocate the space to move the parameters
 *  from registers and the EM64T frame onto the stack.
 *
 *  The isStatic parameter tracks whether the function was declared
 *  with "static" storage class.
 *
 */
void encodeFunctionEntry(ST_ID name, int localsSize, int savedRegsSize,
			 int argBuildSize, BOOLEAN isStatic)
{
	int frameSize;

	/* stack depth at the beginning of a function is *always* zero */
	encodeStackDepth = 0;

	frameSize = roundEven16(localsSize);

	/* need to remember this in order to generate code for LDECLS
	   later */
	sizeOfLocalsArea = frameSize;

	comment("function entry");
	emitSection("text");
	if (!isStatic) {
		emit("\t.globl\t%s", st_get_id_str(name));
	}
	emit("!FUNCTION %s", st_get_id_str(name));
	emit("%s:", st_get_id_str(name));

	/* the function is 16-byte aligned *after* pushing %rbp since
	   the return value makes it only 8-byte aligned... so don't
	   keep track of this in our encodeStackDepth */
	emit("\tpushq\t%%rbp");
	emit("\tmovq\t%%rsp, %%rbp");

	/*
	 * Allocate local variable space on the stack.
	 *
	 * NOTE: The annotation needs to be emitted even if the
	 *       frameSize is zero incase spill code is needed.
	 */
	comment1("%d bytes allocated for locals", frameSize);
	emitAllocLocals(frameSize);
	if (frameSize != 0) {
		emit_stack_grow(frameSize);
	}

	encodeSaveCallee();
}

/*
 *  encodeFunctionExit: generates code for function exit.
 *
 *  We don't use the savedRegsSize and argBuildSize arguments.
 *
 *  The isStatic parameter tracks whether the function was declared
 *  with "static" storage class.
 *
 */
void encodeFunctionExit(ST_ID name, int savedRegsSize, int argBuildSize)
{
	comment("function exit");
	emitLabel("%s$exit", st_get_id_str(name));

	encodeRestoreCallee();

	/* must annotate even with no locals */
	comment1("%d bytes removed for locals and params", sizeOfLocalsArea);
	emitDeallocLocals(sizeOfLocalsArea);
	if (sizeOfLocalsArea != 0) {
		emit_stack_shrink(sizeOfLocalsArea);
	}

	/* the function is 16-byte aligned *after* pushing %rbp since
	   the return value makes it only 8-byte aligned... so don't
	   keep track of this in our encodeStackDepth */
	emit("\tpopq\t%%rbp");

	emitReturn("\tret");
	emit("\t.data");

	if (encodeStackDepth != 0) {
		bug("encodeStackDepth=%d not zero in encodeFunctionExit",
		    encodeStackDepth);
	}

	vartabClear();
	initRegAlloc();
}

/**
 * \brief Encodes the conversion from a floating point type.
 */
static void encodeFloatConversion(UnopTree u)
{
	TypeTag srcType, dstType;
	REG reg;

	dstType = typeQuery(u->expn.type);
	srcType = typeQuery(u->left->type);

	u->expn.reg = allocNextReg(dstType);

	switch (dstType) {
	case TYLONGDOUBLE:
		move(u->left->reg, u->expn.reg);
		break;

	case TYFLOAT:
	case TYDOUBLE:
		if (srcType == dstType)
			break;

		comment1("convert between different floating point types");
		useReg(u->left->reg);
		move(u->left->reg, u->expn.reg);
		defReg(u->expn.reg);
		break;

	case TYUNSIGNEDLONGINT:
	case TYSIGNEDLONGINT:
		comment1("convert floating point to long");
		useReg(u->left->reg);
		move(u->left->reg, u->expn.reg);
		defReg(u->expn.reg);
		break;

	default:
		comment1("convert between floating point and integer type");

		if (dstType != TYSIGNEDINT && dstType != TYUNSIGNEDINT) {
			/* first convert to an int */
			comment1("convert to an integer type via an int");
			reg = allocNextReg(TYSIGNEDINT);
			useReg(u->left->reg);
			move(u->left->reg, reg);
			defReg(reg);
		} else {
			reg = u->left->reg;
		}

		useReg(reg);
		move(reg, u->expn.reg);
		defReg(u->expn.reg);
	}
}

/**
 * \brief Encodes a conversion to an integer type.
 */
static void encodeIntegerConversion(UnopTree u)
{
	TypeTag srcType, dstType;
	REG reg;

	if (computeSize(u->left->type) > computeSize(u->expn.type)) {
		comment1("dest type is smaller than src type: do nothing");
		u->expn.reg = u->left->reg;
		return;
	}

	srcType = typeQuery(u->left->type);
	dstType = typeQuery(u->expn.type);

	u->expn.reg = allocNextReg(dstType);

	switch (dstType) {
	case TYLONGDOUBLE:
		move(u->left->reg, u->expn.reg);
		break;

	case TYFLOAT:
	case TYDOUBLE:
		if (srcType != TYUNSIGNEDINT
		    && srcType != TYSIGNEDINT
		    && srcType != TYUNSIGNEDLONGINT
		    && srcType != TYSIGNEDLONGINT) {
			/* first convert to an int */
			comment1("convert to a float through an int");
			reg = allocNextReg(TYSIGNEDINT);
			useReg(u->left->reg);
			move(u->left->reg, reg);
			defReg(reg);
		} else {
			reg = u->left->reg;
		}
		comment1("convert integer to floating point");
		useReg(reg);
		move(reg, u->expn.reg);
		defReg(u->expn.reg);
		break;

	case TYSIGNEDCHAR:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDSHORTINT:
	case TYUNSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDINT:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
		useReg(u->left->reg);
		move(u->left->reg, u->expn.reg);
		defReg(u->expn.reg);
		break;

	default:
		/* do nothing, I guess... */
		comment1("do-nothing-conversion... aparantly");
		u->expn.reg = u->left->reg;
		break;
	}
}

/**
 * \brief Checks for bugs in the destination type of a conversion node.
 */
static void checkConvertDstType(Tree t, TypeTag type)
{
	switch (type) {
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
	case TYSIGNEDLONGINT:
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDLONGINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYPOINTER:
		break;

	case TYVOID:
		bugT(t, "void destination type mishandled in encodeConvert()");

	case TYERROR:
		bugT(t, "error type seen in encodeConvert()");

	case TYSTRUCT:
		bugT(t, "struct type seen in encodeConvert()");

	case TYUNION:
		bugT(t, "union type seen in encodeConvert()");

	case TYARRAY:
		bugT(t, "array type seen in encodeConvert()");

	case TYFUNCTION:
		bugT(t, "function type seen in encodeConvert()");

	default:
		bugT(t, "unknown type seen in encodeConvert()");
	}
}

/*
 *  encodeConvert
 *
 *  This routine exploits the fact that we store all integer types in
 *  32 bits on the stack.
 *
 *  So the conversion possibilities are greatly restricted. The
 *  four possibilities are then 32-bit integer, 32-bit f.p., 64-bit f.p.
 *  and 80-bit f.p.
 *
 *  And the routine also exploits the fact that in the IA-32 all f.p.
 *  values are stored in a f.p. register using the 80-bit format. So
 *  basically a conversion is just a load of the value into a f.p. register
 *  and then store it back in the required format.
 *
 *  The complication is that, when converting from f.p. type to integer type,
 *  we need to manipulate the f.p. control word in order to set the rounding
 *  mode to "truncate" otherwise the "convert to integer" instruction will
 *  round, rather than truncate.
 *
 *  Also problem when going from unsigned integer types to f.p. types. Need
 *  to load "long long" into the f.p. register in order to make sure the
 *  sign bit is clear.
 *
 */
static void encodeConvert(UnopTree u)
{
	TypeTag srcType, dstType;

	comment("Encode convert");

	/* first some structural sanity checks */
	if (!u)
		bug("null pointer passed to encodeConvert");
	if (!u->left)
		bug("null child pointer passed to encodeConvert");

	if (u->left->absn.tag == INT_CONST_TAG) {
		/* just pass the register descriptor up */
		u->expn.reg = u->left->reg;
	}

	srcType = typeQuery(u->left->type);
	dstType = typeQuery(u->expn.type);

	/* weed out dest types that are bugs */
	checkConvertDstType((Tree) u, dstType);

	/* if error or pointer type just pass the register up */
	if (dstType == TYERROR || dstType == TYPOINTER) {
		u->expn.reg = u->left->reg;
		return;
	}

	/* if destination type is void, just pop the stack */
	if (dstType == TYVOID) {
		freeExpressionStatementRegister((Tree) (u->left));
		u->expn.reg = u->left->reg;
		return;
	}

	switch (srcType) {
	case TYFLOAT:
	case TYDOUBLE:
	case TYLONGDOUBLE:
		comment("convert from floating point");
		encodeFloatConversion(u);
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
		comment("convert from an integer type");
		encodeIntegerConversion(u);
		break;

	case TYPOINTER:
		/* just pass the register up */
		u->expn.reg = u->left->reg;
		break;

	case TYVOID:
		bugT((Tree) u,
		     "void source type seen in encodeConvert()");
		break;
	case TYERROR:
		bugT((Tree) u, "error type seen in encodeConvert()");
		break;
	case TYSTRUCT:
		bugT((Tree) u, "struct type seen in encodeConvert()");
		break;
	case TYUNION:
		bugT((Tree) u, "union type seen in encodeConvert()");
		break;
	case TYARRAY:
		bugT((Tree) u, "array type seen in encodeConvert()");
		break;
	case TYFUNCTION:
		bugT((Tree) u, "function type seen in encodeConvert()");
		break;
	default:
		bugT((Tree) u, "unknown type seen in encodeConvert()");
	}
}

/*
 *  Arrays and functions are always passed by their base address.
 *
 *  Also all parameters are at least 8 bytes in size. This is
 *  because we always keep the run-time stack aligned on 8 byte boundaries.
 */
int computeParamSize(Type t)
{
	int tmp;

	switch (typeQuery(t)) {

	case TYARRAY:
	case TYFUNCTION:
		return TARCH_SIZEOF_POINTER;

	default:
		tmp = computeSize(t);
		if (tmp < TARCH_SIZEOF_STACKWORD)
			tmp = TARCH_SIZEOF_STACKWORD;
		return tmp;

	}
	return 0;		/* NOTREACHED */
}

/*
 *  Given address of stuct/union and the size, copy it to the top of the
 *  stack.
 *
 *  The stack space must be allocated prior to calling this function.
 *
 *  This function uses register r11 as a temporary register.  No other
 *  registers are clobbered by this function so it should be safe to use
 *  while building parameter lists for function calls!
 */
static void copyStructUnionToTopOfStack(REG src, int size, int startOffs)
{
	int stackOffs;
	int structOffs;

	structOffs = 0;
	stackOffs = startOffs;

	/*
 	 * we don't want to use any registers that are used for integer
 	 * argument passing here... lets just use scratch register r11
 	 * since it is not used for argument passing.
 	 */

	comment1("copyStructUnionToTopOfStack src=%s, size=%d, startOffs=%d",
	         regString(src), size, startOffs);
	while (size) {
		emit("\tmovq\t%d(%s), %%r11", structOffs, regString(src));
		useReg(src);
		emit("\tmovq\t%%r11, %d(%%rsp)", stackOffs);
		stackOffs += 8;
		structOffs += 8;
		size -= 8;
	}
}

/*
 *  Recursive function to traverse and encode FCALL parameter list in reverse
 *  order.
 *
 *  Need to track the amount of stack allocated in order to pop the
 *  arguments after the call. And to properly do stack alignment.
 *  Need to worry about alignment because stack must be on 16-byte boundary
 *  boundary when fcall is issued.
 *
 *  The second parameter is the function return type. If the return type
 *  is a struct/union then the size of the first argument is 8: a pointer
 *  to where to put the return value.
 *
 *  Special care must be taken with struct args. Encoding arg will only
 *  produce the base address on top of the stack. I must pop the base
 *  address and copy the struct on to the top of stack.
 *
 */
static void encodeFcallHelper(ExpTree e, Type retType)
{
	ExpTree exp;

	if (e->absn.tag == ESEQ_TAG) {
		/* if we have a seq, node: encode the right and use the
		 * expr on the left of it. */
		encodeFcallHelper(((EseqTree) e)->right, retType);
		exp = ((EseqTree) e)->left;
	} else {
		/* if there is no seq node: just use the given tree */
		exp = e;
	}

	encode((Tree) exp);

	/* if we had a sequence node, pass the register up to it from
 	 * its left expression. */
	if (e->absn.tag == ESEQ_TAG)
		e->reg = exp->reg;
}

/*
 * This routine is called once for each argument in order. It decides
 * if an argument will be passed in a register or on the stack. It
 * is passed the type of the argument, plus counts for how many
 * integer and floating point arguments have been used by prior
 * arguments. These counts are updated by this routine if a register
 * is used by the current argument.
 */
static int analyzeOneArg(Type t, int *intRegCnt, int *floatRegCnt)
{
	switch (typeQuery(t)) {
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		*intRegCnt += 1;
		if (*intRegCnt > 6) {
			return 8;
		} else {
			return 0;
		}

	case TYFLOAT:
	case TYDOUBLE:
		*floatRegCnt += 1;
		if (*floatRegCnt > 6) {
			return 8;
		} else {
			return 0;
		}

	case TYLONGDOUBLE:
		return 16;

	case TYSTRUCT:
	case TYUNION:
		/* TODO: small structs passed in registers? -- pjh */
		return computeSize(t);

	case TYERROR:
		bug("error type in analyzeOneArg");
		break;

	case TYVOID:
		bug("void type in analyzeOneArg");

	case TYARRAY:
		bug("array type in analyzeOneArg");

	case TYFUNCTION:
		bug("function type in analyzeOneArg");

	default:
		bug("unknown type in analyzeOneArg");
	}
	return 0;		/* not reached */
}

/*
 * Calculate how many bytes will be passed on the stack for an argument list.
 */
static int bytesPassedViaStack(ExpTree e, Type retType)
{
	int ret;
	int intRegCnt;
	int floatRegCnt;

	ret = 0;

	intRegCnt = 0;
	floatRegCnt = 0;

	/* first arg might be a pointer to support returning structs */
	if (isStructUnionType(retType)) {
		intRegCnt += 1;
	}

	/* more than one argument */
	if (((Tree) e)->tag == ESEQ_TAG) {
		ExpTree cur;
		cur = e;
		while (((Tree) cur)->tag == ESEQ_TAG) {
			ret +=
			    analyzeOneArg(((EseqTree) cur)->left->type,
					  &intRegCnt, &floatRegCnt);
			cur = ((EseqTree) cur)->right;
		}
		ret += analyzeOneArg(cur->type, &intRegCnt, &floatRegCnt);
	} else {		/* only one argument */
		ret += analyzeOneArg(e->type, &intRegCnt, &floatRegCnt);
	}

	return ret;
}

/*
 * This routine moves a single argument from its register into either
 * another register for the function call, or onto the stack.
 */
static void moveOneArg(Type t, int *intRegCnt, int *floatRegCnt,
		       REG reg, int *em64tOffset)
{
	int size;
	REG tmp;

	comment1("move one arg (%d, %d, %s, %d)", *intRegCnt,
	        *floatRegCnt, regString(reg), *em64tOffset);

	switch (typeQuery(t)) {
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		*intRegCnt += 1;
		if (*intRegCnt > TARCH_INT_REGCLASS_SIZE) {
			if (isMemory(reg))
				bug("memory register passed to moveOneArg");
			comment1("move register to memory argument");
			useReg(reg);
			emit("\t%s\t%s, %d(%%rsp)",
			     moveString(TYPOINTER),
			     regStringAs(TYPOINTER, reg),
			     *em64tOffset);
			*em64tOffset += 8;
		} else {
			comment1("move register to register argument");
			useReg(reg);
			emit("\t%s\t%s, %s",
			     moveString(TYPOINTER),
			     regStringAs(TYPOINTER, reg),
			     int_reg_arg(*intRegCnt));
		}
		break;

	case TYFLOAT:
	case TYDOUBLE:
		*floatRegCnt += 1;
		if (*floatRegCnt > TARCH_SSE_REGCLASS_SIZE) {
			if (isMemory(reg))
				bug("memory register passed to moveOneArg");
			comment1("move register to memory argument");
			useReg(reg);
			emit("\t%s\t%s, %d(%%rsp)",
			     moveString(TYPOINTER),
			     regString(reg),
			     *em64tOffset);
			*em64tOffset += 8;
		} else {
			comment1("move register to register argument");
			useReg(reg);
			emit("\t%s\t%s, %s",
			     moveString(typeQuery(t)),
			     regString(reg),
			     float_reg_arg(*floatRegCnt));
		}
		break;

	case TYLONGDOUBLE:
		comment1("move long double to memory argument");
		tmp = allocReg(TYLONGDOUBLE, "%d(%%rsp)", *em64tOffset);
		useReg(reg);
		move(reg, tmp);
		*em64tOffset += 16;
		break;

	case TYSTRUCT:
	case TYUNION:
		/* TODO: small structs passed in registers? -- pjh */
		size = computeSize(t);
		if (size % 8 != 0) {
			bug("struct/union size not even multiple of 8 "
			    "in moveOneArg!\n");
		}

		/*
		 * XXX - small structs are *still* not handled properly.
		 *
		 * if the whole thing can't fit in registers, then the
		 * whole thing is passed in memory.
		 *
		 * the fields are then stuck in registers depending on
		 * their class (and <64-bit types are packed!).  another
		 * words a float field would be passed in an xmm%d
		 * register.
		 */

		if (size <= 16 && *intRegCnt < TARCH_INT_REGCLASS_SIZE) {
			/* small structs are passed in registers */
			comment1("small struct passed in register");
			*intRegCnt += 1;
			emit("\tmovq\t0(%s), %s",
			     regString(reg), int_reg_arg(*intRegCnt));
			useReg(reg);

			if (size <= 8)
				break;

			if (*intRegCnt < TARCH_INT_REGCLASS_SIZE) {
				/* second half also passed in a reg */
				comment1("16-byte struct second half "
					 "passed in a register");
				*intRegCnt += 1;
				emit("\tmovq\t8(%s), %s",
				     regString(reg), int_reg_arg(*intRegCnt));
				useReg(reg);
			} else {
				/* second half passed on the stack: use
 				 * r11 for memory-to-memory moves */
				comment1("16-byte struct second half "
					 "passed on the stack");
				emit("\tmovq\t8(%s), %%r11",
				     regString(reg));
				useReg(reg);
				emit("\tmovq\t%%r11, %d(%%rsp)",
				     *em64tOffset);
				*em64tOffset += 8;
			}
		} else {
			/* large structs are passed on the stack */
			comment1("move struct|union to memory argument");
			copyStructUnionToTopOfStack(reg, size, *em64tOffset);
			*em64tOffset += size;
		}
		break;

	case TYERROR:
		bug("error type in moveOneArg");

	case TYVOID:
		bug("void type in moveOneArg");

	case TYARRAY:
		bug("array type in moveOneArg");

	case TYFUNCTION:
		bug("function type in moveOneArg");

	default:
		bug("unknown type in moveOneArg");
	}
}

/*
 * This routine moves the arguments from the registers that they were
 * evaluated into, to either an argument register or onto the stack.
 */
static int moveArguments(ExpTree e, Type retType)
{
	ExpTree exp;
	ExpTree cur;
	int intRegCnt;
	int floatRegCnt;
	int em64tOffset;

	intRegCnt = 0;
	floatRegCnt = 0;
	em64tOffset = 0;

	comment1("move arguments into registers or the stack");

	/* get the first parameter */
	if (e->absn.tag == ESEQ_TAG)
		exp = ((EseqTree) e)->left;
	else
		exp = e;

	/* first arg might be a pointer to support returning structs */
	if (isStructUnionType(retType)) {
		comment1("move struct/union return pointer");
		intRegCnt += 1;
		useReg(exp->reg);
		emit("\t%s\t%s, %s",
		     moveString(TYPOINTER),
		     regString(exp->reg),
		     int_reg_arg(intRegCnt));
	}

	if (e) {
		/* more than one argument */
		if (e->absn.tag == ESEQ_TAG) {
			cur = e;
			while (cur->absn.tag == ESEQ_TAG) {
				moveOneArg(((EseqTree) cur)->left->type,
					   &intRegCnt, &floatRegCnt,
					   cur->reg, &em64tOffset);
				cur = ((EseqTree) cur)->right;
			}
			moveOneArg(cur->type, &intRegCnt, &floatRegCnt,
				   cur->reg, &em64tOffset);
		} else {
			moveOneArg(e->type, &intRegCnt, &floatRegCnt,
				   e->reg, &em64tOffset);
		}
	}

	return floatRegCnt;
}

/*
 *  Encode a function call.
 *
 *  Just need to process the argument list in reverse order to get
 *  arguments evaluated on top of the stack. Then emit the call instruction.
 *  When function returns need to push the result on top of stack.
 */
static void encodeFcall(FcallTree b)
{
	int bytesOnStack;
	int numberOfSseRegisters;
	int alignmentIncrement;

	bytesOnStack = 0;
	numberOfSseRegisters = 0;
	alignmentIncrement = 0;

	if (b->params != NULL) {

		/* Use recursive function to load arguments on stack in
		   reverse order. */
		encodeFcallHelper(b->params, b->expn.type);

		/* Figure out how many bytes need to be passed on the
		   stack. */
		bytesOnStack =
		    bytesPassedViaStack(b->params, b->expn.type);
	}

	encodeSaveCaller();

	comment("function call bytesOnStack=%d", bytesOnStack);

	/* What are we calling? */
	encode((Tree) b->func);

	/* keep the stack 16-byte aligned for the function call */
	if ((encodeStackDepth + bytesOnStack) % 16 != 0) {
		alignmentIncrement =
		    16 - ((encodeStackDepth + bytesOnStack) % 16);
	}

	/* allocate space on the stack for arguments to be placed on
	   stack */
	if ((bytesOnStack + alignmentIncrement) > 0) {
		comment1("allocating bytesOnStack=%d, alignmentIncrement=%d",
		         bytesOnStack, alignmentIncrement);
		emit_stack_grow(bytesOnStack + alignmentIncrement);
	}

	/*
	 * Go through the arguments again to move values to registers or to
	 * new EM64T argument area lower on stack.
	 */
	if (b->params != NULL) {
		numberOfSseRegisters = moveArguments(b->params, b->expn.type);

		/* move the number of SSE registers into %rax */
		comment1("%d SSE registers used",
		     numberOfSseRegisters >
		     TARCH_SSE_REGCLASS_SIZE ? TARCH_SSE_REGCLASS_SIZE :
		     numberOfSseRegisters);
		emit("\tmovq\t$%d, %%rax",
		     numberOfSseRegisters >
		     TARCH_SSE_REGCLASS_SIZE ? TARCH_SSE_REGCLASS_SIZE :
	     	     numberOfSseRegisters);
	}

	useReg(b->func->reg);
	emit("\tcall\t%s%s",
	     isRegister(b->func->reg) ? "*": "",
	     regString(b->func->reg));

	if (bytesOnStack + alignmentIncrement != 0) {
		comment1("deallocating bytesOnStack=%d, "
		         "alignmentIncrement=%d",
		         bytesOnStack, alignmentIncrement);
		emit_stack_shrink(bytesOnStack + alignmentIncrement);
	}

	encodeRestoreCaller();

	if (typeQuery(b->expn.type) != TYVOID)
		b->expn.reg = allocNextReg(typeQuery(b->expn.type));

	/* If there is a return value, put it in our return value
 	 * register */
	switch (typeQuery(b->expn.type)) {
	case TYVOID:		/* Do Nothing */
		break;

	case TYFLOAT:
	case TYDOUBLE:
		comment1("float or double return value in %s",
		         regString(b->expn.reg));
		emit("\t%s\t%%xmm0, %s",
		     moveString(typeQuery(b->expn.type)),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	case TYLONGDOUBLE:
		comment1("long double return value in %s",
		         regString(b->expn.reg));
		emitFst(b->expn.reg);
		defReg(b->expn.reg);
		break;

	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYSTRUCT:
	case TYUNION:
	case TYARRAY:
	case TYPOINTER:
		comment1("integer return value in %s",
		         regString(b->expn.reg));
		emit("\t%s\t%s, %s",
		     moveString(typeQuery(b->expn.type)),
		     registerAs('a', typeQuery(b->expn.type)),
		     regString(b->expn.reg));
		defReg(b->expn.reg);
		break;

	default:
		bug("unexpected return type");
	}
}

/*
 * Allocates a register descriptor for the given parameter and if
 * needed, moves the parameter's value to its register.
 */
static void paramToREG(ParamList p, int *intRegs, int *floatRegs,
                       int *em64tOffs)
{
	REG reg;
	ST_DR stdr;
	TypeTag type;
	int size;
	int offset, block;

	*floatRegs = *floatRegs;

	type = typeQuery(p->type);

	/*
	 * I think this is ok, the symbol table for the outter most
	 * block seems to be intact at this point... we just need the
	 * block depth and the offset.  There may be a better way of
	 * doing this:
	 * 	block = 0 (or is it 1?)
	 * 	offset = em64tOffs or somthing?
	 */
	stdr = st_lookup(p->id, &block);
	offset = stdr->u.decl.offset;

	switch (typeQuery(p->type)) {
	case TYSIGNEDSHORTINT:
	case TYSIGNEDINT:
	case TYUNSIGNEDSHORTINT:
	case TYUNSIGNEDINT:
	case TYUNSIGNEDCHAR:
	case TYSIGNEDCHAR:
	case TYSIGNEDLONGINT:
	case TYUNSIGNEDLONGINT:
	case TYPOINTER:
		*intRegs += 1;
		if (*intRegs > TARCH_INT_REGCLASS_SIZE) {
			if (isAddressed(offset, block, PDECL)) {
				/* if the variable is passed on the
 				 * stack and its address is taken, just
 				 * use the memory location on the stack
 				 * as its register descriptor. */
				reg = allocReg(type, "%d(%%rbp)", *em64tOffs);
				vartabSetReg(offset, block, PDECL, reg);
			} else {
				/* variable is not addressed, move it to
 				 * its register. */
				reg = allocNextReg(typeQuery(p->type));
				vartabSetReg(offset, block, PDECL, reg);
				comment1("move variable to register");
				emit("\t%s\t%d(%%rbp), %s",
				     moveString(TYPOINTER),
				     *em64tOffs,
				     regStringAs(TYPOINTER, reg));
				defReg(reg);
			}
			*em64tOffs += TARCH_SIZEOF_STACKWORD;
		} else {
			if (isAddressed(offset, block, PDECL)) {
				/* if the variable is passed in a
 				 * register, and its address is taken,
 				 * we need to push it onto the locals
 				 * space and use this new location as
 				 * its register descriptor. */
				comment1("move addressed param to local slot");
				emit_push_reg(int_reg_arg(*intRegs));
				sizeOfLocalsArea += TARCH_SIZEOF_STACKWORD;
				reg = allocReg(type, "-%d(%%rbp)",
				               sizeOfLocalsArea);
				vartabSetReg(offset, block, PDECL, reg);
			} else {
				/* variable is not addressed, move it to
 				 * its register. */
				reg = allocNextReg(typeQuery(p->type));
				vartabSetReg(offset, block, PDECL, reg);
				comment1("move variable to register");
				emit("\t%s\t%s, %s",
				     moveString(TYPOINTER),
				     int_reg_arg(*intRegs),
				     regStringAs(TYPOINTER, reg));
				defReg(reg);
			}
		}
		break;

	case TYFLOAT:
	case TYDOUBLE:
		*floatRegs += 1;
		if (*floatRegs > TARCH_SSE_REGCLASS_SIZE) {
			if (isAddressed(offset, block, PDECL)) {
				/* if the variable is passed on the
 				 * stack and its address is taken, just
 				 * use the memory location on the stack
 				 * as its register descriptor. */
				reg = allocReg(type, "%d(%%rbp)", *em64tOffs);
				vartabSetReg(offset, block, PDECL, reg);
			} else {
				/* variable is not addressed, move it to
 				 * its register. */
				reg = allocNextReg(typeQuery(p->type));
				vartabSetReg(offset, block, PDECL, reg);
				comment1("move variable to register");
				emit("\t%s\t%d(%%rbp), %s",
				     moveString(typeQuery(p->type)),
				     *em64tOffs,
				     regString(reg));
				defReg(reg);
			}
			*em64tOffs += TARCH_SIZEOF_STACKWORD;
		} else {
			if (isAddressed(offset, block, PDECL)) {
				/* if the variable is passed in a
 				 * register, and its address is taken,
 				 * we need to push it onto the locals
 				 * space and use this new location as
 				 * its register descriptor. */
				comment1("move addressed param to local slot");
				emit_stack_grow(TARCH_SIZEOF_STACKWORD);
				sizeOfLocalsArea += TARCH_SIZEOF_STACKWORD;
				emit("\t%s\t%s, (%%rsp)",
				     moveString(typeQuery(p->type)),
				     float_reg_arg(*floatRegs));
				reg = allocReg(type, "-%d(%%rbp)",
				               sizeOfLocalsArea);
				vartabSetReg(offset, block, PDECL, reg);
			} else {
				/* variable is not addressed, move it to
 				 * its register. */
				reg = allocNextReg(typeQuery(p->type));
				vartabSetReg(offset, block, PDECL, reg);
				comment1("move variable to register");
				emit("\t%s\t%s, %s",
				     moveString(typeQuery(p->type)),
				     float_reg_arg(*floatRegs),
				     regString(reg));
				defReg(reg);
			}
		}
		break;

	case TYLONGDOUBLE:
		reg = allocReg(type, "%d(%%rbp)", *em64tOffs);
		vartabSetReg(offset, block, PDECL, reg);
		*em64tOffs += TARCH_SIZEOF_LDOUBLE;
		break;

	case TYSTRUCT:
	case TYUNION:
		size = computeSize(p->type);
		if (size % 8  != 0)
			bug ("size of struct|union param not a multiple of 8");

		/*
		 * Small structs are still handled wrong, see the
		 * comment in moveOneArg().
		 */

		if (size <= 16 && *intRegs < TARCH_INT_REGCLASS_SIZE) {
			/* small structs are passed in registers */
			comment1("push small struct to locals area size=%d",
			         size);
			*intRegs += 1;
			emit_push_reg(int_reg_arg(*intRegs));
			sizeOfLocalsArea += 8;
			if (size > 8) {
				if (*intRegs < TARCH_INT_REGCLASS_SIZE) {
					/* second half also in a reg */
					comment1("16-byte struct second "
					         "half was in a register");
					*intRegs += 1;
					emit_push_reg(int_reg_arg(*intRegs));
				} else {
					/* second half must be on the
 					 * stack, use r11 for
 					 * memory-to-memory copying */
					comment1("16-byte struct second "
					         "half was on the stack");
					emit_stack_grow(8);
					emit("\t%s\t%d(%%rbp), %%r11",
					     moveString(TYPOINTER),
					     *em64tOffs);
					emit("\t%s\t%%r11, 0(%%rsp)",
					     moveString(TYPOINTER));
					*em64tOffs += 8;
				}
				sizeOfLocalsArea += 8;
			}
			reg = allocReg(type, "-%d(%%rbp)", sizeOfLocalsArea);
			vartabSetReg(offset, block, PDECL, reg);
		} else {
			/* large structs are passed on the stack */
			reg = allocNextReg(TYPOINTER);
			emit("\tleaq\t%d(%%rbp), %s", *em64tOffs,
			     regString(reg));
			defReg(reg);
			vartabSetReg(offset, block, PDECL, reg);
			*em64tOffs += size;
		}
		break;

	case TYERROR:
		bug("error type in paramToREG");

	case TYVOID:
		bug("void type in paramToREG");

	case TYARRAY:
		bug("array type in paramToREG");

	case TYFUNCTION:
		bug("function type in paramToREG");

	default:
		bug("unknown type in paramToREG");
	}
}

/*
 * This routine allocates registers for all local variables and
 * parametres that do not have their addresses taken in this function.
 * If a parameter or local has its address taken, its offset from the
 * base pointer is computed, and its register descriptor is set to this
 * location.
 *
 * For parameters that have had a register allocated for them, the value
 * is moved into their register.
 *
 * For all variables that have a register allocated for them, the
 * register is defReg()'d.
 *
 * \param localsSize This parameter is an input-output parameter that is
 *                   the size of the locals space on the stack.
 *                   Arguments that are passed in registers, but have
 *                   their address taken are moved to the locals space,
 *                   the localsSize parameter is modified so that this
 *                   space can be properly cleaned up at function exit.
 */
void encodeMoveParametersToLocalSlots(ParamList list, Tree t)
{
	int intRegs;
	int floatRegs;
	int em64tOffs;
	ParamList p;

	/* ignore warning */
	t = t;

	/* initialize values */
	intRegs = 0;
	floatRegs = 0;

	/* skip over the pushed base pointer and ret val */
	em64tOffs = 16;

	/*
	 * Move parameters to their respective registers handles.
	 */
	for (p = list; p; p = p->next)
		paramToREG(p, &intRegs, &floatRegs, &em64tOffs);

	/*
	 * Allocate a register for any remaining variable that doesn't
	 * have one yet (local variables).
	 */
	vartabAllocRegs(sizeOfLocalsArea);
}

/*
 *  struct/union assignment.
 */
static void encodeStructUnionAssign(BinopTree b)
{
	int structUnionSize;
	REG src, dst;

	structUnionSize = computeSize(b->expn.type);
	src = b->right->reg;
	dst = b->left->reg;

	comment("struct/union assign");
	useReg(src);
	emit("\tmovq\t%s, %%rsi", regStringAs(TYPOINTER, src));
	useReg(dst);
	emit("\tmovq\t%s, %%rdi", regStringAs(TYPOINTER, dst));
	emit("\tmovq\t$%d, %%rcx", structUnionSize);
	emit("\trep");
	emit("\tmovsb");

	b->expn.reg = allocNextReg(typeQuery(b->expn.type));
	useReg(src);
	comment1("return value of assignemnt in %s", regString(b->expn.reg));
	emit("\t%s\t%s, %s",
	     moveString(TYPOINTER),
	     regStringAs(TYPOINTER, src),
	     regString(b->expn.reg));
	defReg(b->expn.reg);
}

/*
 * Encodes an assignment.
 */
static void encodeAssign(BinopTree b)
{
	/* special handling needed for structs and unions */
	if (isStructUnionType(b->expn.type)) {
		encodeStructUnionAssign(b);
	} else {
		encodeAssignDetail(b->expn.type, (Tree) b);
	}
}

/*
 * Encodes a function return.
 */
static void encodeReturn(ReturnTree r)
{
	comment("return");
	if (r->expr == NULL) {
		emitUncondBranch("\tjmp\t", "%s$exit",
		           st_get_id_str(gCurrentFunctionName));
	} else {
		switch (typeQuery(r->expr->type)) {
		case TYSIGNEDSHORTINT:
		case TYSIGNEDINT:
		case TYUNSIGNEDSHORTINT:
		case TYUNSIGNEDINT:
		case TYUNSIGNEDCHAR:
		case TYSIGNEDCHAR:
		case TYSIGNEDLONGINT:
		case TYUNSIGNEDLONGINT:
		case TYPOINTER:
			useReg(r->expr->reg);
			move(r->expr->reg,
			     allocReg(TYUNSIGNEDLONGINT, "%%rax"));
			break;

		case TYFLOAT:
		case TYDOUBLE:
			useReg(r->expr->reg);
			emit("\t%s\t%s, %%xmm0",
			     moveString(typeQuery(r->expr->type)),
			     regString(r->expr->reg));
			break;

		case TYLONGDOUBLE:
			useReg(r->expr->reg);
			emitFld(r->expr->reg);
			break;

		case TYSTRUCT:
		case TYUNION:
			/* copy the struct content to where the extra
			   argument points */
			comment1("return struct/union");
			useReg(r->expr->reg);
			emit("\tmovq\t%s, %%rsi", regString(r->expr->reg));
			/* push destination address, in first local */
			emit("\tmovq\t8(%%rbp), %%rdi");
			emit("\tmovq\t$%d, %%rcx", computeSize(r->expr->type));

			emit("\trep");
			emit("\tmovsb");

			/* return the struct address */
			emit("\tmovq\t8(%%rbp), %%rax");

			break;

		case TYERROR:
			break;	/* should never get here? */

		case TYVOID:
			bugT((Tree) r, "void type in encodeReturn");

		case TYARRAY:
			bugT((Tree) r, "array type in encodeReturn");

		case TYFUNCTION:
			bugT((Tree) r, "function type in encodeReturn");

		default:
			bugT((Tree) r, "unknown type in encodeReturn");
		}

		emitUncondBranch("\tjmp\t", "%s$exit",
		           st_get_id_str(gCurrentFunctionName));

	}

}

/*
 *  We are not using registers but we need to pop the stack.
 */
void freeExpressionStatementRegister(Tree t)
{
	/* ignore the parameter for now */
	t = t;
}

/*
 *  Need to adjust running offset as shift from params to locals?
 *
 *  On IA-32 I want to reset the offset to zero since params are positive
 *  offsets from %ebp and locals are negative offsets from %ebp.
 */
int adjustFunctionOffset(int currentOffset)
{
	return 0;
}
