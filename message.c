
/****************************************************************/
/*								*/
/*	CS712 - "C" Compiler					*/
/*								*/
/*	--message.c--						*/
/*								*/
/*	This File Contains routines that support the		*/
/*	"Message Module".					*/
/*								*/
/*								*/
/*								*/
/****************************************************************/



#include <stdio.h>
#include <stddef.h>
#include <stdarg.h>
#include <stdlib.h>
#include "defs.h"
#include "message.h"
#include "encode.h"

/* Counter to keep track of the number of compiler errors */
int compiler_errors;


/* Counter to keep track of the number of compiler warnings */
int compiler_warnings;


/* Counter to keep track of the number of informal compiler messages */
int compiler_messages;


/* error file pointer: assumed to be declared and initialized elsewhere */
extern FILE *errfp;


/* output standard prefix of filename, lineno */
static void msgPrefix(void)
{
	fprintf(errfp, "%s, line %d: ", YYfilename(), YYlineno());
}

/* output standard prefix of filename, lineno from AST node */
static void msgPrefixT(Tree t)
{
	fprintf(errfp, "%s, line %d: ", t->filename, t->lineno);
}

/*********************************************************************/

void msgn(char *fmt, ...)

	/* writes a message to errfp file WITH NO NEWLINE */
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	compiler_messages++;
}

/*********************************************************************/

void msg(char *fmt, ...)

	/* writes a message to errfp file WITH NEWLINE. */
{
	va_list ap;
	va_start(ap, fmt);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_messages++;
}

/*********************************************************************/

void info(char *fmt, ...)

	/* writes an informational message to errfp file with line
	   number and newline. */
{

	va_list ap;
	va_start(ap, fmt);
	msgPrefix();
	fputs("[info] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_messages++;
}

/*********************************************************************/

void warning(char *fmt, ...)

	/* this routine issues a formal warning message to errfp file */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefix();
	fputs("[warning] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_warnings++;
}

/*********************************************************************/

void error(char *fmt, ...)

	/* this routine issues a formal error message to errfp file */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefix();
	fputs("[error] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_errors++;
}

/*********************************************************************/

void fatal(char *fmt, ...)

	/* this routine writes a fatal error message to errfp file and
	   the program is aborted.

	   "fatal" usually means an internal data structure of the
	   compiler has overflowed. */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefix();
	fputs("[fatal error] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	exit(1);
}

/*********************************************************************/

void bug(char *fmt, ...)

	/* this routine writes a bug message to errfp file and the
	   program is aborted.

	   "bug" means a problem with the internals of the compiler. */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefix();
	fputs("[bug] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	exit(1);
}

/*********************************************************************/

/*
 *  The following routines are variants of the above routines that
 *  get the filename and line number out of the AST node (rather
 *  than from the scanner). This is for use in post-parse phases,
 *  like the semantic analysis and code generation routines.
 */

/*********************************************************************/

void infoT(Tree t, char *fmt, ...)

	/* writes an informational message to errfp file with line
	   number and newline. */
{

	va_list ap;
	va_start(ap, fmt);
	msgPrefixT(t);
	fputs("[info] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_messages++;
}

/*********************************************************************/

void warningT(Tree t, char *fmt, ...)

	/* this routine issues a formal warning message to errfp file */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefixT(t);
	fputs("[warning] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_warnings++;
}

/*********************************************************************/

void errorT(Tree t, char *fmt, ...)

	/* this routine issues a formal error message to errfp file */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefixT(t);
	fputs("[error] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	compiler_errors++;
}

/*********************************************************************/

void fatalT(Tree t, char *fmt, ...)

	/* this routine writes a fatal error message to errfp file and
	   the program is aborted.

	   "fatal" usually means an internal data structure of the
	   compiler has overflowed. */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefixT(t);
	fputs("[fatal error] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	exit(1);
}

/*********************************************************************/

void bugT(Tree t, char *fmt, ...)

	/* this routine writes a bug message to errfp file and the
	   program is aborted.

	   "bug" means a problem with the internals of the compiler. */
{
	va_list ap;
	va_start(ap, fmt);
	msgPrefixT(t);
	fputs("[bug] ", errfp);
	vfprintf(errfp, fmt, ap);
	va_end(ap);
	putc('\n', errfp);
	exit(1);
}

/*********************************************************************/

/* isolate all varargs stuff in this file */

extern FILE *outfp;		/* generated code goes here: set in
				   main.c */
/********************************************
 * comment
********************************************/
void comment(char *fmt, ...)
{
#if defined(COMMENT_CODE)
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	/*
	fprintf(outfp, "\n\t#\n\t# -- %s --\n\t#\n", buf);
	*/
	fprintf(outfp, "\n\t# -- %s --\n", buf);
#else
	fmt = fmt;
#endif /* COMMENT_CODE */
}

/********************************************
 * comment1
********************************************/
void comment1(char *fmt, ...)
{
#if defined(COMMENT_CODE)
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "\t# %s\n", buf);
#else
	fmt = fmt;
#endif /* COMMENT_CODE */
}

/********************************************
  emit

  Print one line to output (assembly language) file
********************************************/
void emit(char *fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "%s\n", buf);
}

/********************************************
  emitn

  Print partial line (no newline on end) to output (assembly language) file
********************************************/
void emitn(char *fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "%s", buf);
}

/**
 * \brief emits a label and adds the proper annotation.
 */
void emitLabel(const char *fmt, ...)
{
	char buf[1024];
	va_list ap;

	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "!LABEL %s\n", buf);
	fprintf(outfp, "%s:\n", buf);
}

/**
 * \brief emits a branch instruction and adds the proper annotation.
 */
void emitCondBranch(const char *mnemonic, const char *fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "%s %s\n", mnemonic, buf);

	fprintf(outfp, "!CBRANCH %s\n", buf);
}

/**
 * \brief emits a branch instruction and adds the proper annotation.
 */
void emitUncondBranch(const char *mnemonic, const char *fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "%s %s\n", mnemonic, buf);

	fprintf(outfp, "!UBRANCH %s\n", buf);
}

/**
 * \brief emits a function return instruction and adds the proper annotation.
 */
void emitReturn(const char *fmt, ...)
{
	char buf[1024];
	va_list ap;
	va_start(ap, fmt);
	vsprintf(buf, fmt, ap);
	va_end(ap);
	fprintf(outfp, "%s\n", buf);

	fprintf(outfp, "!RETURN\n");
}

void emitSection(const char *sect)
{
	fprintf(outfp, "\t.%s\n", sect);
	fprintf(outfp, "!SECTION %s\n", sect);
}

/**
 * Emits annotation that marks the following line as the allocation of
 * local variables.  This is used when inserting register spill code.
 *
 * \param size The number of bytes that the next line is allocating.
 */
void emitAllocLocals(unsigned int size)
{
	fprintf(outfp, "!ALOCALS %u\n", size);
}

/**
 * Emits annotation that marks the following line as the deallocation of
 * local variables.  This is used when inserting register spill code.
 *
 * \param size The number of bytes being deallocated.  This is not
 *             strictly needed, but is provided for symetry with
 *             emitAllocLocals().
 */
void emitDeallocLocals(unsigned int size)
{
	fprintf(outfp, "!DLOCALS %u\n", size);
}
