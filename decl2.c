/*
 *  Phase 2 code goes here:
 *    processDeclarator
 *    processDeclaration
 *    finishGlobalDeclaration 
 *    finishLocalDeclaration 
 *    plus code to support enum declarations.
 */

#include "symtab.h"
#include "types.h"
#include "globals.h"
#include "decl.h"
#include "decl2.h"
#include "encode.h"
#include "message.h"
#include "semUtils.h"

/*
 *  Process a single declarator.
 *    First parameter is the tree representation of the declarator.
 *    Second parameter is the base type.
 *    Third parameter is an output parameter through which the id is
 *      returned, if there is one.
 *    Fourth parameter is also an output parameter, indicating whether
 *      an error was detected or not.
 *    Fifth parameter is an optional output parameter. It tracks the
 *      last parameter list seen if function modifiers are processed.
 *      By "last" it is meant "farthest from the base type". This is
 *      used when processing function definitions to keep the parameter
 *      list in order to install the id's into the local symbol table.
 *      Pass NULL if you are not interested in this value.
 *    Return value is the modified type.
 */
Type processDeclarator(DECL_TREE declarator, Type baseType,
		       ST_ID * outId, BOOLEAN * outError,
		       ParamList * lastParamListSeen)
{
	DECL_TREE t;
	DECL_TREE next;
	Type retType;
	retType = baseType;
	t = declarator;

	*outId = NULL;
	*outError = FALSE;
	if (lastParamListSeen)
		*lastParamListSeen = NULL;

	while (t != NULL) {
		switch (t->tag) {
		case DECL_PTR:
			retType = typeBuildPointer(retType, t->u.qual);
			break;
		case DECL_ARR:
			if (t->u.dim < 0) {	/* 0 used for no
						   dimension given!
						   (see parse.y) */
				error("illegal array dimension");
				*outError = TRUE;
				t->u.dim = 1;
			}
			if (typeQuery(retType) == TYFUNCTION) {
				error
				    ("array element type cannot be function");
				*outError = TRUE;
				retType =
				    typeBuildBasic(TYSIGNEDINT,
						   NO_QUAL);
			}
			/* 
			 *  hmmm, need explicit check for void type because isCompleteObjectType
			 *  does not check for void. isCompleteObjectType should probably be
			 *  changed.
			 */
			if (typeQuery(retType) == TYVOID) {
				error
				    ("array element type cannot be void");
				*outError = TRUE;
				retType =
				    typeBuildPointer(retType, NO_QUAL);
			}
			if (!isCompleteObjectType(retType)) {
				error
				    ("array element type cannot be incomplete type");
				*outError = TRUE;
				retType =
				    typeBuildBasic(TYSIGNEDINT,
						   NO_QUAL);
			}
			if (t->u.dim == 0) {
				retType =
				    typeBuildArray(retType,
						   NO_DIMENSION,
						   t->u.dim);
			} else
				retType =
				    typeBuildArray(retType,
						   DIMENSION_PRESENT,
						   t->u.dim);
			break;
		case DECL_FUNC:
			switch (typeQuery(retType)) {
			case TYFUNCTION:
			case TYARRAY:
				error("illegal function return type");
				*outError = TRUE;
				retType =
				    typeBuildBasic(TYSIGNEDINT,
						   NO_QUAL);
			default:
				break;
			}
			/* check for void parameter list, if there is a 
			   parameter list */
			if ((t->u.paramInfo.params != NULL) &&
			    (typeQuery(t->u.paramInfo.params->type) ==
			     TYVOID)) {
				if (t->u.paramInfo.params->next == NULL) {
					t->u.paramInfo.params = NULL;
				} else {
					error
					    ("parameter cannot be of type void");
					t->u.paramInfo.params->type =
					    typeBuildBasic(TYSIGNEDINT,
							   NO_QUAL);
					t->u.paramInfo.params->err =
					    TRUE;
				}
			}
			retType =
			    typeBuildFunction(retType,
					      t->u.paramInfo.paramstyle,
					      t->u.paramInfo.params);
			if (lastParamListSeen)
				*lastParamListSeen =
				    t->u.paramInfo.params;
			break;
		case DECL_ID:
			*outId = t->u.id;
			break;
		default:
			bug("unsupported DECL_TAG value in processDeclarator");
		}

		/* cleaning up as we go.... */
		next = t->next;
		deleteDeclNode(t);
		t = next;
	}

	return retType;
}

/*
 *  processDeclaration
 *
 *  Processes a declaration comprising a list of declarators.
 *
 *  Takes a bucket that contains the specifiers and a linked
 *  list of the declarators. Converts the bucket to the base type.
 *  Then for each declarator, modifies the base type with the
 *  declarators's type modifiers (pointer and array). Finally, for
 *  each declarator, it calls either finishGlobalDeclaration or
 *  finishLocalDeclaration to do block-specific processing.
 */
void processDeclaration(BUCKET_PTR specifiers, DECLARATOR_LIST list)
{
	Type baseType;
	Type modType;

	DECLARATOR_LIST declList;
	DECLARATOR_LIST nextDeclarator;
	BOOLEAN foundError;
	BOOLEAN bucketError;
	ST_ID id;
	StorageClass class;

	foundError = FALSE;
	bucketError = FALSE;
	bucketError = is_error_decl(specifiers);
	class = get_class(specifiers);
	baseType = build_base(specifiers);

	/* have list of declarators: process one at a time */
	declList = list;
	while (declList != NULL) {
		/* process one declarator */
		id = NULL;
		foundError = FALSE;
		modType =
		    processDeclarator(declList->tree, baseType, &id,
				      &foundError, NULL);

		if (id == NULL) {
			bug("no id in processDeclaration");
		}

		/* can't have variable of void type */
		if (!bucketError && !foundError
		    && typeQuery(modType) == TYVOID) {
			error("variable cannot have void type");
			foundError = TRUE;
		}

		if (typeQuery(modType) == TYFUNCTION &&
		    (class == REGISTER_SC || class == AUTO_SC)) {
			error
			    ("function cannot have register or auto storage class");
			foundError = TRUE;
		}

		if (class == TYPEDEF_SC) {	/* lsr, 05/08/01 */
			finishTypedefDeclaration(id, modType);
		} else if (st_get_cur_block() == 0) {
			finishGlobalDeclaration(id, modType, class,
						foundError
						|| bucketError, FALSE);
		} else {
			finishLocalDeclaration(id, modType, class,
					       foundError
					       || bucketError, FALSE);
		}

		/* cleaning up as we go along */
		nextDeclarator = declList->next;
		deleteDeclarator(declList);
		declList = nextDeclarator;
	}
}

/*
 *  Finish processing a global declaration: semantic checks, access symbol
 *  table, etc.
 */
void finishGlobalDeclaration(ST_ID id, Type type, StorageClass class,
			     BOOLEAN err, BOOLEAN isFunctionDefinition)
{
	ST_DR stdr;
	ST_DR oldStdr;
	int block;

	/* 
	 *  ANSI 6.7.2
	 *  tentative object definition and internal linkage then type cannot be
	 *  incomplete.
	 */
	if ((typeQuery(type) != TYFUNCTION) &&	/* rule is for objects */
	    (class == STATIC_SC) &&	/* internal linkage */
	    !isCompleteObjectType(type)) {	/* check for incomplete 
						   types */
		error("type must be complete");
		err = TRUE;
	}

  /******** check whether this id is a duplicate one ******/
	stdr = NULL;
	oldStdr = st_lookup(id, &block);
	if (oldStdr != NULL) {
		/* id already defined as enum constant or typedef name? 
		 */
		if (oldStdr->tag != GDECL && oldStdr->tag != FDEF) {
			error("duplicate definition for %s",
			      st_get_id_str(id));
			err = TRUE;
		}
		/* duplicate function definition */
		else if (isFunctionDefinition && oldStdr->tag == FDEF) {
			error("duplicate function definition for %s",
			      st_get_id_str(id));
			err = TRUE;
		} else if (!err &&
			   /* first, types must be compatible */
			   (!areCompatibleTypes
			    (oldStdr->u.decl.type, type) ||
			    /* finally, be careful mixing static with
			       extern */
			    /* static first is okay, but static cannot
			       follow extern */
			    ((oldStdr->u.decl.sc == STATIC_SC)
			     && (class == NO_SC))
			    || ((class == STATIC_SC)
				&& ((oldStdr->u.decl.sc == EXTERN_SC)
				    || (oldStdr->u.decl.sc ==
					NO_SC))))) {
			error
			    ("type or storage class mismatched for the two declarations of %s",
			     st_get_id_str(id));
			err = TRUE;
		} else {
			/* might be an error in one of the declarations 
			 */
			if (oldStdr->u.decl.err || err ||
			    typeQuery(oldStdr->u.decl.type) == TYERROR
			    || typeQuery(type) == TYERROR) {
				oldStdr->u.decl.err = TRUE;
				oldStdr->u.decl.type =
				    typeBuildBasic(TYERROR, NO_QUAL);
			} else {
				oldStdr->u.decl.type =
				    typeFormComposite(oldStdr->u.decl.
						      type, type);
			}
		}
	} else {		/* no prior symtab table entry */

    /*************** Now put the Type and ID into the symbol table *****/
		stdr = stdr_alloc();

		stdr->tag = (isFunctionDefinition ? FDEF : GDECL);
		stdr->u.decl.type = type;
		stdr->u.decl.sc = class;
		stdr->u.decl.err = err;
		stdr->u.decl.offset = 0;

		st_install(id, stdr);
	}

  /************** Now generate code for the declaration ********/

	/* in case of error, we don't generate code */
	/* also don't generate code for function types or incomplete
	   types */
	if (!err && isCompleteObjectType(type)) {
		switch (class) {

		case EXTERN_SC:
			break;	/* no code for extern variable */

		case STATIC_SC:
			encode_static_var_decl(id, type);
			break;

		case NO_SC:
			encode_global_var_decl(id, type);
			break;

		case AUTO_SC:
			error
			    ("auto storage class is not allowed for global variable");
			if (stdr)
				stdr->u.decl.err = TRUE;
			break;

		case REGISTER_SC:
			error
			    ("register storage class is not allowed for global variable");
			if (stdr)
				stdr->u.decl.err = TRUE;
			break;

		default:
			bug("unsupported storage class in finishGlobalDeclaration");

		}
	}
}

/*
 *  Finish processing a local declaration: semantic checks, access symbol
 *  table, etc. Also handles parameters.
 */
void finishLocalDeclaration(ST_ID id, Type type, StorageClass class,
			    BOOLEAN err, BOOLEAN isParam)
{
	ST_DR oldStdr, stdr;
	int block, align;

	/* check for incomplete types if not extern and not a function */
	if (class != EXTERN_SC) {
		if (typeQuery(type) != TYFUNCTION
		    && !isCompleteObjectType(type)) {
			error("type must be complete");
			err = TRUE;
		}
	}

	/* check whether this is a duplicate one in this block */
	stdr = NULL;
	oldStdr = st_lookup(id, &block);

	if ((oldStdr != NULL) && (block == st_get_cur_block())) {
		/* id already defined in this block */
		/* for local declaration, this is not allowed */
		error("duplicate definition for %s", st_get_id_str(id));
		err = TRUE;
		return;
	}

	/* no prior symtab table entry */
	/* put the Type and ID into the symbol table */
	stdr = stdr_alloc();

	stdr->tag = (isParam ? PDECL : LDECL);
	stdr->u.decl.type = type;
	stdr->u.decl.sc = class;
	stdr->u.decl.err = err;

	/* only auto variable is stored in local area, so offset only
	   for auto, or no_sc or register_sc */
	if ((typeQuery(type) != TYFUNCTION) &&
	    ((class == NO_SC) || (class == AUTO_SC)
	     || (class == REGISTER_SC))) {
		align = determineAlignment(type);

		while (gCurrentFunctionOffset % align != 0) {
			gCurrentFunctionOffset++;
		}
		stdr->u.decl.offset = gCurrentFunctionOffset;
		if (!err) {
			if (isParam) {
				gCurrentFunctionOffset +=
				    computeParamSize(type);
			} else {
				gCurrentFunctionOffset +=
				    computeSize(type);
			}
		}
	}

	/* mark tag to GDECL for extern_sc, do nothing */
	else if ((typeQuery(type) == TYFUNCTION) || class == EXTERN_SC) {
		stdr->tag = GDECL;
	}

	/* allocate memory for static variable in .data segment */
	else if (class == STATIC_SC) {
		stdr->u.decl.staticLabel = genLabel();
		if (!err)
			encode_local_static(id,
					    stdr->u.decl.staticLabel,
					    type);
	} else
		/* bug: unknown storage class */
		bug("unknown storage class");

	/* now install the id and type to the symbol table */
	st_install(id, stdr);

	if (typeQuery(type) != TYFUNCTION) {
		vartabAdd(stdr->u.decl.offset, st_get_cur_block(),
		          stdr->tag, type);
	}

}				/* end of finishLocalDeclaration */

/*
 * Static integer which keeps track of the enumeration "counter", (what
 * value an enumeration constant is installed with).
 */
static long enumItemVal;

/*
 * Function sets the enumeration "counter" back to zero
 */
void resetEnumItemVal(void)
{
	enumItemVal = 0;
}

/*
 * Function sets the enumeration counter to the value of its sole
 * integer parm.  Used in cases such as "enum z (a, b=10, c};", where
 * it will set the value of 'enumItemVal' to '10' when processing 'b'.
 */
void setEnumItemVal(long a)
{
	enumItemVal = a;
}

/*
 * Gets the "next" enum counter value and increments the counter for 
 * the next access.
 */
int getNextEnumVal(void)
{
	return enumItemVal++;
}

/*
 * Installs enumeration constant with ST_ID 'id' into the symbol table
 * with next counter value.
 */
void processEnumItem(ST_ID id)
{
	ST_DR stdr;
	stdr = stdr_alloc();

	stdr->tag = ECONST;
	stdr->u.econst.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	stdr->u.econst.val = getNextEnumVal();

	if (st_install(id, stdr) == FALSE)
		error("duplicate declaration of %s", st_get_id_str(id));
}

/*
 * Function used to build a MY_SPECIFIER object with an enum type.
 * Actually no enum type if built. Just use int.
 */
static MY_SPECIFIER newEnumSpecifier(void)
{
	MY_SPECIFIER m;

	m = newSpecifierObject(ENUM_SPEC);
	m.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);

	return m;
}

/*
 * Function installs an enumeration 'type' indicated by the
 * ST_ID parm into the 'tag' symbol table.
 */
MY_SPECIFIER processEnumIdName(ST_ID id)
{
	ST_DR stdr;

	if (id != NULL) {
		stdr = stdr_alloc();
		stdr->tag = TAG;
		stdr->u.stag.type =
		    typeBuildBasic(TYSIGNEDINT, NO_QUAL);

		if (st_tag_install(id, stdr) == FALSE)
			error
			    ("re-declaration of enum tag not allowed at this scope level");
	}

	return newEnumSpecifier();

}

/*
 * Function verifies the existence of the enumeration 'type'
 * indicated by the ST_ID parm at this (or outer) scope.  If
 * verified, a MY_SPECIFIER object with enum type using the
 * ST_ID parm is returned.  Otherwise, a bug is issued.
 */
MY_SPECIFIER verifyEnumId(ST_ID id)
{

	MY_SPECIFIER m;
	int i;

	m = newSpecifierObject(ENUM_SPEC);
	m.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);

	if (!st_tag_lookup(id, &i)) {
		m.type = typeBuildBasic(TYERROR, NO_QUAL);
		m.err = TRUE;
		error("enum tag not defined at current scope");
	}

	return m;

}
