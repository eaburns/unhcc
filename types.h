/*
 * 
 *  interface to Type module: manipulation of objects representing C types
 * 
 */
#ifndef TYPES_H
#define TYPES_H

#include "defs.h"

/* user of module does not need to know details of type objects */
typedef void *Type;

/* used in array Type */
typedef enum { NO_DIMENSION, DIMENSION_PRESENT } DimFlag;

/* returned by typeQuery */
typedef enum {
	TYVOID 			= 0,
	TYFLOAT			= 1,
	TYDOUBLE		= 2,
	TYLONGDOUBLE		= 3,
	TYSIGNEDLONGINT		= 4,
	TYSIGNEDSHORTINT	= 5,
	TYSIGNEDINT		= 6,
	TYUNSIGNEDLONGINT	= 7,
	TYUNSIGNEDSHORTINT	= 8,
	TYUNSIGNEDINT		= 9,
	TYUNSIGNEDCHAR		= 10,
	TYSIGNEDCHAR		= 11,
	TYSTRUCT		= 12,
	TYUNION			= 13,
	TYARRAY			= 14,
	TYFUNCTION		= 15,
	TYPOINTER		= 16,
	TYERROR			= 17,

	NUM_TYPES
} TypeTag;

/* used in function Type */
typedef enum { PROTOTYPE, OLDSTYLE, DOT_DOT_DOT } ParamStyle;

/* compact representation of Type qualifiers */
typedef enum {
	NO_QUAL, CONST_QUAL, VOLATILE_QUAL, CONST_VOLATILE_QUAL
} TypeQualifier;

/*
 *  Parameter list structure IS manipulated directly by "users". types.c
 *  makes no assumption about the parameter list except all Params should
 *  be linked together by the "next" field, and the last Param in that linked
 *  list should have a NULL "next" field. In particular, the order of the
 *  Params is flexible and "prev" field can be used for any purpose (types.c
 *  doesn't use it).
 */
typedef struct param {
	ST_ID id;
	Type type;
	StorageClass sc;
	BOOLEAN err;
	struct param *next;
	struct param *prev;
} Param, *ParamList;

/*
 *  Member list structure IS manipulated directly by "users". types.c
 *  makes no assumption about the member list except all Members should
 *  be linked together by the "next" field, and the last Member in that linked
 *  list should have a NULL "next" field. In particular, the order of the
 *  Members is flexible and "prev" field can be used for any purpose (types.c
 *  doesn't use it).
 */
typedef struct member {
	ST_ID id;
	Type type;
	StorageClass sc;
	BOOLEAN err;
	struct member *next;
	struct member *prev;
} Member, *MemberList;

void typeInitialize(void);
    /* 
     *  Initializes the Type module.
     */

TypeTag typeQuery(Type type);
    /* 
     *  Returns the type category of a type: the "outermost" TypeTag.
     */

BOOLEAN areCompatibleTypes(Type type1, Type type2);
    /* 
     *  Returns TRUE if the two Types are compatible and FALSE otherwise.
     */

Type typeStripModifier(Type type);
    /* 
     *  If input type is a derived declarator type, then remove the "outermost"
     *  modifier and return the underlying type. If the input type
     *  is not a derived type, then return NULL.
     */

Type typeBuildBasic(TypeTag tag, TypeQualifier qualifiers);
    /* 
     *  Builds a Type object for the given TypeTag. As well as the
     *  "basic" types, this routine also can be used to construct
     *  "void" type and an "error" type. Note that ANSI C says that
     *  "char" is a basic type, but this module assumes "char" will
     *  be mapped to either "unsigned char" or "signed char".
     */

Type typeBuildPointer(Type base, TypeQualifier qual);
    /* 
     *  Builds a pointer Type given the Type pointed to and the type
     *  qualifiers, if any.
     */

Type typeBuildArray(Type element, DimFlag dimflag,
		    unsigned int dimension);
    /* 
     *  Builds an array Type given three parameters: the element Type,
     *  a flag indicating whether a dimension is available, and the
     *  dimension of the array.
     */

Type typeBuildStruct(ST_ID tagname, MemberList members);
    /* 
     *  Builds a unqualified struct Type given two parameters: the struct
     *  tag name and a member list, which can both be NULL.
     */

Type typeQualifyStruct(Type type, TypeQualifier qualifiers);
    /* 
     *  Creates a new type by qualifying an existing struct type.
     *  The qualifiers attached to the given struct type are ignored and
     *  a new struct type is created using the given qualifiers.
     */

Type typeBuildUnion(ST_ID tagname, MemberList members);
    /* 
     *  Builds a unqualified union Type given two parameters: the union
     *  tag name and a member list, which can be NULL.
     */

Type typeQualifyUnion(Type type, TypeQualifier qualifiers);
    /* 
     *  Creates a new type by qualifying an existing union type.
     *  The qualifiers attached to the given union type are ignored and
     *  a new union type is created using the given qualifiers.
     */

Type typeBuildFunction(Type retType, ParamStyle paramstyle,
		       ParamList params);
    /* 
     *  Builds a function Type given three parameters: the return Type,
     *  a flag indicating what kind of parameter list is present, and the
     *  parameter list.
     */

TypeQualifier typeGetQualifier(Type type);
    /* 
     *  Retrieves qualifiers for a given type. Doesn't search for
     *  qualifiers, rather returns qualifier field from "outermost"
     *  type record. For array and function types, it returns NO_QUAL.
     */

Type typeQueryFunction(Type type, ParamStyle * paramstyle,
		       ParamList * params);
    /* 
     *  If the first parameter is a function Type, then it returns that
     *  function Type's ParamStyle and ParamList through the second and
     *  third parameter and the function Type's return Type is returned.
     *  If the first parameter is not a function Type, then the "bug" function
     *  is called.
     */

Type typeQueryArray(Type type, DimFlag * dimflag, unsigned int *dim);
    /* 
     *  If the first parameter is an array Type, then it returns that
     *  array Type's DimFlag and dimension through the second and
     *  third parameter and the array Type's element Type is returned.
     *  If the first parameter is not an array Type, the "bug" function
     *  is called.
     */

Type typeQueryPointer(Type type, TypeQualifier * qual);
    /* 
     *  If the first parameter is a pointer Type, then it returns that
     *  pointer Type's TypeQualifier through the second parameter and the
     *  Type pointed to is returned.  If the first parameter is not a pointer
     *  Type, the "bug" function is called.
     */

Type typeAssignMembers(Type type, MemberList members);
    /* 
     *  Takes two parameters: a previously constructed Type
     *  and a struct or union MemberList. If the input Type
     *  is a struct or union Type, the routine assigns the member
     *  list to the input Type. The modified input Type is then
     *  returned. If the input Type is not struct or union then
     *  the "bug" function is called.
     */

MemberList typeRetrieveMembers(Type type);
    /* 
     *  If the single parameter is a struct or union Type, then its
     *  MemberList is returned. If the parameter is not struct or union,
     *  then the "bug" function is called.
     */

ST_ID typeRetrieveTagname(Type type);
    /* 
     *  This routine takes one parameter which should be either struct, union,
     *  or enum TYPE. If so, it returns the tagname of the given struct, union,
     *  enum. If not, the "bug" function is called.
     */

void printType(Type type);
    /* 
     *  This function takes one parameter, a Type, and prints out in
     *  English the meaning of the Type. The printing is done via the
     *  msg/msgn functions.
     */

void printQualifier(TypeQualifier qualifier);
    /* 
     *  This function takes one parameter, a type qualifier, and prints out
     *  in English the meaning of the "C" TYPE qualifier.
     */

void printTypetag(TypeTag tag);
    /* 
     * This function takes one parameter, a TypeTag, and prints out
     *  in English the meaning of the TypeTag.
     */

void printDimflag(DimFlag dimflag);

    /* 
     *  This function takes one parameter, an array Type dimension flag, and
     *  prints out in English the meaning of the dimension flag.
     */
void printParamList(ParamList params);
    /* 
     *  This function takes one parameter, a pointer to a function Type
     *  prototype parameter list, and prints out in English the meaning of
     *  each parameter of that list.
     */

void printMemberList(MemberList members);
    /* 
     *  This function takes one parameter, a pointer to a struct/union Type
     *  member list, and prints out in English the meaning of each member
     *  member of that list.
     */

void printParamStyle(ParamStyle paramstyle);
    /* 
     *  This function takes one parameter, a function Type parameter style
     *  flag, and prints out in English the meaning of the flag.
     */

Type typeUnqualifiedVersion(Type type);
    /* 
     *  This function takes a Type parameter and returns the unqualified version
     *  of that Type. This creates a structurally equivalent Type with
     *  the qualifiers removed from the internal objects, if any.
     */

BOOLEAN isObjectType(Type type);
    /* 
     *  Tests whether the input type is an Object Type.
     */

BOOLEAN isFunctionType(Type type);
    /* 
     *  Tests whether the input type is a Function Type.
     */

BOOLEAN isIncompleteType(Type type);
    /* 
     *  Tests whether the input type is an Incomplete Type.
     */

BOOLEAN isSignedIntegerType(Type type);
    /* 
     *  Tests whether the input type is a signed integer Type.
     */

BOOLEAN isUnsignedIntegerType(Type type);
    /* 
     *  Tests whether the input type is a unsigned integer Type.
     */

BOOLEAN isFloatingType(Type type);
    /* 
     *  Tests whether the input type is a floating Type.
     */

BOOLEAN isBasicType(Type type);
    /* 
     *  Tests whether the input type is a basic Type.
     */

BOOLEAN isCharacterType(Type type);
    /* 
     *  Tests whether the input type is a character Type.
     */

BOOLEAN isIntegralType(Type type);
    /* 
     *  Tests whether the input type is an integral Type.
     */

BOOLEAN isArithmeticType(Type type);
    /* 
     *  Tests whether the input type is an arithmetic Type.
     */

BOOLEAN isScalarType(Type type);
    /* 
     *  Tests whether the input type is a scalar Type.
     */

BOOLEAN isAggregateType(Type type);
    /* 
     *  Tests whether the input type is an aggregate Type.
     */

BOOLEAN isDerivedDeclaratorType(Type type);
    /* 
     *  Tests whether the input type is a derived declarator Type.
     */

Type typeFormComposite(Type type1, Type type2);
    /* 
     *  Given two types, forms the composite type. If it is
     *  not possible then the bug function is called.
     */

BOOLEAN typeHasAllQualifiers(Type type1, Type type2);
    /* 
     *  Checks to see if type1 has all the qualifiers of type2.
     *  The check is done recursively. All inner types must also
     *  satisfy the condition.
     */

Type promoteArrayOrFunction(Type type);
    /* 
     *  Builds new type to support argument/parameter promotion
     *  rules: array of T --> ptr to T; function returning T -->
     *  ptr to function returning T.
     */
#endif
