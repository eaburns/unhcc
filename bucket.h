/*
	BUCKET.H

	Routines in this module are designed to build base types through
	the processing of type specifiers.  The routine update_bucket does
	most of the work.  Basically, the routine is called repeatedly with
	different type specifiers that form a base type for a single
	declaration or series of declarations (ie. unsigned int a; or
	unsigned int a,b,c;).  As a side effect of this processing all
	semantic errors are checked for and reported.
*/

/* BUCKET_REC definition modified 05/27/89 */
#ifndef BUCKET_H
#define BUCKET_H

/*
 *  The type specifiers: ie pieces of a type declaration. That is, there is a
 *  difference between the specifier "float" (just a piece of the
 *  declaration "float **x" perhaps) and the Type float!
 */
typedef enum {
	VOLATILE_SPEC, CONST_SPEC,
	SIGNED_SPEC, UNSIGNED_SPEC, SHORT_SPEC, LONG_SPEC,
	INT_SPEC, FLOAT_SPEC, DOUBLE_SPEC, CHAR_SPEC, VOID_SPEC,
	STRUCT_SPEC, UNION_SPEC, ENUM_SPEC, TYPENAME_SPEC,
	STATIC_SPEC, EXTERN_SPEC, AUTO_SPEC, TYPEDEF_SPEC,
	REGISTER_SPEC
} TYPE_SPECIFIER;

/*
 *  The following are just a set of macros for separating out type specifiers.
 */
#define IS_BASE(typein)  ( (typein == INT_SPEC) || (typein == CHAR_SPEC) ||\
        (typein == VOID_SPEC) || (typein == FLOAT_SPEC) ||\
        (typein == DOUBLE_SPEC) || (typein == STRUCT_SPEC) ||\
        (typein == UNION_SPEC) || (typein == ENUM_SPEC) ||\
        (typein == TYPENAME_SPEC) )

#define IS_QUAL(typein)  ( (typein == CONST_SPEC) || (typein == VOLATILE_SPEC) )

#define IS_SIZE(typein)  ( (typein == SHORT_SPEC) || (typein == LONG_SPEC) )

#define IS_SIGN(typein)  ( (typein == UNSIGNED_SPEC) || (typein == SIGNED_SPEC) )

#define IS_CLASS(typein) ((typein == EXTERN_SPEC) || (typein == STATIC_SPEC) ||\
        (typein == REGISTER_SPEC) || (typein == AUTO_SPEC) ||\
        (typein == TYPEDEF_SPEC) )


typedef struct bucket {
	unsigned int spec_def;
	Type type;		/* type of struct/union/enum/typename */
	BOOLEAN error_decl;
} BUCKET_REC, *BUCKET_PTR;


/***************************************************************************/


extern void init_bucket_module(void);
    /* 
       this routine initializes variables for use in the bucket module */

extern void print_bucket(BUCKET_PTR bucket);
    /* 
       this routine prints whether a passed bucket record is nil */

extern BUCKET_PTR update_bucket(BUCKET_PTR bck, TYPE_SPECIFIER spec,
				Type type);
    /* 

       This routine takes in a pointer to the type bucket and also the 
       specifier to be added to the record.  If there is not something
       already there it adds it to the mask else an error. The third
       parameter is usually NULL.  With enumerations, structs, and
       unions the third field has a type in it.  With typenames, the
       third parameter is an id to be looked up.

       If the first parameter is NULL, then the routine will create a
       new bucket record. */

extern StorageClass get_class(BUCKET_PTR bucket);
    /* 

       This routine takes in the type info bucket and returns the
       storage class of the mask */

extern BOOLEAN is_error_decl(BUCKET_PTR bucket);
    /* 

       This function returns TRUE if there was an error in the
       declartion of the base type else returns FALSE */

extern Type build_base(BUCKET_PTR bucket);
    /* 

       This routine takes in a bucket of type info and builds a base
       type from the mask field */

extern void set_bucket_err(BUCKET_PTR bucket);
    /* 
       This routine allows the internal error flag to be set. This is
       used to track that an error has occured in declaration of
       struct. */

extern BOOLEAN typename_in_bucket(BUCKET_PTR bucket);
    /* 
       Test whether a TYPENAME is in the bucket. */

#endif
