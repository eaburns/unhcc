#!/bin/sh
#
# Really stupidly allocates different registers for the first 7
# integer registers, the first 14 float/double registers and the first 6
# long double registers.
#
# Note xmm0 is not allocated since it is sometimes clobbered.
#
# Note st(0) and st(1) are not allocated since they are the top of the
# floating point stack.
#
  sed 's/vd0/r8/g' \
| sed 's/vd1/r9/g' \
| sed 's/vd2/r10/g' \
| sed 's/vd3/r11/g' \
| sed 's/vd4/r12/g' \
| sed 's/vd5/r13/g' \
| sed 's/vd6/r14/g' \
| sed 's/vd7/r15/g' \
| sed 's/vf0/xmm1/g' \
| sed 's/vf1/xmm2/g' \
| sed 's/vf2/xmm3/g' \
| sed 's/vf3/xmm4/g' \
| sed 's/vf4/xmm5/g' \
| sed 's/vf5/xmm6/g' \
| sed 's/vf6/xmm7/g' \
| sed 's/vf7/xmm8/g' \
| sed 's/vf8/xmm9/g' \
| sed 's/vf9/xmm10/g' \
| sed 's/vf10/xmm11/g' \
| sed 's/vf11/xmm12/g' \
| sed 's/vf12/xmm13/g' \
| sed 's/vf13/xmm14/g' \
| sed 's/vf14/xmm15/g' \
