/* tests a while loop */
int printf(const char *, ...);

int main(void)
{
	int i;

	i = 2;

	while (i < 10)
		i++;

	printf("%d\n", i);
}
