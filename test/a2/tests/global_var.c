/* test a basic global variable usage */
int printf(const char *, ...);
int x;

int main(void)
{
	x = 42;
	printf("%d\n", x);
}
