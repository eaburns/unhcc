/* tests converting a double to a signed short */
int printf(const char *, ...);

int main(void)
{
	double d;
	short s;

	d = 42.0;
	s = d;

	printf("%hd\n", s);
}
