/* tests logical and with a 64-bit type */
int printf(const char *, ...);

int main(void)
{
	long s, t;

	s = 1;
	t = 0;

	printf("%d %d\n", s && 1, t && 1);
}
