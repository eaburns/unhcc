/* tests converting a float to a signed char */
int printf(const char *, ...);

int main(void)
{
	float f;
	char c;

	f = 42.0;
	c = f;

	printf("%hhd\n", c);
}
