/* tests converting a long double to a signed char */
int printf(const char *, ...);

int main(void)
{
	long double ld;
	char c;

	ld = 42.0;
	c = ld;

	printf("%hhd\n", c);
}
