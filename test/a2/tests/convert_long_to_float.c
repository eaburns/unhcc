/* tests converting a signed long to a float */
int printf(const char *, ...);

int main(void)
{
	long l;
	float f;

	l = 42;
	f = l;

	printf("%f\n", f);
}
