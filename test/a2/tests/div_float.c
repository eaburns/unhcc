/* tests division with a float */
int printf(const char *, ...);

int main(void)
{
	float s, t;

	s = 84.0;
	t = 2.0;

	printf("%f\n", s / t);
}
