/* tests multiplication with a double */
int printf(const char *, ...);

int main(void)
{
	double s, t;

	s = 21.0;
	t = 2.0;

	printf("%lf\n", s * t);
}
