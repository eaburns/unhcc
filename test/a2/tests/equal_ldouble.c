/* tests equal-to comparison with a long double */
int printf(const char *, ...);

int main(void)
{
	long double s, t;

	s = 40.0;
	t = 2.0;

	printf("%d %d\n", s == 40.0, t == 40.0);
}
