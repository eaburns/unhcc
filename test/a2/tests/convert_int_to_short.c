/* tests converting a signed int to a signed short */
int printf(const char *, ...);

int main(void)
{
	int i;
	short s;

	i = 42;
	s = i;

	printf("%hd\n", s);
}
