/* tests mod with a 32-bit type */
int printf(const char *, ...);

int main(void)
{
	int s, t;

	s = 83;
	t = 2;

	printf("%d\n", s % t);
}
