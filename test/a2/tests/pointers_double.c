/* tests address of and derefing pointers for double */
int printf(const char *, ...);

int main(void)
{
	double n;
	double *p;

	p = &n;
	*p = 42.0;

	printf("%lf\n", *p);
}
