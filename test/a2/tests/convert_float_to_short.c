/* tests converting a float to a signed short */
int printf(const char *, ...);

int main(void)
{
	float f;
	short s;

	f = 42.0;
	s = f;

	printf("%hd\n", s);
}
