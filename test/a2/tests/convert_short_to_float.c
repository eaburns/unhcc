/* tests converting a signed short to a float */
int printf(const char *, ...);

int main(void)
{
	short s;
	float f;

	s = 42;
	f = s;

	printf("%f\n", f);
}
