/* tests function calls */
int printf(const char *, ...);

void func(int i, float f, long double ld)
{
	printf("%d %f %Lf\n", i, f, ld);
}

int main(void)
{
	int i;
	float f;
	long double ld;

	i = 42;
	f = 3.1415926535;
	ld = -28;

	func(i, f, ld);
}
