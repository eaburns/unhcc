/* tests converting a signed int to a double */
int printf(const char *, ...);

int main(void)
{
	int i;
	double d;

	i = 42;
	d = i;

	printf("%lf\n", d);
}
