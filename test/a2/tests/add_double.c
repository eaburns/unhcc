/* tests addition with a double */
int printf(const char *, ...);

int main(void)
{
	double s, t;

	s = 40.0;
	t = 2.0;

	printf("%lf\n", s + t);
}
