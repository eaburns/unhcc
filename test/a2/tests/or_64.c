/* tests binary or with 64-bit types */
int printf(const char *, ...);

int main(void)
{
	long c, s;

	s = 40;
	c = 2;

	printf("%ld\n", s | c);
}
