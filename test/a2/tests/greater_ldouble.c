/* tests greater-than comparison with a long double */
int printf(const char *, ...);

int main(void)
{
	long double s, t;

	s = 40.0;
	t = 2.0;

	printf("%d %d\n", s > t, t > s);
}
