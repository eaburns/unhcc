/* tests converting a signed int to a long double */
int printf(const char *, ...);

int main(void)
{
	int i;
	long double ld;

	i = 42;
	ld = i;

	printf("%Lf\n", ld);
}
