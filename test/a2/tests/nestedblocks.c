/* tests local variable declarations in nested blocks */
int printf(const char *, ...);

int main(void)
{
	int i;

	i = 42;
	{
		int i;

		i = 100;
	}

	printf("%d\n", i);
}
