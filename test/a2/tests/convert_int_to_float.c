/* tests converting a signed int to a float */
int printf(const char *, ...);

int main(void)
{
	int i;
	float f;

	i = 42;
	f = i;

	printf("%f\n", f);
}
