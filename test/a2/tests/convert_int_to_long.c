/* tests converting a signed int to a signed long */
int printf(const char *, ...);

int main(void)
{
	long l;
	int i;

	i = 42;
	l = i;

	printf("%ld\n", l);
}
