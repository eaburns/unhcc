/* tests converting a double to a signed char */
int printf(const char *, ...);

int main(void)
{
	double d;
	char c;

	d = 42.0;
	c = d;

	printf("%hhd\n", c);
}
