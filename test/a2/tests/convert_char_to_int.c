/* tests converting a signed char to a signed int */
int printf(const char *, ...);

int main(void)
{
	char c;
	int i;

	c = 42;
	i = c;

	printf("%d\n", i);
}
