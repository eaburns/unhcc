/* tests converting a signed short to a signed char */
int printf(const char *, ...);

int main(void)
{
	char c;
	short s;

	s = 42;
	c = s;

	printf("%hhd\n", c);
}
