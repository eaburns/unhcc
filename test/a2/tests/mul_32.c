/* tests multiplication with a 32-bit type */
int printf(const char *, ...);

int main(void)
{
	/* needs to be an int to get rid of an extra convert node that
 	 * would allocate more registers than we can blindly replace. */
	int s, t;

	s = 21;
	t = 2;

	printf("%hd\n", s * t);
}
