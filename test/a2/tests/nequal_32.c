/* tests not-equal-to comparison with a 32-bit types */
int printf(const char *, ...);

int main(void)
{
	int s, t;

	s = 40;
	t = 2;

	printf("%d %d\n", s != 40, t != 40);
}
