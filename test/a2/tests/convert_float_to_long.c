/* tests converting a float to a signed long */
int printf(const char *, ...);

int main(void)
{
	float f;
	long l;

	f = 42.0;
	l = f;

	printf("%ld\n", l);
}
