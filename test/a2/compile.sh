#!/bin/sh

./unhcc test.c || {
	echo -e "\n\nERROR\n\n"
	exit 1
}

# allocate registers
cat test.s | ./deannotate.sh | ./registerize.sh > a.s

gcc -g a.s || {
	echo -e "\n\nERROR\n\n"
	exit 1
}
