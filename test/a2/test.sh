#!/bin/sh
#
# Runs tests for unhcc's register virtual allocator.
# The output of these tests is compiled by using the deannotate.sh
# and registerize.sh scripts.
#

TESTS=`ls -t tests/*.c`

for TEST in ${TESTS}
do
	# output test name
	echo -n "---- "
	head -n 1 ${TEST} | sed 's/\/\*\s//' | sed 's/\s\*\///'

	cp ${TEST} test.c

	# try with unhcc
	./compile.sh >& /dev/null
	./a.out >& test_unhcc.out

	# try with gcc
	gcc test.c
	./a.out >& test_gcc.out

	# check for differences in the output
	diff -up test_unhcc.out test_gcc.out >& diff.out || {
		cat diff.out

		# clean up output files
		rm diff.out test.c a.out a.s test.s \
		   test_unhcc.out test_gcc.out

		exit 1
	}

	# clean up output files
	rm diff.out test.c a.out a.s test.s \
	   test_unhcc.out test_gcc.out
done
