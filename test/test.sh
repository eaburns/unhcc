#!/bin/sh
#
# Runs the tests for unhcc.
#
UNHCC=${PWD}/../unhcc
DIRS="hennessy2 a6"

HERE=${PWD}
for DIR in ${DIRS}
do
	echo "-------------------- ${DIR} --------------------"
	cd ${HERE}/${DIR}
	./test.sh
done
