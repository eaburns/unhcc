/*
 *  This is a suite of benchmarks that are relatively short, both in program
 *  size and execution time. It requires no input. It does a rudimentary check
 *  to make sure each program gets the right output. These programs were
 *  gathered by John Hennessy and modified by Peter Nye.
 */

/*
 *  CS980 Fall 2007 Notes:
 *
 *  1. This file must be preprocessed before it can be handled by pcc3:
 *       i.e. % cpp hennessy2.c hennessy2-cpp.c
 *            % pcc3 hennessy2-cpp.c
 *            % gcc hennessy2-cpp.s
 *            % a.out
 *
 *  2. Set ITERATIONS to determine how many times the benchmarks are run.
 *
 *  3. Set PRINT to 1 if you want to print the results of the benchmarks.
 *     Set PRINT to 0 if you do not want any output.
 *
 */

#define ITERATIONS 1
#define PRINT 1

/* standard C lib stuff */
int printf(const char *, ...);
typedef long unsigned int size_t;
void *malloc(size_t size);

/* functions to print results */
void PrintPerm(void);
void PrintTowers(void);
void PrintQueens(int a[], int b[], int c[], int x[]);
void PrintIntmm(void);
void PrintMm(void);
void PrintPuzzle(void);
void PrintSort(char *);

/* Towers */
#define maxcells         18

/* Intmm, Mm */
#define rowsize          40

/* Puzzle */
#define size             511
#define classmax         3
#define typemax          12
#define d                8

/* Bubble, Quick */
#define sortelements     5000
#define srtelements      500

/* Fft */
#define fftsize          256
#define fftsize2         129

/* Perm */
#define permrange         10

/* Towers */
#define stackrange         3

/* Tree */
struct node {
  struct node *left, *right;
  int val;
};

/* Towers */
struct element {
  int discsize;
  int next;
};

/* Fft */
struct complex {
  float rp, ip;
};

/* Rand */
int seed;

/* Perm */
int permarray[permrange+1];
int pctr;

/* Tree */
struct node *tree;

/* Towers */
int stack[stackrange+1];
struct element cellspace[maxcells+1];
int freelist, movesdone;

/* Intmm, Mm */
int ima[rowsize+1][rowsize+1], imb[rowsize+1][rowsize+1],
    imr[rowsize+1][rowsize+1];
float rma[rowsize+1][rowsize+1], rmb[rowsize+1][rowsize+1],
       rmr[rowsize+1][rowsize+1];


/* Puzzle */
int piececount[classmax+1],
 class[typemax+1],
 piecemax[typemax+1],
 puzzl[size+1],
 p[typemax+1][size+1],
 n,
 kount;

/* Bubble, Quick */
int sortlist[sortelements+1],
    biggest, littlest,
    top;

/* Fft */
struct complex z[fftsize+1], w[fftsize+1], e[fftsize2+1];
float zr, zi;

void Initrand (void)
{
  seed = 74755;
}

int Rand (void)
{
  seed = (seed * 1309 + 13849) & 65535;
  return( seed );
}

void Swap (int *a, int *b )
{
  int t;
  t = *a; *a = *b; *b = t;
}

int Fit (int i, int j)
{
  int k;
  for ( k = 0; k <= piecemax[i]; k++ )
    if ( p[i][k] ) if ( puzzl[j+k] ) return (0);
  return (1);
}

int Place (int i, int j)
{
 int k;
 for ( k = 0; k <= piecemax[i]; k++ )
     if ( p[i][k] ) puzzl[j+k] = 1;
 piececount[class[i]] = piececount[class[i]] - 1;
 for ( k = j; k <= size; k++ )
   if ( ! puzzl[k] )
   {
     return (k);
   }
 return (0);
}

void Remove (int i, int j)
{
 int k;
 for ( k = 0; k <= piecemax[i]; k++ )
   if ( p[i][k] ) puzzl[j+k] = 0;
 piececount[class[i]] = piececount[class[i]] + 1;
}

int Trial (int j)
{
 int i, k;
 kount = kount + 1;
 for ( i = 0; i <= typemax; i++ )
   if ( piececount[class[i]] != 0 )
     if ( Fit (i, j) )
     {
       k = Place (i, j);
       if ( Trial(k) || (k == 0) )
       {
         return (1);
       }
       else Remove (i, j);
     } 
 return (0);
}

void Puzzle (void)
{
  int i, j, k, m;
  for ( m = 0; m <= size; m++ ) puzzl[m] = 1;
  for( i = 1; i <= 5; i++ )
    for( j = 1; j <= 5; j++ )
      for( k = 1; k <= 5; k++ )
        puzzl[i+d*(j+d*k)] = 0;
  for( i = 0; i <= typemax; i++ )
    for( m = 0; m<= size; m++ )
      p[i][m] = 0;
  for( i = 0; i <= 3; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 0; k++ )
        p[0][i+d*(j+d*k)] = 1;
  class[0] = 0;
  piecemax[0] = 3+d*1+d*d*0;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 3; k++ )
        p[1][i+d*(j+d*k)] = 1;
  class[1] = 0;
  piecemax[1] = 1+d*0+d*d*3;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 3; j++ )
      for( k = 0; k <= 1; k++ )
        p[2][i+d*(j+d*k)] = 1;
  class[2] = 0;
  piecemax[2] = 0+d*3+d*d*1;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 3; j++ )
      for( k = 0; k <= 0; k++ )
        p[3][i+d*(j+d*k)] = 1;
  class[3] = 0;
  piecemax[3] = 1+d*3+d*d*0;
  for( i = 0; i <= 3; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 1; k++ )
        p[4][i+d*(j+d*k)] = 1;
  class[4] = 0;
  piecemax[4] = 3+d*0+d*d*1;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 3; k++ )
        p[5][i+d*(j+d*k)] = 1;
  class[5] = 0;
  piecemax[5] = 0+d*1+d*d*3;
  for( i = 0; i <= 2; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 0; k++ )
        p[6][i+d*(j+d*k)] = 1;
  class[6] = 1;
  piecemax[6] = 2+d*0+d*d*0;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 2; j++ )
      for( k = 0; k <= 0; k++ )
        p[7][i+d*(j+d*k)] = 1;
  class[7] = 1;
  piecemax[7] = 0+d*2+d*d*0;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 2; k++ )
        p[d][i+d*(j+d*k)] = 1;
  class[8] = 1;
  piecemax[8] = 0+d*0+d*d*2;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 0; k++ )
        p[9][i+d*(j+d*k)] = 1;
  class[9] = 2;
  piecemax[9] = 1+d*1+d*d*0;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 1; k++ )
        p[10][i+d*(j+d*k)] = 1;
  class[10] = 2;
  piecemax[10] = 1+d*0+d*d*1;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 1; k++ )
        p[11][i+d*(j+d*k)] = 1;
  class[11] = 2;
  piecemax[11] = 0+d*1+d*d*1;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 1; k++ )
        p[12][i+d*(j+d*k)] = 1;
  class[12] = 3;
  piecemax[12] = 1+d*1+d*d*1;
  piececount[0] = 13;
  piececount[1] = 3;
  piececount[2] = 1;
  piececount[3] = 1;
  m = 1+d*(1+d*1);
  kount = 0;
  if ( Fit(0, m) ) n = Place(0, m);
  else printf("Error1 in Puzzle\n");
  if ( ! Trial(n) ) printf ("Error2 in Puzzle.\n");
  else if ( kount != 2005 ) printf ( "Error3 in Puzzle.\n");
}
int main(void)
{
  int i;

  for (i = 0; i < ITERATIONS; i++)
  {
    Puzzle();
    PrintPuzzle();
  }

  return 0;
}

void PrintPuzzle(void)
{
  int i,j;

  printf("\nPuzzle\n");

  printf("piececount\n");
  for (i=1; i< classmax+1; i++)
    printf("%d\n",piececount[i]);
  printf("class\n");
  for (i=1; i< typemax+1; i++)
    printf("%d\n",class[i]);
  printf("piecemax\n");
  for (i=1; i< typemax+1; i++)
    printf("%d\n",piecemax[i]);
  printf("puzzl\n");
  for (i=1; i< size+1; i++)
    printf("%d\n",puzzl[i]);
  printf("p\n");
  for (i=1; i< typemax+1; i++) {
    for (j = 0; j < size+1; j++)
      printf("%d\n",p[i][j]);
  }
}
