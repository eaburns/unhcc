/*
 *  This is a suite of benchmarks that are relatively short, both in program
 *  size and execution time. It requires no input. It does a rudimentary check
 *  to make sure each program gets the right output. These programs were
 *  gathered by John Hennessy and modified by Peter Nye.
 */

/*
 *  CS980 Fall 2007 Notes:
 *
 *  1. This file must be preprocessed before it can be handled by pcc3:
 *       i.e. % cpp hennessy2.c hennessy2-cpp.c
 *            % pcc3 hennessy2-cpp.c
 *            % gcc hennessy2-cpp.s
 *            % a.out
 *
 *  2. Set ITERATIONS to determine how many times the benchmarks are run.
 *
 *  3. Set PRINT to 1 if you want to print the results of the benchmarks.
 *     Set PRINT to 0 if you do not want any output.
 *
 */

#define ITERATIONS 1
#define PRINT 1

/* standard C lib stuff */
int printf(const char *, ...);
typedef long unsigned int size_t;
void *malloc(size_t size);

/* functions to print results */
void PrintPerm(void);
void PrintTowers(void);
void PrintQueens(int a[], int b[], int c[], int x[]);
void PrintIntmm(void);
void PrintMm(void);
void PrintPuzzle(void);
void PrintSort(char *);

/* Towers */
#define maxcells         18

/* Intmm, Mm */
#define rowsize          40

/* Puzzle */
#define size             511
#define classmax         3
#define typemax          12
#define d                8

/* Bubble, Quick */
#define sortelements     5000
#define srtelements      500

/* Fft */
#define fftsize          256
#define fftsize2         129

/* Perm */
#define permrange         10

/* Towers */
#define stackrange         3

/* Tree */
struct node {
  struct node *left, *right;
  int val;
};

/* Towers */
struct element {
  int discsize;
  int next;
};

/* Fft */
struct complex {
  float rp, ip;
};

/* Rand */
int seed;

/* Perm */
int permarray[permrange+1];
int pctr;

/* Tree */
struct node *tree;

/* Towers */
int stack[stackrange+1];
struct element cellspace[maxcells+1];
int freelist, movesdone;

/* Intmm, Mm */
int ima[rowsize+1][rowsize+1], imb[rowsize+1][rowsize+1],
    imr[rowsize+1][rowsize+1];
float rma[rowsize+1][rowsize+1], rmb[rowsize+1][rowsize+1],
       rmr[rowsize+1][rowsize+1];


/* Puzzle */
int piececount[classmax+1],
 class[typemax+1],
 piecemax[typemax+1],
 puzzl[size+1],
 p[typemax+1][size+1],
 n,
 kount;

/* Bubble, Quick */
int sortlist[sortelements+1],
    biggest, littlest,
    top;

/* Fft */
struct complex z[fftsize+1], w[fftsize+1], e[fftsize2+1];
float zr, zi;

void Initrand (void)
{
  seed = 74755;
}

int Rand (void)
{
  seed = (seed * 1309 + 13849) & 65535;
  return( seed );
}

void Swap (int *a, int *b )
{
  int t;
  t = *a; *a = *b; *b = t;
}

void tInitarr(void)
{
  int i, temp;
  Initrand();
  biggest = 0; littlest = 0;
  for ( i = 1; i <= sortelements; i++ )
  {
    temp = Rand();
    sortlist[i] = temp - (temp/100000)*100000 - 50000;
    if ( sortlist[i] > biggest ) biggest = sortlist[i];
    else if ( sortlist[i] < littlest ) littlest = sortlist[i];
  }
}

void CreateNode(struct node **t, int n)
{
  *t = (struct node *)malloc(sizeof(struct node));
  (*t)->left = 0; (*t)->right = 0;
  (*t)->val = n;
}

void Insert(int n, struct node *t)
{
  if ( n > t->val )
    if ( t->left == 0 ) CreateNode(&t->left,n);
    else Insert(n,t->left);
  else if ( n < t->val )
    if ( t->right == 0 ) CreateNode(&t->right,n);
    else Insert(n,t->right);
}

int Checktree(struct node *p)
{
  int result;
  result = 1;
  if ( p->left != 0 )
    if ( p->left->val <= p->val ) result=0;
    else result = Checktree(p->left) && result;
  if ( p->right != 0 )
    if ( p->right->val >= p->val ) result = 0;
    else result = Checktree(p->right) && result;
return( result);
}

void Trees(void)
{
  int i;
  tInitarr();
  tree = (struct node *)malloc(sizeof(struct node));
  tree->left = 0; tree->right=0; tree->val=sortlist[1];
  for ( i = 2; i <= sortelements; i++ ) Insert(sortlist[i],tree);
  if ( ! Checktree(tree) ) printf ( " Error in Tree.\n");
}

int main(void)
{
  int i;

  for (i = 0; i < ITERATIONS; i++)
  {
    Trees();
    PrintSort("Trees");
  }

  return 0;
}

void PrintSort(char *s)
{
  int i;

  printf("\n%s\n",s);
  for (i = 1; i <sortelements+1; i++)
    printf("%d\n",sortlist[i]);
}

