/*
 *  This is a suite of benchmarks that are relatively short, both in program
 *  size and execution time. It requires no input. It does a rudimentary check
 *  to make sure each program gets the right output. These programs were
 *  gathered by John Hennessy and modified by Peter Nye.
 */

/*
 *  CS980 Fall 2007 Notes:
 *
 *  1. This file must be preprocessed before it can be handled by pcc3:
 *       i.e. % cpp hennessy2.c hennessy2-cpp.c
 *            % pcc3 hennessy2-cpp.c
 *            % gcc hennessy2-cpp.s
 *            % a.out
 *
 *  2. Set ITERATIONS to determine how many times the benchmarks are run.
 *
 *  3. Set PRINT to 1 if you want to print the results of the benchmarks.
 *     Set PRINT to 0 if you do not want any output.
 *
 */

#define ITERATIONS 1
#define PRINT 1

/* standard C lib stuff */
int printf(const char *, ...);
typedef long unsigned int size_t;
void *malloc(size_t size);

/* functions to print results */
void PrintPerm(void);
void PrintTowers(void);
void PrintQueens(int a[], int b[], int c[], int x[]);
void PrintIntmm(void);
void PrintMm(void);
void PrintPuzzle(void);
void PrintSort(char *);

/* Towers */
#define maxcells         18

/* Intmm, Mm */
#define rowsize          40

/* Puzzle */
#define size             511
#define classmax         3
#define typemax          12
#define d                8

/* Bubble, Quick */
#define sortelements     5000
#define srtelements      500

/* Fft */
#define fftsize          256
#define fftsize2         129

/* Perm */
#define permrange         10

/* Towers */
#define stackrange         3

/* Tree */
struct node {
  struct node *left, *right;
  int val;
};

/* Towers */
struct element {
  int discsize;
  int next;
};

/* Fft */
struct complex {
  float rp, ip;
};

/* Rand */
int seed;

/* Perm */
int permarray[permrange+1];
int pctr;

/* Tree */
struct node *tree;

/* Towers */
int stack[stackrange+1];
struct element cellspace[maxcells+1];
int freelist, movesdone;

/* Intmm, Mm */
int ima[rowsize+1][rowsize+1], imb[rowsize+1][rowsize+1],
    imr[rowsize+1][rowsize+1];
float rma[rowsize+1][rowsize+1], rmb[rowsize+1][rowsize+1],
       rmr[rowsize+1][rowsize+1];


/* Puzzle */
int piececount[classmax+1],
 class[typemax+1],
 piecemax[typemax+1],
 puzzl[size+1],
 p[typemax+1][size+1],
 n,
 kount;

/* Bubble, Quick */
int sortlist[sortelements+1],
    biggest, littlest,
    top;

/* Fft */
struct complex z[fftsize+1], w[fftsize+1], e[fftsize2+1];
float zr, zi;

void Initrand (void)
{
  seed = 74755;
}

int Rand (void)
{
  seed = (seed * 1309 + 13849) & 65535;
  return( seed );
}

void Swap (int *a, int *b )
{
  int t;
  t = *a; *a = *b; *b = t;
}

void Initialize (void)
{
 int i;
 for ( i = 1; i <= 7; i++ ) {
     permarray[i]=i-1;
 }
}

void Permute (int n)
{
 int k;
 pctr = pctr + 1;
 if ( n!=1 ) {
     Permute(n-1);
     for ( k = n-1; k >= 1; k-- ) {
        Swap(&permarray[n],&permarray[k]);
        Permute(n-1);
        Swap(&permarray[n],&permarray[k]);
     }
  }
}

void Perm (void)
{
  int i;
  pctr = 0;
  for ( i = 1; i <= 5; i++ ) {
    Initialize();
     Permute(7);
  }
  if ( pctr != 43300 )
     printf(" Error in Perm.\n");
}

void Error (char *emsg)
{
 printf(" Error in Towers: %s\n",emsg);
}

void Makenull (int s)
{
 stack[s]=0;
}

int Getelement (void)
{
 int temp;
 if ( freelist>0 )
 {
   temp = freelist;
   freelist = cellspace[freelist].next;
 }
 else
   Error("out of space   ");
 return (temp);
}

void Push(int i, int s)
{
 int errorfound, localel;
 errorfound=0;
 if ( stack[s] > 0 )
   if ( cellspace[stack[s]].discsize<=i )
   {
     errorfound=1;
     Error("disc size error");
   }
 if ( !errorfound )
 {
   localel=Getelement();
   cellspace[localel].next=stack[s];
   stack[s]=localel;
   cellspace[localel].discsize=i;
 }
}

void Init (int s, int n)
{
 int discctr;
 Makenull(s);
 for ( discctr = n; discctr >= 1; discctr-- )
   Push(discctr,s);
}

int Pop (int s)
{
 int temp, temp1;
 if ( stack[s] > 0 )
 {
   temp1 = cellspace[stack[s]].discsize;
   temp = cellspace[stack[s]].next;
   cellspace[stack[s]].next=freelist;
   freelist=stack[s];
   stack[s]=temp;
   return (temp1);
 }
 else
     Error("nothing to pop ");
}

void Move(int s1, int s2)
{
 Push(Pop(s1),s2);
 movesdone=movesdone+1;
}

void tower(int i, int j, int k)
{
  int other;
  if ( k==1 )
    Move(i,j);
  else
  {
    other=6-i-j;
    tower(i,other,k-1);
    Move(i,j);
    tower(other,j,k-1);
  }
}


void Towers (void)
{
  int i;
  for ( i=1; i <= maxcells; i++ )
     cellspace[i].next=i-1;
  freelist=maxcells;
  Init(1,14);
  Makenull(2);
  Makenull(3);
  movesdone=0;
  tower(1,2,14);
  if ( movesdone != 16383 )
    printf (" Error in Towers.\n");
}

void Try(int i, int *q, int a[], int b[], int c[], int x[])
{
  int j;
  j = 0;
  *q = 0;
  while ( (! *q) && (j != 8) )
  {
    j = j + 1;
    *q = 0;
    if ( b[j] && a[i+j] && c[i-j+7] )
    {
      x[i] = j;
      b[j] = 0;
      a[i+j] = 0;
      c[i-j+7] = 0;
      if ( i < 8 )
      {
        Try(i+1,q,a,b,c,x);
        if ( ! *q )
        {
          b[j] = 1;
          a[i+j] = 1;
          c[i-j+7] = 1;
        }
      }
      else *q = 1;
    }
  }
}

void Doit(void)
{
 int i,q;
 int a[9], b[17], c[15], x[9];
 i = 0 - 7;
 while ( i <= 16 )
 {
   if ( (i >= 1) && (i <= 8) ) a[i] = 1;
   if ( i >= 2 ) b[i] = 1;
   if ( i <= 7 ) c[i+7] = 1;
   i = i + 1;
 }
 Try(1, &q, b, a, c, x);
 if ( ! q )
   printf (" Error in Queens.\n");
#if PRINT
 PrintQueens(a,b,c,x);
#endif
}

void Queens(void)
{
  Doit();
}

void Initmatrix(int m[rowsize +1][rowsize +1])
{
 int temp, i, j;
 for ( i = 1; i <= rowsize; i++ )
   for ( j = 1; j <= rowsize; j++ )
   {
     temp = Rand();
     m[i][j] = temp - (temp/120)*120 - 60;
   }
}

void Innerproduct(
  int *result,
  int a[rowsize +1][rowsize +1],
  int b[rowsize +1][rowsize +1],
  int row,
  int column
)
{
 int i;
 *result = 0;
 for (i = 1; i <= rowsize; i++ ) *result = *result+a[row][i]*b[i][column];
}

void Intmm (void)
{
  int i, j;
  Initrand();
  Initmatrix (ima);
  Initmatrix (imb);
  for ( i = 1; i <= rowsize; i++ )
    for ( j = 1; j <= rowsize; j++ ) Innerproduct(&imr[i][j],ima,imb,i,j);
}

void rInitmatrix (float m[rowsize +1][rowsize +1])
{
 int temp, i, j;
 for ( i = 1; i <= rowsize; i++ )
   for ( j = 1; j <= rowsize; j++ )
   {
     temp = Rand();
     m[i][j] = (temp - (temp/120)*120 - 60)/3;
   }
}

void rInnerproduct(
 float *result,
 float a[rowsize +1][rowsize +1],
 float b[rowsize +1][rowsize +1],
 int row,
 int column
)
{
 int i;
 *result = 0.0;
 for (i = 1; i<=rowsize; i++) *result = *result+a[row][i]*b[i][column];
}

void Mm (void)
{
  int i, j;
  Initrand();
  rInitmatrix (rma);
  rInitmatrix (rmb);
  for ( i = 1; i <= rowsize; i++ )
    for ( j = 1; j <= rowsize; j++ )
      rInnerproduct(&rmr[i][j],rma,rmb,i,j);
}

int Fit (int i, int j)
{
  int k;
  for ( k = 0; k <= piecemax[i]; k++ )
    if ( p[i][k] ) if ( puzzl[j+k] ) return (0);
  return (1);
}

int Place (int i, int j)
{
 int k;
 for ( k = 0; k <= piecemax[i]; k++ )
     if ( p[i][k] ) puzzl[j+k] = 1;
 piececount[class[i]] = piececount[class[i]] - 1;
 for ( k = j; k <= size; k++ )
   if ( ! puzzl[k] )
   {
     return (k);
   }
 return (0);
}

void Remove (int i, int j)
{
 int k;
 for ( k = 0; k <= piecemax[i]; k++ )
   if ( p[i][k] ) puzzl[j+k] = 0;
 piececount[class[i]] = piececount[class[i]] + 1;
}

int Trial (int j)
{
 int i, k;
 kount = kount + 1;
 for ( i = 0; i <= typemax; i++ )
   if ( piececount[class[i]] != 0 )
     if ( Fit (i, j) )
     {
       k = Place (i, j);
       if ( Trial(k) || (k == 0) )
       {
         return (1);
       }
       else Remove (i, j);
     } 
 return (0);
}

void Puzzle (void)
{
  int i, j, k, m;
  for ( m = 0; m <= size; m++ ) puzzl[m] = 1;
  for( i = 1; i <= 5; i++ )
    for( j = 1; j <= 5; j++ )
      for( k = 1; k <= 5; k++ )
        puzzl[i+d*(j+d*k)] = 0;
  for( i = 0; i <= typemax; i++ )
    for( m = 0; m<= size; m++ )
      p[i][m] = 0;
  for( i = 0; i <= 3; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 0; k++ )
        p[0][i+d*(j+d*k)] = 1;
  class[0] = 0;
  piecemax[0] = 3+d*1+d*d*0;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 3; k++ )
        p[1][i+d*(j+d*k)] = 1;
  class[1] = 0;
  piecemax[1] = 1+d*0+d*d*3;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 3; j++ )
      for( k = 0; k <= 1; k++ )
        p[2][i+d*(j+d*k)] = 1;
  class[2] = 0;
  piecemax[2] = 0+d*3+d*d*1;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 3; j++ )
      for( k = 0; k <= 0; k++ )
        p[3][i+d*(j+d*k)] = 1;
  class[3] = 0;
  piecemax[3] = 1+d*3+d*d*0;
  for( i = 0; i <= 3; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 1; k++ )
        p[4][i+d*(j+d*k)] = 1;
  class[4] = 0;
  piecemax[4] = 3+d*0+d*d*1;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 3; k++ )
        p[5][i+d*(j+d*k)] = 1;
  class[5] = 0;
  piecemax[5] = 0+d*1+d*d*3;
  for( i = 0; i <= 2; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 0; k++ )
        p[6][i+d*(j+d*k)] = 1;
  class[6] = 1;
  piecemax[6] = 2+d*0+d*d*0;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 2; j++ )
      for( k = 0; k <= 0; k++ )
        p[7][i+d*(j+d*k)] = 1;
  class[7] = 1;
  piecemax[7] = 0+d*2+d*d*0;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 2; k++ )
        p[d][i+d*(j+d*k)] = 1;
  class[8] = 1;
  piecemax[8] = 0+d*0+d*d*2;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 0; k++ )
        p[9][i+d*(j+d*k)] = 1;
  class[9] = 2;
  piecemax[9] = 1+d*1+d*d*0;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 0; j++ )
      for( k = 0; k <= 1; k++ )
        p[10][i+d*(j+d*k)] = 1;
  class[10] = 2;
  piecemax[10] = 1+d*0+d*d*1;
  for( i = 0; i <= 0; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 1; k++ )
        p[11][i+d*(j+d*k)] = 1;
  class[11] = 2;
  piecemax[11] = 0+d*1+d*d*1;
  for( i = 0; i <= 1; i++ )
    for( j = 0; j <= 1; j++ )
      for( k = 0; k <= 1; k++ )
        p[12][i+d*(j+d*k)] = 1;
  class[12] = 3;
  piecemax[12] = 1+d*1+d*d*1;
  piececount[0] = 13;
  piececount[1] = 3;
  piececount[2] = 1;
  piececount[3] = 1;
  m = 1+d*(1+d*1);
  kount = 0;
  if ( Fit(0, m) ) n = Place(0, m);
  else printf("Error1 in Puzzle\n");
  if ( ! Trial(n) ) printf ("Error2 in Puzzle.\n");
  else if ( kount != 2005 ) printf ( "Error3 in Puzzle.\n");
}

void Initarr(void)
{
 int i, temp;
 Initrand();
 biggest = 0; littlest = 0;
 for ( i = 1; i <= sortelements; i++ )
 {
   temp = Rand();
   sortlist[i] = temp - (temp/100000)*100000 - 50000;
   if ( sortlist[i] > biggest ) biggest = sortlist[i];
   else if ( sortlist[i] < littlest ) littlest = sortlist[i];
 }
}

void Quicksort(int a[], int l, int r)
{
  int i,j,x,w;

  i=l; j=r;
  x=a[(l+r) / 2];
  do {
    while ( a[i]<x ) i = i+1;
    while ( x<a[j] ) j = j-1;
    if ( i<=j )
    {
      w = a[i];
      a[i] = a[j];
      a[j] = w;
      i = i+1; j= j-1;
    }
  } while ( i<=j );
  if ( l <j ) Quicksort(a,l,j);
  if ( i<r ) Quicksort(a,i,r);
}


void Quick(void)
{
  Initarr();
  Quicksort(sortlist,1,sortelements);
  if ( (sortlist[1] != littlest) || (sortlist[sortelements] != biggest) )
    printf ( " Error in Quick.\n");
}

void tInitarr(void)
{
  int i, temp;
  Initrand();
  biggest = 0; littlest = 0;
  for ( i = 1; i <= sortelements; i++ )
  {
    temp = Rand();
    sortlist[i] = temp - (temp/100000)*100000 - 50000;
    if ( sortlist[i] > biggest ) biggest = sortlist[i];
    else if ( sortlist[i] < littlest ) littlest = sortlist[i];
  }
}

void CreateNode(struct node **t, int n)
{
  *t = (struct node *)malloc(sizeof(struct node));
  (*t)->left = 0; (*t)->right = 0;
  (*t)->val = n;
}

void Insert(int n, struct node *t)
{
  if ( n > t->val )
    if ( t->left == 0 ) CreateNode(&t->left,n);
    else Insert(n,t->left);
  else if ( n < t->val )
    if ( t->right == 0 ) CreateNode(&t->right,n);
    else Insert(n,t->right);
}

int Checktree(struct node *p)
{
  int result;
  result = 1;
  if ( p->left != 0 )
    if ( p->left->val <= p->val ) result=0;
    else result = Checktree(p->left) && result;
  if ( p->right != 0 )
    if ( p->right->val >= p->val ) result = 0;
    else result = Checktree(p->right) && result;
return( result);
}

void Trees(void)
{
  int i;
  tInitarr();
  tree = (struct node *)malloc(sizeof(struct node));
  tree->left = 0; tree->right=0; tree->val=sortlist[1];
  for ( i = 2; i <= sortelements; i++ ) Insert(sortlist[i],tree);
  if ( ! Checktree(tree) ) printf ( " Error in Tree.\n");
}

void bInitarr(void)
{
 int i, temp;
 Initrand();
 biggest = 0; littlest = 0;
 for ( i = 1; i <= srtelements; i++ )
 {
   temp = Rand();
   sortlist[i] = temp - (temp/100000)*100000 - 50000;
   if ( sortlist[i] > biggest ) biggest = sortlist[i];
   else if ( sortlist[i] < littlest ) littlest = sortlist[i];
 }
}

void Bubble(void)
{
  int i, j;
  bInitarr();
  top=srtelements;

  while ( top>1 )
  {
    i=1;
    while ( i<top )
    {
      if ( sortlist[i] > sortlist[i+1] )
      {
        j = sortlist[i];
        sortlist[i] = sortlist[i+1];
        sortlist[i+1] = j;
      }
      i=i+1;
    }
    top=top-1;
  }
  if ( (sortlist[1] != littlest) || (sortlist[srtelements] != biggest) )
    printf ( "Error3 in Bubble.\n");
}

float Cos (float x)
{
  int i, factor;
  float result,power;

 result = 1.0; factor = 1; power = x;
 for ( i = 2; i <= 10; i++ ) {
   factor = factor * i; power = power*x;
   if ( (i & 1) == 0 ) {
     if ( (i & 3) == 0 ) result = result + power/factor;
     else result = result - power/factor;
   }
 }
 return (result);
}

int Min0(int arg1, int arg2)
{
  if ( arg1 < arg2 )
    return (arg1);
  else
    return (arg2);
}

void Printcomplex(
  int arg1,
  int arg2,
  struct complex zarray[],
  int start,
  int finish,
  int increment
)
{
  int i;
  printf("\n") ;

  i = start;
  do {
    printf("  %15.3e%15.3e",zarray[i].rp,zarray[i].ip) ;
    i = i + increment;
    printf("  %15.3e%15.3e",zarray[i].rp,zarray[i].ip) ;
    printf("\n");
    i = i + increment ;
  } while ( i <= finish );

}

void Uniform11(int iy, float yfl)
{
  iy = (4855*iy + 1731) & 8191;
  yfl = iy/8192.0;
}

void Exptab( int n, struct complex e[])
{
  float theta, divisor, h[26];
  int i, j, k, l, m;

  theta = 3.1415926536;
  divisor = 4.0;
  for ( i=1; i <= 25; i++ )
  {
    h[i] = 1/(2*Cos( theta/divisor ));
    divisor = divisor + divisor;
  }

  m = n / 2 ;
  l = m / 2 ;
  j = 1 ;
  e[1].rp = 1.0 ;
  e[1].ip = 0.0;
  e[l+1].rp = 0.0;
  e[l+1].ip = 1.0 ;
  e[m+1].rp = -1.0 ;
  e[m+1].ip = 0.0 ;

  do {
    i = l / 2 ;
    k = i ;

    do {
      e[k+1].rp = h[j]*(e[k+i+1].rp+e[k-i+1].rp) ;
      e[k+1].ip = h[j]*(e[k+i+1].ip+e[k-i+1].ip) ;
      k = k+l ;
    } while ( k <= m );

    j = Min0( j+1, 25);
    l = i ;
  } while ( l > 1 );

}

void Fft(
  int n,
  struct complex z[],
  struct complex w[],
  struct complex e[],
  float sqrinv
)
{
  int i, j, k, l, m, index;
  m = n / 2 ;
  l = 1 ;

  do {
    k = 0 ;
    j = l ;
    i = 1 ;
    do {
     do {
       w[i+k].rp = z[i].rp+z[m+i].rp ;
       w[i+k].ip = z[i].ip+z[m+i].ip ;
       w[i+j].rp = e[k+1].rp*(z[i].rp-z[i+m].rp)-e[k+1].ip*(z[i].ip-z[i+m].ip);
       w[i+j].ip = e[k+1].rp*(z[i].ip-z[i+m].ip)+e[k+1].ip*(z[i].rp-z[i+m].rp);
       i = i+1 ;
     } while ( i <= j );
     k = j ;
     j = k+l ;
    } while ( j <= m );
    index = 1;
    do {
     z[index] = w[index];
     index = index+1;
    } while ( index <= n );
    l = l+l ;
  } while ( l <= m );

  for ( i = 1; i <= n; i++ )
  {
    z[i].rp = sqrinv*z[i].rp ;
    z[i].ip = -sqrinv*z[i].ip;
  }
}

void Oscar(void)
{
  int i;
  Exptab(fftsize,e);
  seed = 5767 ;
  for ( i = 1; i <= fftsize; i++ )
  {
    Uniform11( seed, zr );
    Uniform11( seed, zi );
    z[i].rp = 20.0*zr - 10.0;
    z[i].ip = 20.0*zi - 10.0;
  }
  for ( i = 1; i <= 20; i++ ) {
   Fft(fftsize,z,w,e,0.0625) ;
#if PRINT
   Printcomplex(6, 99, z, 1, 256, 17);
#endif
  }
}

int main(void)
{
  int i;

  for (i = 0; i < ITERATIONS; i++)
  {
    Perm();
#if PRINT
    PrintPerm();
#endif
    Towers();
#if PRINT
    PrintTowers();
#endif
    Queens();
    Intmm();
#if PRINT
    PrintIntmm();
#endif
    Mm();
#if PRINT
    PrintMm();
#endif
    Puzzle();
#if PRINT
    PrintPuzzle();
#endif
    Quick();
#if PRINT
    PrintSort("Quick");
#endif
    Bubble();
#if PRINT
    PrintSort("Bubble");
#endif
    Trees();
#if PRINT
    PrintSort("Trees");
    printf("\nOscar");
#endif
    Oscar();
  }

  return 0;
}


void PrintPerm (void)
{
  int i;
  printf("\nPerm\n");
  for ( i = 1; i < permrange+1; i++ ) {
    printf("%d\n",permarray[i]);
  }
}

void PrintTowers(void)
{
  int i;
  printf("\nTower\n");
  for (i = 1; i < stackrange+1; i++)
    printf("%d\n",stack[i]);
  for (i = 1; i < maxcells+1; i++)
    printf("%d %d\n",cellspace[i].discsize, cellspace[i].next);
  printf("%d %d\n",freelist,movesdone);
}

void PrintQueens(int a[], int b[], int c[], int x[])
{
  int i;

  printf("\nQueens\n");
  for (i = 1; i < 9; i++)
    printf("%d\n",a[i]);
  for (i = 2; i < 17; i++)
    printf("%d\n",b[i]);
  for (i = 1; i < 15; i++)
    printf("%d\n",c[i]);
  for (i = 1; i < 9; i++)
    printf("%d\n",x[i]);
}

void PrintIntmm(void)
{
  int i,j;

  printf("\nIntmm\n");
  printf("\n\tima\n");
  for (i = 1; i < rowsize + 1; i++) {
    for (j = 1; j < rowsize + 1; j++)
      printf("%d\n",ima[i][j]);
  }
  printf("\n\timb\n");
  for (i = 1; i < rowsize + 1; i++) {
    for (j = 1; j < rowsize + 1; j++)
      printf("%d\n",imb[i][j]);
  }
  printf("\n\timr\n");
  for (i = 1; i < rowsize + 1; i++) {
    for (j = 1; j < rowsize + 1; j++)
      printf("%d\n",imr[i][j]);
  }
}

void PrintMm(void)
{
  int i,j;

  printf("\nMm\n");
  printf("\n\trma\n");
  for (i = 1; i < rowsize + 1; i++) {
    for (j = 1; j < rowsize + 1; j++)
      printf("%e\n",rma[i][j]);
  }
  printf("\n\trmb\n");
  for (i = 1; i < rowsize + 1; i++) {
    for (j = 1; j < rowsize + 1; j++)
      printf("%e\n",rmb[i][j]);
  }
  printf("\n\trmr\n");
  for (i = 1; i < rowsize + 1; i++) {
    for (j = 1; j < rowsize + 1; j++)
      printf("%e\n",rmr[i][j]);
  }
}

void PrintPuzzle(void)
{
  int i,j;

  printf("\nPuzzle\n");

  printf("piececount\n");
  for (i=1; i< classmax+1; i++)
    printf("%d\n",piececount[i]);
  printf("class\n");
  for (i=1; i< typemax+1; i++)
    printf("%d\n",class[i]);
  printf("piecemax\n");
  for (i=1; i< typemax+1; i++)
    printf("%d\n",piecemax[i]);
  printf("puzzl\n");
  for (i=1; i< size+1; i++)
    printf("%d\n",puzzl[i]);
  printf("p\n");
  for (i=1; i< typemax+1; i++) {
    for (j = 0; j < size+1; j++)
      printf("%d\n",p[i][j]);
  }
}

void PrintSort(char *s)
{
  int i;

  printf("\n%s\n",s);
  for (i = 1; i <sortelements+1; i++)
    printf("%d\n",sortlist[i]);
}

