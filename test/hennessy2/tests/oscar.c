/*
 *  This is a suite of benchmarks that are relatively short, both in program
 *  size and execution time. It requires no input. It does a rudimentary check
 *  to make sure each program gets the right output. These programs were
 *  gathered by John Hennessy and modified by Peter Nye.
 */

/*
 *  CS980 Fall 2007 Notes:
 *
 *  1. This file must be preprocessed before it can be handled by pcc3:
 *       i.e. % cpp hennessy2.c hennessy2-cpp.c
 *            % pcc3 hennessy2-cpp.c
 *            % gcc hennessy2-cpp.s
 *            % a.out
 *
 *  2. Set ITERATIONS to determine how many times the benchmarks are run.
 *
 *  3. Set PRINT to 1 if you want to print the results of the benchmarks.
 *     Set PRINT to 0 if you do not want any output.
 *
 */

#define ITERATIONS 1
#define PRINT 1

/* standard C lib stuff */
int printf(const char *, ...);
typedef long unsigned int size_t;
void *malloc(size_t size);

/* functions to print results */
void PrintPerm(void);
void PrintTowers(void);
void PrintQueens(int a[], int b[], int c[], int x[]);
void PrintIntmm(void);
void PrintMm(void);
void PrintPuzzle(void);
void PrintSort(char *);

/* Towers */
#define maxcells         18

/* Intmm, Mm */
#define rowsize          40

/* Puzzle */
#define size             511
#define classmax         3
#define typemax          12
#define d                8

/* Bubble, Quick */
#define sortelements     5000
#define srtelements      500

/* Fft */
#define fftsize          256
#define fftsize2         129

/* Perm */
#define permrange         10

/* Towers */
#define stackrange         3

/* Tree */
struct node {
  struct node *left, *right;
  int val;
};

/* Towers */
struct element {
  int discsize;
  int next;
};

/* Fft */
struct complex {
  float rp, ip;
};

/* Rand */
int seed;

/* Perm */
int permarray[permrange+1];
int pctr;

/* Tree */
struct node *tree;

/* Towers */
int stack[stackrange+1];
struct element cellspace[maxcells+1];
int freelist, movesdone;

/* Intmm, Mm */
int ima[rowsize+1][rowsize+1], imb[rowsize+1][rowsize+1],
    imr[rowsize+1][rowsize+1];
float rma[rowsize+1][rowsize+1], rmb[rowsize+1][rowsize+1],
       rmr[rowsize+1][rowsize+1];


/* Puzzle */
int piececount[classmax+1],
 class[typemax+1],
 piecemax[typemax+1],
 puzzl[size+1],
 p[typemax+1][size+1],
 n,
 kount;

/* Bubble, Quick */
int sortlist[sortelements+1],
    biggest, littlest,
    top;

/* Fft */
struct complex z[fftsize+1], w[fftsize+1], e[fftsize2+1];
float zr, zi;

void Initrand (void)
{
  seed = 74755;
}

int Rand (void)
{
  seed = (seed * 1309 + 13849) & 65535;
  return( seed );
}

void Swap (int *a, int *b )
{
  int t;
  t = *a; *a = *b; *b = t;
}

float Cos (float x)
{
  int i, factor;
  float result,power;

 result = 1.0; factor = 1; power = x;
 for ( i = 2; i <= 10; i++ ) {
   factor = factor * i; power = power*x;
   if ( (i & 1) == 0 ) {
     if ( (i & 3) == 0 ) result = result + power/factor;
     else result = result - power/factor;
   }
 }
 return (result);
}

int Min0(int arg1, int arg2)
{
  if ( arg1 < arg2 )
    return (arg1);
  else
    return (arg2);
}

void Printcomplex(
  int arg1,
  int arg2,
  struct complex zarray[],
  int start,
  int finish,
  int increment
)
{
  int i;
  printf("\n") ;

  i = start;
  do {
    printf("  %15.3e%15.3e",zarray[i].rp,zarray[i].ip) ;
    i = i + increment;
    printf("  %15.3e%15.3e",zarray[i].rp,zarray[i].ip) ;
    printf("\n");
    i = i + increment ;
  } while ( i <= finish );

}

void Uniform11(int iy, float yfl)
{
  iy = (4855*iy + 1731) & 8191;
  yfl = iy/8192.0;
}

void Exptab( int n, struct complex e[])
{
  float theta, divisor, h[26];
  int i, j, k, l, m;

  theta = 3.1415926536;
  divisor = 4.0;
  for ( i=1; i <= 25; i++ )
  {
    h[i] = 1/(2*Cos( theta/divisor ));
    divisor = divisor + divisor;
  }

  m = n / 2 ;
  l = m / 2 ;
  j = 1 ;
  e[1].rp = 1.0 ;
  e[1].ip = 0.0;
  e[l+1].rp = 0.0;
  e[l+1].ip = 1.0 ;
  e[m+1].rp = -1.0 ;
  e[m+1].ip = 0.0 ;

  do {
    i = l / 2 ;
    k = i ;

    do {
      e[k+1].rp = h[j]*(e[k+i+1].rp+e[k-i+1].rp) ;
      e[k+1].ip = h[j]*(e[k+i+1].ip+e[k-i+1].ip) ;
      k = k+l ;
    } while ( k <= m );

    j = Min0( j+1, 25);
    l = i ;
  } while ( l > 1 );

}

void Fft(
  int n,
  struct complex z[],
  struct complex w[],
  struct complex e[],
  float sqrinv
)
{
  int i, j, k, l, m, index;
  m = n / 2 ;
  l = 1 ;

  do {
    k = 0 ;
    j = l ;
    i = 1 ;
    do {
     do {
       w[i+k].rp = z[i].rp+z[m+i].rp ;
       w[i+k].ip = z[i].ip+z[m+i].ip ;
       w[i+j].rp = e[k+1].rp*(z[i].rp-z[i+m].rp)-e[k+1].ip*(z[i].ip-z[i+m].ip);
       w[i+j].ip = e[k+1].rp*(z[i].ip-z[i+m].ip)+e[k+1].ip*(z[i].rp-z[i+m].rp);
       i = i+1 ;
     } while ( i <= j );
     k = j ;
     j = k+l ;
    } while ( j <= m );
    index = 1;
    do {
     z[index] = w[index];
     index = index+1;
    } while ( index <= n );
    l = l+l ;
  } while ( l <= m );

  for ( i = 1; i <= n; i++ )
  {
    z[i].rp = sqrinv*z[i].rp ;
    z[i].ip = -sqrinv*z[i].ip;
  }
}

void Oscar(void)
{
  int i;
  Exptab(fftsize,e);
  seed = 5767 ;
  for ( i = 1; i <= fftsize; i++ )
  {
    Uniform11( seed, zr );
    Uniform11( seed, zi );
    z[i].rp = 20.0*zr - 10.0;
    z[i].ip = 20.0*zi - 10.0;
  }
  for ( i = 1; i <= 20; i++ ) {
   Fft(fftsize,z,w,e,0.0625) ;
#if PRINT
   Printcomplex(6, 99, z, 1, 256, 17);
#endif
  }
}

int main(void)
{
  int i;

  for (i = 0; i < ITERATIONS; i++)
  {
    printf("\nOscar");
    Oscar();
  }

  return 0;
}
