/*
 *  This is a suite of benchmarks that are relatively short, both in program
 *  size and execution time. It requires no input. It does a rudimentary check
 *  to make sure each program gets the right output. These programs were
 *  gathered by John Hennessy and modified by Peter Nye.
 */

/*
 *  CS980 Fall 2007 Notes:
 *
 *  1. This file must be preprocessed before it can be handled by pcc3:
 *       i.e. % cpp hennessy2.c hennessy2-cpp.c
 *            % pcc3 hennessy2-cpp.c
 *            % gcc hennessy2-cpp.s
 *            % a.out
 *
 *  2. Set ITERATIONS to determine how many times the benchmarks are run.
 *
 *  3. Set PRINT to 1 if you want to print the results of the benchmarks.
 *     Set PRINT to 0 if you do not want any output.
 *
 */

#define ITERATIONS 1
#define PRINT 1

/* standard C lib stuff */
int printf(const char *, ...);
typedef long unsigned int size_t;
void *malloc(size_t size);

/* functions to print results */
void PrintPerm(void);
void PrintTowers(void);
void PrintQueens(int a[], int b[], int c[], int x[]);
void PrintIntmm(void);
void PrintMm(void);
void PrintPuzzle(void);
void PrintSort(char *);

/* Towers */
#define maxcells         18

/* Intmm, Mm */
#define rowsize          40

/* Puzzle */
#define size             511
#define classmax         3
#define typemax          12
#define d                8

/* Bubble, Quick */
#define sortelements     5000
#define srtelements      500

/* Fft */
#define fftsize          256
#define fftsize2         129

/* Perm */
#define permrange         10

/* Towers */
#define stackrange         3

/* Tree */
struct node {
  struct node *left, *right;
  int val;
};

/* Towers */
struct element {
  int discsize;
  int next;
};

/* Fft */
struct complex {
  float rp, ip;
};

/* Rand */
int seed;

/* Perm */
int permarray[permrange+1];
int pctr;

/* Tree */
struct node *tree;

/* Towers */
int stack[stackrange+1];
struct element cellspace[maxcells+1];
int freelist, movesdone;

/* Intmm, Mm */
int ima[rowsize+1][rowsize+1], imb[rowsize+1][rowsize+1],
    imr[rowsize+1][rowsize+1];
float rma[rowsize+1][rowsize+1], rmb[rowsize+1][rowsize+1],
       rmr[rowsize+1][rowsize+1];


/* Puzzle */
int piececount[classmax+1],
 class[typemax+1],
 piecemax[typemax+1],
 puzzl[size+1],
 p[typemax+1][size+1],
 n,
 kount;

/* Bubble, Quick */
int sortlist[sortelements+1],
    biggest, littlest,
    top;

/* Fft */
struct complex z[fftsize+1], w[fftsize+1], e[fftsize2+1];
float zr, zi;

void Initrand (void)
{
  seed = 74755;
}

int Rand (void)
{
  seed = (seed * 1309 + 13849) & 65535;
  return( seed );
}

void Swap (int *a, int *b )
{
  int t;
  t = *a; *a = *b; *b = t;
}

void Error (char *emsg)
{
 printf(" Error in Towers: %s\n",emsg);
}

void Makenull (int s)
{
 stack[s]=0;
}

int Getelement (void)
{
 int temp;
 if ( freelist>0 )
 {
   temp = freelist;
   freelist = cellspace[freelist].next;
 }
 else
   Error("out of space   ");
 return (temp);
}

void Push(int i, int s)
{
 int errorfound, localel;
 errorfound=0;
 if ( stack[s] > 0 )
   if ( cellspace[stack[s]].discsize<=i )
   {
     errorfound=1;
     Error("disc size error");
   }
 if ( !errorfound )
 {
   localel=Getelement();
   cellspace[localel].next=stack[s];
   stack[s]=localel;
   cellspace[localel].discsize=i;
 }
}

void Init (int s, int n)
{
 int discctr;
 Makenull(s);
 for ( discctr = n; discctr >= 1; discctr-- )
   Push(discctr,s);
}

int Pop (int s)
{
 int temp, temp1;
 if ( stack[s] > 0 )
 {
   temp1 = cellspace[stack[s]].discsize;
   temp = cellspace[stack[s]].next;
   cellspace[stack[s]].next=freelist;
   freelist=stack[s];
   stack[s]=temp;
   return (temp1);
 }
 else
     Error("nothing to pop ");
}

void Move(int s1, int s2)
{
 Push(Pop(s1),s2);
 movesdone=movesdone+1;
}

void tower(int i, int j, int k)
{
  int other;
  if ( k==1 )
    Move(i,j);
  else
  {
    other=6-i-j;
    tower(i,other,k-1);
    Move(i,j);
    tower(other,j,k-1);
  }
}


void Towers (void)
{
  int i;
  for ( i=1; i <= maxcells; i++ )
     cellspace[i].next=i-1;
  freelist=maxcells;
  Init(1,14);
  Makenull(2);
  Makenull(3);
  movesdone=0;
  tower(1,2,14);
  if ( movesdone != 16383 )
    printf (" Error in Towers.\n");
}
int main(void)
{
  int i;

  for (i = 0; i < ITERATIONS; i++)
  {
    Towers();
    PrintTowers();
  }

  return 0;
}

void PrintTowers(void)
{
  int i;
  printf("\nTower\n");
  for (i = 1; i < stackrange+1; i++)
    printf("%d\n",stack[i]);
  for (i = 1; i < maxcells+1; i++)
    printf("%d %d\n",cellspace[i].discsize, cellspace[i].next);
  printf("%d %d\n",freelist,movesdone);
}
