/* tests a continue statement */
int printf(const char *, ...);

int main(void)
{
	int i;

	i = 1;
	while (i < 4) {
		i++;
		continue;
		break;
	}
	printf("%d\n", i);
}
