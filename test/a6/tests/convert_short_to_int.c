/* tests converting a signed short to a signed int */
int printf(const char *, ...);

int main(void)
{
	short s;
	int i;

	s = 42;
	i = s;

	printf("%d\n", i);
}
