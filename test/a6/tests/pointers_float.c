/* tests address of and derefing pointers for float */
int printf(const char *, ...);

int main(void)
{
	float n;
	float *p;

	p = &n;
	*p = 42.0;

	printf("%f\n", *p);
}
