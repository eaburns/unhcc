/* tests left shift with 32-bit types */
int printf(const char *, ...);

int main(void)
{
	char c;
	short s;

	s = 40;
	c = 2;

	printf("%hhd %hd\n", c << 1, s << 1);
}
