/* tests converting a signed short to a long double */
int printf(const char *, ...);

int main(void)
{
	short s;
	long double ld;

	s = 42;
	ld = s;

	printf("%Lf\n", ld);
}
