/* tests mod with a 64-bit type */
int printf(const char *, ...);

int main(void)
{
	long s, t;

	s = 83;
	t = 2;

	printf("%d\n", s % t);
}
