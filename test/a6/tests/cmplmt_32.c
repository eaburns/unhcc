/* tests one's compliment with 32-bit types */
int printf(const char *, ...);

int main(void)
{
	char c;
	short s;

	c = 1;
	s = 2;

	printf("%hhd %hd\n", ~c, ~s);
}
