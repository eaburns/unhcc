/* tests dereferencing a global variable */
int printf(const char *, ...);
int *x;

int main(void)
{
	int y;

	y = 42;
	x = &y;
	printf("%d\n", *x);
}
