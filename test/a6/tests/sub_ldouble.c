/* tests subtraction with a long double */
int printf(const char *, ...);

int main(void)
{
	long double s, t;

	s = 44.0;
	t = 2.0;

	printf("%Lf\n", s - t);
}
