/* tests converting a signed char to a double */
int printf(const char *, ...);

int main(void)
{
	char c;
	double d;

	c = 42;
	d = c;

	printf("%lf\n", d);
}
