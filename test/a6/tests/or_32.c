/* tests binary or with 32-bit types */
int printf(const char *, ...);

int main(void)
{
	char c;
	short s;

	s = 40;
	c = 2;

	printf("%d\n", s | c);
}
