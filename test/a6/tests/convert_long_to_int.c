/* tests converting a signed long to a signed int */
int printf(const char *, ...);

int main(void)
{
	int i;
	long l;

	l = 42;
	i = l;

	printf("%d\n", i);
}
