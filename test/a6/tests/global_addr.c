/* tests the address operator with a global variable */
int printf(const char *, ...);
int x;

int main(void)
{
	int *y;

	x = 42;
	y = &x;
	printf("%d\n", *y);
}
