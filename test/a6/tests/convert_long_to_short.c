/* tests converting a signed long to a signed short */
int printf(const char *, ...);

int main(void)
{
	short s;
	long l;

	l = 42;
	s = l;

	printf("%hd\n", s);
}
