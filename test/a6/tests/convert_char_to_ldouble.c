/* tests converting a signed char to a long double */
int printf(const char *, ...);

int main(void)
{
	char c;
	long double ld;

	c = 42;
	ld = c;

	printf("%Lf\n", ld);
}
