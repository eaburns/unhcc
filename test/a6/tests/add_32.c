/* tests addition with a 32-bit type */
int printf(const char *, ...);

int main(void)
{
	short s, t;

	s = 40;
	t = 2;

	printf("%hd\n", s + t);
}
