/* tests converting a signed char to a float */
int printf(const char *, ...);

int main(void)
{
	char c;
	float f;

	c = 42;
	f = c;

	printf("%f\n", f);
}
