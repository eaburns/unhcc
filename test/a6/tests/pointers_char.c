/* tests address of and derefing pointers for char */
int printf(const char *, ...);

int main(void)
{
	char n;
	char *p;

	p = &n;
	*p = 42;

	printf("%hhd\n", *p);
}
