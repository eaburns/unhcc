/* tests converting a signed long to a signed char */
int printf(const char *, ...);

int main(void)
{
	char c;
	long l;

	l = 42;
	c = l;

	printf("%hhd\n", c);
}
