/* tests not-equal-to comparison with a float */
int printf(const char *, ...);

int main(void)
{
	float s, t;

	s = 40.0;
	t = 2.0;

	printf("%d %d\n", s != 40.0, t != 40.0);
}
