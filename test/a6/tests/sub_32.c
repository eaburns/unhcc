/* tests subtraction with a 32-bit type */
int printf(const char *, ...);

int main(void)
{
	char s, t;

	s = 44;
	t = 2;

	printf("%hhd\n", s - t);
}
