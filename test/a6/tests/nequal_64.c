/* tests not-equal-to comparison with a 64-bit type */
int printf(const char *, ...);

int main(void)
{
	long s, t;

	s = 40;
	t = 2;

	printf("%d %d\n", s != 40, t != 40);
}
