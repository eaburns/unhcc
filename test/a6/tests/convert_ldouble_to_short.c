/* tests converting a long double to a signed short */
int printf(const char *, ...);

int main(void)
{
	long double ld;
	short s;

	ld = 42.0;
	s = ld;

	printf("%hd\n", s);
}
