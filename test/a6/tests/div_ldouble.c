/* tests division with a double */
int printf(const char *, ...);

int main(void)
{
	long double s, t;

	s = 84.0;
	t = 2.0;

	printf("%Lf\n", s / t);
}
