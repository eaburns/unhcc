/* tests one's compliment with 64-bit types */
int printf(const char *, ...);

int main(void)
{
	long l;

	l = 100;

	printf("%ld\n", ~l);
}
