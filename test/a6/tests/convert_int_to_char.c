/* tests converting a signed int to a signed char */
int printf(const char *, ...);

int main(void)
{
	char c;
	int i;

	i = 42;
	c = i;

	printf("%hhd\n", c);
}
