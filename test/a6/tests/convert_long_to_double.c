/* tests converting a signed long to a double */
int printf(const char *, ...);

int main(void)
{
	long l;
	double d;

	l = 42;
	d = l;

	printf("%lf\n", d);
}
