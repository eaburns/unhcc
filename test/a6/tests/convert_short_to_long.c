/* tests converting a signed short to a signed long */
int printf(const char *, ...);

int main(void)
{
	short s;
	long l;

	s = 42;
	l = s;

	printf("%ld\n", l);
}
