/* tests division with a 64-bit type */
int printf(const char *, ...);

int main(void)
{
	long s, t;

	s = 84;
	t = 2;

	printf("%ld\n", s / t);
}
