/* tests converting a long double to a signed long */
int printf(const char *, ...);

int main(void)
{
	long double ld;
	long l;

	ld = 42.0;
	l = ld;

	printf("%ld\n", l);
}
