/* tests converting a signed char to a signed short */
int printf(const char *, ...);

int main(void)
{
	char c;
	short s;

	c = 42;
	s = c;

	printf("%hd\n", s);
}
