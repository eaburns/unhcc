/* tests converting a signed long to a long double */
int printf(const char *, ...);

int main(void)
{
	long l;
	long double ld;

	l = 42;
	ld = l;

	printf("%Lf\n", ld);
}
