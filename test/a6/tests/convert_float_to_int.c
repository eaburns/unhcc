/* tests converting a float to a signed int */
int printf(const char *, ...);

int main(void)
{
	float f;
	int i;

	f = 42.0;
	i = f;

	printf("%d\n", i);
}
