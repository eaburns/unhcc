/* tests converting a long double to a signed int */
int printf(const char *, ...);

int main(void)
{
	long double ld;
	int i;

	ld = 42.0;
	i = ld;

	printf("%d\n", i);
}
