/* tests converting a signed short to a double */
int printf(const char *, ...);

int main(void)
{
	short s;
	double d;

	s = 42;
	d = s;

	printf("%lf\n", d);
}
