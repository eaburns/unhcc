/* tests multiplication with a long double */
int printf(const char *, ...);

int main(void)
{
	long double s, t;

	s = 21.0;
	t = 2.0;

	printf("%Lf\n", s * t);
}
