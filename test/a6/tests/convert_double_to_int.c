/* tests converting a double to a signed int */
int printf(const char *, ...);

int main(void)
{
	double d;
	int i;

	d = 42.0;
	i = d;

	printf("%d\n", i);
}
