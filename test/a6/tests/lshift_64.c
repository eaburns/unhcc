/* tests left shift with a 64-bit type */
int printf(const char *, ...);

int main(void)
{
	long l;

	l = 42;

	printf("%ld\n", l << 1);
}
