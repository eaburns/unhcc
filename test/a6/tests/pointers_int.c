/* tests address of and derefing pointers for int */
int printf(const char *, ...);

int main(void)
{
	int n;
	int *p;

	p = &n;
	*p = 42;

	printf("%d\n", *p);
}
