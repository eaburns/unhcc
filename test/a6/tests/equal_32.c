/* tests equal-to comparison with a 32-bit type */
int printf(const char *, ...);

int main(void)
{
	int s, t;

	s = 40;
	t = 2;

	printf("%d %d\n", s == 40, t == 40);
}
