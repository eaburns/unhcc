/* tests address of and derefing pointers for short */
int printf(const char *, ...);

int main(void)
{
	short n;
	short *p;

	p = &n;
	*p = 42;

	printf("%hd\n", *p);
}
