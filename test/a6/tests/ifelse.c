/* tests an if-else statement */
int printf(const char *, ...);

int main(void)
{
	int i, j;

	i = 42;

	if (i == 42)
		j = 1;
	else
		j = 2;

	printf("%d\n", j);
}
