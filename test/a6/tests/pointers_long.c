/* tests address of and derefing pointers for long */
int printf(const char *, ...);

int main(void)
{
	long n;
	long *p;

	p = &n;
	*p = 42;

	printf("%ld\n", *p);
}
