/* tests converting a double to a signed long */
int printf(const char *, ...);

int main(void)
{
	double d;
	long l;

	d = 42.0;
	l = d;

	printf("%ld\n", l);
}
