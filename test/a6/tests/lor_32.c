/* tests logical or with a 32-bit type */
int printf(const char *, ...);

int main(void)
{
	int s, t;

	s = 1;
	t = 0;

	printf("%d %d\n", s || 0, t || 0);
}
