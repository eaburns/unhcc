/* tests address of and derefing pointers for long double */
int printf(const char *, ...);

int main(void)
{
	long double n;
	long double *p;

	p = &n;
	*p = 42.0;

	printf("%Lf\n", *p);
}
