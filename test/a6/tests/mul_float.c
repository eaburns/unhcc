/* tests multiplication with a float */
int printf(const char *, ...);

int main(void)
{
	float s, t;

	s = 21.0;
	t = 2.0;

	printf("%f\n", s * t);
}
