/* tests a break statement */
int printf(const char *, ...);

int main(void)
{
	int i;

	i = 2;
	while (i < 45) {
		break;
		i++;
	}

	printf("%d\n", i);
}
