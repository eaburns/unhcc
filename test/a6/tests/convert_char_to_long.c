/* tests converting a signed char to a signed long */
int printf(const char *, ...);

int main(void)
{
	char c;
	long l;

	c = 42;
	l = c;

	printf("%ld\n", l);
}
