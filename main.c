/*  unhcc - main.c
 *
 *
 */
#include <stdio.h>
#include "defs.h"
#include "types.h"
#include "bucket.h"
#include "symtab.h"
#include "tree.h"
#include "message.h"
#include "encode.h"
#include "regAlloc.h"

/*
#define OUTPUT_TO_STDOUT
*/

#if 0
#define ST_DUMP
#endif

void *errfp;			/* file to which message.c will write */

void *outfp;			/* file to which encode.c will write */

/* globals used when compiling functions */
ST_ID gCurrentFunctionName;
int gCurrentFunctionOffset;
Type gCurrentFunctionReturnType;
StorageClass gCurrentFunctionClass;

static void init_globals(void)
{
	/* main.c */
	gCurrentFunctionName = NULL;
	gCurrentFunctionOffset = 0;
	gCurrentFunctionReturnType = NULL;
	gCurrentFunctionClass = NO_SC;

	/* regAlloc.c */
	initRegAlloc();
}

static void *openOutputFile(int argc, char *argv[])
{
#ifndef OUTPUT_TO_STDOUT
	int namelen;
	char *outname;
#endif
	extern void *yyin;
	void *ret;

	if (argc != 2) {
		fatal("usage: %s file\n", argv[0]);
	}

	if (!(yyin = fopen(argv[1], "r"))) {
		fatal("can't open %s\n", argv[1]);
	}

#ifndef OUTPUT_TO_STDOUT
	namelen = strlen(argv[1]);
	/* check if input filename ends in ".c" */
	if (argv[1][namelen - 2] == '.' && argv[1][namelen - 1] == 'c') {
		/* if so, switch the ".c" to ".s" */
		outname = malloc(namelen + 1);
		strcpy(outname, argv[1]);
		outname[namelen - 1] = 's';
	} else {
		/* if not, just append ".s" */
		outname = malloc(namelen + 3);
		strcpy(outname, argv[1]);
		strcat(outname, ".s");
	}

	if (!(ret = fopen(outname, "w"))) {
		fatal("can't open %s\n", outname);
	}
#else
	ret = stdout;
#endif

	/* tell the scanner what the initial filename is */
	YYinitializeFilename(argv[1]);

#ifndef OUTPUT_TO_STDOUT
	free(outname);
#endif

	return ret;
}

int main(int argc, char *argv[])
{
	int status, yyparse(void);

	init_globals();

	errfp = stderr;
	outfp = openOutputFile(argc, argv);
	typeInitialize();
	st_init_symtab();
	poolInit();
	init_bucket_module();
	st_establish_data_dump_func(stdr_dump);
	st_establish_tdata_dump_func(stdr_dump);
	encode_init();
	status = yyparse();
#ifdef ST_DUMP
	st_dump();
#endif
	return status;
}
