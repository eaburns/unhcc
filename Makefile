#
# unhcc makefile
#
#

#
# use which C compiler?
#
CC = gcc

#
# use which parser generator?
#
#YACC = bison
YACC = yacc

#
# Yacc/Bison flags
#
#bison
#YFLAGS = -d -y
#yacc
YFLAGS = -d

#
# use which scanner generator?
#
LEX = flex
#LEX = lex

#
# compile everything for debugger?
#
#yes
CFLAGS = -g -std=c89 -Wall -Werror
#no
#CFLAGS =

PCC3SRC = main.c message.c symtab.c types.c utils.c bucket.c encode.c decl.c \
          tree.c astmem.c semUtils.c regAlloc.c analyze.c decl2.c \
          analyzeExpr.c analyzeStmt.c encodeExpr.c encodeStmt.c constExpr.c \
	  tarch_em64t.c eval.c

PCC3H	= defs.h types.h symtab.h bucket.h decl.h tree.h semUtils.h regAlloc.h \
          analyze.h globals.h encode.h message.h decl2.h constExpr.h \
	  tarch.h tarch_em64t.h tarch_ia32.h eval.h

PCC3OBJ = main.o message.o symtab.o types.o utils.o bucket.o \
          y.tab.o lex.yy.o encode.o decl.o tree.o astmem.o semUtils.o regAlloc.o \
          analyze.o decl2.o analyzeExpr.o analyzeStmt.o encodeExpr.o \
          encodeStmt.o constExpr.o tarch_em64t.o eval.o

# unhcc rules
#
all: unhcc
	cd optimizer ; $(MAKE) && cp registerize .. ; cd ..

unhcc: $(PCC3OBJ)
	$(CC) $(CFLAGS) $(PCC3OBJ) -lm -o unhcc


# dependencies for compiler modules

main.o: main.c $(PCC3H)

types.o: types.c $(PCC3H)

symtab.o: symtab.c $(PCC3H)

bucket.o: bucket.c $(PCC3H)

message.o: message.c $(PCC3H)
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c message.c

utils.o: utils.c $(PCC3H)

encode.o: encode.c $(PCC3H)
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c encode.c

tarch_em64t.o: tarch_em64t.c $(PCC3H)
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c tarch_em64t.c

eval.o: eval.c $(PCC3H)
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c eval.c

tree.o: tree.c $(PCC3H)

astmem.o: astmem.c $(PCC3H)

semUtils.o: semUtils.c $(PCC3H)

regAlloc.o: regAlloc.c $(PCC3H)
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c regAlloc.c

decl.o: decl.c $(PCC3H)

decl2.o: decl2.c $(PCC3H)

analyze.o: analyze.c $(PCC3H)

analyzeExpr.o: analyzeExpr.c $(PCC3H)

analyzeStmt.o: analyzeStmt.c $(PCC3H)

encodeExpr.o: encodeExpr.c $(PCC3H)

encodeStmt.o: encodeStmt.c $(PCC3H)

constExpr.o: constExpr.c $(PCC3H)

y.tab.o : parse.y $(PCC3H)
	$(YACC) $(YFLAGS) parse.y
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c y.tab.c

lex.yy.o : scan.l y.tab.o $(PCC3H)
	$(LEX) scan.l
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -c lex.yy.c

y.output: parse.y
	$(YACC) -v -y parse.y

lexdbg: scan.l y.tab.o symtab.o message.o $(PCC3H)
	$(LEX) scan.l
	$(CC) $(CFLAGS) -DUSE_STANDARD_HEADERS -DDEBUG lex.yy.c symtab.o \
           message.o -ll -o lexdbg
	rm lex.yy.c

test: unhcc
	cd test ; ./test.sh ; cd ..

clean:
	-rm -f unhcc lexdbg y.tab.h y.output y.tab.c lex.yy.c
	-rm -f *.o
	cd optimizer ; $(MAKE) clean ; cd ..
	-rm -f registerize

.PHONY: test
