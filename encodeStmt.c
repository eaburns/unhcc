/*****************************************************************************/
/*                                                                           */
/*          U N I V E R S I T Y    O F    N E W    H A M P S H I R E         */
/*                                                                           */
/*                       COMPUTER  SCIENCE  DEPARTMENT                       */
/*                                                                           */
/*                            CS 812 -- Compilers                            */
/*                                                                           */
/*          --------------------------------------------------------         */
/*                                                                           */
/*  Professor:   Phil Hatcher <pjh@cs.unh.edu>                               */
/*  Programmer:  Andy Foulks <rafoulks@cs.unh.edu>                           */
/*  File:        encodeStmt.c                                                */
/*  Due:         4-13-03                                                     */
/*                                                                           */
/*****************************************************************************/

#define MAX_NESTING_DEPTH 32	/* min req'd by ANSI is 15 levels of
				   nesting */
#define MAX_NUM_LABELS    1024	/* min req'd by ANSI is 257 for case
				   labels */
#define MAX_LABEL_LEN     1024	/* min req'd by ANSI is 31 characters */

#include "encode.h"
#include "message.h"
#include "constExpr.h"
#include "semUtils.h"
#include "tarch.h"

extern int atoi(const char *nptr);
extern int printf(char *, ...);
extern int sprintf(char *, char *, ...);
extern char *strncpy(char *, const char *, size_t);

/* ===========================  DATA STRUCTURES  =========================== */
/**
 *  Keeps track of goto labels.  
 *  Need to clear this list for each new function body.
 *  Also used to keep track of case labels for a switch statement.
 */
typedef struct JumpLabelList {
	int tail;
	char jumpLabels[MAX_NUM_LABELS][MAX_LABEL_LEN + 1];
	int assmLabels[MAX_NUM_LABELS];
} JumpLabelList;

/**
 *  Keeps track of switch statements.  Push when we enter a switch
 *  statement, pop when we leave a switch statement.  When we see
 *  a case label, attach it to the current switch node.  Here we
 *  use the JumpLabelList data structure to implement the case
 *  labels.
 */
typedef struct SwitchStack {
	int top;
	JumpLabelList caseLabels[MAX_NESTING_DEPTH];
} SwitchStack;

/**
 *  Keeps track of break and continue jumps.  Create one of these
 *  LabelStacks for each.  When we enter a construct that can 
 *  have a break in it (while, do-while, for, switch), push
 *  the assembly label (i.e. the break jump point or the 
 *  continue jump point) onto this stack.  The continue
 *  stack works the same way, except that there are no 
 *  continue's in a switch.
 */
typedef struct LabelStack {
	int top;
	int labels[MAX_NUM_LABELS];
} LabelStack;


/* =========================  FUNCTION PROTOTYPES  ========================= */

/* JumpLabelList functions */

/* these four functions are used by analyzeStmt */
int addJumpLabel(JumpLabelList * l, char *jmp, int asm);
int findAssmLabel(JumpLabelList * l, char *label);
void clearJumpList(JumpLabelList * labelList);
JumpLabelList *newJumpLabelList(void);

/* the rest of these functions are used in this file only */
static int numJumpLabels(JumpLabelList * labelList);
static char *getJumpLabelAt(JumpLabelList * labelList, int index);
static int getAssmLabelAt(JumpLabelList * labelList, int index);

/* SwitchStack functions */

static void pushSwitch(SwitchStack * switchStack);
static void popSwitch(SwitchStack * switchStack);
static int addDefaultLabel(SwitchStack * switchStack, int asmlbl);
static int addCaseLabel(SwitchStack * s, int caslbl, int asmlbl);
static JumpLabelList *getTopOfSwitchStack(SwitchStack * switchStack);
static void clearSwitchStack(SwitchStack * switchStack);
static int isSwitchStackEmpty(SwitchStack * switchStack);

/* LabelStack functions */
static void pushLabel(LabelStack * labelStack, int assmLabel);
static void popLabel(LabelStack * labelStack);
static int getCurrentLabel(LabelStack * labelStack);
static int isLabelStackEmpty(LabelStack * labelStack);
static void clearLabelStack(LabelStack * labelStack);

/* ===============================  GLOBALS  =============================== */

/* these globals are used by this file and also by analyzeStmt.c */
JumpLabelList *gGotoList;
int NOT_FOUND_LABEL;

/* these globals are used by this file only */
static LabelStack *gBreakStack;
static LabelStack *gContinueStack;
static SwitchStack *gSwitchStack;

/* ========================================================================= */
/*
 *  These two routines allow any necessary initialization and cleanup to be 
 *  performed for the functions in this file. beforeEncode is called before 
 *  encode is called for each function body being compiled. afterEncode is 
 *  called after encode returns. (These calls are in parse.y.)
 */
void beforeEncode(void)
{
	/* 
	 * here we set up the data structures that will be used
	 * to keep track of switches, breaks, and continues.
	 *
	 * the data structure to handle labels is initialized 
	 * in beforeAnalyze().
	 *
	 * because this function is called every time we process
	 * a new function body, we probably have poor memory management
	 * here but speed is not an issue.
	 */

	free(gBreakStack);
	free(gContinueStack);
	free(gSwitchStack);

	gSwitchStack = (SwitchStack *) malloc(sizeof(SwitchStack));
	gBreakStack = (LabelStack *) malloc(sizeof(LabelStack));
	gContinueStack = (LabelStack *) malloc(sizeof(LabelStack));

	if (gBreakStack == NULL || gContinueStack == NULL
	    || gSwitchStack == NULL)
		bug("FATAL: beforeEncode: out of memory error!");

	clearLabelStack(gBreakStack);
	clearLabelStack(gContinueStack);
	clearSwitchStack(gSwitchStack);
}

/* ========================================================================= */

void afterEncode(void)
{
	/* nothing */
}

/* ==============================  75% LEVEL  ============================== */
/**
 *  Algorithm for the while loop:
 *      L1:
 *          <encode exp>
 *          if result is 0 goto L2
 *          <encode stmt>
 *          goto L1:
 *      L2:
 *
 *  Whenever we enter a while loop, we need to keep track of the break
 *  jump point and the continue jump point so that if we see a 
 *  break or continue somewhere in the while body, we know where 
 *  to jump to.  We use the globals gBreakStack and gContinueStack
 *  for this.
 */
void encodeWhile(WhileTree w)
{
	int continueLabel, breakLabel;

	continueLabel = genLabel();
	breakLabel = genLabel();

	pushLabel(gBreakStack, breakLabel);
	pushLabel(gContinueStack, continueLabel);

	comment("while");

	/* set up continueLabel */
	emitLabel("L$%d", continueLabel);

	/* encode the control expression */
	encode((Tree) w->expr);

	/* if result == 0, goto breakLabel */
	emitCondBranch("\tjz\t", "L$%d", breakLabel);

	/* encode the statement */
	encode((Tree) w->statement);

	/* pop the stack if needed */
	freeExpressionStatementRegister((Tree) w->statement);

	/* jump to continueLabel */
	emitUncondBranch("\tjmp\t", "L$%d", continueLabel);

	/* set up breakLabel */
	emitLabel("L$%d", breakLabel);

	comment1("-- while end --");

	popLabel(gBreakStack);
	popLabel(gContinueStack);
}

/* ========================================================================= */
/**
 *  if we have           do this:
 *      if( exp )           <encode exp>
 *          stmt            if result == 0 goto L1
 *                          <encode stmt>
 *                       L1:
 *
 *  but if we have       do this:
 *      if( exp )           <encode exp>
 *          stmt1           if result == 0 goto L1
 *      else                <encode stmt1>
 *          stmt2           goto L2
 *                       L1:
 *                          <encode stmt2>
 *                       L2:
 */
void encodeIfElse(IfElseTree w)
{
	int label1, label2;

	label1 = genLabel();
	label2 = genLabel();

	comment("if else");

	encode((Tree) w->expr);

	/* if result == 0, goto L1 */
	emitCondBranch("\tjz\t", "L$%d", label1);

	/* encode the statement */
	encode((Tree) w->ifstmt);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) w->ifstmt);

	if (w->elsestmt != NULL)
		emitUncondBranch("\tjmp\t", "L$%d", label2);

	/* set up label1 */
	emitLabel("L$%d", label1);

	if (w->elsestmt != NULL) {
		encode((Tree) w->elsestmt);

		/* pop the stack if necessary */
		freeExpressionStatementRegister((Tree) w->elsestmt);

		/* set up label2 */
		emitLabel("L$%d", label2);
	}

	comment1("-- if else --");
}

/* ========================================================================= */
/**
 *  jump to the break point of the closest loop / switch
 *  make sure the break is inside a loop or switch
 */
void encodeBreak(BreakTree br)
{
	if (!isLabelStackEmpty(gBreakStack)) {
		comment("break");
		emitUncondBranch("\tjmp\t", "L$%d",
		                 getCurrentLabel(gBreakStack));
	} else {
		errorT((Tree) br,
		       "break must be within a loop / switch");
	}
}

/* ========================================================================= */
/**
 *  jump to the continue point of the closest loop, and make
 *  sure that the continue is inside a loop
 */
void encodeContinue(ContinueTree co)
{
	if (!isLabelStackEmpty(gContinueStack)) {
		comment("continue");
		emitUncondBranch("\tjmp\t", "L$%d",
		                 getCurrentLabel(gContinueStack));
	} else {
		errorT((Tree) co, "continue must be within a loop");
	}
}

/* ==============================  85% LEVEL  ============================== */
/**
 *  Algorithm:
 *      L1:
 *          <encode stmt>
 *      L2:                                <- continue jump point
 *          <encode expr>
 *          if result == 0 goto L3
 *          goto L1
 *      L3:                                <- break jump point
 *
 *  Whenever we enter a do-while loop, we need to keep track of the break
 *  jump point and the continue jump point so that if we see a 
 *  break or continue somewhere in the do-while body, we know where 
 *  to jump to.  We use the globals gBreakStack and gContinueStack
 *  for this.
 */
void encodeDoWhile(DoWhileTree w)
{
	int label1, continueLabel, breakLabel;

	label1 = genLabel();
	continueLabel = genLabel();
	breakLabel = genLabel();

	pushLabel(gBreakStack, breakLabel);
	pushLabel(gContinueStack, continueLabel);

	comment("do while");

	/* set up label1 */
	emitLabel("L$%d", label1);

	/* encode the statement */
	encode((Tree) w->statement);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) w->statement);

	/* set up continueLabel */
	emitLabel("L$%d", continueLabel);

	/* encode the control expression */
	encode((Tree) w->expr);

	/* if result == 0, goto breakLabel */
	emitCondBranch("\tjz\t", "L$%d", breakLabel);

	/* jump to label1 */
	emitUncondBranch("\tjmp\t", "L$%d", label1);

	/* set up breakLabel */
	emitLabel("L$%d", breakLabel);

	comment1("-- do while end --");

	popLabel(gBreakStack);
	popLabel(gContinueStack);
}

/* ========================================================================= */
/**
 *  Algorithm:
 *          <encode exp1>
 *      L1:                             <-- top of loop
 *          <encode exp2>
 *          if result is 0 goto L2
 *          <encode stmt>
 *      L3:                             <-- continue label
 *          <encode exp3>
 *          goto L1:
 *      L2:                             <-- break label
 *
 *  Whenever we enter a for loop, we need to keep track of the break
 *  jump point and the continue jump point so that if we see a 
 *  break or continue somewhere in the for body, we know where 
 *  to jump to.  We use the globals gBreakStack and gContinueStack
 *  for this.
 */
void encodeFor(ForTree w)
{
	int continueLabel, breakLabel, topOfLoopLabel;

	topOfLoopLabel = genLabel();
	continueLabel = genLabel();
	breakLabel = genLabel();

	pushLabel(gBreakStack, breakLabel);
	pushLabel(gContinueStack, continueLabel);

	comment("for");

	/* encode the first expression */
	encode((Tree) w->expr1);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) w->expr1);

	/* set up topOfLoopLabel */
	emitLabel("L$%d", topOfLoopLabel);

	/* encode the control expression */
	encode((Tree) w->expr2);

	/* if result == 0, goto breakLabel */
	emitCondBranch("\tjz\t", "L$%d", breakLabel);

	/* encode the statement */
	encode((Tree) w->statement);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) w->statement);

	/* set up continueLabel */
	emitLabel("L$%d", continueLabel);

	/* encode third expression */
	encode((Tree) w->expr3);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) w->expr3);

	/* jump to topOfLoopLabel */
	emitUncondBranch("\tjmp\t", "L$%d", topOfLoopLabel);

	/* set up breakLabel */
	emitLabel("L$%d", breakLabel);

	comment1("-- for end --");

	popLabel(gBreakStack);
	popLabel(gContinueStack);
}

/* ==============================  100% LEVEL  ============================= */
/** 
 *  Algorithm:
 *
 *  When we see a label definition
 *
 *      LABEL:
 *
 *  The list of labels were build up in the analyze phase.  So we
 *  should find the label here, or else some kind of wierd error
 *  has taken place.... but in any event we take the label
 *  and simply emit the assembly label associated with it.
 */
void encodeLabel(LabelTree l)
{
	int assmLabel;

	assmLabel = findAssmLabel(gGotoList, st_get_id_str(l->label));

	if (assmLabel == NOT_FOUND_LABEL)
		bug("encodeLabel: label not found! impossible!");

	comment("label");

	/* set up assmLabel */
	emitLabel("L$%d", assmLabel);

	encode(l->left);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) l->left);
}

/* ========================================================================= */
/**
 *  When we see a case label, attach it to the current switch 
 *  "node", i.e. put it on the switch stack.  When you get to the
 *  bottom of the encodeSwitch() function, the list should have
 *  been build up.  Use the list in encodeSwitch() to emit the
 *  case instructions.  Also need to make sure that the case is
 *  inside of a switch statement.
 */
void encodeCase(CaseTree ca)
{
	long caseLabel, assmLabel;
	int success;

	caseLabel = getConstExprValue(ca->caseexpr);

	assmLabel = genLabel();

	if (!isSwitchStackEmpty(gSwitchStack)) {
		success =
		    addCaseLabel(gSwitchStack, caseLabel, assmLabel);

		if (!success)
			errorT((Tree) ca,
			       "duplicate case label '%d' found",
			       caseLabel);

		comment("case");

		/* now emit the label and encode the case statement */
		emitLabel("L$%d", assmLabel);

		encode(ca->statement);

		/* pop the stack if necessary */
		freeExpressionStatementRegister((Tree) ca->statement);

		comment1("-- case end --");
	} else {
		errorT((Tree) ca, "case '%d' must be inside a switch",
		       caseLabel);
	}
}

/* ========================================================================= */
/**
 *  Similar to case, need to make sure that there are no duplicate
 *  defaults, and that the default is inside a switch.
 */
void encodeDefault(DefaultTree d)
{
	int assmLabel, success;

	assmLabel = genLabel();

	if (!isSwitchStackEmpty(gSwitchStack)) {
		success = addDefaultLabel(gSwitchStack, assmLabel);

		if (!success)
			errorT((Tree) d,
			       "duplicate 'default' found in switch");

		comment("default");

		/* now emit the label and encode the default statement */
		emitLabel("L$%d", assmLabel);

		encode(d->statement);

		/* pop the stack if necessary */
		freeExpressionStatementRegister((Tree) d->statement);

		comment1("-- default end --");
	} else {
		errorT((Tree) d, "'default' must be inside a switch");
	}

}

/* ========================================================================= */
/**
 *  'continue' is no use inside a switch, so we do NOT add this
 *  switch statement to the continue label stack.  but we do add it
 *  to the break label stack.
 *
 *  Algorithm:
 *          <encode expr>
 *          jump to L1
 *          <encode stmt>
 *          jump to L2
 *      L1:
 *          get result off of stack
 *          if result == first case constant goto label for that case constant
 *          if result == 2nd case constant goto label for that case constant
 *          if result == 3rd case constant goto label for that case constant
 *          etc.
 *      L2:        <- break jump point
 */
void encodeSwitch(SwitchTree sw)
{
	int i, breakLabel, label1, numCaseLabels, assmLabel,
	    caseConstant;
	JumpLabelList *caseLabelList;
	char *caseLabel;
	REG reg;	

	/* push the switch stack to make room to store the case labels */
	pushSwitch(gSwitchStack);

	breakLabel = genLabel();
	label1 = genLabel();

	pushLabel(gBreakStack, breakLabel);

	comment("switch");

	/* encode the expression */
	encode((Tree) sw->expr);
	reg =  getRegister(sw->expr->reg);

	/* jump to the encoding of the case constants */
	emitUncondBranch("\tjmp\t", "L$%d", label1);

	/* encode the statement, hope the case labels are buried there */
	encode((Tree) sw->statement);

	/* pop the stack if necessary */
	freeExpressionStatementRegister((Tree) sw->statement);

	/* jump over all of the case label encodings to the break label 
	   pt */
	emitUncondBranch("\tjmp\t", "L$%d", breakLabel);

	/* set up the starting point of the case constants encoding */
	emitLabel("L$%d", label1);

	caseLabelList = getTopOfSwitchStack(gSwitchStack);
	numCaseLabels = numJumpLabels(caseLabelList);

	/* for each case label found, encode a pair of instructions */
	for (i = 0; i < numCaseLabels; i++) {
		caseLabel = getJumpLabelAt(caseLabelList, i);
		assmLabel = getAssmLabelAt(caseLabelList, i);

		/* special case (unconditional jump) for the default */
		if (strcmp(caseLabel, "default") == 0) {
			emitUncondBranch("\tjmp\t", "L$%d", assmLabel);
		} else {
			caseConstant = atoi(caseLabel);

			emit("\tcmp%c\t$%d, %s",
			     intelSuffix(regType(reg)),
 			     caseConstant,
			     regString(reg));
			useReg(reg);
			emitCondBranch("\tje\t", "L$%d", assmLabel);
		}
	}

	/* set up the break label */
	emitLabel("L$%d", breakLabel);

	comment1("-- switch end --");

	popLabel(gBreakStack);

	/* pop the switch stack to clear the case labels */
	popSwitch(gSwitchStack);
}

/* ========================================================================= */
/**
 *  When we see a 
 *
 *      goto LABEL
 *
 *  if the label is not in the list, then there is an error, because
 *  we are jumping to a label that is not defined.  Remember, all
 *  of the labels were added to the gGotoList in the analyze 
 *  phase, so they should all be here.  Assuming we find the
 *  label, emit an unconditional jump to it.
 */
void encodeGoto(GotoTree g)
{
	int assmLabel;
	char *label;

	label = st_get_id_str(g->label);

	assmLabel = findAssmLabel(gGotoList, label);

	if (assmLabel == NOT_FOUND_LABEL)
		errorT((Tree) g,
		       "label '%s' not found in enclosing function",
		       label);

	comment("goto");

	/* emit the unconditional jump */
	emitUncondBranch("\tjmp\t", "L$%d", assmLabel);
}

/* ========================================================================= */
/*
 *  All of the functions defined below are part of the data structure
 *  support to store goto / case labels, keep track of which 
 *  switch nesting level we are in, and keep track of the
 *  current break / continue points
 */
/* =========================  JUMP LABEL FUNCTIONS  ======================== */
/**
 *  adds jump label and assembly label pair to the end of the list.
 *
 *  returns 1 (true) if it was successfully added
 *  returns 0 (false) if there was a duplicate label
 */
int addJumpLabel(JumpLabelList * labelList, char *jumpLabel,
		 int assmLabel)
{
	int retval;
	int duplicateLabel;

	if (labelList->tail >= MAX_NUM_LABELS)
		bug("implementation limit reached for MAX_NUM_LABELS (%d)", MAX_NUM_LABELS);

	if (labelList->tail < 0)
		bug("addJumpLabel: index out of bounds!(%d)",
		    labelList->tail);

	retval = 1;

	/* need to make sure there are no duplicates */
	duplicateLabel = findAssmLabel(labelList, jumpLabel);

	if (duplicateLabel == NOT_FOUND_LABEL) {
		labelList->tail++;

		if (strlen(jumpLabel) < MAX_LABEL_LEN) {
			strcpy(labelList->jumpLabels[labelList->tail],
			       jumpLabel);

			labelList->assmLabels[labelList->tail] =
			    assmLabel;
		} else {
			error("label '%s' exceeds maximum length",
			      jumpLabel);
			retval = 0;
		}
	} else {
		retval = 0;
	}

	return retval;
}

/* ========================================================================= */
/**
 *  Allocates memory for a jump label list object (i.e. a goto label).
 */
JumpLabelList *newJumpLabelList(void)
{
	JumpLabelList *l;

	l = (JumpLabelList *) malloc(sizeof(JumpLabelList));

	if (l == NULL)
		bug("FATAL: newJumpLabelList: out of memory");

	return l;
}

/* ========================================================================= */
/**
 *  searches for jump label 'label' and returns the assembly label
 *  if found or NOT_FOUND_LABEL (-1) if not found.  
 */
int findAssmLabel(JumpLabelList * labelList, char *jumpLabel)
{
	int i;
	int assmLabel;

	assmLabel = NOT_FOUND_LABEL;

	for (i = 0; i < MAX_NUM_LABELS; i++) {
		if (strcmp(labelList->jumpLabels[i], jumpLabel) == 0) {
			assmLabel = labelList->assmLabels[i];
			break;
		}
	}

	return assmLabel;
}

/* ========================================================================= */

static int numJumpLabels(JumpLabelList * labelList)
{
	return labelList->tail;
}

/* ========================================================================= */

static char *getJumpLabelAt(JumpLabelList * labelList, int index)
{
	/* since we don't use the 0 element in the list, we use index+1 
	 * maybe this was a bad implementation decision, but its too late now
	 */

	if (index + 1 > MAX_NUM_LABELS || index + 1 <= 0)
		bug("getJumpLabelAt: index out of bounds(%d+1)", index);

	return labelList->jumpLabels[index + 1];
}

/* ========================================================================= */

static int getAssmLabelAt(JumpLabelList * labelList, int index)
{
	/* we don't use the 0 element in the array, so add 1 (start
	   with index 1) maybe this was a bad implementation decision,
	   but its too late now */

	if (index + 1 > MAX_NUM_LABELS || index + 1 <= 0)
		bug("getAssmLabelAt: index out of bounds(%d+1)", index);

	return labelList->assmLabels[index + 1];
}

/* ========================================================================= */
/**
 *  Clears the jump label list.  The 0 index is reserved to indicate
 *  an empty list.
 */
void clearJumpList(JumpLabelList * labelList)
{
	int i;

	labelList->tail = 0;

	strcpy(labelList->jumpLabels[0],
	       "empty list - shouldn't be reading this");

	labelList->assmLabels[0] = NOT_FOUND_LABEL;

	for (i = 1; i < MAX_NUM_LABELS; i++) {
		if (labelList->jumpLabels[i][0] == '\0')
			break;

		labelList->jumpLabels[i][0] = '\0';
		labelList->assmLabels[i] = 0;
	}
}

/* ========================  SWTICH STACK FUNCTIONS  ======================= */
/**
 *  increments the top of the switch stack.  call this when you
 *  process a switch statement.
 */
static void pushSwitch(SwitchStack * switchStack)
{
	switchStack->top++;

	if (switchStack->top >= MAX_NESTING_DEPTH)
		bug("pushSwitch: implementation limit reached for "
		    "MAX_NESTING_DEPTH (%d)", MAX_NESTING_DEPTH);

	if (switchStack->top < 0)
		bug("pushSwitch: stack out of bounds!(%d)",
		    switchStack->top);
}

/* ========================================================================= */
/**
 *  decrements the top of the switch stack.  call this when you
 *  leave a switch statement.
 */
static void popSwitch(SwitchStack * switchStack)
{
	if (switchStack->top >= MAX_NESTING_DEPTH)
		bug("popSwitch: implementation limit reached for "
		    "MAX_NESTING_DEPTH (%d)", MAX_NESTING_DEPTH);

	clearJumpList(&switchStack->caseLabels[switchStack->top]);

	switchStack->top--;

	if (switchStack->top < 0)
		bug("popSwitch: stack out of bounds!(%d)",
		    switchStack->top);
}

/* ========================================================================= */
/** 
 *  appends the case label to the top of the switch stack
 *
 *  returns 1 (true) if successfully added
 *  returns 0 (false) if duplicate label detected
 */
static int addCaseLabel(SwitchStack * switchStack, int caseLabel,
			int assmLabel)
{
	int retval;
	JumpLabelList *labelList;
	char buf[MAX_LABEL_LEN];

	if (switchStack->top >= MAX_NESTING_DEPTH)
		bug("addCaseLabel: implementation limit reached for "
		    "MAX_NESTING_DEPTH (%d)", MAX_NESTING_DEPTH);

	if (switchStack->top <= 0)
		bug("addCaseLabel: stack out of bounds!(%d)",
		    switchStack->top);

	retval = 1;
	labelList = &switchStack->caseLabels[switchStack->top];

	/* convert case label integer to a string */
	sprintf(buf, "%d", caseLabel);

	retval = addJumpLabel(labelList, buf, assmLabel);

	return retval;
}

/* ========================================================================= */
/**
 *  returns 1 (true) if label added successfully
 *  returns 0 (false) if duplicate "default" found
 */
static int addDefaultLabel(SwitchStack * switchStack, int assmLabel)
{
	int retval;
	JumpLabelList *labelList;

	if (switchStack->top >= MAX_NESTING_DEPTH)
		bug("addDefaultLabel: implementation limit reached for "
		    "MAX_NESTING_DEPTH (%d)", MAX_NESTING_DEPTH);

	if (switchStack->top <= 0)
		bug("addDefaultLabel: stack out of bounds!(%d)",
		    switchStack->top);

	retval = 1;
	labelList = &switchStack->caseLabels[switchStack->top];

	retval = addJumpLabel(labelList, "default", assmLabel);

	return retval;
}

/* ========================================================================= */

static int isSwitchStackEmpty(SwitchStack * switchStack)
{
	if (switchStack->top < 0)
		bug("isSwitchStackEmpty: stack out of bounds!(%d)",
		    switchStack->top);

	return switchStack->top == 0;
}

/* ========================================================================= */

static JumpLabelList *getTopOfSwitchStack(SwitchStack * switchStack)
{
	if (switchStack->top >= MAX_NESTING_DEPTH)
		bug("getTopOfSwitchStack: implementation limit reached for " "MAX_NESTING_DEPTH (%d)", MAX_NESTING_DEPTH);

	if (switchStack->top == 0)
		bug("getTopOfSwitchStack: stack is empty!");

	if (switchStack->top < 0)
		bug("getTopOfSwitchStack: stack out of bounds!(%d)",
		    switchStack->top);

	return &switchStack->caseLabels[switchStack->top];
}

/* ========================================================================= */
/**
 *  resets the switch stack.  call this when entering a new 
 *  function body.
 */
static void clearSwitchStack(SwitchStack * switchStack)
{
	int i;

	switchStack->top = 0;

	for (i = 0; i < MAX_NESTING_DEPTH; i++)
		clearJumpList(&switchStack->caseLabels[i]);
}

/* ========================  LABEL STACK FUNCTIONS  ======================== */
/**
 *  pushes an assembly label (integer) onto the stack.
 */
static void pushLabel(LabelStack * labelStack, int assmLabel)
{
	labelStack->top++;

	if (labelStack->top >= MAX_NUM_LABELS)
		bug("pushLabel: implementation limit reached for "
		    "MAX_NUM_LABELS (%d)", MAX_NUM_LABELS);

	if (labelStack->top <= 0)
		bug("pushLabel: stack out of bounds!(%d)",
		    labelStack->top);

	labelStack->labels[labelStack->top] = assmLabel;
}

/* ========================================================================= */
/**
 *  pops the top label from the stack.  call this when you leave
 *  a do-while, while, for, etc. block.
 */
static void popLabel(LabelStack * labelStack)
{
	labelStack->top--;

	if (labelStack->top >= MAX_NUM_LABELS)
		bug("pushLabel: implementation limit reached for "
		    "MAX_NUM_LABELS (%d)", MAX_NUM_LABELS);

	if (labelStack->top < 0)
		bug("popLabel: stack out of bounds!(%d)",
		    labelStack->top);
}

/* ========================================================================= */
/**
 *  returns the value at the top of the stack.  call this when you
 *  want to get the break / continue label to jump to for the 
 *  current loop / switch statement.
 */
static int getCurrentLabel(LabelStack * labelStack)
{
	if (labelStack->top >= MAX_NUM_LABELS)
		bug("getCurrentLabel: implementation limit reached for "
		    "MAX_NUM_LABELS (%d)", MAX_NUM_LABELS);

	if (labelStack->top < 0)
		bug("getCurrentLabel: stack out of bounds!(%d)",
		    labelStack->top);

	if (labelStack->top == 0)
		bug("getCurrentLabel: stack is empty!");

	return labelStack->labels[labelStack->top];
}

/* ========================================================================= */
/**
 *  returns 1 (true) if the stack is empty.
 *  returns 0 (false) if the stack is not empty.
 */
static int isLabelStackEmpty(LabelStack * labelStack)
{
	if (labelStack->top >= MAX_NUM_LABELS || labelStack->top < 0)
		bug("isLabelStackEmpty: stack out of bounds!(%d)",
		    labelStack->top);

	return labelStack->top == 0;
}

/* ========================================================================= */
/**
 *  resets the label stack.  call this for each new function body 
 *  encountered.
 */
static void clearLabelStack(LabelStack * labelStack)
{
	labelStack->top = 0;

	labelStack->labels[0] = NOT_FOUND_LABEL;
}

/* ========================================================================= */
