void *vp;
int (*fp)(int);

int f(int i)
{
  vp++;
  vp--;
  fp++;
  fp--;
  i = fp > fp;
  i = fp <= fp;
  i = fp < fp;
  i = fp >= fp;
  vp = fp;
  fp = vp;
  vp = (void *) fp;
  fp = (int (*)(int)) vp;
  vp = (int *) fp;
  fp += 2;
  fp -= 2;
  vp += 2;
  vp -= 2;
  fp = fp + 2;
  fp = fp - 2;
  vp = vp + 2;
  vp = vp - 2;
  i = fp - fp;
  i = vp - vp;
  vp();
  (*vp) = 1;
  return 0;
}
