int printf(char*, ...);

int i;
double d;
void *vp1, *vp2, *vp3, *vp4;
int a[10];

int main(void)
{
  i = 19;
  d = 3.14159;
  vp1 = &i;
  vp2 = &d;
  vp3 = (void *) i;
  vp4 = 0;
  printf("%d %d %d %d ", !vp1, !vp4, vp1 == vp2, vp1 != vp2);
  printf("%d %d %d %d ", vp4 == vp4, vp3 != vp3, vp1 && vp2, vp1 || vp2);
  printf("%d %d\n", vp1 && vp4, vp1 || vp4);
  vp1 = &a[4];
  vp2 = &a[7];
  printf("%d %d %d %d\n", vp1 < vp2, vp1 > vp2, vp1 >= vp2, vp1 <= vp2);
  vp3 = (9 > 18 ? vp1 : vp2);
  printf("%d\n", vp1 == vp3);
  return 0;
}
  

