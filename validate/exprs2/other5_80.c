
int printf(char*, ...);
void* malloc(unsigned long);
int i;
int *pi1, *pi2;
double *pd1, *pd2;

int main(void)
{
  pi1 = malloc(sizeof(int)*10);
  pd1 = malloc(sizeof(double)*10);
  pi2 = pi1 + 3;
  pd2 = pd1 + 7;
  printf("%d %d %d %d %d %d\n", pi1 < pi2, pi1 > pi2, pi1 <= pi2, pi1 >= pi2,
    pi1 == pi2, pi1 != pi2);
  printf("%d %d %d %d %d %d\n", pd1 < pd2, pd1 > pd2, pd1 <= pd2, pd1 >= pd2,
    pd1 == pd2, pd1 != pd2);
  printf("%d %d %d %d %d %d\n", pi2 < pi2, pi2 > pi2, pi2 <= pi2, pi2 >= pi2,
    pi2 == pi2, pi2 != pi2);
  return 0;
}
  

