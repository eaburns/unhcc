int printf(char*, ...);

int i;
int a[10];
int (*p)[10];

int main(void)
{
  printf("%d\n", sizeof(a));
  p = &a;
  for (i = 0; i < 10; i++)
  {
    (*p)[i] = i;
  }
  for (i = 0; i < 10; i++)
  {
    printf("%d\n", a[i]);
  }
  return 0;
}
