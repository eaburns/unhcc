int printf(char*, ...);

struct x {
  int i;
  char c;
  double d;
} x;

int main(void)
{
  x.i = 19;
  x.c = 'a';
  x.d = 3.14159;
  printf("%d %c %f\n", x.i, x.c, x.d);
  return 0;
}

