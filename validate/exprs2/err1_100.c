struct x {
  int i;
  float f;
  double d;
} x, *px;

struct y {
  int i;
  float f;
  double d;
} y, *py;

int i;

void f(void)
{
  x = y;
  i.i = 9;
  x->i = 10;
  i->i = 11;
  x.zzz = 12;
  y->zzz = 13;
  (x = x).i = 14;
  x = 5;
  x++;
  ++x;
  x--;
  --x;
  i = x + 5;
  i = x - 5;
  i = x * 5;
  i = x / 5;
  i = x % 5;
  i = 5 | x;
  i = 5 & x;
  i = 5 ^ x;
  i = -x;
  i = +x;
  i = x && 1;
  i = x || 1;
  i = !x;
  x = *x;
  *x = x;
  i = ~x;
  x = (struct x) y;
  i = x >> 5;
  i = x << 5;
  i = 5 >> x;
  i = 5 << x;
  i = x < x;
  i = x > x;
  i = x <= x;
  i = x >= x;
  i = x == x;
  i = x != x;
  x = 1 ? x : y;
  x *= 1;
  x /= 1;
  x %= 1;
  x += 1;
  x -= 1;
  x <<= 1;
  x >>= 1;
  x &= 1;
  x ^= 1;
  x |= 1;
  x();
}

