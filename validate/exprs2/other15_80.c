int printf(char*, ...);

int i;
double dp;
double *dp1, *dp2;

int main(void)
{
  dp1 = &dp;
  dp2 = 0;
  if (dp1) printf("A");
  while (dp1)
  {
    printf("B");
    dp1 = 0;
  }
  for (dp1 = &dp; dp1; dp1 = 0) printf("C");
  do {
    printf("D");
    dp1 = 0;
  }
  while (dp1);
  if (main) printf("E");
  while (main) 
  {
    printf("F");
    break;
  }
  for (;main;)
  {
    printf("G");
    break;
  }
  i = 0;
  do {
    if (i) break;
    printf("H");
    i++;
  }
  while (main);
  printf("\n");
  return 0;
}
