struct x {
  int i;
  float f;
  double d;
} x;

int main(void)
{
  if (x) x.i = 5;
  switch (x) { }
  for (;x;);
  while (x) ;
  do {} while (x);
}
