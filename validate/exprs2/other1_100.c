int printf(char*, ...);

struct x {
  int i;
  char c;
  double d;
} x;

union y {
  int i;
  char c;
  double d;
} y;

int main(void)
{
  printf("%d %d %d %d\n", sizeof(struct x), sizeof(x), sizeof(union y),
    sizeof(y));
  return 0;
}

