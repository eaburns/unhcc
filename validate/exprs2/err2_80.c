const int *p1;
volatile int *p2;
const volatile int *p3;
int *p4;
double *p5;
int i;

void f(void)
{
  p3 = p1;
  p3 = p2;
  p1 = p2;
  p2 = p1;
  p1 = p3;
  p2 = p3;
  *p1 = 5;
  *p3 = 7;
  *(18) = 9;
  i = p5 - p4;
  i = p5 * 5;
  i = p5 / 5;
  i = p5 % 5;
  i = -p5;
  i = +p5;
  i = ~p5;
  p5();
  i = sizeof(f);
  i = p5 >> 5;
  i = p5 << 5;
  i = p5 & 5;
  i = 5 | p5;
  i = 5 ^ p5;
  p5 = 1 ? p4 : p5;
  p5 *= 2;
  p5 /= 2;
  p5 %= 2;
  p5 <<= 2;
  p5 >>= 2;
  p5 &= 2;
  p5 |= 2;
  p5 ^= 2;
}
