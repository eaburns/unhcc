int printf(char*, ...);

struct x {
  int i;
  float f;
  union y *p;
  struct z {
    int i;
    float f;
  } z;
} x;

struct z *pz;

union y {
  struct x *px;
  int i;
} y;

int *pi;

float *pf;

int main(void)
{
  x.p = &y;
  pi = &x.i;
  pf = &x.f;
  *pi = 19;
  *pf = 3.14159;
  x.p->i = x.i + 1;
  printf("%d\n", x.p->i);
  y.px = &x;
  printf("%f\n", x.p->px->f);
  printf("%d\n", x.p->px->p->px->p->px->i);
  pz = &x.z;
  x.z.i = 33;
  x.z.f = 44.44;
  printf("%d %f\n", (*pz).i, (*pz).f);
  x.p->px->p->px->p->px->i = 55;
  x.p->px->f = 10.66;
  printf("%d %f\n", x.i, x.f);
  x.p->px->p->px->p->px->i += 55.99;
  x.p->px->f += 10;
  printf("%d %f\n", x.i, x.f);
  pi = &(x.p->px->p->px->p->px->i);
  pf = &(x.p->px->f);
  printf("%d %f\n", *pi, *pf);
  printf("%d\n", (void *)pi < (void *)pf);
  return 0;
}
