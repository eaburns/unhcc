int printf(char*, ...);
void* malloc(unsigned long);
int i;
int *pi, *qi;
double d;
double *pd;

int main(void)
{
  pi = &i;
  *pi = 19;
  printf("%d %d\n", *pi, i);
  d = 3.14159;
  pd = &d;
  printf("%f %f\n", *pd, d);
  pi = malloc(sizeof(int)*10);
  qi = pi + 5;
  *(qi - 5) = 99;
  *(pi + 5) = 1914;
  printf("%d %d %d %d\n", *pi, *qi, *(qi-5), *(pi+5));
  printf("%d\n", qi - pi);
  return 0;
}
  
  
