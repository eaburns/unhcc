int printf(char*, ...);
int i;
int a[10];
double d[10];
int isum;
double dsum;

int main(void)
{
  for (i = 0; i < 10; i++)
  {
    a[i] = i;
    d[i] = 3.14159 + i;
  }
  dsum = 0;
  isum = 0;
  for (i = 0; i < 10; i++)
  {
    isum += a[i];
    dsum += d[i];
  }
  printf("%d %f\n", isum, dsum);
  return 0;
}

