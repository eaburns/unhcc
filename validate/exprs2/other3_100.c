int printf(char*, ...);

struct y {
  char c[10];
};

struct x {
  double d[10][5];
  struct y ay[3];
} x[2];

int i, j;

double *dp;

int main(void)
{
  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 5; j++)
    {
      x[0].d[i][j] = i * 5 + j;
    }
  }
  for (i = 0; i < 10; i++)
  {
    x[0].ay[1].c[i] = 'a' + i;
    x[0].ay[2].c[i] = 'A' + i;
    x[0].ay[3].c[i] = '0' + i;
  }
  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 5; j++)
    {
      printf("%f\n", x[0].d[i][j]);
    }
  }
  for (i = 0; i < 10; i++)
  {
    printf("%c %c %c\n", x[0].ay[1].c[i], x[0].ay[2].c[i], x[0].ay[3].c[i]);
  }
  x[0].d[3][2] += 4;
  dp = &x[0].d[3][2];
  printf("%f\n", *dp);
  dp++;
  printf("%f\n", *dp);
  return 0;
}
