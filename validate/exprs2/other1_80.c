int printf(char*, ...);
void* malloc(unsigned long);
int i;
int *pi;
double d;
double *pd;

int main(void)
{
  pi = malloc(sizeof(int)*10);
  pd = malloc(sizeof(double)*10);
  for (i = 0; i < 10; i++)
  {
    *pi = i;
    pi++;
    *pd = i + 3.14159;
    pd++;
  }
  for (i = 0; i < 10; i++)
  {
    --pi;
    --pd;
    printf("%d %f\n", *pi, *pd);
  }
  for (i = 0; i < 10; i++)
  {
    *pi = i;
    ++pi;
    *pd = i + 3.14159;
    ++pd;
  }
  for (i = 0; i < 10; i++)
  {
    pi--;
    pd--;
    printf("%d %f\n", *pi, *pd);
  }
  return 0;
}
  

