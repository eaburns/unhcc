int printf(char*, ...);
int i, j;
int a[10][5];
double d[10][5];
int isum;
double dsum;

int main(void)
{
  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 5; j++)
    {
      a[i][j] = i*5 + j;
      d[i][j] = 3.14159 + (i*5 + j);
    }
  }
  dsum = 0;
  isum = 0;
  for (i = 0; i < 10; i++)
  {
    for (j = 0; j < 5; j++)
    {
      isum += a[i][j];
      dsum += d[i][j];
    }
  }
  printf("%d %f\n", isum, dsum);
  return 0;
}

