int printf(char*, ...);

int *pi;
int i;

int main(void)
{
  i = 19;
  pi = &i;
  *pi += 5.0;
  printf("%d\n", *pi);
  *pi *= 2.0;
  printf("%d\n", *pi);
  *pi /= 2.0;
  printf("%d\n", *pi);
  *pi -= 1.0;
  printf("%d\n", *pi);
  *pi %= 25557;
  printf("%d\n", *pi);
  *pi &= 0x3FF0FFF0;
  printf("%d\n", *pi);
  *pi |= 0x300F00FF;
  printf("%d\n", *pi);
  *pi ^= 0x3FF0FFF0;
  printf("%d\n", *pi);
  *pi <<= 5;
  printf("%d\n", *pi);
  *pi >>= 5;
  printf("%d\n", *pi);
  return 0;
}

