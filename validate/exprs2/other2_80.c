int printf(char*, ...);
int i;
int *pi;
double d;
double *pd;
void *v1, *v2;

int main(void)
{
  pi = 0;
  pd = &d;
  v1 = pi;
  v2 = pd;
  printf("%d %d %d\n", !pi, pi && pd, pd || pi);
  printf("%d %d %d\n", !pd, pd && pd, pi || pi);
  printf("%d %d\n", pi && pi, pd || pd);
  printf("%d %d %d\n", !v1, v1 && pd, v2 || pi);
  return 0;
}
  

