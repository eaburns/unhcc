int printf(char*, ...);

struct x {
  int i;
  char c;
  double d;
} x, *p;

int main(void)
{
  p = &x;
  p->i = 19;
  p->c = 'a';
  p->d = 3.14159;
  printf("%d %c %f\n", p->i, p->c, p->d);
  return 0;
}

