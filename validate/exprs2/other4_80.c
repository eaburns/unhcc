int printf(char*, ...);
void* malloc(unsigned long);

double d;
double *pd;

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;

int j;

int main(void)
{
  pd = malloc(sizeof(double)*10);
  for (j = 0; j < 10; j++)
  {
    pd[j] = 3.14159 + j;
  }
  c = 0;
  uc = 1;
  s = 2;
  us = 3;
  i = 4;
  ui = 5;
  l = 6;
  ul = 7;
  printf("%f\n", pd[c]);
  printf("%f\n", pd[s]);
  printf("%f\n", pd[i]);
  printf("%f\n", pd[l]);
  printf("%f\n", pd[uc]);
  printf("%f\n", pd[us]);
  printf("%f\n", pd[ui]);
  printf("%f\n", pd[ul]);
  printf("%f\n", c[pd]);
  printf("%f\n", s[pd]);
  printf("%f\n", i[pd]);
  printf("%f\n", l[pd]);
  printf("%f\n", uc[pd]);
  printf("%f\n", us[pd]);
  printf("%f\n", ui[pd]);
  printf("%f\n", ul[pd]);
  pd = pd + 9;
  printf("%f\n", *(pd - c));
  printf("%f\n", *(pd - s));
  printf("%f\n", *(pd - i));
  printf("%f\n", *(pd - l));
  printf("%f\n", *(pd - uc));
  printf("%f\n", *(pd - us));
  printf("%f\n", *(pd - ui));
  printf("%f\n", *(pd - ul));
  return 0;
}
  

