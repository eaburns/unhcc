int printf(char*, ...);
void* malloc(unsigned long);
int i;
const int ci;
volatile int vi;
const int *pi1;
volatile int *pi2;
int  * volatile pi3;
void *vp;

int main(void)
{
  pi1 = malloc(sizeof(int)*10);
  vp = (void *) (pi1 + 5);
  pi2 = vp;
  pi3 = vp;
  printf("%d %d\n", pi2 - pi1, pi3 - pi1);
  return 0;
}
  

