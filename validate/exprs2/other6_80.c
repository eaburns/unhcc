int printf(char*, ...);
void* malloc(unsigned long);
int i;
int *pi1, *pi2, *pi3;
double *pd1, *pd2, *pd3;
void *vp;

int main(void)
{
  pi1 = malloc(sizeof(int)*10);
  pd1 = malloc(sizeof(double)*10);
  for (i = 0; i < 10; i++)
  {
    pi1[i] = i;
    pd1[i] = 3.14159 + i;
  }
  pi2 = pi1 + 3;
  pd2 = pd1 + 7;
  vp = pi2;
  pi3 = (7 > 5 ? pi1 : pi2);
  pd3 = (5 > 17 ? pd1 : pd2);
  printf("%d %f\n", *pi3, *pd3);
  pi3 = (0 > 0 ? pi1 : vp);
  pd3 = (1 > 0 ? pd2 : 0);
  printf("%d %f\n", *pi3, *pd3);
  return 0;
}
  

