int printf(char*, ...);
int *p;
int (*fp)();
int i;

int main(int argc, char* argv[])
{
  p = &i;
  fp = (int (*)())main;
  i = (int) p;
printf("%x %p\n", i, p);
  i = (int) fp;
  p = (int *) i;
  fp = (int (*)()) i;
printf("%x %p\n", i, p);
  return 0;
}

