int printf(char*, ...);
int i;
int *pi;
float f;
float *pf;
void *v1, *v2;

int main(void)
{
  pf = &f;
  pi = (int *) pf;
  v1 = pf;
  v2 = v1;
  f = 3.14159;
  printf("%x\n", *pi);
  pf = v2;
  printf("%f\n", *pf);
  pf = (float *) pi;
  printf("%f\n", *pf);
  return 0;
}
  

