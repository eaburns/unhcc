int printf(char*, ...);

int f(int i)
{
  return i + 19;
}

int g(int i)
{
  return i * 3;
}

int (*fp1)(int);
int (*fp2)(int);
int (*fp3)(int);

int main(void)
{
  fp1 = &f;
  fp2 = g;
  fp3 = 0;
  printf("%d\n", fp1(10));
  printf("%d\n", (*fp2)(17));
  printf("%d %d %d %d ", !fp3, !fp2, fp1 == fp2, fp1 == fp1);
  printf("%d %d %d %d\n", fp1 != fp2, fp1 != fp2, fp1 && fp3, fp1 || fp3);
  fp3 = 10 > 5 ? fp1 : fp2;
  printf("%d\n", fp3(fp2(fp1(2))));
  return 0;
}
  

