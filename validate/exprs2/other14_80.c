int printf(char*, ...);

double d;
double *dp1, **dp2, ***dp3;

int main(void)
{
  dp1 = &d;
  dp2 = &dp1;
  dp3 = &dp2;
  **dp2 = 19.19;
  printf("%f %f %f %f\n", d, *dp1, **dp2, ***dp3);
  ***dp3 = 20.01;
  printf("%d %d\n", d == *dp1, **dp2 == ***dp3);
  return 0;
}
  

