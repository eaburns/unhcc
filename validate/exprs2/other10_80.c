int printf(char*, ...);
void *malloc(unsigned long);
int i;
int *pi1, *pi2, *pi3;
double *pd1, *pd2, *pd3;

int main(void)
{
  pi1 = malloc(sizeof(int)*10);
  pd1 = malloc(sizeof(double)*10);
  for (i = 0; i < 10; i++)
  {
    pi1[i] = i;
    pd1[i] = 3.14159 + i;
  }
  pd3 = ((pi3 = pi1 + 3), (pd1 + 2));
  printf("%d %f\n", *pi3, *pd3);
  return 0;
}
  

