/*
 *   test1/t5.c
 *
 *   tests processDeclarator
 *
 *   test errors for arrays
 *
 *   uses structs because variables may not be implemented
 *
 */

struct t5 {
  int e1[-1];
  int e2[0];
  int e3[10][];
  void e4[10];
  int  e5[10](void);
  struct t5 e6[10];
 };


