/**
 * Ryan Long
 * CS 712 - Compiler Design
 * This bug appears because the compiler is bugging out when a function 
 * is passed as a paramater to another function.
 * In order to fix this, I modified the makeParamList function in decl.c.
 * I added functionality such that if a function paramater is noticed, it
 * now changes that paramater to a pointer to a function of that type.
**/ 

int printf(char*, ...);

void f1(char *str)
{
   printf("in f1 [%s]\n", str); 
}

void f2(char *str)
{
   printf("in f2 [%s]\n", str); 
}

void f3(void func(char*))
{
   func("hello world");
}

int main(void)
{
   f3(f1);
   f3(f2);
}
