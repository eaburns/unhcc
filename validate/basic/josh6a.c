/*
 *  I think this should compile clean.
 *
 *  It does with cc/Alpha.
 *
 *  gcc issues a warning.
 */
struct x;

int f(struct x);      /* gcc issues warning here */

struct x {int i;};

int main(void)
{
  struct x y;

  f(y);
}
