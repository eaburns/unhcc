int printf(const char *format, ...);

typedef struct x {
  int i;
  float f;
  struct x *next;
} X;

enum pet {dog, cat=1999, horse};

X f(int a, double b[], enum pet c, int d, float e, X x, X *y, int f)
{
  printf("%d %d %d %f %d\n", a, c, d, e, f);
  return x;
}

int main(int argc, char argv[])
{
  X z;
  double b[5];

  z = f(17, b, dog, cat, 10.66, z, 0, horse);
}
