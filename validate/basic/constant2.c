/*
 *  Test constants.
 *
 *  The boundary tests assume 32-bit ints and longs.
 *
 *  gcc misses the underflow case.
 */
int printf(char*, ...);

int main(int argc, char*argv[])
{
  printf("%d\n", 0x1FFFFFFFF);
  printf("%ld\n", 0x1FFFFFFFFl);
  printf("%u\n", 0x1FFFFFFFFu);
  printf("%lu\n", 0x1FFFFFFFFul);
  printf("%d\n", 01234567012345671);
  printf("%lf\n", 123.456789012345e1000);
  printf("%le\n", 123.456789012345e100000L);
  printf("%le\n", 123.456789012345e-100000L);
}
