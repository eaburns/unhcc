/*
 *   test1/t18.c
 *
 *   tests enums: executable test
 *
 */

enum pet { dog, cat, hamster = 1999, emu, pig = 1, chicken};

enum pet f(enum pet in)
{
  return 1066;
}

int printf(const char *format, ...);

int main(int argc, char *argv[])
{
  enum pet x;
  enum pet {snake, bird, lobster} y;
  printf("%d %d %d %d %d %d\n", dog, cat, hamster, emu, pig, chicken);
  x = 17;
  y = x;
  printf("%d %d\n", x, y);
  printf("%d\n", f(bird));
}

