int printf(char*,...);

void f(char c1)
{
  printf("c1 = %c\n", c1);
}

int main(void)
{
  f('a');
}
