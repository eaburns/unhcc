int printf(char*, ...);

struct x {
  int i;
  float f;
};

void f(struct x x)
{
  printf("%d %f\n", x.i, x.f);
}

int main(int argc, char *argv[])
{
  struct x x;

  x.i = 17;
  x.f = 19.19;
  f(x);

  return 0;
}
