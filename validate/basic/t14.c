/*
 *   test1/t14.c
 *
 *   tests processDeclaration on globals with errors
 *
 */

int ok1;

typedef float dup1;

float dup2;

int dup3;

register int e1;

auto int e2;

signed int ok1;

extern float dup1;

int dup2;

const int dup3;

extern int ok2[];

int e3[];  /* we only detect this via a link error */

int ok2[10];

int dup4[10];

int dup4[11];

int dup5(int, int, int);

float dup5(int, int, int);

int dup6(int, int, int);

int dup6(int, float, int);

int ok3(int, int, int);

extern int ok3(int, int, int);

static int *dup7;

int *dup7;

static int *ok4;

extern int *ok4;

extern int *dup8;

static int *dup8;

int ** const * dup9;

int ** volatile * dup9;

int e4[];

int e4[10];

int e4[15];

int e5[10];

int e5[];

int e5[15];
