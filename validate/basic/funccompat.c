/*
 *  This was a bug in Spring 2002. We did not properly handle
 *  compatibility of function types. We said that an oldstyle
 *  function could never be compatible with a prototype.
 *
 *  Now it is okay.
 *
 *  Should compile without error.
 */

typedef void (*user_func)();
typedef struct st_dr {
    int foo;
  } ST_DATA_REC, *ST_DR;
void stdr_dump(ST_DR stdr);
extern void st_establish_data_dump_func(user_func f);
int main(void)
{
        st_establish_data_dump_func(stdr_dump);
        return 0;
}

