/*
 *  This tests assignments of void pointers and qualified pointers.
 *
 *  ANSI 3.3.16.1
 *
 *    1. "both operands are pointers to qualified or unqualified versions
 *       of compatible types, and the type pointed to by the left has all
 *       the qualifiers of the type pointed to by the right"
 *
 *   2. "one operand is a pointer to an object or incomplete type and the
 *      other is a pointer to a qualified or unqualified version of void,
 *      and the type pointed to by the left has all the qualifiers of the
 *      type pointed to by the right"
 *
 */
const void * p1;
void * p2;

const int * * * p3; 
int * * const * p4;
int * const ** p5;
const int * const * const * p6;
 
const int * const * const *p7;
int * const * const *p8;
const int * * const *p9;
const int * const * *p10;

void func(void)
{ 
p1 = p3;
p1 = p4;
p1 = p5;
p1 = p6;

p2 = p3;
p2 = p4;  /* error */
p2 = p5;
p2 = p6;  /* error */

p3 = p1;  /* error */
p4 = p1;
p5 = p1;  /* error */
p6 = p1;

p3 = p2;
p4 = p2;
p5 = p2;
p6 = p2;

p8 = p7;  /* error */
p9 = p7;  /* error */
p10 = p7; /* error */
}

