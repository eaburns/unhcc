int printf(char*, ...);

struct x {
  int i;
  float f;
};

struct x f(void)
{
  struct x x;
  x.i = 17;
  x.f = 19.19;
  return x;
}

int main(int argc, char *argv[])
{
  struct x x;

  x = f();

  printf("%d %f\n", x.i, x.f);

  return 0;
}
