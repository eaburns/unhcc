/*
 *  Test constants.
 *
 *  The boundary tests assume 32-bit ints and longs.
 */
int printf(char*, ...);

int main(int argc, char*argv[])
{
  printf("%d\n", 0xFFFFFFFF);
  printf("%ld\n", 0xFFFFFFFFl);
  printf("%u\n", 0xFFFFFFFFu);
  printf("%lu\n", 0xFFFFFFFFul);
  printf("%d\n", 012345670);
  printf("%f\n", 123.456f);
  printf("%lf\n", 123.456789012345l);
  printf("%e\n", 123.456e-10F);
  printf("%le\n", 123.456789012345E10L);
  printf("%e\n", .12345E+3);
  printf("%Lg\n", 1.18973149535723176508575932662800702e+4932);
}
