/*
 *  I think this should be an error. The struct in the parameter list
 *  is an incomplete type that is never completed since it is referenced
 *  at parameter scope. However, cc/Alpha says nothing. gcc simply issues
 *  warning.
 *
 *  Our compiler will bind the struct to the later (file scope) definition.
 *
 *  I will not include this file in the validation for now.
 */

struct x f(struct x);

struct x {int a;};

int main(void)
{
  struct x a;

  a = f(a);

  return 0;
}
