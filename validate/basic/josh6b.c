/*
 *  We can't properly handle this because we don't detect function
 *  prototype scope. y should be considered to have incomplete type.
 *
 */
int f( struct x { int v; } );

struct x y;  /* incomplete type */

void g(struct x); /* ditto */

void h(void)
{
  f(y); /* structs are not compatible */
}

/*
 *  function declaration and function definition are incompatible
 */
void foo( struct y a );
struct y{ int v; } a;
void foo( struct y a )
{
}

