/*
 *  assign1.c
 *
 *  Test assignment operator on base types. In particular test conversions
 *  between base types. Also, of course, tests DEREF on all base types.
 *
 */

enum {xyz = -11};

int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;
long double ld;

void init(void)
{
  c = 11;
  uc = 22;
  s = 33;
  us = 44;
  i = 55;
  ui = 66;
  l = 77;
  ul = 88;
  f = 10.66f;
  d = 19.14;
  ld = 19.99l;
}

void init2(void)
{
  c = xyz;
  uc = xyz;
  s = xyz;
  us = xyz;
  i = xyz;
  ui = xyz;
  l = xyz;
  ul = xyz;
  f = xyz;
  d = xyz;
  ld = xyz;
}

void ac(void)
{
  c = uc;
  printf("%d ", c);
  c = s;
  printf("%d ", c);
  c = us;
  printf("%d ", c);
  c = i;
  printf("%d ", c);
  c = ui;
  printf("%d ", c);
  c = l;
  printf("%d ", c);
  c = ul;
  printf("%d ", c);
  c = f;
  printf("%d ", c);
  c = d;
  printf("%d ", c);
  c = ld;
  printf("%d ", c);
  printf("\n");
}

void auc(void)
{
  uc = c;
  printf("%u ", uc);
  uc = s;
  printf("%u ", uc);
  uc = us;
  printf("%u ", uc);
  uc = i;
  printf("%u ", uc);
  uc = ui;
  printf("%u ", uc);
  uc = l;
  printf("%u ", uc);
  uc = ul;
  printf("%u ", uc);
  uc = f;
  printf("%u ", uc);
  uc = d;
  printf("%u ", uc);
  uc = ld;
  printf("%u ", uc);
  printf("\n");
}

void as(void)
{
  s = c;
  printf("%d ", s);
  s = uc;
  printf("%d ", s);
  s = us;
  printf("%d ", s);
  s = i;
  printf("%d ", s);
  s = ui;
  printf("%d ", s);
  s = l;
  printf("%d ", s);
  s = ul;
  printf("%d ", s);
  s = f;
  printf("%d ", s);
  s = d;
  printf("%d ", s);
  s = ld;
  printf("%d ", s);
  printf("\n");
}

void aus(void)
{
  us = c;
  printf("%u ", us);
  us = uc;
  printf("%u ", us);
  us = s;
  printf("%u ", us);
  us = i;
  printf("%u ", us);
  us = ui;
  printf("%u ", us);
  us = l;
  printf("%u ", us);
  us = ul;
  printf("%u ", us);
  us = f;
  printf("%u ", us);
  us = d;
  printf("%u ", us);
  us = ld;
  printf("%u ", us);
  printf("\n");
}

void ai(void)
{
  i = c;
  printf("%d ", i);
  i = uc;
  printf("%d ", i);
  i = s;
  printf("%d ", i);
  i = us;
  printf("%d ", i);
  i = ui;
  printf("%d ", i);
  i = l;
  printf("%d ", i);
  i = ul;
  printf("%d ", i);
  i = f;
  printf("%d ", i);
  i = d;
  printf("%d ", i);
  i = ld;
  printf("%d ", i);
  printf("\n");
}

void aui(void)
{
  ui = c;
  printf("%u ", ui);
  ui = uc;
  printf("%u ", ui);
  ui = s;
  printf("%u ", ui);
  ui = us;
  printf("%u ", ui);
  ui = i;
  printf("%u ", ui);
  ui = l;
  printf("%u ", ui);
  ui = ul;
  printf("%u ", ui);
  ui = f;
  printf("%u ", ui);
  ui = d;
  printf("%u ", ui);
  ui = ld;
  printf("%u ", ui);
  printf("\n");
}

void al(void)
{
  l = c;
  printf("%ld ", l);
  l = uc;
  printf("%ld ", l);
  l = s;
  printf("%ld ", l);
  l = us;
  printf("%ld ", l);
  l = i;
  printf("%ld ", l);
  l = ui;
  printf("%ld ", l);
  l = ul;
  printf("%ld ", l);
  l = f;
  printf("%ld ", l);
  l = d;
  printf("%ld ", l);
  l = ld;
  printf("%ld ", l);
  printf("\n");
}

void aul(void)
{
  ul = c;
  printf("%lu ", ul);
  ul = uc;
  printf("%lu ", ul);
  ul = s;
  printf("%lu ", ul);
  ul = us;
  printf("%lu ", ul);
  ul = i;
  printf("%lu ", ul);
  ul = ui;
  printf("%lu ", ul);
  ul = l;
  printf("%lu ", ul);
  ul = f;
  printf("%lu ", ul);
  ul = d;
  printf("%lu ", ul);
  ul = ld;
  printf("%lu ", ul);
  printf("\n");
}

void af(void)
{
  f = c;
  printf("%f ", f);
  f = uc;
  printf("%f ", f);
  f = s;
  printf("%f ", f);
  f = us;
  printf("%f ", f);
  f = i;
  printf("%f ", f);
  f = ui;
  printf("%f ", f);
  f = l;
  printf("%f ", f);
  f = ul;
  printf("%f ", f);
  f = d;
  printf("%f ", f);
  f = ld;
  printf("%f ", f);
  printf("\n");
}

void ad(void)
{
  d = c;
  printf("%f ", d);
  d = uc;
  printf("%f ", d);
  d = s;
  printf("%f ", d);
  d = us;
  printf("%f ", d);
  d = i;
  printf("%f ", d);
  d = ui;
  printf("%f ", d);
  d = l;
  printf("%f ", d);
  d = ul;
  printf("%f ", d);
  d = f;
  printf("%f ", d);
  d = ld;
  printf("%f ", d);
  printf("\n");
}

void ald(void)
{
  ld = c;
  printf("%Lf ", ld);
  ld = uc;
  printf("%Lf ", ld);
  ld = s;
  printf("%Lf ", ld);
  ld = us;
  printf("%Lf ", ld);
  ld = i;
  printf("%Lf ", ld);
  ld = ui;
  printf("%Lf ", ld);
  ld = l;
  printf("%Lf ", ld);
  ld = ul;
  printf("%Lf ", ld);
  ld = f;
  printf("%Lf ", ld);
  ld = d;
  printf("%Lf ", ld);
  printf("\n");
}

int main(void)
{
  /* first test using positive values */
  init();
  ac();
  init();
  auc();
  init();
  as();
  init();
  aus();
  init();
  ai();
  init();
  aui();
  init();
  al();
  init();
  aul();
  init();
  af();
  init();
  ad();
  init();
  ald();

  /* now test using negative values */
  init2();
  ac();
  init2();
  auc();
  init2();
  as();
  init2();
  aus();
  init2();
  ai();
  init2();
  aui();
  init2();
  al();
  init2();
  aul();
  init2();
  af();
  init2();
  ad();
  init2();
  ald();

  return 0;
}

