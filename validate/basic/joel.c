/* This should generate a duplicate declaration error for 3rd decl. of 'x' */
extern int i;
void f(void)
{
  extern int i;
  float i;
}
