/*
 *   test1/t17.c
 *
 *   tests processDeclaration: executable test that uses globals and locals
 *
 */

int g1;

int printf(const char *format, ...);

void f5(int a)
{
  static l1;
  printf("f5: %d\n", l1);
  l1  = a;
}

int f4(int a)
{
  printf("f4: %d\n", a);
  return g1;
}

int f3(int a, int b, int c)
{
  printf("f3: %d %d %d\n", a, b, c);
  return f4(c);
}

int f2(int a, int b, int c)
{
  printf("f2: %d %d %d\n", a, b, c);
  return f3(c, b, a);
}

int f1(int a, int b, int c)
{
  printf("f1: %d %d %d\n", a, b, c);
  return f2(c, b, a);
}

int main(int argc, char *argv[])
{
  int l1;

  l1 = argc;

  f5(l1);

  {
    int l2;
    l2 = 19;

    f5(l2);

    {
      int l3;
      l3 = 29;
      f5(l3);

      l3 = f1(l1, l2, l3);

      f5(l3);
    }
  }
}

