/*
 *  test proper handling of struct/union tags
 *
 */

struct {int a; float b;} a;

struct x b;

struct x {int a; float b;} c;

struct x d;

void func1(void)
{
  a = b;  /* error */
  a = c;  /* error */
  a = d;  /* error */

  b = a;  /* error */
  b = c;
  b = d;

  c = a;  /* error */
  c = b;
  c = d;

  d = a;  /* error */
  d = b;
  d = c;
}

struct x *e;

void func2(void)
{
  struct x;

  struct x *p;

  struct x q;  /* error */

  struct x {int a; float b;} r;

  struct x s;

  struct x *t;

  e = p;  /* error */
  p = e;  /* error */

  s = c;  /* error */
  c = s;  /* error */

  p = t;
  t = p;

  r = s;
  s = r;
}

union {int a; float b;} a2;

union x2 b2;

union x2 {int a; float b;} c2;

union x2 d2;

void func3(void)
{
  a2 = b2;  /* error */
  a2 = c2;  /* error */
  a2 = d2;  /* error */

  b2 = a2;  /* error */
  b2 = c2;
  b2 = d2;

  c2 = a2;  /* error */
  c2 = b2;
  c2 = d2;

  d2 = a2;  /* error */
  d2 = b2;
  d2 = c2;
}

union x2 *e2;

void func4(void)
{
  union x2;

  union x2 *p2;

  union x2 q2;  /* error */

  union x2 {int a; float b;} r2;

  union x2 s2;

  union x2 *t2;

  e2 = p2;  /* error */
  p2 = e2;  /* error */

  s2 = c2;  /* error */
  c2 = s2;  /* error */

  p2 = t2;
  t2 = p2;

  r2 = s2;
  s2 = r2;
}

