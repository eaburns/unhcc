/*
 *   test1/t9.c
 *
 *   tests processDeclarator
 *
 *   tests for errors for functions (using pointer to function)
 *
 *   uses structs because variables may not be implemented
 *
 */

struct t9 {
  int (*e1)(int i)(int i);
  int (*e2)(int i)[10];
  int (*e3)(void v);       /* detected actually in decl.c */
  int (*e4)(int, void);    /* ditto */
  int (*e5)(void, int);    /* but this must be detected in processDeclarator */
 };


