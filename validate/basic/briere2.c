/*
 *  gcc says this is a syntax error.
 *
 *  I think actually it violates constraints about how typedefs can
 *  be used to build function types.
 */

typedef int F(void);
F f;

int main( void )
{
 return 0;
}

F f { }

