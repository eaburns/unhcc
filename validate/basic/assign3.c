/*
 *  test assign errors
 *
 */

struct x { int a; float b;} sx;

int i;

long double ld;

void *pv;
void *pv2;

float ***pppf;
float ***pppf2;

int *pi;
int *pi2;

int func(int, float);
int (*pfunc)(int, float);
int (*pfunc2)(int, double);

const int ci;
const volatile cvi;
volatile vi;

int * const cp;
int * volatile vp;
int * const volatile cvp;

int * const *cp2;
int * volatile *vp2;
int * const volatile *cvp2;

void foo(void)
{
  i = sx;    /* error */
  sx = i;    /* error */
  ld = sx;    /* error */
  sx = ld;    /* error */
  sx = pi;    /* error */
  pi = sx;    /* error */

  pv = pv2;
  pv = pppf;
  pppf = pv;
  pppf = pppf2;
  pppf = pi;    /* error */
  pi = pppf;    /* error */
  pv = pi;
  pi = pv;

  pi = pfunc;    /* error */
  pfunc = pfunc2;    /* error */
  pfunc = func;
  func = pfunc;    /* error */

  pi = 0;
  pi = 1;    /* error */

  ci = 19;    /* error */
  cvi = 20;    /* error */
  vi = 21;

  cp = 0;    /* error */
  vp = 0;
  cvp = 0;    /* error */

  cp2 = 0;
  vp2 = 0;
  cvp2 = 0;

  cp = cp;    /* error */
  cp = cvp;    /* error */
  cp = vp;    /* error */

  cvp = cp;    /* error */
  cvp = cvp;    /* error */
  cvp = vp;    /* error */

  vp = cp;
  vp = cvp;
  vp = vp;

  cp2 = cp2;
  cp2 = cvp2;    /* error */
  cp2 = vp2;    /* error */

  cvp2 = cp2;
  cvp2 = cvp2;
  cvp2 = vp2;

  vp2 = cp2;    /* error */
  vp2 = cvp2;    /* error */
  vp2 = vp2;

  cp = cp2;    /* error */
  cvp = cvp2;    /* error */
  vp = vp2;    /* error */

  (i = 9) = 19;  /* error */
  1066 = i;  /* error */

}
