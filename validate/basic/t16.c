/*
 *   test1/t16.c
 *
 *   tests processDeclaration on locals with errors
 *
 */

int ok1;

int main(int argc, char *argv[])
{
  int e1[];

  extern int ok2[];

  auto int e2[];

  register int e3[];

  static int e4[];

  struct x e5;

  int e6;

  double ok1;

  int e6;

}

