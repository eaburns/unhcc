/*
 *  assign2.c
 *
 *  Test assignment operator on structs, unions, pointers, arrays and
 *  functions.
 *
 *  Only rudimentary testing because I don't have any operators beside
 *  assignment and function call.
 *
 */

int printf(char*, ...);

int *pi1, *pi2;

int a[10];

int (*pf)(void);

int func(void)
{
  return 1999;
}

struct x {
  int i;
  char c;
  double d;
} sx1, sx2;

typedef struct x X;

X *psx1;

struct x *psx2;

union y {
  int i;
  char c;
  double d;
} su1, su2;

typedef union y Y;

Y *puy1;

union y *puy2;

int main(void)
{
  pi1 = 0;
  pi2 = pi1;
  pf = 0;

  /*
   *  Note: ANSI says %p expects "pointer to void" (7.9.6.2) so
   *  using it with "pointer to function" is suspect.
   */
  printf("should be all nil: %p %p %p\n", pi1, pi2, pf);

  pi1 = a;
  printf("should be the same: %p %p\n", pi1, a);

  pf = func;
  printf("should be both 1999: %d %d\n", func(), pf());

  psx2 = 0;
  psx1 = psx2;

  printf("should be both nil: %p %p\n", psx1, psx2);

  sx1 = sx2;

  puy2 = 0;
  puy1 = puy2;

  printf("should be both nil: %p %p\n", puy1, puy2);

  su1 = su2;

  return 0;
}

