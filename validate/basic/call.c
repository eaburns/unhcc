/*
 *   test function call
 *
 */

int printf(char*, ...);

char c;
short s;
int i;
long l;
char uc;
short us;
int ui;
long ul;

float f;
double d;
long double ld;

struct x {int a; float b;} sx;
union y {int a; float b;} uy;

void init(void)
{
  c = 0xEF;
  s = 0xFFEE;
  i = 0xFFFFFFED;
  l = 0xFFFFFFEC;
  uc = 19;
  us = 20;
  ui = 21;
  ul = 22;

  f = 10.66;
  d = 19.99;
  ld = 17.76;
}

float ffunc(void)
{
  return f;
}

double dfunc(void)
{
  return d;
}

long double ldfunc(void)
{
  return ld;
}

int main(int argc, char *argv[])
{
  init();
  printf("%d\n",
    printf("%d %d %d %ld %u %u %u %lu %f %f %Lf %s\n", c, s, i, l, uc, us, ui,
      ul, f, d, ld, "blah blah blah"));
  printf("%f %f %Lf\n", ffunc(), dfunc(), ldfunc());
  return 0;
}
