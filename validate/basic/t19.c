/*
 *   test1/t19.c
 *
 *   tests enums: executable test
 *
 */

int printf(const char *format, ...);

enum x {a= 19, b, c};

int main(int argc, char *argv[])
{
  enum y {a = a + 1, b = a, c};

  printf("%d %d %d\n", a, b, c);
}

