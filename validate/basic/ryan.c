int printf(char*, ...);

int main(void)
{
  char c1;
  char c2;

  c1 = 'a';
  c2 = 'b';

  printf("%c%c\n", c1, c2);
}
