/* This bug appears when a declaration like the following is made */
/* Ryan E Long - CS 712 Compiler Design */
/* Friday, May 09th */

typedef struct ExpTree {
   int dval;
} ExpTree;

int main(void)
{
   int newtree
   newTree = 0;
   ExpTree oldtree; /* bug occurs here */
}

/*
      bug1.c, line 13: [bug] unknown ST_DR tag in newVar()

   This is occurring in tree.c in the function newVar. The
   ST_DR tag is a TDEF tag, since ExpTree is a TDEF. The
   switch is not setup to handle TDEFs, it bugs out when
   they are recieved.

   The newVar function function is called in parse.y at the
   primary_expr tag.

   In order to fix this bug, I added the TDEF case to the
   switch. I set the type of the variable to the type of the
   TDEF.

      bug1.c, line 12: [error] parse error (column 10)

*/

