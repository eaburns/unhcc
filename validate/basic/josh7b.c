/*
 *  gcc misses all of these errors?
 *    misses first two completely
 *    issues bogus(?) warning for third
 *
 */
static int var;	
void f(void)
{
  extern int var;
  static int var;   /* duplicate */
}

static int var2;
void f2(void)
{
  static int var2;
  extern int var2;   /* duplicate */
}

void f3( void )
{
  extern int v;
  static int v;   /* duplicate */
}
