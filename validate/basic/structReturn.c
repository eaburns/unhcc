int printf(char*, ...);

struct x {
  int i;
  float f;
};

struct x f(int i, float f)
{
  struct x x;
  x.i = i;
  x.f = f;
  return x;
}

int main(int argc, char *argv[])
{
  struct x x;

  x = f(17, 19.19);

  printf("%d %f\n", x.i, x.f);

  return 0;
}
