static int foo(void)
{
  return 19;
}

void goo(void);

int printf(char*, ...);

int main(int argc, char* argv[])
{
  goo();
  printf("foo 1 returns %d\n", foo());
  return 0;
}
