int printf(char*, ...);

float f;
double d;
long double ld;

int main(int argc, char *argv[])
{
  f = 10.0;
  d = 0.0;
  ld = -1.0;

  printf("%d\n", f && d);
  printf("%d\n",f || d);
  printf("%d\n",f && ld);
  printf("%d\n",f || ld);

  f = 0.0;
  d = 1.0;
  ld = 0.0;

  printf("%d\n",f && d);
  printf("%d\n",f || d);
  printf("%d\n",f && ld);
  printf("%d\n",f || ld);
}
