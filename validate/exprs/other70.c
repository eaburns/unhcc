
int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;

void init(void)
{
  c = -17;
  uc = 17;
  s = -29;
  us = 29;
  i = -37;
  ui = 37;
  l = -39;
  ul = 39;
  f = -49;
  d = 59;
}

int main(void)
{
 init();

 printf("%x %x %x %x %x\n", +c, +uc, +s, +us, +i);
 printf("%x %lx %lx %e %e\n\n", +ui, +l, +ul, +f, +d);

 printf("%x %x %x %x %x\n", -c, -uc, -s, -us, -i);
 printf("%x %lx %lx %e %e\n\n", -ui, -l, -ul, -f, -d);

 c = 66;
 printf("%x %x %x %x %x\n", c % c, c % uc, c % s, c % us, c % i);
 printf("%x %lx %lx\n\n", c % ui, c % l, c % ul);

 uc = 166;
 printf("%x %x %x %x %x\n", uc % c, uc % uc, uc % s, uc % us, uc % i);
 printf("%x %lx %lx\n\n", uc % ui, uc % l, uc % ul);

 s = 1066;
 printf("%x %x %x %x %x\n", s % c, s % uc, s % s, s % us, s % i);
 printf("%x %lx %lx\n\n", s % ui, s % l, s % ul);

 us = 1066;
 printf("%x %x %x %x %x\n", us % c, us % uc, us % s, us % us, us % i);
 printf("%x %lx %lx\n\n", us % ui, us % l, us % ul);

 i = 1066;
 printf("%x %x %x %x %x\n", i % c, i % uc, i % s, i % us, i % i);
 printf("%x %lx %lx\n\n", i % ui, i % l, i % ul);

 ui = 1066;
 printf("%x %x %x %x %x\n", ui % c, ui % uc, ui % s, ui % us, ui % i);
 printf("%x %lx %lx\n\n", ui % ui, ui % l, ui % ul);

 l = 1066;
 printf("%lx %lx %lx %lx %lx\n", l % c, l % uc, l % s, l % us, l % i);
 printf("%lx %lx %lx\n\n", l % ui, l % l, l % ul);

 ul = 1066;
 printf("%lx %lx %lx %lx %lx\n", ul % c, ul % uc, ul % s, ul % us, ul % i);
 printf("%lx %lx %lx\n\n", ul % ul, ul % l, ul % ul);

 return 0;
}
