
int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;

void init(void)
{
  c = 17;
  uc = 135;
  s = 29;
  us = 35529;
  i = 37;
  ui = 0x80000000u;
  l = 39;
  ul = 0x80000000ul;
  f = 49;
  d = 59;
}

int fail(void)
{
  printf("fail executing\n");
  return 0;
}

int succeed(void)
{
  printf("succeed executing\n");
  return 1;
}

int main(void)
{
 init();

 printf("%d %d %d %d %d\n", !c, !uc, !s, !us, !i);
 printf("%d %d %d %d %d\n\n", !ui, !l, !ul, !f, !d);

 printf("%d %d %d %d\n\n", fail() && fail(), fail() && succeed(),
   succeed() && fail(), succeed() && succeed());

 printf("%d %d %d %d\n\n", fail() || fail(), fail() || succeed(),
   succeed() || fail(), succeed() || succeed());

 return 0;
}
