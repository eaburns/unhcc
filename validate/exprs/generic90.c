
int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;

void init(void)
{
  c = 3;
  uc = 3;
  s = 5;
  us = 5;
  i = 7;
  ui = 7;
  l = 0;
  ul = 1;
}

int main(void)
{
 init();

 printf("%x %x %x %x %x\n", c OP c, c OP uc, c OP s, c OP us, c OP i);
 printf("%x %lx %lx\n\n", c OP ui, c OP l, c OP ul);

 printf("%x %x %x %x %x\n", uc OP c, uc OP uc, uc OP s, uc OP us, uc OP i);
 printf("%x %lx %lx\n\n", uc OP ui, uc OP l, uc OP ul);

 printf("%x %x %x %x %x\n", s OP c, s OP uc, s OP s, s OP us, s OP i);
 printf("%x %lx %lx\n\n", s OP ui, s OP l, s OP ul);

 printf("%x %x %x %x %x\n", us OP c, us OP uc, us OP s, us OP us, us OP i);
 printf("%x %lx %lx\n\n", us OP ui, us OP l, us OP ul);

 printf("%x %x %x %x %x\n", i OP c, i OP uc, i OP s, i OP us, i OP i);
 printf("%x %lx %lx\n\n", i OP ui, i OP l, i OP ul);

 printf("%x %x %x %x %x\n", ui OP c, ui OP uc, ui OP s, ui OP us, ui OP i);
 printf("%x %lx %lx\n\n", ui OP ui, ui OP l, ui OP ul);

 printf("%lx %lx %lx %lx %lx\n", l OP c, l OP uc, l OP s, l OP us, l OP i);
 printf("%lx %lx %lx\n\n", l OP ui, l OP l, l OP ul);

 printf("%lx %lx %lx %lx %lx\n", ul OP c, ul OP uc, ul OP s, ul OP us, ul OP i);
 printf("%lx %lx %lx\n\n", ul OP ui, ul OP l, ul OP ul);

 return 0;
}
