

int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;
long double ld;

void init(void)
{
  c = 17;
  uc = 135;
  s = 29;
  us = 35529;
  i = 37;
  ui = 0x80000000u;
  l = 39;
  ul = 0x80000000lu;
  f = 49;
  d = 59;
  ld = 69;
}

int main(void)
{
 init();

 printf("%d %d %d ", c OP c, c OP uc, c OP s);
 printf("%d %d\n", c OP us, c OP i);
 printf("%d %d %d ", c OP ui, c OP l, c OP ul);
 printf("%d %d %d\n\n", c OP f, c OP d, c OP ld);

 printf("%d %d %d ", uc OP c, uc OP uc, uc OP s);
 printf("%d %d\n", uc OP us, uc OP i);
 printf("%d %d %d ", uc OP ui, uc OP l, uc OP ul);
 printf("%d %d %d\n\n", uc OP f, uc OP d, uc OP ld);

 printf("%d %d %d ", s OP c, s OP uc, s OP s);
 printf("%d %d\n", s OP us, s OP i);
 printf("%d %d %d ", s OP ui, s OP l, s OP ul);
 printf("%d %d %d\n\n", s OP f, s OP d, s OP ld);

 printf("%d %d %d ", us OP c, us OP uc, us OP s);
 printf("%d %d\n", us OP us, us OP i);
 printf("%d %d %d ", us OP ui, us OP l, us OP ul);
 printf("%d %d %d\n\n", us OP f, us OP d, us OP ld);

 printf("%d %d %d ", i OP c, i OP uc, i OP s);
 printf("%d %d\n", i OP us, i OP i);
 printf("%d %d %d ", i OP ui, i OP l, i OP ul);
 printf("%d %d %d\n\n", i OP f, i OP d, i OP ld);

 printf("%d %d %d ", ui OP c, ui OP uc, ui OP s);
 printf("%d %d\n", ui OP us, ui OP i);
 printf("%d %d %d ", ui OP ui, ui OP l, ui OP ul);
 printf("%d %d %d\n\n", ui OP f, ui OP d, ui OP ld);

 printf("%d %d %d ", l OP c, l OP uc, l OP s);
 printf("%d %d\n", l OP us, l OP i);
 printf("%d %d %d ", l OP ui, l OP l, l OP ul);
 printf("%d %d %d\n\n", l OP f, l OP d, l OP ld);

 printf("%d %d %d ", ul OP c, ul OP uc, ul OP s);
 printf("%d %d\n", ul OP us, ul OP i);
 printf("%d %d %d ", ul OP ui, ul OP l, ul OP ul);
 printf("%d %d %d\n\n", ul OP f, ul OP d, ul OP ld);

 printf("%d %d %d ", f OP c, f OP uc, f OP s);
 printf("%d %d\n", f OP us, f OP i);
 printf("%d %d %d ", f OP ui, f OP l, f OP ul);
 printf("%d %d %d\n\n", f OP f, f OP d, f OP ld);

 printf("%d %d %d ", d OP c, d OP uc, d OP s);
 printf("%d %d\n", d OP us, d OP i);
 printf("%d %d %d ", d OP ui, d OP l, d OP ul);
 printf("%d %d %d\n\n", d OP f, d OP d, d OP ld);

 printf("%d %d %d ", ld OP c, ld OP uc, ld OP s);
 printf("%d %d\n", ld OP us, ld OP i);
 printf("%d %d %d ", ld OP ui, ld OP l, ld OP ul);
 printf("%d %d %d\n\n", ld OP f, ld OP d, ld OP ld);

 return 0;
}
