int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;

int main(void)
{
 uc = 0x80;
 c = 0x80;
 printf("%x %x\n\n", uc >> 2, c >> 2);

 us = 0x8000;
 s = 0x8000;
 printf("%x %x\n\n", us >> 2, s >> 2);

 ui = 0x80000000u;
 i = 0x80000000u;
 printf("%x %x\n\n", ui >> 2, i >> 2);

 ul = 0x80000000ul;
 l = 0x80000000ul;
 printf("%lx %lx\n\n", ul >> 2, l >> 2);

 return 0;
}
