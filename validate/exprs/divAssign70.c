int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;
long double ld;

signed char c2;
unsigned char uc2;
signed short s2;
unsigned short us2;
signed int i2;
unsigned int ui2;
signed long l2;
unsigned long ul2;

void init(void)
{
  c = -17;
  uc = 17;
  s = -29;
  us = 29;
  i = -37;
  ui = 37;
  l = -39;
  ul = 39;
  f = -49;
  d = 59;
  ld = -69;
}

void init2(void)
{
  c2 = 2;
  uc2 = 2;
  s2 = 2;
  us2 = 2;
  i2 = 2;
  ui2 = 2;
  l2 = 2;
  ul2 = 2;
}

int main(void)
{
 init();

 c /= uc;
 c /= s;
 c /= us;
 c /= i;
 c /= ui;
 c /= l;
 c /= ul;
 c /= f;
 c /= d;
 c /= ld;
 printf("%d\n\n", c);

 init();

 uc /= c;
 uc /= s;
 uc /= us;
 uc /= i;
 uc /= ui;
 uc /= l;
 uc /= ul;
 uc /= f;
 uc /= d;
 uc /= ld;
 printf("%x\n\n", uc);

 init();

 s /= c;
 s /= uc;
 s /= us;
 s /= i;
 s /= ui;
 s /= l;
 s /= ul;
 s /= f;
 s /= d;
 s /= ld;
 printf("%d\n\n", s);

 init();

 us /= c;
 us /= uc;
 us /= s;
 us /= i;
 us /= ui;
 us /= l;
 us /= ul;
 us /= f;
 us /= d;
 us /= ld;
 printf("%x\n\n", us);

 init();

 i /= c;
 i /= uc;
 i /= s;
 i /= us;
 i /= ui;
 i /= l;
 i /= ul;
 i /= f;
 i /= d;
 i /= ld;
 printf("%d\n\n", i);

 init();

 ui /= c;
 ui /= uc;
 ui /= s;
 ui /= us;
 ui /= i;
 ui /= l;
 ui /= ul;
 ui /= f;
 ui /= d;
 ui /= ld;
 printf("%x\n\n", ui);

 init();

 l /= c;
 l /= uc;
 l /= s;
 l /= us;
 l /= i;
 l /= ui;
 l /= ul;
 l /= f;
 l /= d;
 l /= ld;
 printf("%ld\n\n", l);

 init();

 ul /= c;
 ul /= uc;
 ul /= s;
 ul /= us;
 ul /= i;
 ul /= ui;
 ul /= l;
 ul /= f;
 ul /= d;
 ul /= ld;
 printf("%lx\n\n", ul);

 init();

 f /= c;
 f /= uc;
 f /= s;
 f /= us;
 f /= i;
 f /= ui;
 f /= l;
 f /= ul;
 f /= f;
 f /= d;
 f /= ld;
 printf("%e\n\n", f);

 init();

 d /= c;
 d /= uc;
 d /= s;
 d /= us;
 d /= i;
 d /= ui;
 d /= l;
 d /= ul;
 d /= f;
 d /= d;
 d /= ld;
 printf("%e\n\n", d);

 init();

 ld /= c;
 ld /= uc;
 ld /= s;
 ld /= us;
 ld /= i;
 ld /= ui;
 ld /= l;
 ld /= ul;
 ld /= f;
 ld /= d;
 ld /= ld;
 printf("%Le\n\n", ld);

 init();
 init2();

 c /= c2;
 uc /= uc2;
 s /= s2;
 us /= us2;
 i /= i2;
 ui /= ui2;
 l /= l2;
 ul /= ul2;
 printf("%d %x %d %x\n\n", c, uc, s, us);
 printf("%d %x %ld %lx\n\n", i, ui, l, ul);

 return 0;
}
