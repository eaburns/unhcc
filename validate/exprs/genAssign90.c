
int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;

signed char c2;
unsigned char uc2;
signed short s2;
unsigned short us2;
signed int i2;
unsigned int ui2;
signed long l2;
unsigned long ul2;

void init(void)
{
  c = 7;
  uc = 1;
  s = 2;
  us = 2;
  i = 0;
  ui = 3;
  l = 3;
  ul = 3;
}

void init2(void)
{
  c2 = 2;
  uc2 = 2;
  s2 = 2;
  us2 = 2;
  i2 = 2;
  ui2 = 2;
  l2 = 2;
  ul2 = 2;
}

int main(void)
{
 init();

 c OP uc;
 c OP s;
 c OP us;
 c OP i;
 c OP ui;
 c OP l;
 c OP ul;
 printf("%d\n\n", c);

 init();

 uc OP c;
 uc OP s;
 uc OP us;
 uc OP i;
 uc OP ui;
 uc OP l;
 uc OP ul;
 printf("%x\n\n", uc);

 init();

 s OP c;
 s OP uc;
 s OP us;
 s OP i;
 s OP ui;
 s OP l;
 s OP ul;
 printf("%d\n\n", s);

 init();

 us OP c;
 us OP uc;
 us OP s;
 us OP i;
 us OP ui;
 us OP l;
 us OP ul;
 printf("%x\n\n", us);

 init();

 i OP c;
 i OP uc;
 i OP s;
 i OP us;
 i OP ui;
 i OP l;
 i OP ul;
 printf("%d\n\n", i);

 init();

 ui OP c;
 ui OP uc;
 ui OP s;
 ui OP us;
 ui OP i;
 ui OP l;
 ui OP ul;
 printf("%x\n\n", ui);

 init();

 l OP c;
 l OP uc;
 l OP s;
 l OP us;
 l OP i;
 l OP ui;
 l OP ul;
 printf("%ld\n\n", l);

 init();

 ul OP c;
 ul OP uc;
 ul OP s;
 ul OP us;
 ul OP i;
 ul OP ui;
 ul OP l;
 printf("%lx\n\n", ul);

 init();
 init2();

 c OP c2;
 uc OP uc2;
 s OP s2;
 us OP us2;
 i OP i2;
 ui OP ui2;
 l OP l2;
 ul OP ul2;
 printf("%d %x %d %x\n\n", c, uc, s, us);
 printf("%d %x %ld %lx\n\n", i, ui, l, ul);

 return 0;
}
