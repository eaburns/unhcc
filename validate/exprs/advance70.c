int printf(char*, ...);

int i;
float f;
double d;

int main(void)
{
  i = 17;
  f = 19.18;
  d = 13;

  printf("%d ", i += 19);
  printf("%e ", f += f);
  printf("%e ", d += 10.66);
  printf("%e ", f += d);
  printf("%d\n\n", i += d);

  printf("%d ", i *= 19);
  printf("%e ", f *= f);
  printf("%e ", d *= 10.66);
  printf("%e ", f *= d);
  printf("%d\n\n", i *= d);

  printf("%d ", i -= 19);
  printf("%e ", f -= f);
  printf("%e ", d -= 10.66);
  printf("%e ", f -= d);
  printf("%d\n\n", i -= d);

  printf("%d\n", i %= 19);

  i = 198;
  d = 1.23;

  printf("%d ", i /= 19);
  printf("%d ", i /= d);
  printf("%e ", f /= f);
  printf("%e ", d /= 10.66);
  printf("%e\n", f /= d);

  printf("%d ", i);
  printf("%e ", f);
  printf("%e\n", d);

  return 0;
}
