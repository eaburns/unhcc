int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;

void init(void)
{
  c = 3;
  uc = 3;
  s = 5;
  us = 5;
  i = 7;
  ui = 7;
  l = 0;
  ul = 1;
}

int main(void)
{
 init();

 printf("%x %x %x %x %x\n", c ^ c, c ^ uc, c ^ s, c ^ us, c ^ i);
 printf("%x %lx %lx\n\n", c ^ ui, c ^ l, c ^ ul);

 printf("%x %x %x %x %x\n", uc ^ c, uc ^ uc, uc ^ s, uc ^ us, uc ^ i);
 printf("%x %lx %lx\n\n", uc ^ ui, uc ^ l, uc ^ ul);

 printf("%x %x %x %x %x\n", s ^ c, s ^ uc, s ^ s, s ^ us, s ^ i);
 printf("%x %lx %lx\n\n", s ^ ui, s ^ l, s ^ ul);

 printf("%x %x %x %x %x\n", us ^ c, us ^ uc, us ^ s, us ^ us, us ^ i);
 printf("%x %lx %lx\n\n", us ^ ui, us ^ l, us ^ ul);

 printf("%x %x %x %x %x\n", i ^ c, i ^ uc, i ^ s, i ^ us, i ^ i);
 printf("%x %lx %lx\n\n", i ^ ui, i ^ l, i ^ ul);

 printf("%x %x %x %x %x\n", ui ^ c, ui ^ uc, ui ^ s, ui ^ us, ui ^ i);
 printf("%x %lx %lx\n\n", ui ^ ui, ui ^ l, ui ^ ul);

 printf("%lx %lx %lx %lx %lx\n", l ^ c, l ^ uc, l ^ s, l ^ us, l ^ i);
 printf("%lx %lx %lx\n\n", l ^ ui, l ^ l, l ^ ul);

 printf("%lx %lx %lx %lx %lx\n", ul ^ c, ul ^ uc, ul ^ s, ul ^ us, ul ^ i);
 printf("%lx %lx %lx\n\n", ul ^ ui, ul ^ l, ul ^ ul);

 return 0;
}
