int printf(char*, ...);

int i;
float f;
double d;

int main(void)
{
  i = 177;
  f = 19.18;
  d = 13;

  printf("%d %d\n\n", i > 17, i <= 17);
  printf("%d %d\n\n", f < 17.0f, f >= 17.0f);
  printf("%d %d\n\n", d == 17.0, d != 17);
  printf("%d %d %d\n", i && 1, 1 || i, !i);

  return 0;
}
