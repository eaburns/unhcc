
int printf(char*, ...);

int i;
float f;
double d;

int main(void)
{
  i = 17;
  f = 19.18;
  d = 13;

  printf("%d ", i++);
  printf("%d ", ++i);
  printf("%d\n\n", i);

  printf("%d ", i--);
  printf("%d ", --i);
  printf("%d\n\n", i);

  printf("%e ", f++);
  printf("%e ", ++f);
  printf("%e\n\n", f);

  printf("%e ", f--);
  printf("%e ", --f);
  printf("%e\n\n", f);

  printf("%e ", d++);
  printf("%e ", ++d);
  printf("%e\n\n", d);

  printf("%e ", d--);
  printf("%e ", --d);
  printf("%e\n\n", d);

  printf("%d ", 0 ? i : i + 1);
  printf("%e ", 1 ? d : d + 1);
  printf("%e\n\n", 1 ? f : f + 1);

  printf("%e %d\n\n", (double) i, (int) d);

  printf("%d\n\n", sizeof(i*f+1));

  printf("%e\n\n", (i, i++, i++, d + i));

  return 0;
}
