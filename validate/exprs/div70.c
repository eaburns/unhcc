int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;
long double ld;

void init(void)
{
  c = -17;
  uc = 17;
  s = -29;
  us = 29;
  i = -37;
  ui = 37;
  l = -39;
  ul = 39;
  f = -49;
  d = 59;
  ld = -69;
}

int main(void)
{
 init();

 printf("%x %x %x %x %x\n", c / c, c / uc, c / s, c / us, c / i);
 printf("%x %lx %lx %e %e %Le\n\n", c / ui, c / l, c / ul, c / f, c / d,
   c / ld);

 printf("%x %x %x %x %x\n", uc / c, uc / uc, uc / s, uc / us, uc / i);
 printf("%x %lx %lx %e %e %Le\n\n", uc / ui, uc / l, uc / ul, uc / f,
   uc / d, uc / ld);

 printf("%x %x %x %x %x\n", s / c, s / uc, s / s, s / us, s / i);
 printf("%x %lx %lx %e %e %Le\n\n", s / ui, s / l, s / ul, s / f, s / d,
   s / ld);

 printf("%x %x %x %x %x\n", us / c, us / uc, us / s, us / us, us / i);
 printf("%x %lx %lx %e %e %Le\n\n", us / ui, us / l, us / ul, us / f,
   us / d, us / ld);

 printf("%x %x %x %x %x\n", i / c, i / uc, i / s, i / us, i / i);
 printf("%x %lx %lx %e %e %Le\n\n", i / ui, i / l, i / ul, i / f, i / d,
   i / ld);

 printf("%x %x %x %x %x\n", ui / c, ui / uc, ui / s, ui / us, ui / i);
 printf("%x %lx %lx %e %e %Le\n\n", ui / ui, ui / l, ui / ul, ui / f,
   ui / d, ui / ld);

 printf("%lx %lx %lx %lx %lx\n", l / c, l / uc, l / s, l / us, l / i);
 printf("%lx %lx %lx %e %e %Le\n\n", l / ui, l / l, l / ul, l / f, l / d,
   l / ld);

 printf("%lx %lx %lx %lx %lx\n", ul / c, ul / uc, ul / s, ul / us, ul / i);
 printf("%lx %lx %lx %e %e %Le\n\n", ul / ui, ul / l, ul / ul, ul / f,
   ul / d, ul / ld);

 printf("%e %e %e %e %e\n", f / c, f / uc, f / s, f / us, f / i);
 printf("%e %e %e %e %e %Le\n\n", f / ui, f / l, f / ul, f / f, f / d,
   f / ld);

 printf("%e %e %e %e %e\n", d / c, d / uc, d / s, d / us, d / i);
 printf("%e %e %e %e %e %Le\n\n", d / ui, d / l, d / ul, d / f, d / d,
   d / ld);

 printf("%Le %Le %Le %Le %Le\n", ld / c, ld / uc, ld / s, ld / us, ld / i);
 printf("%Le %Le %Le %Le %Le %Le\n\n", ld / ui, ld / l, ld / ul, ld / f,
   ld / d, ld / ld);

 return 0;
}
