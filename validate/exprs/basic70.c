int printf(char*, ...);

int i;
float f;
double d;

int main(void)
{
  i = 17;
  f = 19.18;
  d = 13;

  printf("%d ", i + 19);
  printf("%e ", f + f);
  printf("%e ", d + 10.66);
  printf("%e ", f + d);
  printf("%e\n\n", i + d);

  printf("%d ", i * 19);
  printf("%e ", f * f);
  printf("%e ", d * 10.66);
  printf("%e ", f * d);
  printf("%e\n\n", i * d);

  printf("%d ", i / 19);
  printf("%e ", f / f);
  printf("%e ", d / 10.66);
  printf("%e ", f / d);
  printf("%e\n\n", i / d);

  printf("%d ", i - 19);
  printf("%e ", f - f);
  printf("%e ", d - 10.66);
  printf("%e ", f - d);
  printf("%e\n\n", i - d);

  printf("%d ", i % 19);
  printf("%d ", -+i);
  printf("%d\n\n", +-i);

  return 0;
}
