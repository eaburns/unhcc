int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;

signed char c2;
unsigned char uc2;
signed short s2;
unsigned short us2;
signed int i2;
unsigned int ui2;
signed long l2;
unsigned long ul2;

void init(void)
{
  c = 7;
  uc = 1;
  s = 2;
  us = 2;
  i = 0;
  ui = 3;
  l = 3;
  ul = 3;
}

void init2(void)
{
  c2 = 2;
  uc2 = 2;
  s2 = 2;
  us2 = 2;
  i2 = 2;
  ui2 = 2;
  l2 = 2;
  ul2 = 2;
}

int main(void)
{
 init();

 c &= uc;
 c &= s;
 c &= us;
 c &= i;
 c &= ui;
 c &= l;
 c &= ul;
 printf("%d\n\n", c);

 init();

 uc &= c;
 uc &= s;
 uc &= us;
 uc &= i;
 uc &= ui;
 uc &= l;
 uc &= ul;
 printf("%x\n\n", uc);

 init();

 s &= c;
 s &= uc;
 s &= us;
 s &= i;
 s &= ui;
 s &= l;
 s &= ul;
 printf("%d\n\n", s);

 init();

 us &= c;
 us &= uc;
 us &= s;
 us &= i;
 us &= ui;
 us &= l;
 us &= ul;
 printf("%x\n\n", us);

 init();

 i &= c;
 i &= uc;
 i &= s;
 i &= us;
 i &= ui;
 i &= l;
 i &= ul;
 printf("%d\n\n", i);

 init();

 ui &= c;
 ui &= uc;
 ui &= s;
 ui &= us;
 ui &= i;
 ui &= l;
 ui &= ul;
 printf("%x\n\n", ui);

 init();

 l &= c;
 l &= uc;
 l &= s;
 l &= us;
 l &= i;
 l &= ui;
 l &= ul;
 printf("%ld\n\n", l);

 init();

 ul &= c;
 ul &= uc;
 ul &= s;
 ul &= us;
 ul &= i;
 ul &= ui;
 ul &= l;
 printf("%lx\n\n", ul);

 init();
 init2();

 c &= c2;
 uc &= uc2;
 s &= s2;
 us &= us2;
 i &= i2;
 ui &= ui2;
 l &= l2;
 ul &= ul2;
 printf("%d %x %d %x\n\n", c, uc, s, us);
 printf("%d %x %ld %lx\n\n", i, ui, l, ul);

 return 0;
}
