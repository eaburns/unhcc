
int printf(char*, ...);

int i;

int main(void)
{
  i = 17;

  printf("%d ", i <<= 5);
  printf("%d ", i >>= 5);
  printf("%d ", i &= 5);
  printf("%d ", i |= 5);
  printf("%d\n", i ^= 5);

  return 0;
}
