
int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;
long double ld;

void init(void)
{
  c = -17;
  uc = 17;
  s = -29;
  us = 29;
  i = -37;
  ui = 37;
  l = -39;
  ul = 39;
  f = -49;
  d = 59;
  ld = -69;
}

int main(void)
{
 init();

 printf("%x %x %x %x %x\n", c OP c, c OP uc, c OP s, c OP us, c OP i);
 printf("%x %lx %lx %e %e %Le\n\n", c OP ui, c OP l, c OP ul, c OP f, c OP d,
   c OP ld);

 printf("%x %x %x %x %x\n", uc OP c, uc OP uc, uc OP s, uc OP us, uc OP i);
 printf("%x %lx %lx %e %e %Le\n\n", uc OP ui, uc OP l, uc OP ul, uc OP f,
   uc OP d, uc OP ld);

 printf("%x %x %x %x %x\n", s OP c, s OP uc, s OP s, s OP us, s OP i);
 printf("%x %lx %lx %e %e %Le\n\n", s OP ui, s OP l, s OP ul, s OP f, s OP d,
   s OP ld);

 printf("%x %x %x %x %x\n", us OP c, us OP uc, us OP s, us OP us, us OP i);
 printf("%x %lx %lx %e %e %Le\n\n", us OP ui, us OP l, us OP ul, us OP f,
   us OP d, us OP ld);

 printf("%x %x %x %x %x\n", i OP c, i OP uc, i OP s, i OP us, i OP i);
 printf("%x %lx %lx %e %e %Le\n\n", i OP ui, i OP l, i OP ul, i OP f, i OP d,
   i OP ld);

 printf("%x %x %x %x %x\n", ui OP c, ui OP uc, ui OP s, ui OP us, ui OP i);
 printf("%x %lx %lx %e %e %Le\n\n", ui OP ui, ui OP l, ui OP ul, ui OP f,
   ui OP d, ui OP ld);

 printf("%lx %lx %lx %lx %lx\n", l OP c, l OP uc, l OP s, l OP us, l OP i);
 printf("%lx %lx %lx %e %e %Le\n\n", l OP ui, l OP l, l OP ul, l OP f, l OP d,
   l OP ld);

 printf("%lx %lx %lx %lx %lx\n", ul OP c, ul OP uc, ul OP s, ul OP us, ul OP i);
 printf("%lx %lx %lx %e %e %Le\n\n", ul OP ui, ul OP l, ul OP ul, ul OP f,
   ul OP d, ul OP ld);

 printf("%e %e %e %e %e\n", f OP c, f OP uc, f OP s, f OP us, f OP i);
 printf("%e %e %e %e %e %Le\n\n", f OP ui, f OP l, f OP ul, f OP f, f OP d,
   f OP ld);

 printf("%e %e %e %e %e\n", d OP c, d OP uc, d OP s, d OP us, d OP i);
 printf("%e %e %e %e %e %Le\n\n", d OP ui, d OP l, d OP ul, d OP f, d OP d,
   d OP ld);

 printf("%Le %Le %Le %Le %Le\n", ld OP c, ld OP uc, ld OP s, ld OP us, ld OP i);
 printf("%Le %Le %Le %Le %Le %Le\n\n", ld OP ui, ld OP l, ld OP ul, ld OP f,
   ld OP d, ld OP ld);

 return 0;
}
