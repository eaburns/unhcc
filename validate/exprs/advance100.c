
int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;

void init(void)
{
  c = -17;
  uc = 17;
  s = -29;
  us = 29;
  i = -37;
  ui = 37;
  l = -39;
  ul = 39;
  f = -49;
  d = 59;
}

void one(void)
{
  printf("1 ");
}

int main(void)
{
 init();

 printf("%x %x %x %x %x\n", ++c, ++uc, ++s, ++us, ++i);
 printf("%x %lx %lx %e %e\n\n", ++ui, ++l, ++ul, ++f, ++d);

 printf("%x %x %x %x %x\n", c++, uc++, s++, us++, i++);
 printf("%x %lx %lx %e %e\n\n", ui++, l++, ul++, f++, d++);

 printf("%x %x %x %x %x\n", --c, --uc, --s, --us, --i);
 printf("%x %lx %lx %e %e\n\n", --ui, --l, --ul, --f, --d);

 printf("%x %x %x %x %x\n", c--, uc--, s--, us--, i--);
 printf("%x %lx %lx %e %e\n\n", ui--, l--, ul--, f--, d--);

 printf("%x %x %x %x %x\n", (unsigned char) c, (unsigned char) uc,
          (unsigned char) s, (unsigned char) us,  (unsigned char) i);
 printf("%x %x %x %x %x\n\n", (unsigned char) ui, (unsigned char) l,
          (unsigned char) ul, (unsigned char) f,  (unsigned char) d);

 printf("%x %x %x %x %x\n", (signed char) c, (signed char) uc,
          (signed char) s, (signed char) us,  (signed char) i);
 printf("%x %x %x %x %x\n\n", (signed char) ui, (signed char) l,
          (signed char) ul, (signed char) f,  (signed char) d);

 printf("%x %x %x %x %x\n", (unsigned short) c, (unsigned short) uc,
          (unsigned short) s, (unsigned short) us,  (unsigned short) i);
 printf("%x %x %x %x %x\n\n", (unsigned short) ui, (unsigned short) l,
          (unsigned short) ul, (unsigned short) f,  (unsigned short) d);

 printf("%x %x %x %x %x\n", (signed short) c, (signed short) uc,
          (signed short) s, (signed short) us,  (signed short) i);
 printf("%x %x %x %x %x\n\n", (signed short) ui, (signed short) l,
          (signed short) ul, (signed short) f,  (signed short) d);

 printf("%x %x %x %x %x\n", (unsigned int) c, (unsigned int) uc,
          (unsigned int) s, (unsigned int) us,  (unsigned int) i);
 printf("%x %x %x %x %x\n\n", (unsigned int) ui, (unsigned int) l,
          (unsigned int) ul, (unsigned int) f,  (unsigned int) d);

 printf("%x %x %x %x %x\n", (signed int) c, (signed int) uc,
          (signed int) s, (signed int) us,  (signed int) i);
 printf("%x %x %x %x %x\n\n", (signed int) ui, (signed int) l,
          (signed int) ul, (signed int) f,  (signed int) d);

 printf("%x %x %x %x %x\n", (unsigned long) c, (unsigned long) uc,
          (unsigned long) s, (unsigned long) us,  (unsigned long) i);
 printf("%x %x %x %x %x\n\n", (unsigned long) ui, (unsigned long) l,
          (unsigned long) ul, (unsigned long) f,  (unsigned long) d);

 printf("%x %x %x %x %x\n", (signed long) c, (signed long) uc,
          (signed long) s, (signed long) us,  (signed long) i);
 printf("%x %x %x %x %x\n\n", (signed long) ui, (signed long) l,
          (signed long) ul, (signed long) f,  (signed long) d);

 printf("%e %e %e %e %e\n", (float) c, (float) uc,
          (float) s, (float) us,  (float) i);
 printf("%e %e %e %e %e\n\n", (float) ui, (float) l,
          (float) ul, (float) f,  (float) d);

 printf("%e %e %e %e %e\n", (double) c, (double) uc,
          (double) s, (double) us,  (double) i);
 printf("%e %e %e %e %e\n\n", (double) ui, (double) l,
          (double) ul, (double) f,  (double) d);

 printf("%d %d %d %d %d\n", sizeof(c), sizeof(uc), sizeof(s), sizeof(us),
         sizeof(i));
 printf("%d %d %d %d %d\n\n", sizeof(ui), sizeof(l), sizeof(ul), sizeof(f),
         sizeof(d));

 printf("%d %d %d %d %d\n", (one(), c), (one(), uc), (one(), s), (one(), us),
         (one(), i));
 printf("%d %d %d %e %e\n\n", (one(), ui), (one(), l), (one(), ul), (one(), f),
         (one(), d));

 return 0;
}
