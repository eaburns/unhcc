int printf(char*, ...);

signed char c;
unsigned char uc;
signed short s;
unsigned short us;
signed int i;
unsigned int ui;
signed long l;
unsigned long ul;
float f;
double d;
long double ld;

void init(void)
{
  c = 17;
  uc = 135;
  s = 29;
  us = 35529;
  i = 37;
  ui = 0x80000000u;
  l = 39;
  ul = 0x80000000lu;
  f = 49;
  d = 59;
  ld = 69;
}

int main(void)
{
 init();

 printf("%d %d %d ", c >= c, c >= uc, c >= s);
 printf("%d %d\n", c >= us, c >= i);
 printf("%d %d %d ", c >= ui, c >= l, c >= ul);
 printf("%d %d %d\n\n", c >= f, c >= d, c >= ld);

 printf("%d %d %d ", uc >= c, uc >= uc, uc >= s);
 printf("%d %d\n", uc >= us, uc >= i);
 printf("%d %d %d ", uc >= ui, uc >= l, uc >= ul);
 printf("%d %d %d\n\n", uc >= f, uc >= d, uc >= ld);

 printf("%d %d %d ", s >= c, s >= uc, s >= s);
 printf("%d %d\n", s >= us, s >= i);
 printf("%d %d %d ", s >= ui, s >= l, s >= ul);
 printf("%d %d %d\n\n", s >= f, s >= d, s >= ld);

 printf("%d %d %d ", us >= c, us >= uc, us >= s);
 printf("%d %d\n", us >= us, us >= i);
 printf("%d %d %d ", us >= ui, us >= l, us >= ul);
 printf("%d %d %d\n\n", us >= f, us >= d, us >= ld);

 printf("%d %d %d ", i >= c, i >= uc, i >= s);
 printf("%d %d\n", i >= us, i >= i);
 printf("%d %d %d ", i >= ui, i >= l, i >= ul);
 printf("%d %d %d\n\n", i >= f, i >= d, i >= ld);

 printf("%d %d %d ", ui >= c, ui >= uc, ui >= s);
 printf("%d %d\n", ui >= us, ui >= i);
 printf("%d %d %d ", ui >= ui, ui >= l, ui >= ul);
 printf("%d %d %d\n\n", ui >= f, ui >= d, ui >= ld);

 printf("%d %d %d ", l >= c, l >= uc, l >= s);
 printf("%d %d\n", l >= us, l >= i);
 printf("%d %d %d ", l >= ui, l >= l, l >= ul);
 printf("%d %d %d\n\n", l >= f, l >= d, l >= ld);

 printf("%d %d %d ", ul >= c, ul >= uc, ul >= s);
 printf("%d %d\n", ul >= us, ul >= i);
 printf("%d %d %d ", ul >= ui, ul >= l, ul >= ul);
 printf("%d %d %d\n\n", ul >= f, ul >= d, ul >= ld);

 printf("%d %d %d ", f >= c, f >= uc, f >= s);
 printf("%d %d\n", f >= us, f >= i);
 printf("%d %d %d ", f >= ui, f >= l, f >= ul);
 printf("%d %d %d\n\n", f >= f, f >= d, f >= ld);

 printf("%d %d %d ", d >= c, d >= uc, d >= s);
 printf("%d %d\n", d >= us, d >= i);
 printf("%d %d %d ", d >= ui, d >= l, d >= ul);
 printf("%d %d %d\n\n", d >= f, d >= d, d >= ld);

 printf("%d %d %d ", ld >= c, ld >= uc, ld >= s);
 printf("%d %d\n", ld >= us, ld >= i);
 printf("%d %d %d ", ld >= ui, ld >= l, ld >= ul);
 printf("%d %d %d\n\n", ld >= f, ld >= d, ld >= ld);

 return 0;
}
