
int printf(char*, ...);

int i;
int j;
int k;

int main(void)
{
  for (i = 0; i < 5; i++)
  {
    printf("%d",i);
    printf("\n");
  }
  i = 0;
  for (; i < 5; i++)
  {
    printf("%d",i);
    printf("\n");
  }
  i = 0;
  for (; i < 5;)
  {
    printf("%d",i);
    printf("\n");
    i++;
  }
  i = 0;
  for (;;)
  {
    printf("%d",i);
    printf("\n");
    i++;
    if (i == 5) break;
    continue;
  }
  for (i = 0; i < 10; i++)
  {
    if (i == 2) continue;
    printf("%d",i);
    printf("\n");
    if (i == 8) break;
  }
  for (i = 0; i < 5; i++)
    for (j = 0, k = 0; j < 3; j++, k++)
      printf("%d %d %d\n",i, j, k);
  return 0;
}
