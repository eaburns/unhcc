int printf(char*, ...);

char c;
short s;
int i;
long l;
unsigned char uc;
unsigned short us;
unsigned int ui;
unsigned long ul;
float f;
double d;
long double ld;

int j;

int main(void)
{
  j = 0;
  c = 5;
  switch (c) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  s = 5;
  switch (s) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  i = 5;
  switch (i) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  l = 5;
  switch (l) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  uc = 5;
  switch (uc) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  us = 5;
  switch (us) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  ui = 5;
  switch (ui) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  j = 0;
  ul = 5;
  switch (ul) {
  case 1: j++;
  case 3: j++;
  case 5: j++;
  case 7: j++;
  default: j++;
  }
  printf("%d\n", j);
  return 0;
}
