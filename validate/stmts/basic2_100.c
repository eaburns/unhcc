int printf(char*, ...);

int i;

int main(void)
{
  i = 10;
  while (i > 0)
  {
    i--;
    if (i == 8) goto contin;
    printf("%d\n", i);
    if (i == 2) goto brk;
    printf("LOOP\n");
    contin: i = i;
  }
brk: i = i;
  return 0;
}
