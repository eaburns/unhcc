int printf(char*, ...);

int i;
int j;

int main(void)
{
  i = 3;
  j = 0;
  while (i < 20)
  {
    switch (i + 1)
    {
      case 0: j++;
      case 1: j++;
      case 2: j++;
      case 3: j++;
      case 4: j++;
      case 5: j++;
      case 6: j++;
      default: j++;
    }
    printf("%d\n", j);
    i += 10;
  }
  return 0;
}
