int printf(char*, ...);

int i;

int main(void)
{
  i = 10;
  while (i > 0)
  {
    i--;
    if (i == 8) continue;
    printf("%d\n", i);
    if (i == 2) break;
    printf("LOOP\n");
  }
  return 0;
}
