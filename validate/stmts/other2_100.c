int printf(char*, ...);

int prime(int x)
{
  switch (x)
  {
    case 1:
    case 2:
    case 3:
    case 5:
    case 7:
    case 11:
    case 13:
    case 17:
    case 19:
      return 1;
  }
  return 0;
}

void weird(int x)
{
  switch (x)
    default:
      if (prime(x))
        case 2: case 3: case 5: case 7:
          printf("PRIME\n");
      else
        case 4: case 6: case 8: case 9: case 10:
          printf("COMPOSITE\n");
}

int main(void)
{
  weird(2);
  weird(9);
  weird(18);
  weird(19);
  return 0;
}
