int printf(char*, ...);

char c;
short s;
int i;
long l;
unsigned char uc;
unsigned short us;
unsigned int ui;
unsigned long ul;
float f;
double d;
long double ld;

int main(void)
{
  c = 5;
  while (c)
  {
    printf("c\n");
    c = c - 1;
  }

  s = 5;
  while (s)
  {
    printf("s\n");
    s = s - 1;
  }

  i = 5;
  while (i)
  {
    printf("i\n");
    i = i - 1;
  }

  l = 5;
  while (l)
  {
    printf("l\n");
    l = l - 1;
  }

  uc = 5;
  while (uc)
  {
    printf("uc\n");
    uc = uc - 1;
  }

  us = 5;
  while (us)
  {
    printf("us\n");
    us = us - 1;
  }

  ui = 5;
  while (ui)
  {
    printf("ui\n");
    ui = ui - 1;
  }

  ul = 5;
  while (ul)
  {
    printf("ul\n");
    ul = ul - 1;
  }

  f = 5;
  while (f)
  {
    printf("f\n");
    f = f - 1;
  }

  d = 5;
  while (d)
  {
    printf("d\n");
    d = d - 1;
  }

  ld = 5;
  while (ld)
  {
    printf("ld\n");
    ld = ld - 1;
  }
  return 0;
}
