int printf(char*, ...);

char c;
short s;
int i;
long l;
unsigned char uc;
unsigned short us;
unsigned int ui;
unsigned long ul;
float f;
double d;
long double ld;

int main(void)
{
  c = 5;
  do {
    printf("c\n");
    c = c - 1;
  }
  while (c);

  s = 5;
  do {
    printf("s\n");
    s = s - 1;
  }
  while (s);

  i = 5;
  do {
    printf("i\n");
    i = i - 1;
  }
  while (i);

  l = 5;
  do {
    printf("l\n");
    l = l - 1;
  }
  while (l);

  uc = 5;
  do {
    printf("uc\n");
    uc = uc - 1;
  }
  while (uc);

  us = 5;
  do {
    printf("us\n");
    us = us - 1;
  }
  while (us);

  ui = 5;
  do {
    printf("ui\n");
    ui = ui - 1;
  }
  while (ui);

  ul = 5;
  do {
    printf("ul\n");
    ul = ul - 1;
  }
  while (ul);

  f = 5;
  do {
    printf("f\n");
    f = f - 1;
  }
  while (f);

  d = 5;
  do {
    printf("d\n");
    d = d - 1;
  }
  while (d);

  ld = 5;
  do {
    printf("ld\n");
    ld = ld - 1;
  }
  while (ld);
  return 0;
}
