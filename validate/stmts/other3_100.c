int printf(char*, ...);

int i;

void f(void)
{
  i = 10;
  top:
  if (i <= 0) goto brk;
  {
    i--;
    if (i == 8) goto contin;
    printf("%d\n", i);
    if (i == 2) goto brk;
    printf("LOOP\n");
    contin: i = i;
    goto top;
  }
brk: i = i;
}

int main(void)
{
  i = 10;
  top:
  if (i <= 0) goto brk;
  {
    i--;
    if (i == 8) goto contin;
    printf("%d\n", i);
    if (i == 2) goto brk;
    printf("LOOP\n");
    contin: i = i;
    goto top;
  }
brk: i = i;
f();
return 0;
}
