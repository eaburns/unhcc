
int printf(char*, ...);

int i;
int j;
int k;

int main(void)
{
  i = 3;
  do {
    j = 2;
    do {
      k = 1;
      do {
        k--; printf("%d %d %d\n", i, j, k);
        if (k == 0) continue;
        printf("NO\n");
      }
      while (k > 0);
      j--; printf("%d %d %d\n", i, j, k);
      if (j == 1) break;
    }
    while (j > 0);
    i--; printf("%d %d %d\n", i, j, k);
  }
  while (i > 0);
  return 0;
}
