int printf(char*, ...);

int i;

int main(void)
{
  i = 9;
  do
  {
    printf("%d\n",i);
    i--;
  }
  while (i > 0);
  return 0;
}
