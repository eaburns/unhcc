int printf(char*, ...);

int i;

int main(void)
{
  i = 5;
  while (i > 0)
  {
    printf("%d\n", i);
    i--;
  }
  return 0;
}
