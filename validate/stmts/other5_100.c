int printf(char*, ...);

int i;

int main(void)
{
  i = 5;
  top: switch (i) {
  case 1:
          i++;
          break;
  case 2:
          goto done;
  case 5:
          while (i > 0)
          {
            printf("%d\n", i);
            i--;
            if (i == 3) break;
          }
          break;
  default:
          printf("NO\n");
  }
  if (i== 3)
  {
    i = 1;
    goto top;
  }
  else if (i == 2)
  {
    i = 2;
    goto top;
  }
  else
  {
    done: printf("YES\n");
  }
  return 0;
}
