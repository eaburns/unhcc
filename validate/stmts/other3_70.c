int printf(char*, ...);

char c;
short s;
int i;
long l;
unsigned char uc;
unsigned short us;
unsigned int ui;
unsigned long ul;
float f;
double d;
long double ld;

int main(void)
{
  c = 0;
  if (c)
  {
    printf("c\n");
    c = c - 1;
  }

  s = 0;
  if (s)
  {
    printf("s\n");
    s = s - 1;
  }

  i = 0;
  if (i)
  {
    printf("i\n");
    i = i - 1;
  }

  l = 0;
  if (l)
  {
    printf("l\n");
    l = l - 1;
  }

  uc = 0;
  if (uc)
  {
    printf("uc\n");
    uc = uc - 1;
  }

  us = 0;
  if (us)
  {
    printf("us\n");
    us = us - 1;
  }

  ui = 0;
  if (ui)
  {
    printf("ui\n");
    ui = ui - 1;
  }

  ul = 0;
  if (ul)
  {
    printf("ul\n");
    ul = ul - 1;
  }

  f = 0;
  if (f)
  {
    printf("f\n");
    f = f - 1;
  }

  d = 0;
  if (d)
  {
    printf("d\n");
    d = d - 1;
  }

  ld = 0;
  if (ld)
  {
    printf("ld\n");
    ld = ld - 1;
  }
  return 0;
}
