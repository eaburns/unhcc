
int printf(char*, ...);

int i;
int j;

int main(void)
{
  i = 10;
  while (i > 0)
  {
    j = 5;
    while (j > 0)
    {
      printf("%d %d\n", i, j);
      j--;
      if (j == 3) continue;
      if (j == 1) break;
    }
    i--;
    if (i == 8) continue;
    if (i == 6) break;
  }
  return 0;
}
