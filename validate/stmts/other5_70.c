
int printf(char*, ...);

int i;
int j;

int main(void)
{
  i = 10;
  while (i > 0)
  {
    j = 5;
    while (j > 0)
    {
      printf("%d %d\n", i, j);
      j--;
    }
    i--;
  }
  return 0;
}
