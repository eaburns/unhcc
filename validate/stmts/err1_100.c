void f(void)
{
  never: never: goto nowhere;
}

double d;

void g(void)
{
  never: switch (d) {}
}

void h(void)
{
  switch (1) {
  case 1:
  case 2:
  case 1: switch(0) { case 1: case 2: default: d = 1.0; }
  default:
  default: d = 0.0;
  }
}
