/* Peter Frontiero, cs812, phase 1 assignment, due March 4 
 *
 * tree.h specifies structures for the individual nodes of the abstract syntax
 * tree
 */

#ifndef TREE_H
#define TREE_H

#include "defs.h"
#include "types.h"
#include "symtab.h"

typedef enum { ADD_OP, CONVERT_OP, INDEX_OP, INCRA_OP,
	DECRA_OP, INCRB_OP, DECRB_OP, SIZEOF_OP, MULT_OP, DIV_OP,
	    MOD_OP, SUB_OP,
	LSHIFT_OP, RSHIFT_OP, LT_OP, GT_OP, LTE_OP, GTE_OP, EQL_OP,
	    NEQL_OP, ANDD_OP,
	XORR_OP, ORR_OP, LAND_OP, LOR_OP, COND_OP, ASSIGN_OP,
	    MULT_ASSIGN_OP,
	DIV_ASSIGN_OP, MOD_ASSIGN_OP, ADD_ASSIGN_OP, SUB_ASSIGN_OP,
	    LEFT_ASSIGN_OP,
	RIGHT_ASSIGN_OP, AND_ASSIGN_OP, XOR_ASSIGN_OP, OR_ASSIGN_OP,
	    ADDR_OF_OP,
	DEREF_OP, UADD_OP, USUB_OP, COMPLMT_OP, LNOT_OP, COMMA_OP,
	    NO_OP, DUP_OP
} TreeOp;

typedef enum { UNOP_TAG, BINOP_TAG, TRIOP_TAG, FCALL_TAG, ESEQ_TAG,
	    PTR_TAG,
	FIELDREF_TAG, CASTOP_TAG, VAR_TAG, INT_CONST_TAG, FP_CONST_TAG,
	STRING_LIT_TAG, LONG_CONST_TAG, UINT_CONST_TAG, ULONG_CONST_TAG,
	    LABEL_TAG,
	CASE_TAG, DEFAULT_TAG, SEQ_TAG, IFELSE_TAG, SWITCH_TAG,
	    WHILE_TAG,
	DOWHILE_TAG, FOR_TAG, GOTO_TAG, CONTINUE_TAG, BREAK_TAG,
	    RETURN_TAG,
	ERROR_TAG
} TreeTag;

 /*------------------------------ TREE --------------------------------
  * Abstract node definition
  */

typedef struct Tree {

	TreeTag tag;

	int lineno;		/* for error reporting */
	char *filename;

} *Tree;

 /*------------------------------ EXPTREE -----------------------------
  * Abstract expression node definition
  */

typedef struct ExpTree {

	struct Tree absn;
	Type type;
	REG reg;

} *ExpTree;

typedef struct UnopTreeNode {

	struct ExpTree expn;
	TreeOp op;
	ExpTree left;

} *UnopTree;

typedef struct BinopTreeNode {

	struct ExpTree expn;
	TreeOp op;
	ExpTree left;
	ExpTree right;

} *BinopTree;

typedef struct TriopTreeNode {

	struct ExpTree expn;
	TreeOp op;
	ExpTree left;
	ExpTree middle;
	ExpTree right;

} *TriopTree;

typedef struct FcallTreeNode {

	struct ExpTree expn;
	ExpTree func;
	ExpTree params;

} *FcallTree;

typedef struct EseqTreeNode {

	struct ExpTree expn;
	ExpTree left;
	ExpTree right;

} *EseqTree;

typedef struct PtrTreeNode {

	struct ExpTree expn;
	ST_ID id;
	ExpTree left;

} *PtrTree;

typedef struct FieldRefTreeNode {

	struct ExpTree expn;
	ST_ID id;
	ExpTree left;

} *FieldRefTree;

typedef struct CastopTreeNode {

	struct ExpTree expn;
	Type typename;
	ExpTree left;

} *CastopTree;

typedef struct VarTreeNode {

	struct ExpTree expn;
	ST_ID name;
	StorageClass class;
	int offset;
	int block;
	unsigned int staticLabel;
	STDR_TAG stdrTag;

} *VarTree;

typedef struct IntConstTreeNode {

	struct ExpTree expn;
	int value;

} *IntConstTree;

typedef struct FpConstTreeNode {

	struct ExpTree expn;
	char *value;

} *FpConstTree;

typedef struct StringLitTreeNode {

	struct ExpTree expn;
	char *string;

} *StringLitTree;

typedef struct LongConstTreeNode {

	struct ExpTree expn;
	long value;

} *LongConstTree;

typedef struct UIntConstTreeNode {

	struct ExpTree expn;
	unsigned int value;

} *UIntConstTree;

typedef struct ULongConstTreeNode {

	struct ExpTree expn;
	unsigned long value;

} *ULongConstTree;



 /*------------------------------ STATEMENTS -------------------------- */

typedef struct LabelTreeNode {

	struct Tree absn;
	ST_ID label;
	Tree left;

} *LabelTree;

typedef struct CaseTreeNode {

	struct Tree absn;
	ExpTree caseexpr;
	Tree statement;

} *CaseTree;

typedef struct DefaultTreeNode {

	struct Tree absn;
	Tree statement;

} *DefaultTree;

typedef struct SeqTreeNode {

	struct Tree absn;
	Tree left;
	Tree right;

} *SeqTree;

typedef struct IfElseTreeNode {

	struct Tree absn;
	ExpTree expr;
	Tree ifstmt;
	Tree elsestmt;

} *IfElseTree;

typedef struct SwitchTreeNode {

	struct Tree absn;
	ExpTree expr;
	Tree statement;

} *SwitchTree;

typedef struct WhileTreeNode {

	struct Tree absn;
	ExpTree expr;
	Tree statement;

} *WhileTree;

typedef struct DoWhileTreeNode {

	struct Tree absn;
	Tree statement;
	ExpTree expr;

} *DoWhileTree;

typedef struct ForTreeNode {

	struct Tree absn;
	ExpTree expr1;
	ExpTree expr2;
	ExpTree expr3;
	Tree statement;

} *ForTree;

typedef struct GotoTreeNode {

	struct Tree absn;
	ST_ID label;

} *GotoTree;

typedef struct ContinueTreeNode {

	struct Tree absn;

} *ContinueTree;

typedef struct BreakTreeNode {

	struct Tree absn;

} *BreakTree;

typedef struct ReturnTreeNode {

	struct Tree absn;
	ExpTree expr;

} *ReturnTree;

 /*------------------------------ ErrorTree --------------------------
  * Error node definition: used for handling parse errors
  */

typedef struct ErrorTreeNode {

	struct Tree absn;

} *ErrorTree;

 /*------------------------------ FUNCTION PROTOTYPES ----------------- */

extern ExpTree newUnop(TreeOp, Type, ExpTree);
extern ExpTree newBinop(TreeOp, Type, ExpTree, ExpTree);
extern ExpTree newTriop(TreeOp, Type, ExpTree, ExpTree, ExpTree);
extern ExpTree newFcall(Type, ExpTree, ExpTree);
extern ExpTree newEseq(Type, ExpTree, ExpTree);
extern ExpTree newPtr(ST_ID, Type, ExpTree);
extern ExpTree newFieldRef(ST_ID, Type, ExpTree);
extern ExpTree newCastop(Type, Type, ExpTree);
extern ExpTree newVar(ST_ID);
extern ExpTree dummyVar(void);
extern ExpTree newIntConst(int);
extern ExpTree newFpConst(char *, Type);
extern ExpTree newStringLit(char *, Type);
extern ExpTree newLongConst(long);
extern ExpTree newUIntConst(unsigned int);
extern ExpTree newULongConst(unsigned long);

extern Tree newLabel(ST_ID, Tree);
extern Tree newCase(ExpTree, Tree);
extern Tree newDefault(Tree);
extern Tree newSeq(Tree, Tree);
extern Tree newIfElse(ExpTree, Tree, Tree);
extern Tree newSwitch(ExpTree, Tree);
extern Tree newWhile(ExpTree, Tree);
extern Tree newDoWhile(Tree, ExpTree);
extern Tree newFor(ExpTree, ExpTree, ExpTree, Tree);
extern Tree newGoto(ST_ID);
extern Tree newContinue(void);
extern Tree newBreak(void);
extern Tree newReturn(ExpTree);
extern Tree newError(void);

extern Tree poolAlloc(int);
extern void printTree(Tree);
extern void poolInit(void);
extern void poolReset(void);

extern BOOLEAN isExpTree(Tree t);

extern char *TreeOptoString(TreeOp t);

extern BOOLEAN isAssignmentOperator(TreeOp);

extern BOOLEAN isConstTag(TreeTag t);

extern BOOLEAN isCompileTimeEvalable(TreeOp op);

#endif
