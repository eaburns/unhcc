/* Peter Frontiero, cs812, phase 1 assignment, due March 4
 *
 * tree.c implements functions for abstract syntax tree manipulation
 */

#include "eval.h"
#include "tree.h"
#include "regAlloc.h"
#include "message.h"

static void initTree(Tree t, TreeTag tag)
{
	t->tag = tag;
	t->filename = YYfilename();
	t->lineno = YYlineno();
}

ExpTree newUnop(TreeOp op, Type type, ExpTree left)
{
	Tree pt;
	ExpTree t;
	UnopTree p;

	pt = poolAlloc(sizeof(struct UnopTreeNode));
	initTree(pt, UNOP_TAG);

	t = (ExpTree) pt;

	p = (UnopTree) t;

	p->op = op;
	p->left = left;
	p->expn.type = type;
	p->expn.reg = nullReg();

	if (t->absn.tag == UNOP_TAG
	    && isCompileTimeEvalable(((UnopTree) p)->op)
	    && isConstTag(((UnopTree) p)->left->absn.tag)) {
		/* if we are able to evaluate t his unary operation at
		   compile-time, then do it now */

		return evalUnopTree((UnopTree) p);
	}

	return t;

}

ExpTree newBinop(TreeOp op, Type type, ExpTree left, ExpTree right)
{
	Tree pt;
	ExpTree t;
	BinopTree p;

	pt = poolAlloc(sizeof(struct BinopTreeNode));
	initTree(pt, BINOP_TAG);

	t = (ExpTree) pt;

	p = (BinopTree) t;

	p->op = op;
	p->left = left;
	p->right = right;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newTriop(TreeOp op, Type type, ExpTree left,
		 ExpTree middle, ExpTree right)
{
	Tree pt;
	ExpTree t;
	TriopTree p;

	pt = poolAlloc(sizeof(struct TriopTreeNode));
	initTree(pt, TRIOP_TAG);

	t = (ExpTree) pt;

	p = (TriopTree) t;

	p->op = op;
	p->left = left;
	p->middle = middle;
	p->right = right;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newFcall(Type type, ExpTree func, ExpTree params)
{
	Tree pt;
	ExpTree t;
	FcallTree p;

	pt = poolAlloc(sizeof(struct FcallTreeNode));
	initTree(pt, FCALL_TAG);

	t = (ExpTree) pt;

	p = (FcallTree) t;

	p->func = func;
	p->params = params;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newEseq(Type type, ExpTree left, ExpTree right)
{
	Tree pt;
	ExpTree t;
	EseqTree p;

	pt = poolAlloc(sizeof(struct EseqTreeNode));
	initTree(pt, ESEQ_TAG);

	t = (ExpTree) pt;

	p = (EseqTree) t;

	p->left = left;
	p->right = right;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newPtr(ST_ID id, Type type, ExpTree left)
{
	Tree pt;
	ExpTree t;
	PtrTree p;

	pt = poolAlloc(sizeof(struct PtrTreeNode));
	initTree(pt, PTR_TAG);

	t = (ExpTree) pt;

	p = (PtrTree) t;

	p->id = id;
	p->left = left;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newFieldRef(ST_ID id, Type type, ExpTree left)
{
	Tree pt;
	ExpTree t;
	FieldRefTree p;

	pt = poolAlloc(sizeof(struct FieldRefTreeNode));
	initTree(pt, FIELDREF_TAG);

	t = (ExpTree) pt;

	p = (FieldRefTree) t;

	p->id = id;
	p->left = left;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newCastop(Type typename, Type type, ExpTree left)
{
	Tree pt;
	ExpTree t;
	CastopTree p;

	pt = poolAlloc(sizeof(struct CastopTreeNode));
	initTree(pt, CASTOP_TAG);

	t = (ExpTree) pt;

	p = (CastopTree) t;

	p->typename = typename;
	p->left = left;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

/* pjh: must lookup id *now* when symbol table entry is still present */
ExpTree newVar(ST_ID name)
{
	ST_DR stdr;
	Tree t;
	VarTree v;
	IntConstTree i;		/* lsr, 05/?/01 */

	t = poolAlloc(sizeof(struct VarTreeNode));
	initTree(t, VAR_TAG);

	v = (VarTree) t;

	/* default values */
	v->name = name;
	v->class = NO_SC;
	v->offset = -1;
	v->block = -1;
	v->stdrTag = 0;
	v->staticLabel = 0;
	v->expn.type = NULL;
	v->expn.reg = nullReg();

	stdr = st_lookup(name, &v->block);
	if (stdr == NULL) {
		error("identifier (%s) not declared",
		      st_get_id_str(v->name));
		v->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
	} else {
		switch (stdr->tag) {

		case LDECL:
			/* set block to 0 for local extern */
			if (stdr->u.decl.sc == EXTERN_SC) {
				v->block = 0;
			}
			/* Note: control falls through to GDECL/PDECL
			   case! */
		case GDECL:
		case PDECL:
			v->expn.type = stdr->u.decl.type;
			v->class = stdr->u.decl.sc;
			v->offset = stdr->u.decl.offset;
			v->staticLabel = stdr->u.decl.staticLabel;
			v->stdrTag = stdr->tag;
			break;

		case FDEF:
			v->expn.type = stdr->u.decl.type;
			break;

		case ECONST:
			t->tag = INT_CONST_TAG;
			i = (IntConstTree) v;
			i->expn.type = stdr->u.econst.type;
			i->value = stdr->u.econst.val;
			break;

			/* this can only happen via a parse error? */
		case TDEF:
			v->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			break;

		default:
			bug("unknown ST_DR tag in newVar()");
		}
	}

	return (ExpTree) t;

}

/* Also a constructor for nodes for compiler-generated temporaries. */
ExpTree dummyVar(void)
{
	Tree t;
	VarTree v;

	t = poolAlloc(sizeof(struct VarTreeNode));
	initTree(t, VAR_TAG);

	v = (VarTree) t;

	/* default values */
	v->name = NULL;
	v->class = NO_SC;
	v->offset = -1;
	v->block = -1;
	v->stdrTag = 0;
	v->staticLabel = 0;
	v->expn.type = NULL;
	v->expn.reg = nullReg();

	return (ExpTree) v;
}

ExpTree newIntConst(int value)
{
	Tree pt;
	ExpTree t;
	IntConstTree p;

	pt = poolAlloc(sizeof(struct IntConstTreeNode));
	initTree(pt, INT_CONST_TAG);

	t = (ExpTree) pt;

	p = (IntConstTree) t;

	p->value = value;
	p->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	p->expn.reg = nullReg();

	return t;

}

ExpTree newFpConst(char *value, Type type)
{
	Tree pt;
	ExpTree t;
	FpConstTree p;

	pt = poolAlloc(sizeof(struct FpConstTreeNode));
	initTree(pt, FP_CONST_TAG);

	t = (ExpTree) pt;

	p = (FpConstTree) t;

	p->value = value;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newStringLit(char *string, Type type)
{
	Tree pt;
	ExpTree t;
	StringLitTree p;

	pt = poolAlloc(sizeof(struct StringLitTreeNode));
	initTree(pt, STRING_LIT_TAG);

	t = (ExpTree) pt;

	p = (StringLitTree) t;

	p->string = string;
	p->expn.type = type;
	p->expn.reg = nullReg();

	return t;

}

ExpTree newLongConst(long value)
{
	Tree pt;
	ExpTree t;
	LongConstTree p;

	pt = poolAlloc(sizeof(struct LongConstTreeNode));
	initTree(pt, LONG_CONST_TAG);

	t = (ExpTree) pt;

	p = (LongConstTree) t;

	p->value = value;
	p->expn.type = typeBuildBasic(TYSIGNEDLONGINT, NO_QUAL);
	p->expn.reg = nullReg();

	return t;

}

ExpTree newUIntConst(unsigned int value)
{
	Tree pt;
	ExpTree t;
	UIntConstTree p;

	pt = poolAlloc(sizeof(struct UIntConstTreeNode));
	initTree(pt, UINT_CONST_TAG);

	t = (ExpTree) pt;

	p = (UIntConstTree) t;

	p->value = value;
	p->expn.type = typeBuildBasic(TYUNSIGNEDINT, NO_QUAL);
	p->expn.reg = nullReg();

	return t;

}

ExpTree newULongConst(unsigned long value)
{
	Tree pt;
	ExpTree t;
	ULongConstTree p;

	pt = poolAlloc(sizeof(struct ULongConstTreeNode));
	initTree(pt, ULONG_CONST_TAG);

	t = (ExpTree) pt;

	p = (ULongConstTree) t;

	p->value = value;
	p->expn.type = typeBuildBasic(TYUNSIGNEDLONGINT, NO_QUAL);
	p->expn.reg = nullReg();

	return t;

}

Tree newLabel(ST_ID label, Tree left)
{
	Tree t;
	LabelTree p;

	t = poolAlloc(sizeof(struct LabelTreeNode));
	initTree(t, LABEL_TAG);

	p = (LabelTree) t;

	p->label = label;
	p->left = left;

	return t;

}

Tree newCase(ExpTree caseexpr, Tree statement)
{
	Tree t;
	CaseTree p;

	t = poolAlloc(sizeof(struct CaseTreeNode));
	initTree(t, CASE_TAG);

	p = (CaseTree) t;

	p->caseexpr = caseexpr;
	p->statement = statement;

	return t;

}

Tree newDefault(Tree statement)
{
	Tree t;
	DefaultTree p;

	t = poolAlloc(sizeof(struct DefaultTreeNode));
	initTree(t, DEFAULT_TAG);

	p = (DefaultTree) t;

	p->statement = statement;

	return t;

}

Tree newSeq(Tree left, Tree right)
{
	Tree t;
	SeqTree p;

	t = poolAlloc(sizeof(struct SeqTreeNode));
	initTree(t, SEQ_TAG);

	p = (SeqTree) t;

	p->left = left;
	p->right = right;

	return t;

}

Tree newIfElse(ExpTree expr, Tree ifstmt, Tree elsestmt)
{
	Tree t;
	IfElseTree p;

	t = poolAlloc(sizeof(struct IfElseTreeNode));
	initTree(t, IFELSE_TAG);

	p = (IfElseTree) t;

	p->expr = expr;
	p->ifstmt = ifstmt;
	p->elsestmt = elsestmt;

	return t;

}

Tree newSwitch(ExpTree expr, Tree statement)
{
	Tree t;
	SwitchTree p;

	t = poolAlloc(sizeof(struct SwitchTreeNode));
	initTree(t, SWITCH_TAG);

	p = (SwitchTree) t;

	p->expr = expr;
	p->statement = statement;

	return t;

}

Tree newWhile(ExpTree expr, Tree statement)
{
	Tree t;
	WhileTree p;

	t = poolAlloc(sizeof(struct WhileTreeNode));
	initTree(t, WHILE_TAG);

	p = (WhileTree) t;

	p->expr = expr;
	p->statement = statement;

	return t;

}

Tree newDoWhile(Tree statement, ExpTree expr)
{
	Tree t;
	DoWhileTree p;

	t = poolAlloc(sizeof(struct DoWhileTreeNode));
	initTree(t, DOWHILE_TAG);

	p = (DoWhileTree) t;

	p->statement = statement;
	p->expr = expr;

	return t;

}

Tree newFor(ExpTree expr1, ExpTree expr2, ExpTree expr3, Tree statement)
{
	Tree t;
	ForTree p;

	t = poolAlloc(sizeof(struct ForTreeNode));
	initTree(t, FOR_TAG);

	p = (ForTree) t;

	p->expr1 = expr1;
	p->expr2 = expr2;
	p->expr3 = expr3;
	p->statement = statement;

	return t;

}

Tree newGoto(ST_ID label)
{
	Tree t;
	GotoTree p;

	t = poolAlloc(sizeof(struct GotoTreeNode));
	initTree(t, GOTO_TAG);

	p = (GotoTree) t;

	p->label = label;

	return t;

}

Tree newContinue(void)
{
	Tree t;

	t = poolAlloc(sizeof(struct ContinueTreeNode));
	initTree(t, CONTINUE_TAG);

	return t;

}

Tree newBreak(void)
{
	Tree t;

	t = poolAlloc(sizeof(struct BreakTreeNode));
	initTree(t, BREAK_TAG);


	return t;

}

Tree newReturn(ExpTree expr)
{
	Tree t;
	ReturnTree p;

	t = poolAlloc(sizeof(struct ReturnTreeNode));
	initTree(t, RETURN_TAG);

	p = (ReturnTree) t;

	p->expr = expr;

	return t;

}

Tree newError(void)
{
	Tree t;

	t = poolAlloc(sizeof(struct ErrorTreeNode));
	initTree(t, ERROR_TAG);


	return t;

}


void printTree(Tree t)
{
	UnopTree u;
	BinopTree b;
	TriopTree tr;
	FcallTree f;
	EseqTree e;
	PtrTree ptr;
	FieldRefTree fr;
	CastopTree c;
	VarTree v;
	IntConstTree i;
	FpConstTree fp;
	StringLitTree s;
	LongConstTree l;
	UIntConstTree ui;
	ULongConstTree ul;
	LabelTree la;
	CaseTree ca;
	DefaultTree d;
	SeqTree se;
	IfElseTree ie;
	SwitchTree sw;
	WhileTree w;
	DoWhileTree dw;
	ForTree fo;
	GotoTree g;
	ReturnTree r;

	char *p;

	if (t == NULL) {
		return;

	}

	switch (t->tag) {

	case UNOP_TAG:
		u = (UnopTree) t;
		p = (char *) TreeOptoString(u->op);
		msgn("UNOP %s (", p);
		printType(u->expn.type);
		msgn(", ");
		printReg(u->expn.reg);
		msg(")");
		printTree((Tree) u->left);
		break;

	case BINOP_TAG:
		b = (BinopTree) t;
		p = (char *) TreeOptoString(b->op);
		msgn("BINOP %s (", p);
		printType(b->expn.type);
		msgn(", ");
		printReg(b->expn.reg);
		msg(")");
		printTree((Tree) b->left);
		printTree((Tree) b->right);
		break;

	case TRIOP_TAG:
		tr = (TriopTree) t;
		p = (char *) TreeOptoString(tr->op);
		msgn("TRIOP %s (", p);
		printType(tr->expn.type);
		msgn(", ");
		printReg(tr->expn.reg);
		msg(")");
		printTree((Tree) tr->left);
		printTree((Tree) tr->middle);
		printTree((Tree) tr->right);
		break;

	case FCALL_TAG:
		f = (FcallTree) t;
		msgn("FCALL (");
		printType(f->expn.type);
		msgn(", ");
		printReg(f->expn.reg);
		msg(")");
		printTree((Tree) f->func);
		printTree((Tree) f->params);
		break;

	case ESEQ_TAG:
		e = (EseqTree) t;
		msgn("ESEQ (");
		printType(e->expn.type);
		msgn(", ");
		printReg(e->expn.reg);
		msg(")");
		printTree((Tree) e->left);
		printTree((Tree) e->right);
		break;

	case PTR_TAG:
		ptr = (PtrTree) t;
		p = (char *) st_get_id_str(ptr->id);
		msgn("PTR %s (", p);
		printType(ptr->expn.type);
		msgn(", ");
		printReg(ptr->expn.reg);
		msg(")");
		printTree((Tree) ptr->left);
		break;

	case FIELDREF_TAG:
		fr = (FieldRefTree) t;
		p = (char *) st_get_id_str(fr->id);
		msgn("FIELDREF %s (", p);
		printType(fr->expn.type);
		msgn(", ");
		printReg(fr->expn.reg);
		msg(")");
		printTree((Tree) fr->left);
		break;

	case CASTOP_TAG:
		c = (CastopTree) t;
		msgn("CASTOP ");
		printType(c->typename);
		msgn(" (");
		printType(c->expn.type);
		msgn(", ");
		printReg(c->expn.reg);
		msg(")");
		printTree((Tree) c->left);
		break;

	case VAR_TAG:
		v = (VarTree) t;
		p = (char *) st_get_id_str(v->name);
		msgn("VAR %s (", p);
		printType(v->expn.type);
		msgn(", ");
		printClass(v->class);
		msgn(", ");
		printReg(v->expn.reg);
		msgn(", block = %d, offset = %d, staticLabel = %d, stdrTag = ", v->block, v->offset, v->staticLabel);
		stdr_print_tag(v->stdrTag);
		msg(")");
		break;

	case INT_CONST_TAG:
		i = (IntConstTree) t;
		msgn("INTCONST %d (", i->value);
		printType(i->expn.type);
		msgn(", ");
		printReg(i->expn.reg);
		msg(")");
		break;

	case FP_CONST_TAG:
		fp = (FpConstTree) t;
		p = fp->value;
		msgn("FPCONST %s (", p);
		printType(fp->expn.type);
		msgn(", ");
		printReg(fp->expn.reg);
		msg(")");
		break;

	case STRING_LIT_TAG:
		s = (StringLitTree) t;
		p = s->string;
		msgn("STRINGLIT %s (", p);
		printType(s->expn.type);
		msgn(", ");
		printReg(s->expn.reg);
		msg(")");
		break;

	case LONG_CONST_TAG:
		l = (LongConstTree) t;
		msgn("LONGCONST %ld (", l->value);
		printType(l->expn.type);
		msgn(", ");
		printReg(l->expn.reg);
		msg(")");
		break;

	case UINT_CONST_TAG:
		ui = (UIntConstTree) t;
		msgn("UINTCONST %d (", ui->value);
		printType(ui->expn.type);
		msgn(", ");
		printReg(ui->expn.reg);
		msg(")");
		break;

	case ULONG_CONST_TAG:
		ul = (ULongConstTree) t;
		msgn("ULONGCONST %ld (", ul->value);
		printType(ul->expn.type);
		msgn(", ");
		printReg(ul->expn.reg);
		msg(")");
		break;

	case LABEL_TAG:
		la = (LabelTree) t;
		p = (char *) st_get_id_str(la->label);
		msg("LABEL %s", p);
		printTree(la->left);
		break;

	case CASE_TAG:
		ca = (CaseTree) t;
		msg("CASE");
		printTree((Tree) ca->caseexpr);
		printTree(ca->statement);
		break;

	case DEFAULT_TAG:
		d = (DefaultTree) t;
		msg("DEFAULT");
		printTree(d->statement);
		break;

	case SEQ_TAG:
		se = (SeqTree) t;
		msg("SEQ");
		printTree(se->left);
		printTree(se->right);
		break;

	case IFELSE_TAG:
		ie = (IfElseTree) t;
		msg("IFELSE");
		printTree((Tree) ie->expr);
		printTree(ie->ifstmt);
		printTree(ie->elsestmt);
		break;

	case SWITCH_TAG:
		sw = (SwitchTree) t;
		msg("SWITCH");
		printTree((Tree) sw->expr);
		printTree(sw->statement);
		break;

	case WHILE_TAG:
		w = (WhileTree) t;
		msg("WHILE");
		printTree((Tree) w->expr);
		printTree(w->statement);
		break;

	case DOWHILE_TAG:
		dw = (DoWhileTree) t;
		msg("DOWHILE");
		printTree(dw->statement);
		printTree((Tree) dw->expr);
		break;

	case FOR_TAG:
		fo = (ForTree) t;
		msg("FOR");
		printTree((Tree) fo->expr1);
		printTree((Tree) fo->expr2);
		printTree((Tree) fo->expr3);
		printTree(fo->statement);
		break;

	case GOTO_TAG:
		g = (GotoTree) t;
		p = (char *) st_get_id_str(g->label);
		msg("GOTO %s", p);
		break;

	case CONTINUE_TAG:
		msg("CONTINUE");
		break;

	case BREAK_TAG:
		msg("BREAK");
		break;

	case RETURN_TAG:
		r = (ReturnTree) t;
		msg("RETURN");
		printTree((Tree) r->expr);
		break;

	case ERROR_TAG:
		msg("ERROR");
		break;

	default:
		msg("Invalid tree tag in printTree");

	}

}

char *TreeOptoString(TreeOp t)
{
	char *p;

	switch (t) {

	case ADD_OP:
		p = "ADD";
		break;

	case CONVERT_OP:
		p = "CONVERT";
		break;

	case INDEX_OP:
		p = "INDEX";
		break;

	case INCRA_OP:
		p = "INCRA";
		break;

	case DECRA_OP:
		p = "DECRA";
		break;

	case INCRB_OP:
		p = "INCRB";
		break;

	case DECRB_OP:
		p = "DECRB";
		break;

	case SIZEOF_OP:
		p = "SIZEOF";
		break;

	case MULT_OP:
		p = "MULT";
		break;

	case DIV_OP:
		p = "DIV";
		break;

	case MOD_OP:
		p = "MOD";
		break;

	case SUB_OP:
		p = "SUB";
		break;

	case LSHIFT_OP:
		p = "LSHIFT";
		break;

	case RSHIFT_OP:
		p = "RSHIFT";
		break;

	case LT_OP:
		p = "LT";
		break;

	case GT_OP:
		p = "GT";
		break;

	case LTE_OP:
		p = "LTE";
		break;

	case GTE_OP:
		p = "GTE";
		break;

	case EQL_OP:
		p = "EQ";
		break;

	case NEQL_OP:
		p = "NEQ";
		break;

	case ANDD_OP:
		p = "AND";
		break;

	case XORR_OP:
		p = "XOR";
		break;

	case ORR_OP:
		p = "OR";
		break;

	case LAND_OP:
		p = "LAND";
		break;

	case LOR_OP:
		p = "LOR";
		break;

	case COND_OP:
		p = "COND";
		break;

	case ASSIGN_OP:
		p = "ASSIGN";
		break;

	case MULT_ASSIGN_OP:
		p = "MULT ASSIGN";
		break;

	case DIV_ASSIGN_OP:
		p = "DIV ASSIGN";
		break;

	case MOD_ASSIGN_OP:
		p = "MOD ASSIGN";
		break;

	case ADD_ASSIGN_OP:
		p = "ADD ASSIGN";
		break;

	case SUB_ASSIGN_OP:
		p = "SUB ASSIGN";
		break;

	case LEFT_ASSIGN_OP:
		p = "LEFT ASSIGN";
		break;

	case RIGHT_ASSIGN_OP:
		p = "RIGHT ASSIGN";
		break;

	case AND_ASSIGN_OP:
		p = "AND ASSIGN";
		break;

	case XOR_ASSIGN_OP:
		p = "XOR ASSIGN";
		break;

	case OR_ASSIGN_OP:
		p = "OR ASSIGN";
		break;

	case ADDR_OF_OP:
		p = "ADDR OF";
		break;

	case DEREF_OP:
		p = "DEREF";
		break;

	case UADD_OP:
		p = "UADD";
		break;

	case USUB_OP:
		p = "USUB";
		break;

	case COMPLMT_OP:
		p = "COMPLMT";
		break;

	case LNOT_OP:
		p = "LNOT";
		break;

	case COMMA_OP:
		p = "COMMA";
		break;

	case NO_OP:
		p = "NO_OP";
		break;

	case DUP_OP:
		p = "DUP_OP";
		break;

	default:
		p = NULL;
		break;

	}

	return p;

}

BOOLEAN isExpTree(Tree t)
{
	if (t == NULL)
		return FALSE;

	switch (t->tag) {

	case UNOP_TAG:
	case BINOP_TAG:
	case TRIOP_TAG:
	case FCALL_TAG:
	case ESEQ_TAG:
	case PTR_TAG:
	case FIELDREF_TAG:
	case CASTOP_TAG:
	case VAR_TAG:
	case INT_CONST_TAG:
	case FP_CONST_TAG:
	case STRING_LIT_TAG:
	case LONG_CONST_TAG:
	case UINT_CONST_TAG:
	case ULONG_CONST_TAG:
		return TRUE;

	case LABEL_TAG:
	case CASE_TAG:
	case DEFAULT_TAG:
	case SEQ_TAG:
	case IFELSE_TAG:
	case SWITCH_TAG:
	case WHILE_TAG:
	case DOWHILE_TAG:
	case FOR_TAG:
	case GOTO_TAG:
	case CONTINUE_TAG:
	case BREAK_TAG:
	case RETURN_TAG:
	case ERROR_TAG:
		return FALSE;

	default:
		bug("Invalid tree tag in isExpTree");
	}

	return FALSE;
/*NOTREACHED*/}

BOOLEAN isAssignmentOperator(TreeOp op)
{
	switch (op) {
	case INCRA_OP:
	case DECRA_OP:
	case INCRB_OP:
	case DECRB_OP:
	case ASSIGN_OP:
	case MULT_ASSIGN_OP:
	case DIV_ASSIGN_OP:
	case MOD_ASSIGN_OP:
	case ADD_ASSIGN_OP:
	case SUB_ASSIGN_OP:
	case LEFT_ASSIGN_OP:
	case RIGHT_ASSIGN_OP:
	case AND_ASSIGN_OP:
	case XOR_ASSIGN_OP:
	case OR_ASSIGN_OP:
		return TRUE;
	default:
		return FALSE;
	}

	/* can't reach */
	return FALSE;
}

/**
 * Returns true if the given TreeTag is for a constant expression.
 */
BOOLEAN isConstTag(TreeTag t)
{
	return t == INT_CONST_TAG
	    || t == FP_CONST_TAG
	    || t == LONG_CONST_TAG
	    || t == UINT_CONST_TAG || t == ULONG_CONST_TAG;
}

/**
 * Returns true if the given operation is evaluatable at compile-time
 * for constant expressions.
 */
BOOLEAN isCompileTimeEvalable(TreeOp op)
{
	return
	    /* unary operations */
	    op == LNOT_OP
	    || op == CONVERT_OP
	    || op == UADD_OP || op == USUB_OP || op == COMPLMT_OP
	    /* binary operations */
	    || op == INDEX_OP
	    || op == LSHIFT_OP
	    || op == RSHIFT_OP
	    || op == ANDD_OP
	    || op == XORR_OP
	    || op == ORR_OP
	    || op == MULT_OP
	    || op == DIV_OP
	    || op == MOD_OP
	    || op == ADD_OP
	    || op == SUB_OP
	    || op == LT_OP
	    || op == GT_OP
	    || op == LTE_OP
	    || op == GTE_OP
	    || op == EQL_OP
	    || op == NEQL_OP || op == LAND_OP || op == LOR_OP;
}
