/*
 *
 *  analyze.h - semantic analysis
 *
 *  The driver is a recursive routine called analyze that traverses the AST
 *  for a function definition in (basically) a bottom-up fashion.
 */
#ifndef ANALYZE_H
#define ANALYZE_H

#include "defs.h"
#include "tree.h"

/* perform semantic analysis on a "generic" tree */
Tree analyze(Tree t);

/* use to analyze compound assignment operators */
ExpTree analyzeAssign(BinopTree b);

/* convert func/array operand node type to ptr */
void promoteFunctionOrArrayTree(ExpTree e);

/* analyzeUnop: semantic analysis for unary operators */
ExpTree analyzeUnop(UnopTree u);

/* worker routines to be implemented in phase 3 */

ExpTree analyzeAdd(BinopTree b);	/* 70% level */
ExpTree analyzeSub(BinopTree b);
ExpTree analyzeDiv(BinopTree b);
ExpTree analyzeMult(BinopTree b);
ExpTree analyzeMod(BinopTree b);
ExpTree analyzeMultAssign(BinopTree b);
ExpTree analyzeDivAssign(BinopTree b);
ExpTree analyzeModAssign(BinopTree b);
ExpTree analyzeAddAssign(BinopTree b);
ExpTree analyzeSubAssign(BinopTree b);
ExpTree analyzeUsub(UnopTree u);
ExpTree analyzeUadd(UnopTree u);

ExpTree analyzeLnot(UnopTree b);	/* 80% level */
ExpTree analyzeLt(BinopTree b);
ExpTree analyzeGt(BinopTree b);
ExpTree analyzeLte(BinopTree b);
ExpTree analyzeGte(BinopTree u);
ExpTree analyzeEql(BinopTree u);
ExpTree analyzeNeql(BinopTree u);
ExpTree analyzeLand(BinopTree b);
ExpTree analyzeLor(BinopTree b);

ExpTree analyzeComplmt(UnopTree u);	/* 90% level */
ExpTree analyzeLshift(BinopTree b);
ExpTree analyzeRshift(BinopTree b);
ExpTree analyzeAndd(BinopTree b);
ExpTree analyzeXorr(BinopTree b);
ExpTree analyzeOrr(BinopTree b);
ExpTree analyzeLeftAssign(BinopTree b);
ExpTree analyzeRightAssign(BinopTree b);
ExpTree analyzeAndAssign(BinopTree b);
ExpTree analyzeOrAssign(BinopTree b);
ExpTree analyzeXorAssign(BinopTree b);

ExpTree analyzeIncra(UnopTree u);	/* 100% level */
ExpTree analyzeIncrb(UnopTree u);
ExpTree analyzeDecra(UnopTree u);
ExpTree analyzeDecrb(UnopTree u);
ExpTree analyzeComma(BinopTree b);
ExpTree analyzeCond(TriopTree t);
ExpTree analyzeCast(CastopTree t);
ExpTree analyzeSizeof(UnopTree u);

/* worker routines to be implemented in phase 4 */

void beforeAnalyze(void);
void afterAnalyze(void);

Tree analyzeWhile(WhileTree w);	/* 70% level */
Tree analyzeIfElse(IfElseTree w);
Tree analyzeBreak(BreakTree br);
Tree analyzeContinue(ContinueTree co);

Tree analyzeFor(ForTree w);	/* 85% level */
Tree analyzeDoWhile(DoWhileTree dw);

Tree analyzeLabel(LabelTree l);	/* 100% level */
Tree analyzeCase(CaseTree ca);
Tree analyzeDefault(DefaultTree d);
Tree analyzeSwitch(SwitchTree sw);
Tree analyzeGoto(GotoTree g);

/* worker routines to be implemented in phase 5 */

ExpTree analyzeAddrOf(UnopTree u);
ExpTree analyzeIndex(BinopTree b);
ExpTree analyzePtr(PtrTree p);
ExpTree analyzeFieldRef(FieldRefTree f);

#endif
