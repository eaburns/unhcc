/*
 * encode.h - code generation interface
 *
 */

#ifndef ENCODE_H
#define ENCODE_H

/* comment this line out to disable commenting of ouput code blocks */
#if 1
#define COMMENT_CODE
#endif

#include "defs.h"
#include "types.h"
#include "tree.h"
#include "regAlloc.h"

/* the size of the locals area on the stack */
extern int sizeOfLocalsArea;

/* initialize the code generator */
void encode_init(void);

/* generate a unique label number */
unsigned int genLabel(void);

/* comment a piece of code */
void comment(char *fmt, ...);
/* a lesser-comment for a piece of code */
void comment1(char *fmt, ...);

/* emit one line to assembly language output file */
void emit(char *fmt, ...);

/* emit a piece of a line (no newline output) */
void emitn(char *fmt, ...);

/* emits a label */
void emitLabel(const char *fmt, ...);

/* emits a section change */
void emitSection(const char *);

/* marks the allocation and deallocation of local variable */
void emitAllocLocals(unsigned int);
void emitDeallocLocals(unsigned int);

/* emits a branch */
void emitCondBranch(const char *mnemonic, const char *fmt, ...);
void emitUncondBranch(const char *mnemonic, const char *fmt, ...);
void emitReturn(const char *fmt, ...);

/*
 * Returns the size of the parameter list
 */
unsigned int paramListSize(ParamList params);

/*
 * Functions for emitting code that manipulates the stack pointer.
 * These functions are necessary to keep track of the stack depth so
 * that function calls can have a 16-byte alligned stack.
 */
void emit_push_const(unsigned long int val);
void emit_push_reg(const char *reg);
void emit_pop_reg(const char *reg);
void emit_stack_grow(unsigned int size);
void emit_stack_shrink(unsigned int size);

/* generate code for one global variable given its name and type */
void encode_global_var_decl(ST_ID id, Type type);

/* generate code for one static global variable given its name and type */
void encode_static_var_decl(ST_ID id, Type type);

/* generate code for local static variables */
void encode_local_static(ST_ID id, unsigned int label, Type type);

/* compute the size in bytes for the given Type */
int computeSize(Type type);

/* return the size of the given type tag: this gives
 * TARCH_SIZEOF_POINTER for aggregates. */
int typeSize(TypeTag tag);

/* compute the alignment for the given Type t*/
int determineAlignment(Type type);

/* need to know worst case alignment to pad structs */
int worstCaseAlignment(void);

/* compute the size of a parameter */
int computeParamSize(Type);

/* is the given Type deref-able on the target machine? */
BOOLEAN isDerefable(Type t);

/* generate code for an expression or statement */
void encode(Tree t);

/* generate code for function entry */
void encodeFunctionEntry(ST_ID name, int localsSize, int savedRegsSize,
			 int argBuildSize, BOOLEAN isStatic);

/* generate code for function exit */
void encodeFunctionExit(ST_ID name, int savedRegsSize, int argBuildSize);

/* need to adjust running offset as shift from params to locals? */
int adjustFunctionOffset(int currentOffset);

/* compute size of a struct/union type */
int computeStructSize(Type);
int computeUnionSize(Type);

/* compute offset for a member of a struct/union type */
int computeMemberOffset(Type type, ST_ID fieldName);

/* given C type determine corresponding IA-32 type */
char determineIA32Type(Type t);

/* function and parameter handling: does nothing on IA-32 */
void encodeMoveParametersToLocalSlots(ParamList list, Tree t);

/* make available DEREF and ASSIGN details available for +=, -=, etc */
void encodeAssignDetail(Type type, Tree t);
void encodeDerefDetail(Type type, Tree t);

/* helper function for freeing registers at top of trees */
void freeExpressionStatementRegister(Tree t);

/* worker routines to be implemented in phase3 */

void encodeAdd(BinopTree b);	/* 70% level */
void encodeSub(BinopTree b);
void encodeDiv(BinopTree b);
void encodeMult(BinopTree b);
void encodeMod(BinopTree b);
void encodeMultAssign(BinopTree b);
void encodeDivAssign(BinopTree b);
void encodeModAssign(BinopTree b);
void encodeAddAssign(BinopTree b);
void encodeSubAssign(BinopTree b);
void encodeUsub(UnopTree b);

void encodeLnot(UnopTree b);	/* 80% level */
void encodeLt(BinopTree b);
void encodeGt(BinopTree b);
void encodeLte(BinopTree b);
void encodeGte(BinopTree b);
void encodeEql(BinopTree b);
void encodeNeql(BinopTree b);
void encodeLand(BinopTree b);
void encodeLor(BinopTree b);

void encodeComplmt(UnopTree u);	/* 90% level */
void encodeLshift(BinopTree b);
void encodeRshift(BinopTree b);
void encodeAndd(BinopTree b);
void encodeXorr(BinopTree b);
void encodeOrr(BinopTree b);
void encodeLeftAssign(BinopTree b);
void encodeRightAssign(BinopTree b);
void encodeAndAssign(BinopTree b);
void encodeOrAssign(BinopTree b);
void encodeXorAssign(BinopTree b);

void encodeIncra(UnopTree b);	/* 100% level */
void encodeIncrb(UnopTree b);
void encodeDecra(UnopTree b);
void encodeDecrb(UnopTree b);
void encodeComma(BinopTree b);
void encodeCond(TriopTree t);
void encodeCast(CastopTree t);

/* worker routines to be implemented in phase4 */

void beforeEncode(void);
void afterEncode(void);

void encodeWhile(WhileTree w);	/* 70% level */
void encodeIfElse(IfElseTree w);
void encodeBreak(BreakTree br);
void encodeContinue(ContinueTree co);

void encodeFor(ForTree w);	/* 85% level */
void encodeDoWhile(DoWhileTree dw);

void encodeLabel(LabelTree l);	/* 100% level */
void encodeCase(CaseTree ca);
void encodeDefault(DefaultTree d);
void encodeSwitch(SwitchTree sw);
void encodeGoto(GotoTree g);

/* worker routines to be implemented in phase5 */

void encodeAddrOf(UnopTree u);
void encodeIndex(BinopTree b);
void encodeFieldRef(FieldRefTree f);
void encodePtr(PtrTree p);

#endif
