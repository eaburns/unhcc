/*
 *  decl.c: code for processing declarations
 *
 */
#include "decl.h"
#include "decl2.h"
#include "types.h"
#include "bucket.h"
#include "globals.h"
#include "message.h"
#include "semUtils.h"
#include "encode.h"

/* global used in processing typedefs */
GP_SWITCH g_id_lookup;

/*
 *  newPointerDeclNode: constructor for PTR DECL_NODE
 */
DECL_TREE newPointerDeclNode(TypeQualifier qual, DECL_TREE tree)
{
	DECL_TREE ret;

	ret = malloc(sizeof(DECL_NODE));
	ret->tag = DECL_PTR;
	ret->u.qual = qual;
	ret->next = tree;

	return ret;
}

/*
 *  newArrayDeclNode: constructor for ARRAY DECL_NODE
 */
DECL_TREE newArrayDeclNode(int dim, DECL_TREE tree)
{
	DECL_TREE ret;

	ret = malloc(sizeof(DECL_NODE));
	ret->tag = DECL_ARR;
	ret->u.dim = dim;
	ret->next = tree;

	return ret;
}

/*
 *  newFuncDeclNode: constructor for FUNC DECL_NODE
 */
DECL_TREE newFuncDeclNode(ParamStyle style, ParamList list,
			  DECL_TREE tree)
{
	DECL_TREE ret;

	ret = malloc(sizeof(DECL_NODE));
	ret->tag = DECL_FUNC;
	ret->u.paramInfo.paramstyle = style;
	ret->u.paramInfo.params = list;
	ret->next = tree;

	return ret;
}

/*
 *  newIdDeclNode: constructor for ID DECL_NODE
 */
DECL_TREE newIdDeclNode(ST_ID id)
{
	DECL_TREE ret;

	ret = malloc(sizeof(DECL_NODE));
	ret->tag = DECL_ID;
	ret->u.id = id;
	ret->next = NULL;

	return ret;
}

void deleteDeclNode(DECL_NODE * n)
{
	free(n);
}

/*  addPointersToDeclTree
 *
 *  Series of pointer DECL_NODEs are placed on top of a subtree.
 */
DECL_TREE addPointersToDeclTree(DECL_TREE pointers, DECL_TREE subtree)
{
	DECL_TREE p;

	p = pointers;
	while (p->next != NULL)
		p = p->next;

	p->next = subtree;

	return pointers;
}

/*
 *  newDeclarator
 *
 *  Link a new DECLARATOR node onto the front of a list of DECLARATOR nodes.
 */
DECLARATOR_LIST newDeclarator(DECL_TREE tree, DECLARATOR_LIST list)
{
	DECLARATOR_LIST ret;

	ret = malloc(sizeof(DECLARATOR));
	ret->tree = tree;
	ret->next = list;

	return ret;
}

void deleteDeclarator(DECLARATOR * d)
{
	free(d);
}

/*
 *  convertSpecifierToQualifier
 *
 *  Used in processing of qualifiers attached to a pointer.
 */
TypeQualifier convertSpecifierToQualifier(TYPE_SPECIFIER spec)
{
	switch (spec) {

	case CONST_SPEC:
		return CONST_QUAL;

	case VOLATILE_SPEC:
		return VOLATILE_QUAL;

	default:
		bug("specifier not a qualifier in convertSpecifierToQualifier");
		return NO_QUAL;
	/*NOTREACHED*/}
}

/*
 *  addQualifierToList
 *
 *  Used in processing of qualifiers attached to a pointer. The "list"
 *  is not really a list as we can accumulate the qualifiers using
 *  the values of the TypeQualifier enum, because it includes a
 *  CONST_VOLATILE_QUAL value.
 *
 *  Shortcoming: there is currently no way to track that an error
 *  occurred in the qualifier list. So the duplicate qualifier simply
 *  gets discarded. The symbol table is not marked (err field of ST_DR).
 *  This could lead to unnecessary, extra error messages in cases like:
 *    extern int * volatile const p;
 *    int * volatile volatile p;
 */
TypeQualifier addQualifierToList(TypeQualifier list, TypeQualifier qual)
{
	if (list == CONST_VOLATILE_QUAL) {
		error("duplicate pointer qualifier");
		return list;
	}
	if (list == NO_QUAL) {
		bug("list is NO_QUAL in addQualifierToList");
		return list;
	/*NOTREACHED*/}

	switch (qual) {

	case CONST_QUAL:
		if (list == CONST_QUAL) {
			error("duplicate qualifier");
			return list;
		} else {	/* list must be VOLATILE_QUAL */

			return CONST_VOLATILE_QUAL;
		}

	case VOLATILE_QUAL:
		if (list == VOLATILE_QUAL) {
			error("duplicate qualifier");
			return list;
		} else {	/* list must be CONST_QUAL */

			return CONST_VOLATILE_QUAL;
		}

	default:
		bug("unexpected qualifier seen in addQualifierToList()");
		return list;
	/*NOTREACHED*/}

	return list;
/*NOTREACHED*/}

/* forward references */
static BOOLEAN alreadyDefined(ST_ID id, MemberList mem_list);
static Type tagInstall(ST_ID id, TYPE_SPECIFIER t_spec);
static BOOLEAN sameType(TYPE_SPECIFIER t_spec, ST_DR st_dr);
static BOOLEAN hasMemberList(ST_DR st_dr);
static void installTag(BUCKET_PTR bucket);

/*
 *  Compute offset for a member of a struct/union type.
 *
 *  Should return -1 if member name not found in struct/union type.
 *
 */
int computeMemberOffset(Type type, ST_ID memberName)
{
	MemberList member_list;
	int offset;
	int align;

	member_list = NULL;
	offset = 0;

	if (typeQuery(type) != TYSTRUCT && typeQuery(type) != TYUNION) {
		bug("computeMemberOffset called for non-struct/union type");
		return -1;
	}

	member_list = typeRetrieveMembers(type);

	while ((member_list != NULL) &&
	       strcmp(st_get_id_str(memberName),
		      st_get_id_str(member_list->id)
	       )
	    ) {			/* not equal */
		/* nothing to do for union type: offset is always zero */

		if (typeQuery(type) == TYSTRUCT) {
			offset += computeSize(member_list->type);

			/* determine alignment */
			if (member_list->next != NULL) {
				align =
				    determineAlignment(member_list->
						       next->type);
				while (offset % align)
					offset += 1;
			}	/* end of if */
		}

		member_list = member_list->next;

	}			/* end of while */

	if (member_list != NULL)
		return offset;
	return -1;		/* member not found in the struct */

}				/* end of computeMemberOffset */


MY_SPECIFIER newSpecifierObject(TYPE_SPECIFIER type_spec)
{
	MY_SPECIFIER my_spec;

	my_spec.type_spec = type_spec;
	my_spec.type = NULL;
	my_spec.err = FALSE;
	return my_spec;

}

/* this functions works in the way similar to processDeclaration */
MemberList createMemberList(BUCKET_PTR spec_qual_list,
			    DECLARATOR_LIST d_list)
{
	BOOLEAN bucket_error;
	BOOLEAN found_error;
	ST_ID id;

	Type baseType, temp_type;
	DECLARATOR_LIST decl_list, nextDeclarator;

	StorageClass t_class;

	MemberList member;
	MemberList member_list;

	member = NULL;
	member_list = NULL;
	bucket_error = FALSE;
	found_error = FALSE;
	bucket_error = is_error_decl(spec_qual_list);
	t_class = get_class(spec_qual_list);
	baseType = build_base(spec_qual_list);

	/* have a list of declarators: process one at a time */
	decl_list = d_list;

	while (decl_list != NULL) {
		id = NULL;
		found_error = FALSE;
		temp_type = processDeclarator(decl_list->tree, baseType,
					      &id, &found_error, NULL);


		if (id == NULL) {
			bug("createMemberList: processDeclarator did not return an id!");
		}

		/* check whether member id has already existed */
		if (alreadyDefined(id, member_list)) {
			found_error = TRUE;
			error("struct/union member %s redefined",
			      st_get_id_str(id));
		}

		/* can't have member of void type */
		if (!bucket_error && !found_error
		    && typeQuery(temp_type) == TYVOID) {
			error("member cannot have void type");
			found_error = TRUE;
		}

		/* can't have member of function or incomplete type */
		if (!bucket_error && !found_error
		    && !isCompleteObjectType(temp_type)) {
			error("member must have complete object type");
			found_error = TRUE;
		}

		/* install the id and type to the member */
		member = (MemberList) malloc(sizeof(Member));
		member->id = id;
		member->type = temp_type;
		member->sc = t_class;
		member->err = (found_error || bucket_error);
		member->next = member_list;
		member->prev = NULL;
		if (member_list != NULL)
			member_list->prev = member;
		member_list = member;

		/* cleaning up as we go on */
		nextDeclarator = decl_list->next;
		deleteDeclarator(decl_list);
		decl_list = nextDeclarator;

	}			/* end of outer while loop */

	return member_list;

}

/*****************************************************************************/
/* function called by createMemberList */
static BOOLEAN alreadyDefined(ST_ID id, MemberList mem_list)
{
	MemberList one_member;

	one_member = mem_list;
	while (one_member != NULL) {
		if (!strcmp(st_get_id_str(id),
			    st_get_id_str(one_member->id)))
			return TRUE;
		one_member = one_member->next;
	}

	return FALSE;
}				/* end of alreadyDefined */


/*****************************************************************************/
/* function to merge struct_declarator and struct_declarator_list */

MemberList mergeMemberList(MemberList list1, MemberList list2)
{
	MemberList temp_list;

	temp_list = list1;

	if (temp_list == NULL)
		return list2;

	/* check for duplicates when merging */
	while (temp_list->next != NULL) {
		if (alreadyDefined(temp_list->id, list2)) {
			error("struct/union member %s redefined",
			      st_get_id_str(temp_list->id));
			temp_list->err = TRUE;
		}
		temp_list = temp_list->next;
	}

	if (alreadyDefined(temp_list->id, list2)) {
		error("struct/union member %s redefined",
		      st_get_id_str(temp_list->id));
		temp_list->err = TRUE;
	}

	temp_list->next = list2;

	return list1;
}				/* end of mergeMemberList */

/*****************************************************************************/
/* function to install tag to symtab where the struct has memberlist attached */
MY_SPECIFIER *installTagWhenMemberListComing(ST_ID id,
					     TYPE_SPECIFIER t_spec)
{
	Type type;
	ST_DR old_st_dr;
	MY_SPECIFIER *my_specifier;
	int block;
	BOOLEAN err;

	err = FALSE;

    /**first check whether this tag has been installed before ***/
	old_st_dr = st_tag_lookup(id, &block);

	if (old_st_dr == NULL) {	/* id never installed before */
		type = tagInstall(id, t_spec);
	} else
		/* tag installed before */
	if (block != st_get_cur_block()) {
		/* id not installed in this BLOCK before */ type =
		    tagInstall(id, t_spec);
	} else
		/* id installed in current block */
	if (!sameType(t_spec, old_st_dr)) {
		error("duplicate tag definition");
		err = TRUE;
		/* give it a NULL struct type to avoid segfault */
		if (t_spec == STRUCT_SPEC)
			type = typeBuildStruct(NULL, NULL);
		else
			type = typeBuildUnion(NULL, NULL);
	} else
		/* same type tag defined before */
	if (hasMemberList(old_st_dr)) {
		/* complete tag defined before */
		error("struct/union tag defined in this block before");
		err = TRUE;
		/* give it a NULL struct type to avoid segfault */
		if (t_spec == STRUCT_SPEC)
			type = typeBuildStruct(NULL, NULL);
		else
			type = typeBuildUnion(NULL, NULL);
	} else
		/* incomplete tag defined before */
		/* get the type installed in the st_dr */
		type = old_st_dr->u.stag.type;

	my_specifier = (MY_SPECIFIER *) malloc(sizeof(MY_SPECIFIER));
	my_specifier->type_spec = t_spec;
	my_specifier->type = type;
	my_specifier->err = err;

	return my_specifier;

}				/* end of tagWithMemList */


/*
 *  Build a struct/union type and then place it in a MY_SPECIFIER.
 *
 *  Note: tag is NOT installed now.
 */
MY_SPECIFIER *buildStructUnionType(ST_ID id, TYPE_SPECIFIER t_spec,
				   MemberList mem_list)
{

	Type type;
	ST_DR st_dr;
	MY_SPECIFIER *my_specifier;
	int block;

	my_specifier = (MY_SPECIFIER *) malloc(sizeof(MY_SPECIFIER));
	my_specifier->type_spec = t_spec;
	my_specifier->err = FALSE;

	/* if tag is not NULL, is tag already defined? */
	if (id != NULL)
		st_dr = st_tag_lookup(id, &block);
	if (id != NULL && st_dr != NULL) {
		TypeTag tag;
		type = st_dr->u.stag.type;
		tag = typeQuery(type);
		if ((tag == TYSTRUCT && t_spec != STRUCT_SPEC) || (tag == TYUNION && t_spec != UNION_SPEC) || (tag == TYSIGNEDINT	/* ie 
																	   TYENUM 
																	 */
													       ))
		{
			error("illegal struct/union tag reference");
			my_specifier->err = TRUE;
		}
	} else {
		switch (t_spec) {
		case STRUCT_SPEC:
			type = typeBuildStruct(id, mem_list);
			break;

		case UNION_SPEC:
			type = typeBuildUnion(id, mem_list);
			break;

		default:
			bug("unknown type specifier in instantiateStructUnion");

		}		/* end of switch */
	}

	my_specifier->type = type;

	return my_specifier;
}


/*****************************************************************************/
/* function to attach Member list to struct/union Type */
void attachMemberList(MY_SPECIFIER my_spec, MemberList mem_list)
{
	/* here we should check whether struct is redefined by checking 
	   whether member list is attached twice or more */
	if (my_spec.type == NULL) {
		bug("type member is NULL in attachMemberList\n");
	}
	if (typeRetrieveMembers(my_spec.type) != NULL) {
		error("struct/union redefined");
		return;
	}
	/* 
	 *  mem_list might be NULL because of earlier error. if so,
	 *  don't try to attach because typeAssignMembers will bug out
	 */
	if (mem_list != NULL)
		typeAssignMembers(my_spec.type, mem_list);
	return;
}				/* end of attachMember */


/* function to compute the size of struct. */
int computeStructSize(Type type)
{
	MemberList mem_list;
	MemberList lastMember;
	int size;

	size = 0;

	if (typeQuery(type) != TYSTRUCT) {
		bug("non-struct type in structSize ");
	}
	mem_list = typeRetrieveMembers(type);

	/* the total size is the (offset+size) of the last member */
	if (mem_list == NULL) {
		error("unknown size for struct: %s",
		      st_get_id_str(typeRetrieveTagname(type)));
		return 0;
	}
	while (mem_list != NULL) {
		lastMember = mem_list;
		mem_list = mem_list->next;
	}			/* end of while */

	size = computeMemberOffset(type, lastMember->id);
	size += computeSize(lastMember->type);

	/* padding: in case of array case: struct a arr[ 100 ] */
	while ((size % worstCaseAlignment()))
		size++;

	return size;

}				/* end of structSize */


/* function to compute the size of union. */
int computeUnionSize(Type type)
{
	int size;
	int m_size;
	MemberList mem_list;

	size = 0;
	if (typeQuery(type) != TYUNION)
		bug("non-union type in unionSize");

	mem_list = typeRetrieveMembers(type);

	if (mem_list == NULL) {
		error("unknown size for union: %s",
		      st_get_id_str(typeRetrieveTagname(type)));
		return 0;
	}

	while (mem_list != NULL) {
		m_size = computeSize(mem_list->type);
		if (m_size > size)
			size = m_size;

		mem_list = mem_list->next;
	}			/* end of while */

	/* padding: in case of array case: union a arr[ 100 ] */
	while ((size % worstCaseAlignment()))
		size++;

	return size;
}				/* end of unionSize */


/*
 *  Process a bucket when the declaration has no declarator.
 *
 *  To be legal there must be some kind of side-effect, like a struct tag.
 *
 */
void declarationWithoutDeclarator(BUCKET_PTR bucket)
{
	ST_DR old_st_dr;
	ST_ID id;
	int block;

	/* must be struct/union/enum and therefore type in bucket must
	   be set */
	if (bucket->type == NULL) {
		error
		    ("declaration without declarator must be struct, union or enum");
		return;
	}

	/* enum type must have a side effect or its a syntax error */
	if (typeQuery(bucket->type) == TYSIGNEDINT)
		return;

	if (typeQuery(bucket->type) == TYERROR)
		return;

	if ((typeQuery(bucket->type) != TYSTRUCT) &&
	    (typeQuery(bucket->type) != TYUNION)) {
		error
		    ("declaration without declarator must be struct, union or enum");
		return;
	}

	/* is tag already defined? */
	id = typeRetrieveTagname(bucket->type);

	/* 
	 *  If no tag, then no side effect to declaration.
	 */
	if (id == NULL) {
		error("unnamed struct/union that defines no instances");
		return;
	}

	/* 
	 *  Doesn't make sense to have qualifier without declarator.
	 *  Issue warning because don't see an ANSI constraint that is violated.
	 */
	if (typeGetQualifier(build_base(bucket)) != NO_QUAL) {
		warning("useless type qualifier");
	}

	old_st_dr = st_tag_lookup(id, &block);

	/* 
	 *  handle: struct x;
	 *
	 *  problem is when the tag is defined at outer scope. need
	 *  to ignore that and create new struct/union for this scope.
	 *
	 *  type defined at outer scope, if there is one, is in bucket
	 */
	if ((old_st_dr != NULL) && (block != st_get_cur_block())) {
		/* need to make a new struct/union type */
		{
			if (typeQuery(bucket->type) == TYSTRUCT)
				bucket->type =
				    typeBuildStruct(id, NULL);
			else
				bucket->type = typeBuildUnion(id, NULL);
		}
	}

	if (typeRetrieveMembers(bucket->type) != NULL) {
		/* this case: struct/union a { int a; }; */
		return;
	}

	/* install tag in the symbol table */
	installTag(bucket);
}


/* function to check the tag in st_dr is the same as t_spec */
static BOOLEAN sameType(TYPE_SPECIFIER t_spec, ST_DR st_dr)
{
	if (st_dr == NULL)
		return FALSE;

	if (st_dr->tag != TAG)
		bug("Attempt to compare non tag ST_DR");

	switch (t_spec) {
	case STRUCT_SPEC:
		if (typeQuery(st_dr->u.stag.type) == TYSTRUCT)
			return TRUE;
		else
			return FALSE;

	case UNION_SPEC:
		if (typeQuery(st_dr->u.stag.type) == TYUNION)
			return TRUE;
		else
			return FALSE;

	default:
		bug("bad type specifier in sameType");
		return FALSE;

	}			/* end of switch */

}				/* end of sameType */


/*function to do something for struct/union tag before process declaration */
void beforeProcessDeclaration(BUCKET_PTR bucket)
{
	ST_DR old_st_dr;
	ST_ID id;
	int block;

	/* do nothing for non struct/union type */
	if (bucket->type == NULL)
		return;

	/* do nothing for typename */
	if (typename_in_bucket(bucket))
		return;

	/* do nothing for enums */
	if (typeQuery(bucket->type) == TYSIGNEDINT)
		return;

	/* is tag already defined? */
	id = typeRetrieveTagname(bucket->type);
	old_st_dr = st_tag_lookup(id, &block);

	/* 
	 * struct has member list
	 * case:  struct a { int a; float b; } p ;
	 * tag has already been process and installed: do nothing */
	if (typeRetrieveMembers(bucket->type) != NULL)
		return;

	/* struct doesn't have memberlist */
	/* case: struct a p; */
	/* first step: see whether tag is installed in symtab */

	/* struct/union a is never defined before */
	if (old_st_dr == NULL) {
		installTag(bucket);
		return;
	}

	/* structure/union is defined before */

	/* if they are both structs or both unions */

	if (areCompatibleTypes(bucket->type, old_st_dr->u.stag.type)) {
		return;
	}

	/* installed and current type are different */
	if (block == st_get_cur_block()) {
		error("tag redefined");
		set_bucket_err(bucket);
		return;
	}

	/* installed and current type tag are in different block */

	return;

}				/* end of before process Declaration */



/* function to install tag, given a bucket */
static void installTag(BUCKET_PTR bucket)
{
	/* install tag in the symbol table */
	ST_DR st_dr;
	ST_ID id;

	st_dr = stdr_alloc();
	st_dr->tag = TAG;
	st_dr->u.stag.type = bucket->type;
	id = typeRetrieveTagname(bucket->type);
	st_tag_install(id, st_dr);

	return;

}				/* end of installTag */


/* function to check whether struct ST_DR has member List attached  */
static BOOLEAN hasMemberList(ST_DR st_dr)
{
	/* non tag st_dr */
	if (st_dr->tag != TAG)
		return FALSE;

	/* non struct or union type */
	if ((typeQuery(st_dr->u.stag.type) != TYSTRUCT) &&
	    (typeQuery(st_dr->u.stag.type) != TYUNION))
		return FALSE;

	/* has member list attached */
	if (typeRetrieveMembers(st_dr->u.stag.type) != NULL)
		return TRUE;

	return FALSE;
}				/* end of hasMemberList */


/* function to install tag from st_id and type_specifier */
static Type tagInstall(ST_ID id, TYPE_SPECIFIER t_spec)
{
	ST_DR st_dr;
	Type type;

	switch (t_spec) {
	case STRUCT_SPEC:
		type = typeBuildStruct(id, NULL);
		/* member list will be attached later */
		break;

	case UNION_SPEC:
		type = typeBuildUnion(id, NULL);
		/* member list will be attached later */
		break;

	default:
		bug("unknown type specifier in tagInstall");

	}			/* end of switch */

	st_dr = stdr_alloc();
	st_dr->tag = TAG;
	st_dr->u.stag.type = type;
	st_tag_install(id, st_dr);

	return type;

}				/* end of tagInstall */

/* process declarator for function definition */
ParamList processFunctionDef(BUCKET_PTR bucket, DECL_TREE tree)
{
	ST_ID retID;
	ParamStyle tempStyle;
	ParamList retList;
	ParamList ignore;
	Type baseType;
	Type modType;
	BOOLEAN bucketError;
	BOOLEAN foundError;
	StorageClass class;
	BOOLEAN functionNodePresent;
	DECL_TREE tmp;

	bucketError = FALSE;
	foundError = FALSE;
	functionNodePresent = FALSE;

	/* must deal with struct/union tag if present */
	beforeProcessDeclaration(bucket);

	/* 
	 *  Function type cannot obtain its type category via a typedef. See
	 *  ANSI 3.7.1 Constraints (and footnote 79 on page 82). So there
	 *  must be a function node on the TREE_DECL.
	 */
	tmp = tree;
	while (tmp) {
		if (tmp->tag == DECL_FUNC) {
			functionNodePresent = TRUE;
			break;
		}
		if (tmp->tag == DECL_ID) {
			break;
		}
		tmp = tmp->next;
	}
	if (!functionNodePresent) {
		error
		    ("function definition must get its type category from declarator");
	}

	bucketError = is_error_decl(bucket);
	class = get_class(bucket);
	baseType = build_base(bucket);
	modType =
	    processDeclarator(tree, baseType, &retID, &foundError,
			      &retList);

	if (class == TYPEDEF_SC || class == AUTO_SC
	    || class == REGISTER_SC) {
		error
		    ("illegal storage class for a function definition");
		class = NO_SC;
	}

	if (typeQuery(modType) != TYFUNCTION) {
		error
		    ("function definition does not specify a function type");

		gCurrentFunctionReturnType =
		    typeBuildBasic(TYVOID, NO_QUAL);

		retList = NULL;
	} else {
		ParamStyle paramstyle;
		ParamList params;

		typeQueryFunction(modType, &paramstyle, &params);
		if (paramstyle == OLDSTYLE) {
			info("old style function definitions are not supported!");
		}
		finishGlobalDeclaration(retID, modType, class,
					bucketError
					|| foundError, TRUE);

		gCurrentFunctionReturnType =
		    typeQueryFunction(modType, &tempStyle, &ignore);
	}

	gCurrentFunctionName = retID;
	gCurrentFunctionClass = class;

	return retList;
}

ParamList makeParamListNode(BUCKET_PTR bucket, DECL_TREE tree)
{
	ParamList retParam;
	BOOLEAN foundError;

	foundError = FALSE;

	retParam = malloc(sizeof(Param));
	if (retParam == NULL)
		fatal("out of memory in makeParam");

	retParam->type = processDeclarator(tree, build_base(bucket),
					   &retParam->id, &foundError,
					   NULL);

	retParam->err = is_error_decl(bucket) || foundError;

	if (retParam->id != NULL && typeQuery(retParam->type) == TYVOID) {
		error("parameter can't be of void type");
		retParam->type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
		retParam->err = TRUE;
	}

	/* change "array[] of T" to "ptr to T" */
	if (typeQuery(retParam->type) == TYARRAY) {
		retParam->type =
		    typeBuildPointer(typeStripModifier(retParam->type),
				     NO_QUAL);
	}

	/* change "func returning T" to "ptr to func returning T" */
	if (typeQuery(retParam->type) == TYFUNCTION) {
		retParam->type =
		    typeBuildPointer(retParam->type, NO_QUAL);
	}

	retParam->sc = get_class(bucket);
	retParam->next = NULL;
	retParam->prev = NULL;
	return (retParam);
}

ParamList appendToParamList(ParamList list1, ParamList list2)
{
	ParamList tempList;
	ST_ID id;
	Type type;

	/* list1 should be non-NULL */
	if (!list1)
		bug("list1 is NULL in attachList");

	/* list2 should be a single node to be attached */
	if (list2->next != NULL || list2->prev != NULL)
		bug("list2 not a singleton in attachList");

	id = list2->id;
	type = list2->type;

	if (typeQuery(type) == TYVOID) {
		error("parameter cannot be of type void");
		list2->err = TRUE;
		list2->type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	tempList = list1;
	while (tempList->next != NULL) {
		if (id != NULL && id == tempList->id) {
			error
			    ("duplicate parameter (%s) declaration ignored",
			     st_get_id_str(id));
			return list1;
		}
		tempList = tempList->next;
	}

	tempList->next = list2;
	list2->prev = tempList;

	return list1;
}

/*
 *  Need to allocate stack slots for parameters.
 */
void allocateStackSlotsForParameters(ParamList list)
{
	Type retType;
	TypeTag retTag;

	/* need to worry about return of struct/union */
	retType = gCurrentFunctionReturnType;
	retTag = typeQuery(retType);
	if (retTag == TYSTRUCT || retTag == TYUNION) {
		gCurrentFunctionOffset +=
		    computeSize(typeBuildPointer
				(typeBuildBasic(TYVOID, NO_QUAL),
				 NO_QUAL));
	}

	while (list != NULL) {
		if (list->id == NULL) {
			error
			    ("function definition parameter must have name");
		} else {
			finishLocalDeclaration(list->id, list->type,
					       list->sc, list->err,
					       TRUE);
		}
		list = list->next;
	}
}

/*
 * Function completes the processing of a type definition indicated
 * by the ST_ID and Type parms.  It checks to see
 * if the id has been previously used at this scope to define a 
 * type.  If so, it issues an error and returns.  Otherwise it
 * installs the type definition into the symbol table.
 */
void finishTypedefDeclaration(ST_ID id, Type type)
{
	ST_DR stdr, oldStdr;
	int block;

	/* Check for previous typedef with this id in this block */
	oldStdr = st_lookup(id, &block);

	if ((oldStdr != NULL) && (block == st_get_cur_block())) {
		error("duplicate typedef in same scope");
		return;
	}

	/* Looks good, so install in symbol table */
	stdr = stdr_alloc();

	stdr->tag = TDEF;
	stdr->u.tdef.type = type;

	st_install(id, stdr);

	return;
}

/*
 * Function builds and returns a MY_SPECIFIER object from the info
 * obtained by retrieving an id's stdr from the symbol table.
 */
MY_SPECIFIER getTypenameSpecifierObject(ST_ID id)
{
	MY_SPECIFIER m;
	ST_DR stdr;
	int i;

	stdr = st_lookup(id, &i);
	m.type = stdr->u.tdef.type;
	m.type_spec = TYPENAME_SPEC;
	m.err = FALSE;

	return m;
}
