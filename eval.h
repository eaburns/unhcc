/**
 * \file eval.h
 * \brief Contains function declarations for compile-time evaluation of
 *        constant expressions.
 * \author Ethan Burns
 * \date 2007-09-22
 */

#include "tree.h"

ExpTree evalUnopTree(UnopTree unop);
ExpTree evalBinopTree(BinopTree binop);

/* vi: set tabstop=8 textwidth=72: */
