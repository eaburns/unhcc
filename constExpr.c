/*
 *  constExpr.c - constant expressions
 *
 *  Unfortunately this code is incomplete and buggy. But it seems
 *  to do the simple cases correctly.
 *
 *  An example of a shortcoming: no support for f.p. constants.
 *
 */

#include "tree.h"
#include "globals.h"
#include "analyze.h"
#include "message.h"
#include "constExpr.h"

static ExpTree reduceBinop(ExpTree);
static ExpTree reduceUnop(ExpTree);
static ExpTree reduceTriop(ExpTree);

/* to get the value from a constant node (eg used for newArrayDeclNode) */
long getConstExprValue(ExpTree e)
{
	int retval;

	retval = 0;

	switch (((Tree) e)->tag) {
	case LONG_CONST_TAG:
		retval = ((LongConstTree) e)->value;
		break;
	case INT_CONST_TAG:
		retval = ((IntConstTree) e)->value;
		break;
	case UINT_CONST_TAG:
		retval = ((UIntConstTree) e)->value;
		break;
	case ULONG_CONST_TAG:
		retval = ((ULongConstTree) e)->value;
		break;
	default:
		bugT((Tree) e,
		     "unexpected tree node in getConstExprValue");
	}
	return retval;
}

/* this is the main entry point: ExpTree is reduced to a constant node. */
/* pjh April 2002: returns TYERROR type if not a constant expression */
ExpTree reduceConstExpr(ExpTree e)
{

	switch (((Tree) e)->tag) {
	case UNOP_TAG:
		e = reduceUnop(e);
		break;
	case BINOP_TAG:
		e = reduceBinop(e);
		break;
	case TRIOP_TAG:
		e = reduceTriop(e);
		break;
	case INT_CONST_TAG:
	case LONG_CONST_TAG:
	case UINT_CONST_TAG:
	case ULONG_CONST_TAG:
		break;
	default:
		e->type = typeBuildBasic(TYERROR, NO_QUAL);
	}

	return e;
}

/*
 *  pjh May 2003
 *
 *  ugly, ugly, ugly
 *
 *  This code needs to be totally reworked, but I am just going
 *  to macro-tize it now so I can begin to straighten some things out.
 *
 *  Note, however, that in general using a complicated macro like this
 *  is NOT a good idea.
 *
 *  Also note that the below code is not portable. It is only correct
 *  when sizeof(int) == sizeof(long). Otherwise the arithmetic conversion
 *  rules (long int, unsigned int) hard-coded below are wrong.
 */
#define binopMacro(OPNAME, OP)\
    case OPNAME: \
      switch( ((Tree)(b->left))->tag ) { \
        case LONG_CONST_TAG: \
          switch( ((Tree)(b->right))->tag ) { \
            case LONG_CONST_TAG: \
              enew = newLongConst( ((LongConstTree)(b->left))->value OP \
                                   ((LongConstTree)(b->right))->value ); \
              break; \
            case INT_CONST_TAG: \
              enew = newLongConst( ((LongConstTree)(b->left))->value OP \
                ((long)((IntConstTree)(b->right))->value) ); \
              break; \
            case UINT_CONST_TAG: \
              enew = newULongConst( \
                ((unsigned long)((LongConstTree)(b->left))->value) OP \
                ((unsigned long)((UIntConstTree)(b->right))->value) ); \
              break; \
            case ULONG_CONST_TAG: \
              enew = newULongConst( \
                ((unsigned long)((LongConstTree)(b->left))->value) OP \
                ((ULongConstTree)(b->right))->value ); \
              break; \
            default: \
              bugT((Tree)(b->right), "unexpected tree node in reduceBinnop"); \
          } \
          break; \
        case INT_CONST_TAG: \
          switch( ((Tree)(b->right))->tag ) { \
            case LONG_CONST_TAG: \
              enew = newLongConst( ((long)((IntConstTree)(b->left))->value) OP \
                                   ((LongConstTree)(b->right))->value ); \
              break; \
            case INT_CONST_TAG: \
              enew = newIntConst( ((IntConstTree)(b->left))->value OP \
                                  ((IntConstTree)(b->right))->value ); \
              break; \
            case UINT_CONST_TAG: \
              enew = newUIntConst( \
                ((unsigned int)((IntConstTree)(b->left))->value) OP \
                ((UIntConstTree)(b->right))->value ); \
              break; \
            case ULONG_CONST_TAG: \
              enew = newULongConst( \
                ((unsigned long)((IntConstTree)(b->left))->value) OP \
                ((ULongConstTree)(b->right))->value ); \
              break; \
            default: \
              bugT((Tree)(b->right), "unexpected tree node in reduceBinnop"); \
          } \
          break; \
        case UINT_CONST_TAG: \
          switch( ((Tree)(b->right))->tag ) { \
            case LONG_CONST_TAG: \
              enew = newULongConst( \
                ((unsigned long)((UIntConstTree)(b->left))->value) OP \
                ((unsigned long)((LongConstTree)(b->right))->value) ); \
              break; \
            case INT_CONST_TAG: \
              enew = newUIntConst( ((UIntConstTree)(b->left))->value OP \
                ((unsigned int)((IntConstTree)(b->right))->value) ); \
              break; \
            case UINT_CONST_TAG: \
              enew = newUIntConst( ((UIntConstTree)(b->left))->value OP \
                                   ((UIntConstTree)(b->right))->value ); \
              break; \
            case ULONG_CONST_TAG: \
              enew = newULongConst( \
                ((unsigned long)((UIntConstTree)(b->left))->value) OP \
                ((ULongConstTree)(b->right))->value ); \
              break; \
            default: \
              bugT((Tree)(b->right), "unexpected tree node in reduceBinnop"); \
          } \
          break; \
        case ULONG_CONST_TAG: \
          switch( ((Tree)(b->right))->tag ) { \
            case LONG_CONST_TAG: \
              enew = newULongConst( ((ULongConstTree)(b->left))->value OP \
                ((unsigned long)((LongConstTree)(b->right))->value) ); \
              break; \
            case INT_CONST_TAG: \
              enew = newULongConst( ((ULongConstTree)(b->left))->value OP \
                ((unsigned long)((IntConstTree)(b->right))->value) ); \
              break; \
            case UINT_CONST_TAG: \
              enew = newULongConst( ((ULongConstTree)(b->left))->value OP \
                ((unsigned long)((UIntConstTree)(b->right))->value) ); \
              break; \
            case ULONG_CONST_TAG: \
              enew = newULongConst( ((ULongConstTree)(b->left))->value OP \
                                    ((ULongConstTree)(b->right))->value ); \
              break; \
            default: \
              bugT((Tree)(b->right), "unexpected tree node in reduceBinnop"); \
          } \
          break; \
        default: \
          bugT((Tree)(b->left), "unexpected tree node in reduceBinnop"); \
      } \
      break;

static ExpTree reduceBinop(ExpTree e)
{
	BinopTree b;
	ExpTree enew;

	b = (BinopTree) e;
	b->right = reduceConstExpr(b->right);
	if (typeQuery(b->right->type) == TYERROR) {
		return b->right;
	}
	b->left = reduceConstExpr(b->left);
	if (typeQuery(b->left->type) == TYERROR) {
		return b->left;
	}

	/* quite the long-winded switch...but it works */
	switch (b->op) {
		binopMacro(ADD_OP, +);
		binopMacro(SUB_OP, -);
		binopMacro(MULT_OP, *);
		binopMacro(DIV_OP, /);
		binopMacro(MOD_OP, %);
		binopMacro(LSHIFT_OP, <<);
		binopMacro(RSHIFT_OP, >>);
		binopMacro(LT_OP, <);
		binopMacro(GT_OP, >);
		binopMacro(LTE_OP, <=);
		binopMacro(GTE_OP, >=);
		binopMacro(EQL_OP, ==);
		binopMacro(NEQL_OP, !=);
		binopMacro(XORR_OP, ^);
		binopMacro(ANDD_OP, &&);
		binopMacro(ORR_OP, ||);
		binopMacro(LAND_OP, &);
		binopMacro(LOR_OP, |);
	default:
		e->type = typeBuildBasic(TYERROR, NO_QUAL);
		enew = e;
	}

	return enew;
}


static ExpTree reduceUnop(ExpTree e)
{
	UnopTree u;
	ExpTree enew;

	u = (UnopTree) e;
	u->left = reduceConstExpr(u->left);

	if (typeQuery(u->left->type) == TYERROR) {
		return u->left;
	}

	switch (u->op) {
	case SIZEOF_OP:
		return analyzeSizeof(u);
	case LNOT_OP:
		switch (((Tree) (u->left))->tag) {
		case LONG_CONST_TAG:
			enew =
			    newLongConst(!((LongConstTree) (u->left))->
					 value);
			break;
		case INT_CONST_TAG:
			enew =
			    newIntConst(!((IntConstTree) (u->left))->
					value);
			break;
		case UINT_CONST_TAG:
			enew =
			    newUIntConst(!((UIntConstTree) (u->left))->
					 value);
			break;
		case ULONG_CONST_TAG:
			enew =
			    newULongConst(!
					  ((ULongConstTree) (u->left))->
					  value);
			break;
		default:
			bugT((Tree) (u->left),
			     "unexpected tree node in reduceUnop");
		}
		break;
	case COMPLMT_OP:
		switch (((Tree) (u->left))->tag) {
		case LONG_CONST_TAG:
			enew =
			    newLongConst(~((LongConstTree) (u->left))->
					 value);
			break;
		case INT_CONST_TAG:
			enew =
			    newIntConst(~((IntConstTree) (u->left))->
					value);
			break;
		case UINT_CONST_TAG:
			enew =
			    newUIntConst(~((UIntConstTree) (u->left))->
					 value);
			break;
		case ULONG_CONST_TAG:
			enew =
			    newULongConst(~
					  ((ULongConstTree) (u->left))->
					  value);
			break;
		default:
			bugT((Tree) (u->left),
			     "unexpected tree node in reduceUnop");
		}
		break;
	case USUB_OP:
		switch (((Tree) (u->left))->tag) {
		case LONG_CONST_TAG:
			enew =
			    newLongConst(-((LongConstTree) (u->left))->
					 value);
			break;
		case INT_CONST_TAG:
			enew =
			    newIntConst(-((IntConstTree) (u->left))->
					value);
			break;
		case UINT_CONST_TAG:
			enew =
			    newUIntConst(-((UIntConstTree) (u->left))->
					 value);
			break;
		case ULONG_CONST_TAG:
			enew =
			    newULongConst(-
					  ((ULongConstTree) (u->left))->
					  value);
			break;
		default:
			bugT((Tree) (u->left),
			     "unexpected tree node in reduceUnop");
		}
		break;
	case UADD_OP:
		enew = u->left;
		break;
	default:
		e->type = typeBuildBasic(TYERROR, NO_QUAL);
		enew = e;
	}

	return enew;
}

static ExpTree reduceTriop(ExpTree e)
{
	TriopTree t;
	int control;

	t = (TriopTree) e;
	t->right = reduceConstExpr(t->right);
	if (typeQuery(t->right->type) == TYERROR) {
		return t->right;
	}
	t->left = reduceConstExpr(t->left);
	if (typeQuery(t->left->type) == TYERROR) {
		return t->left;
	}
	t->middle = reduceConstExpr(t->middle);
	if (typeQuery(t->middle->type) == TYERROR) {
		return t->middle;
	}

	switch (((Tree) (t->left))->tag) {
	case LONG_CONST_TAG:
		control = ((LongConstTree) (t->left))->value;
		break;
	case INT_CONST_TAG:
		control = ((IntConstTree) (t->left))->value;
		break;
	case UINT_CONST_TAG:
		control = ((UIntConstTree) (t->left))->value;
		break;
	case ULONG_CONST_TAG:
		control = ((ULongConstTree) (t->left))->value;
		break;
	default:
		bugT((Tree) t, "unexpected tree tag!");
	}

	if (control) {
		return t->middle;
	} else {
		return t->right;
	}
}
