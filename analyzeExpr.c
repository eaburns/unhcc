/*  Fanny Xu, CS812, Spring 2003
 *  Phase 3
 *  
 * 
 *
 *  analyzeExpr.c - semantic analysis for expressions
 *
 */

#include "analyze.h"
#include "encode.h"
#include "eval.h"
#include "message.h"
#include "semUtils.h"
#include "tree.h"

/* rel start */

/**
 * Returns true if the type is a pointer to an object type. Returns
 * false otherwise.
**/

static BOOLEAN isPointerToObjectType(ExpTree t)
{
	TypeQualifier qual;

	if (!t)
		bug("Null pointer in isPointerToObjectType()");
	if (!isPointerType(t->type))
		return FALSE;
	if (!isObjectType(typeQueryPointer(t->type, &qual)))
		return FALSE;

	return TRUE;
}

/**
 * Returns true if the type is a pointer to an incomplete type. Returns
 * false otherwise.
**/

static BOOLEAN isPointerToInCompleteType(ExpTree t)
{
	TypeQualifier qual;

	if (!t)
		bug("Null pointer in isPointerToInCompleteType()");
	if (!isPointerType(t->type))
		return FALSE;
	if (!isIncompleteType(typeQueryPointer(t->type, &qual)))
		return FALSE;

	return TRUE;
}

/**
 * Returns the 'or' of the type qualifiers of the given types.
**/

static TypeQualifier getBothQualifiers(Type t1, Type t2)
{
	switch (typeGetQualifier(t1)) {
	case NO_QUAL:
		{
			return typeGetQualifier(t2);
		}

	case CONST_QUAL:
		{
			TypeQualifier qual;
			qual = typeGetQualifier(t2);
			if (qual == CONST_VOLATILE_QUAL
			    || qual == VOLATILE_QUAL)
				return CONST_VOLATILE_QUAL;

			return CONST_QUAL;
		}

	case VOLATILE_QUAL:
		{
			TypeQualifier qual;
			qual = typeGetQualifier(t2);
			if (qual == CONST_VOLATILE_QUAL
			    || qual == CONST_QUAL)
				return CONST_VOLATILE_QUAL;

			return VOLATILE_QUAL;
		}

	case CONST_VOLATILE_QUAL:
		{
			return CONST_VOLATILE_QUAL;
		}

	default:
		bug("unknown type qualifier in getBothQualifiers");
	}
	return NO_QUAL;		/* can't reach */
}

/**
 * Computes the member offset and returns the type of the member
 * if it exists through the output paramater. su is the struct/union,
 * id is the id of the member, and type is the output paramater.
**/

static int getMemberOffset(Type su, ST_ID id, Type * type)
{
	int offset;
	offset = computeMemberOffset(su, id);

	if (offset < 0) {
		return offset;
	}

	else {
		MemberList list;
		list = typeRetrieveMembers(su);
		while (list && list->id != id)
			list = list->next;

		if (!list)
			bug("expected to find member in getMemberOffset()");

		*type = list->type;
		return offset;
	}
}

/**
 *
 *******************************************************************************
 * Returns TRUE if the type is an error Type, FALSE otherwise.                 *
 *******************************************************************************
 *
**/

static BOOLEAN isErrorType(Type t)
{
	return typeQuery(t) == TYERROR;
}

/**
 * Sets the line and file information from the from Tree to the to tree.
**/

static void setTreeFileInfo(Tree from, Tree to)
{
	to->lineno = from->lineno;
	to->filename = from->filename;
}

/**
 *
 *******************************************************************************
 * Generic Function used to analyze an opAssign operation, where op is any     *
 * legal c binary operation which can take the form [op]=. For example, this   *
 * function can be used to analyze +=, -=, *=, /=, %=... The paramaters are:   *
 *      b:       The BinopTree with the op assign node as the root.            *
 *      op:      The op in the op assign node.                                 *
 *      analyzeOp: A function which takes a BinopTree, analyzes it, and returns*
 *               an ExpressionTree. This method will be called to analyze      *
 *               the op part of the op assign tree.                            *
 *******************************************************************************
 *
**/

static
ExpTree analyzeOpAssign(BinopTree b, TreeOp op,
			ExpTree analyzeOp(BinopTree))
{
	ExpTree t1;
	ExpTree t2;
	ExpTree t3;
	ExpTree t4;
	ExpTree t5;
	UnopTree left;

	if (!b)
		bug("null tree pointer in analyzeOpAssign op = %d", op);

	if (!b->left || !b->right)
		bug("null sub tree pointer in analyzeOpAssign op = %d",
		    op);

	/* step 0 */
	left = (UnopTree) b->left;

	/* step 1 */
	/* create new op node, remove old opAssign node */
	t1 = newBinop(op, NULL, b->left, b->right);
	setTreeFileInfo((Tree) b, (Tree) t1);

	/* analyze new op node */
	t2 = analyzeOp((BinopTree) t1);
	setTreeFileInfo((Tree) b, (Tree) t2);

	/* if errors, return */
	if (isErrorType(t2->type))
		return t2;

	/* step 2 */
	/* add assign node */
	t3 = newBinop(ASSIGN_OP, NULL, b->left, t2);
	setTreeFileInfo((Tree) b, (Tree) t3);

	/* analyze new assign node */
	t4 = analyzeAssign((BinopTree) t3);

	/* if errors, return */
	if (isErrorType(t4->type))
		return t4;

	t5 = newBinop(ASSIGN_OP, t4->type, (ExpTree) left, ((BinopTree) t4)->right);

	setTreeFileInfo((Tree) b, (Tree) t5);

	return t5;
}

/**
 *
 *******************************************************************************
 * Performs the necessary semantic analyisis for a relational operation tree.  *
 * Checks for errors and performs usual arithmetic conversions if needed.      *
 * Returns the analyzed tree as an ExpTree.                                    *
 *******************************************************************************
 *
**/

static ExpTree analyzeRelational(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;
	TypeQualifier qual;
	Type lType;
	Type rType;

	if (!b)
		bug("null tree pointer in analyzeRelational");

	if (!b->left || !b->right)
		bug("null sub tree pointer in analyzeRelational");

	lop = b->left;
	rop = b->right;

	/* both are are object pointers */
	if (isPointerToObjectType(lop) && isPointerToObjectType(rop)) {
		lType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (lop->type, &qual));
		rType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (rop->type, &qual));

		if (!areCompatibleTypes(lType, rType)) {
			errorT((Tree) b,
			       "invalid operands to relational operation");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isPointerToInCompleteType(lop)
		 && isPointerToInCompleteType(rop)) {
		lType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (lop->type, &qual));
		rType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (rop->type, &qual));

		if (!areCompatibleTypes(lType, rType)) {
			errorT((Tree) b,
			       "invalid operands to relational operation");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isArithmeticType(lop->type)
		 && isArithmeticType(rop->type)) {
		b = (BinopTree) arithmeticConversion(b);
		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else {
		errorT((Tree) b,
		       "invalid operands to relational operation");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	return (ExpTree) b;
}

/* rel end */

/*  70% level  */

/**
 * This function does the semantic checking for the binary '+' operator.
 * ANSI 3.3.6
**/

ExpTree analyzeAdd(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeAdd()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeAdd()\n");

	lop = b->left;
	rop = b->right;

	/* rel start */
	if (isPointerToObjectType(lop) && isPointerToObjectType(rop)) {
		/* both operands can not be of pointer type */
		errorT((Tree) b,
		       "multiple pointer types in pointer addition");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	else if (isPointerToObjectType(lop)) {
		TypeQualifier qual;	/* a temporary output paramater 
					 */
		ExpTree tSize;	/* size of (*lop)->type in tree form */
		ExpTree tMul;	/* the multiplication tree for scaling */

		/* rop must be of integral type */
		if (!isIntegralType(rop->type)) {
			errorT((Tree) b,
			       "non-integral type in pointer addition");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		/* integral promotions on rop */
		rop = (ExpTree) integralPromotion(rop);
		/* build size tree */
		tSize =
		    newIntConst(computeSize
				(typeQueryPointer(lop->type, &qual)));
		setTreeFileInfo((Tree) b, (Tree) tSize);
		/* build the scaling tree */
		tMul = newBinop(MULT_OP, tSize->type, tSize, rop);
		setTreeFileInfo((Tree) b, (Tree) tMul);

		if (tMul->absn.tag == BINOP_TAG
		    && isCompileTimeEvalable(((BinopTree) tMul)->op)
		    && isConstTag(((BinopTree) tMul)->left->absn.tag)
		    && isConstTag(((BinopTree) tMul)->right->absn.tag)) {
			/* compile-time evaluate this if we have two
 			 * constants... this is useful for constant
 			 * array indexing. */
			tMul = evalBinopTree((BinopTree) tMul);
		}

		/* modify return tree */
		b->expn.type = lop->type;
		b->right = tMul;
	}

	else if (isPointerToObjectType(rop)) {
		TypeQualifier qual;	/* a temporary output paramater 
					 */
		ExpTree tSize;	/* size of (*lop)->type in tree form */
		ExpTree tMul;	/* the multiplication tree for scaling */

		/* lop must be of integral type */
		if (!isIntegralType(lop->type)) {
			errorT((Tree) b,
			       "non-integral type in pointer addition");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		/* integral promotions on rop */
		lop = (ExpTree) integralPromotion(lop);
		/* build size tree */
		tSize =
		    newIntConst(computeSize
				(typeQueryPointer(rop->type, &qual)));
		setTreeFileInfo((Tree) b, (Tree) tSize);
		/* build the scaling tree */
		tMul = newBinop(MULT_OP, tSize->type, tSize, lop);
		setTreeFileInfo((Tree) b, (Tree) tMul);
		/* modify return tree */
		b->expn.type = rop->type;
		b->left = tMul;
	}

	else if (!
		 (isArithmeticType(lop->type)
		  && isArithmeticType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary + ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	/* rel end */
	else {
		b = (BinopTree) arithmeticConversion(b);
		b->expn.type = b->left->type;
	}

	return (ExpTree) b;
}

/* This function does the semantic checking for the binary '-' operator.
 * ANSI 3.3.6
 */
ExpTree analyzeSub(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeAdd()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeAdd()\n");

	lop = b->left;
	rop = b->right;


	/* rel start */
	if (isPointerToObjectType(lop) && isPointerToObjectType(rop)) {
		TypeQualifier qual;	/* a temporary output paramater 
					 */
		Type lType;	/* unqualified pointed to type of lop */
		Type rType;	/* unqualified pointed to type of rop */
		ExpTree tSub;	/* the new subtraction tree */
		ExpTree tSize;	/* size of (*lop)->type in tree form */
		ExpTree tDiv;	/* the division tree for scaling */

		lType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (lop->type, &qual));
		rType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (rop->type, &qual));

		/* op's must be pointers to compatible types */
		if (!areCompatibleTypes(lType, rType)) {
			errorT((Tree) b,
			       "non-compatible types in pointer subtraction");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		/* build size tree */
		tSize =
		    newIntConst(computeSize
				(typeQueryPointer(lop->type, &qual)));
		setTreeFileInfo((Tree) b, (Tree) tSize);
		/* modify the new left node of the scaling tree */
		b->expn.type = lop->type;
		tSub =
		    (ExpTree) prefixConvert((ExpTree) b, TYSIGNEDINT);
		/* build the scaling tree */
		tDiv = newBinop(DIV_OP, tSub->type, tSub, tSize);
		setTreeFileInfo((Tree) b, (Tree) tDiv);
		/* modify return type */
		b = (BinopTree) tDiv;
	}

	else if (isPointerToObjectType(lop)) {
		TypeQualifier qual;	/* a temporary output paramater 
					 */
		ExpTree tSize;	/* size of (*lop)->type in tree form */
		ExpTree tMul;	/* the multiplication tree for scaling */

		/* rop must be of integral type */
		if (!isIntegralType(rop->type)) {
			errorT((Tree) b,
			       "non-integral type in pointer subtraction");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		/* integral promotions on rop */
		rop = (ExpTree) integralPromotion(rop);
		/* build size tree */
		tSize =
		    newUIntConst(computeSize
				 (typeQueryPointer(lop->type, &qual)));
		setTreeFileInfo((Tree) b, (Tree) tSize);
		/* build the scaling tree */
		tMul = newBinop(MULT_OP, tSize->type, tSize, rop);
		setTreeFileInfo((Tree) b, (Tree) tMul);
		/* modify return tree */
		b->expn.type = lop->type;
		b->right = tMul;
	}

	else if (isPointerToObjectType(rop)) {
		errorT((Tree) b,
		       "rhs pointer type in pointer subtraction");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	else if (!
		 (isArithmeticType(lop->type)
		  && isArithmeticType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary - ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}
	/* rel end */
	else {
		b = (BinopTree) arithmeticConversion(b);
		b->expn.type = b->left->type;
	}

	return (ExpTree) b;
}

/* This function does the semantic checking for the binary '/' operator.
 * ANSI 3.3.5
 */
ExpTree analyzeDiv(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeDiv()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeDiv()\n");

	lop = b->left;
	rop = b->right;

	if (!
	    (isArithmeticType(lop->type)
	     && isArithmeticType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary / ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	b = (BinopTree) arithmeticConversion(b);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;

}

/* This function does the semantic checking for the binary '*' operator.
 * ANSI 3.3.5
 */
ExpTree analyzeMult(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeMult()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeMult()\n");

	lop = b->left;
	rop = b->right;

	if (!
	    (isArithmeticType(lop->type)
	     && isArithmeticType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary * ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	b = (BinopTree) arithmeticConversion(b);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary '%' opeator.
 * ANSI 3.3.5
 */
ExpTree analyzeMod(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeMod()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeMod()\n");

	lop = b->left;
	rop = b->right;

	/* both operands have to be integral types */
	if (!(isIntegralType(lop->type) && isIntegralType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary % ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	b = (BinopTree) arithmeticConversion(b);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/**
 * This function does the semantic checking for the '*=' operator.
 * ANSI 3.3.16
**/

ExpTree analyzeMultAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, MULT_OP, analyzeMult);
	/* rel end */
}

/**
 *  This function does the semantic checking for the '/=' operator.
 * ANSI 3.3.16
**/

ExpTree analyzeDivAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, DIV_OP, analyzeDiv);
	/* rel end */
}

/**
 * This function does the semantic checking for the '%=' operator.
 * ANSI 3.3.16
**/

ExpTree analyzeModAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, MOD_OP, analyzeMod);
	/* rel end */
}

/**
 * This function does the semantic checking for the '+=' operator.
 * ANSI 3.3.16
**/

ExpTree analyzeAddAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, ADD_OP, analyzeAdd);
	/* rel end */
}

/**
 * This function does the semantic checking for the '-=' operator.
 * ANSI 3.3.16
**/

ExpTree analyzeSubAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, SUB_OP, analyzeSub);
	/* rel end */
}

/**
 * This function does the semantic checking for the unary '-' operator.
 * ANSI 3.3.3.3
**/

ExpTree analyzeUsub(UnopTree u)
{
	ExpTree lop;
	lop = u->left;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeUsub()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeUsub()\n");

	/* the operand shall have arithmetic type */
	if (!(isArithmeticType(lop->type))) {
		errorT((Tree) u, "invalid operand to unary - ");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the integral promotion is performed on the operand */
	u->left = (ExpTree) integralPromotion(lop);
	u->expn.type =
	    typeBuildBasic(typeQuery(u->left->type), NO_QUAL);
	return (ExpTree) u;
}

/* This function does the semantic checking for the unary '+' operator.
 * ANSI 3.3.3.3
 */
ExpTree analyzeUadd(UnopTree u)
{
	ExpTree lop;
	lop = u->left;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeUadd()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeUadd()\n");

	/* the operand shall have arithmetic type */
	if (!(isArithmeticType(lop->type))) {
		errorT((Tree) u, "invalid operand to unary + ");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the integral promotion is performed on the operand */
	u->left = (ExpTree) integralPromotion(lop);
	return (ExpTree) u->left;
}

/*  80% level  */

/* This function does the semantic checking for the unary '!' operator.
 * ANSI 3.3.3
 */
ExpTree analyzeLnot(UnopTree u)
{
	ExpTree lop;
	ExpTree myConst;
	BinopTree biTree;
	Type myType;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeLnot()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeLnot()\n");

	lop = u->left;
	if (!isScalarType(lop->type)) {
		errorT((Tree) u, "invalid operands to unary ! ");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* transforms the tree to 0 == E */
	myConst = newIntConst(0);
	myType = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	biTree = (BinopTree) newBinop(EQL_OP, myType, lop, myConst);

	biTree = (BinopTree) analyzeEql(biTree);
	return (ExpTree) biTree;
}

/**
 * This function does the semantic checking for the binary operator '<'.
 * ANSI 3.3.8
**/

ExpTree analyzeLt(BinopTree b)
{
	/* rel start */
	return analyzeRelational(b);
	/* rel end */
}

/**
 * This function does the semantic checking for the binary operator '>'.
 * ANSI 3.3.8
**/

ExpTree analyzeGt(BinopTree b)
{
	/* rel start */
	return analyzeRelational(b);
	/* rel end */
}

/**
 * This function does the semantic checking for the binary operator '<='.
 * ANSI 3.3.8
**/

ExpTree analyzeLte(BinopTree b)
{
	/* rel start */
	return analyzeRelational(b);
	/* rel end */
}

/**
 * This function does the semantic checking for the binary operator '>='.
 * ANSI 3.3.8
**/

ExpTree analyzeGte(BinopTree b)
{
	/* rel start */
	return analyzeRelational(b);
	/* rel end */
}

/* This function does the semantic checking for the binary operator '=='.
 * ANSI 3.3.9
 */
ExpTree analyzeEql(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeEql()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeEql()\n");

	lop = b->left;
	rop = b->right;

	/* rel start */
	if (isArithmeticType(lop->type) && isArithmeticType(rop->type)) {
		/* legal */
		b = (BinopTree) arithmeticConversion(b);
		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isNullPointerConst(lop)) {
		/* must be null pointer const and pointer type */
		if (!isPointerType(rop->type)) {
			errorT((Tree) b,
			       "invalid operands to binary == ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isNullPointerConst(rop)) {
		/* must be null pointer const and pointer type */
		if (!isPointerType(lop->type)) {
			errorT((Tree) b,
			       "invalid operands to binary == ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isVoidPointerType(lop->type)) {
		/* must be void pointer and pointer to object or
		   incomplete type */
		if (!
		    (isPointerToObjectType(rop)
		     || isPointerToInCompleteType(rop))) {
			errorT((Tree) b,
			       "invalid operands to binary == ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isVoidPointerType(rop->type)) {
		/* must be void pointer and pointer to object or
		   incomplete type */
		if (!
		    (isPointerToObjectType(lop)
		     || isPointerToInCompleteType(lop))) {
			errorT((Tree) b,
			       "invalid operands to binary == ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isPointerType(lop->type) && isPointerType(rop->type)) {
		/* must be compatible types */
		if (!areCompatibleTypes(lop->type, rop->type)) {
			errorT((Tree) b,
			       "invalid operands to binary == ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	/* rel end */
	else {
		errorT((Tree) b, "invalid operands to binary == ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '!='.
 * ANSI 3.3.9
 */
ExpTree analyzeNeql(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeEql()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeEql()\n");

	lop = b->left;
	rop = b->right;

	/* rel start */
	if (isArithmeticType(lop->type) && isArithmeticType(rop->type)) {
		/* legal */
		b = (BinopTree) arithmeticConversion(b);
		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isNullPointerConst(lop)) {
		/* must be null pointer const and pointer type */
		if (!isPointerType(rop->type)) {
			errorT((Tree) b,
			       "invalid operands to binary != ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isNullPointerConst(rop)) {
		/* must be null pointer const and pointer type */
		if (!isPointerType(lop->type)) {
			errorT((Tree) b,
			       "invalid operands to binary != ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isVoidPointerType(lop->type)) {
		/* must be void pointer and pointer to object or
		   incomplete type */
		if (!
		    (isPointerToObjectType(rop)
		     || isPointerToInCompleteType(rop))) {
			errorT((Tree) b,
			       "invalid operands to binary != ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isVoidPointerType(rop->type)) {
		/* must be void pointer and pointer to object or
		   incomplete type */
		if (!
		    (isPointerToObjectType(lop)
		     || isPointerToInCompleteType(lop))) {
			errorT((Tree) b,
			       "invalid operands to binary != ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	else if (isPointerType(lop->type) && isPointerType(rop->type)) {
		/* must be compatible types */
		if (!areCompatibleTypes(lop->type, rop->type)) {
			errorT((Tree) b,
			       "invalid operands to binary != ");
			b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) b;
		}

		b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	}

	/* rel end */
	else {
		errorT((Tree) b, "invalid operands to binary != ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '&&'.
 * ANSI 3.3.13
 */
ExpTree analyzeLand(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeLand()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeLand()\n");

	lop = b->left;
	rop = b->right;

	/* rel start */
	if (!(isScalarType(lop->type) && isScalarType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary && ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}
	/* rel end */

	b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '||'.
 * ANSI 3.3.13
 */
ExpTree analyzeLor(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeLor()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeLor()\n");

	lop = b->left;
	rop = b->right;

	/* rel start */
	if (!(isScalarType(lop->type) && isScalarType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary && ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}
	/* rel end */

	b->expn.type = typeBuildBasic(TYSIGNEDINT, NO_QUAL);
	return (ExpTree) b;
}

/*  90% level  */

/* This function does the semantic checking for the unary operator '~'.
 * ANSI 3.3.3.3
 */
ExpTree analyzeComplmt(UnopTree u)
{
	ExpTree lop;

	if (!u)
		bugT((Tree) u,
		     "Null tree pointer in analyzeComplmt()\n");
	if (!(u->left))
		bugT((Tree) u,
		     "Node must have one operand in analyzeComplmt()\n");

	lop = u->left;
	if (!(isIntegralType(lop->type))) {
		errorT((Tree) u, "invalid operands to unary ~ ");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the integral promotion is performed on the operand */
	u->left = (ExpTree) integralPromotion(lop);
	u->expn.type = u->left->type;
	return (ExpTree) u;
}

/* This function does the semantic checking for the binary operator '<<'.
 * ANSI 3.3.7
 */
ExpTree analyzeLshift(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	if (!b)
		bugT((Tree) b,
		     "Null tree pointer in analyzeLshift()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeLshift()\n");

	lop = b->left;
	rop = b->right;
	if (!(isIntegralType(lop->type) && isIntegralType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary << ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	/* the integral promotion is performed on the operands */
	b->left = (ExpTree) integralPromotion(b->left);
	b->right = (ExpTree) integralPromotion(b->right);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '>>'.
 * ANSI 3.3.7
 */
ExpTree analyzeRshift(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	if (!b)
		bugT((Tree) b,
		     "Null tree pointer in analyzeRshift()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeRshift()\n");

	lop = b->left;
	rop = b->right;
	if (!(isIntegralType(lop->type) && isIntegralType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary >> ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	/* the integral promotion is performed on the operands */
	b->left = (ExpTree) integralPromotion(b->left);
	b->right = (ExpTree) integralPromotion(b->right);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '&'.
 * ANSI 3.3.10
 */
ExpTree analyzeAndd(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeAndd()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeAndd()\n");

	lop = b->left;
	rop = b->right;
	if (!(isIntegralType(lop->type) && isIntegralType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary & ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	b = (BinopTree) arithmeticConversion(b);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '^'.
 * ANSI 3.3.11
 */
ExpTree analyzeXorr(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeXorr()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeXorr()\n");

	lop = b->left;
	rop = b->right;
	if (!(isIntegralType(lop->type) && isIntegralType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary ^ ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	b = (BinopTree) arithmeticConversion(b);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '|'.
 * ANSI 3.3.12
 */
ExpTree analyzeOrr(BinopTree b)
{
	ExpTree lop;
	ExpTree rop;

	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeOrr()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have both left and right operands in analyzeOrr()\n");

	lop = b->left;
	rop = b->right;
	if (!(isIntegralType(lop->type) && isIntegralType(rop->type))) {
		errorT((Tree) b, "invalid operands to binary | ");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	b = (BinopTree) arithmeticConversion(b);
	b->expn.type =
	    typeBuildBasic(typeQuery(b->left->type), NO_QUAL);
	return (ExpTree) b;
}

/* This function does the semantic checking for the binary operator '<<='.
 * ANSI 3.3.16
 */
ExpTree analyzeLeftAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, LSHIFT_OP, analyzeLshift);
	/* rel end */
}

/* This function does the semantic checking for the binary operator '>>='. 
 * ANSI 3.3.16
 */
ExpTree analyzeRightAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, RSHIFT_OP, analyzeRshift);
	/* rel end */
}

/* This function does the semantic checking for the binary operator '&='.
 * ANSI 3.3.16
 */
ExpTree analyzeAndAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, ANDD_OP, analyzeAndd);
	/* rel end */
}

/* This function does the semantic checking for the binary operator '|='.
 * ANSI 3.3.16
 */
ExpTree analyzeOrAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, ORR_OP, analyzeOrr);
	/* rel end */
}

/* This function does the semantic checking for the binary operator '^='.
 * ANSI 3.3.16
 */
ExpTree analyzeXorAssign(BinopTree b)
{
	/* rel start */
	return analyzeOpAssign(b, XORR_OP, analyzeXorr);
	/* rel end */
}

/*  100% level  */

/* This function does the semantic checking for the postfix '++' operator.
 * ANSI 3.3.2.4
 */
ExpTree analyzeIncra(UnopTree u)
{
	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeIncra()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeIncra()\n");

	/* the following checks could be done at once, but it's tested
	   in two steps in order to issue more specific error messages.
	   the operand has to be a Lvalue */
	if (!isLval(u->left)) {
		errorT((Tree) u, "invalid lvalue in postfix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the operand has to be modifiable */
	if (!isModifiableLval(u->left)) {
		errorT((Tree) u,
		       "read-only variable in posifix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the operand has to be a scalar type */
	if (!isScalarType(u->left->type)) {
		errorT((Tree) u,
		       "invalid type argument to postfix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* if is pointer - must be pointer to object type */
	if (isPointerType(u->left->type) && !isPointerToObjectType(u->left)) {
		errorT((Tree) u,
		       "invalid type argument to postfix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	u->expn.type = u->left->type;

	return (ExpTree) u;
}

/* This function does the semantic checking for the prefix '++' operator.
 * ANSI 3.3.3
 */
ExpTree analyzeIncrb(UnopTree u)
{
	ExpTree myConst;
	BinopTree biTree;
	Type myType;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeIncrb()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeIncrb()\n");

	/* the operand shall be a modifiable lvalue */
	if (!isLval(u->left)) {
		errorT((Tree) u, "invalid lvalue in prefix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}
	if (!isModifiableLval(u->left)) {
		errorT((Tree) u,
		       "read-only variable in prefix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the operand has to be a scalar type */
	if (!isScalarType(u->left->type)) {
		errorT((Tree) u, "invalid operand to prefix ++");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* rel start */
	/* if is pointer - must be pointer to object type */
	if (isPointerType(u->left->type) && !isPointerToObjectType(u->left)) {
		errorT((Tree) u,
		       "invalid type argument to postfix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}
	/* rel end */

	/* transforms the tree and let addAssign does the analysis */
	myConst = newIntConst(1);
	/* rel start */
	myType = u->left->type;
	/* rel finish */
	biTree =
	    (BinopTree) newBinop(ADD_ASSIGN_OP, myType, u->left, myConst);
	biTree = (BinopTree) analyzeAddAssign(biTree);
	return (ExpTree) biTree;
}

/* This function does the semantic checking for the postfix '--' operator.
 * ANSI 3.3.2.4
 */
ExpTree analyzeDecra(UnopTree u)
{
	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeDecra()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeDecra()\n");

	/* the operand shall be a modifiable lvalue */
	if (!isLval(u->left)) {
		errorT((Tree) u, "invalid lvalue in postfix decrement");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}
	if (!isModifiableLval(u->left)) {
		errorT((Tree) u,
		       "read-only variable in postfix decrement");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the operand has to be a scalar type */
	if (!isScalarType(u->left->type)) {
		errorT((Tree) u,
		       "invalid operand to postfix decrement");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* if is pointer - must be pointer to object type */
	if (isPointerType(u->left->type) && !isPointerToObjectType(u->left)) {
		errorT((Tree) u,
		       "invalid type argument to postfix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	u->expn.type = u->left->type;
	return (ExpTree) u;
}

/* This function does the semantic checking for the prefix '--' operator.
 * ANSI 3.3.3
 */
ExpTree analyzeDecrb(UnopTree u)
{
	ExpTree myConst;
	BinopTree biTree;
	Type myType;

	/* error checkings */
	if (!u)
		bugT((Tree) u, "Null tree pointer in analyzeDecrb()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeDecrb()\n");

	/* he operand shall be a modifiable lvalue */
	if (!isLval(u->left)) {
		errorT((Tree) u, "invalid lvalue in prefix decrement");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}
	if (!isModifiableLval(u->left)) {
		errorT((Tree) u,
		       "read-only variable in prefix decrement");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* the operand has to be a scalar type */
	if (!isScalarType(u->left->type)) {
		errorT((Tree) u, "invalid operand to prefix --");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* rel start */
	/* if is pointer - must be pointer to object type */
	if (isPointerType(u->left->type) && !isPointerToObjectType(u->left)) {
		errorT((Tree) u,
		       "invalid type argument to postfix increment");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}
	/* rel end */

	myConst = newIntConst(1);
	/* rel start */
	myType = u->left->type;
	/* rel finish */
	biTree =
	    (BinopTree) newBinop(SUB_ASSIGN_OP, myType, u->left, myConst);
	biTree = (BinopTree) analyzeSubAssign(biTree);
	return (ExpTree) biTree;
}

/* This function does semantic checking on the comma operator.
 * ANSI 3.3.17
 */
ExpTree analyzeComma(BinopTree b)
{
	/* error checkings */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeComma()\n");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Node must have two operands in analyzeComma()\n");

	/* the result has the right operand's type */
	b->expn.type = b->right->type;
	return (ExpTree) b;
}

/* This function does semantic checking for the conditional operator.
 * ANSI 3.3.15
 */
ExpTree analyzeCond(TriopTree t)
{
	ExpTree lop;
	ExpTree mop;
	ExpTree rop;
	TypeQualifier qual;

	lop = t->left;
	mop = t->middle;
	rop = t->right;

	/* error checkings */
	if (!t)
		bugT((Tree) t, "Null tree pointer in analyzeCond()\n");
	if (!(lop && mop && rop))
		bugT((Tree) t,
		     "Node must have three operands in analyzeCond()\n");

	/* the first operand shall have scalar type */
	if (!isScalarType(lop->type)) {
		errorT((Tree) t,
		       "invalid first operand to conditional operator ?:");
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) t;
	}

	/* both are arithmetic types */
	if (isArithmeticType(mop->type) && isArithmeticType(rop->type)) {
		t->left = rop;
		t = (TriopTree) arithmeticConversion((BinopTree) t);
		t->right = t->left;
		t->left = lop;
		t->expn.type =
		    typeBuildBasic(typeQuery(t->right->type), NO_QUAL);
	}

  /*************/
	/* rel start */
  /*************/

	else if (typeQuery(mop->type) == TYVOID
		 && typeQuery(rop->type) == TYVOID) {
		t->expn.type = typeBuildBasic(TYVOID, NO_QUAL);
	}

	/* both are struct/union types */
	else if (isStructUnionType(mop->type)
		 && isStructUnionType(rop->type)) {
		if (!areCompatibleTypes(mop->type, rop->type)) {
			errorT((Tree) t,
			       "invalid operand type in conditional expression ?: ");
			t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) t;
		}

		t->expn.type = mop->type;
	}

	/* null pointer and pointer */
	else if (isNullPointerConst(mop) && isPointerType(rop->type)) {
		Type type;
		type = typeQueryPointer(rop->type, &qual);
		qual = getBothQualifiers(mop->type, rop->type);
		t->expn.type = typeBuildPointer(type, qual);
	}

	/* pointer and null pointer const */
	else if (isPointerType(mop->type) && isNullPointerConst(rop)) {
		Type type;
		type = typeQueryPointer(mop->type, &qual);
		qual = getBothQualifiers(mop->type, rop->type);
		t->expn.type = typeBuildPointer(type, qual);
	}

	/* pointer to void and non-function pointer */
	else if (isVoidPointerType(mop->type)
		 && isPointerType(rop->type)) {
		Type type;
		if (isFunctionPointerType(rop->type)) {
			errorT((Tree) t,
			       "invalid operand type in conditional expression ?: ");
			t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) t;
		}

		type = typeQueryPointer(mop->type, &qual);
		qual = getBothQualifiers(mop->type, rop->type);
		t->expn.type = typeBuildPointer(type, qual);
	}

	/* non-function pointer and pointer to void */
	else if (isPointerType(mop->type)
		 && isVoidPointerType(rop->type)) {
		Type type;
		if (isFunctionPointerType(mop->type)) {
			errorT((Tree) t,
			       "invalid operand type in conditional expression ?: ");
			t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) t;
		}

		type = typeQueryPointer(rop->type, &qual);
		qual = getBothQualifiers(mop->type, rop->type);
		t->expn.type = typeBuildPointer(type, qual);
	}

	/* both are compatible pointers */
	else if (isPointerType(mop->type) && isPointerType(rop->type)) {
		TypeQualifier qual;
		Type mType;
		Type rType;
		Type type;

		mType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (mop->type, &qual));
		rType =
		    typeUnqualifiedVersion(typeQueryPointer
					   (rop->type, &qual));

		if (!areCompatibleTypes(mType, rType)) {
			errorT((Tree) t,
			       "invalid operand type in conditional expression ?: ");
			t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) t;
		}

		qual = getBothQualifiers(mop->type, rop->type);
		type = typeFormComposite(mType, rType);
		t->expn.type = typeBuildPointer(type, qual);
	}

	else {
		errorT((Tree) t,
		       "invalid operand type in conditional expression ?: ");
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) t;
	}

	t->left = newUnop(LNOT_OP, t->left->type, t->left);
	setTreeFileInfo((Tree) t, (Tree) (t->left));
	if (t->left->absn.tag == UNOP_TAG
	    && ((UnopTree) t->left)->op == LNOT_OP) {
		t->left = analyzeLnot((UnopTree) t->left);
	}

  /**************/
	/* rel finish */
  /**************/

	if (typeQuery(((ExpTree) t->left)->type) == TYERROR) {
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
	}

	return (ExpTree) t;
}

/* This function does semantic checking for the cast operator.
 * ANSI 3.3.4
 */
ExpTree analyzeCast(CastopTree t)
{
	ExpTree lop;
	UnopTree uTree;

	/* error checkings */
	if (!t)
		bugT((Tree) t, "Null tree pointer in analyzeCast()\n");
	if (!t->left)
		bugT((Tree) t,
		     "Node must have left operand in analyzeCast()\n");

	/* type name shall specify void type or scalar type */
	if (!
	    (typeQuery(t->typename) == TYVOID
	     || isScalarType(t->typename))) {
		errorT((Tree) t, "invalid type name to cast");
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) t;
	}

	lop = t->left;
	/* the operand shall have scalar type */
	if (!isScalarType(lop->type)) {
		errorT((Tree) t, "invalid operand to cast");
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) t;
	}

	/* can't mix function pointers and non-function pointers */
	if (isFunctionPointerType(t->typename)
	    && isObjectPointerType(lop->type)) {
		errorT((Tree) t, "can't convert to function pointer");
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) t;
	}

	/* ditto */
	if (isObjectPointerType(t->typename)
	    && isFunctionPointerType(lop->type)) {
		errorT((Tree) t, "can't convert from function pointer");
		t->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) t;
	}

	uTree = (UnopTree) newUnop(CONVERT_OP, t->typename, lop);
	return (ExpTree) uTree;
}

/* This function does semantic checking for the sizeof operator.
 * ANSI 3.3.3.4
 */
ExpTree analyzeSizeof(UnopTree u)
{
	int mySize;
	UIntConstTree myConst;

	/* error checkings */
	if (!u)
		bugT((Tree) u,
		     "Null tree pointer in analyzeSizeof()\n");
	if (!u->left)
		bugT((Tree) u,
		     "Node must have left operand in analyzeSizeof()\n");

	/* the operator shall not be applied to an expression that has
	   function type */
	if (typeQuery(u->left->type) == TYFUNCTION) {
		errorT((Tree) u, "sizeof applied to a function type");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* operator shall not be applied to an expression that has
	   incomplete type */
	if (isIncompleteType(u->left->type)) {
		errorT((Tree) u,
		       "sizeof applied to an incomplete type");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	mySize = computeSize(u->left->type);
	myConst = (UIntConstTree) newUIntConst(mySize);
	return (ExpTree) myConst;
}

/*************
 * rel start *
*************/

/**
 * This function does semantic checking for the '&' operator.
 * ANSI 3.3.3.2
**/

ExpTree analyzeAddrOf(UnopTree u)
{
	Type ptr2expr;		/* the new pointer expression from the
				   resulting operator */

	/* check for bugs */
	if (!u)
		bug("Null tree pointer in analyzeAddrOf()");
	if (!u->left)
		bugT((Tree) u,
		     "Null subtree pointer in analyzeAddrOf()");

	/* operator shall be function designator or lval */
	if (!(isFunctionType(u->left->type) || isLval(u->left))) {
		/* print error msg, set error type and exit early */
		errorT((Tree) u,
		       "addrOf type must be function designator or lval");
		u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) u;
	}

	/* value can not have register storage class */
	if (u->left->absn.tag == VAR_TAG) {
		VarTree var;
		var = (VarTree) u->left;
		if (var->class == REGISTER_SC) {
			errorT((Tree) u, "addrOf type must not have "
			       "register storage class");
			u->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
			return (ExpTree) u;
		}
		if (var->stdrTag == LDECL || var->stdrTag == PDECL) {
			vartabAddressed(var->offset,
			                var->block,
			                var->stdrTag);
		}
	}

	/* build new ptr type */
	ptr2expr = typeBuildPointer(u->left->type, NO_QUAL);

	/* modify return type */
	u->expn.type = ptr2expr;

	/* return the new expression */
	return (ExpTree) u;
}

/**
 * Analyzes the index '[]' operator. Modifies the tree so that
 * it is a DEREF ontop of an ADD_OP on top of expr1 and expr2.
**/

ExpTree analyzeIndex(BinopTree b)
{
	ExpTree add;
	ExpTree deref;
	ExpTree lop;
	ExpTree rop;

	/* check for bugs */
	if (!b)
		bugT((Tree) b, "Null tree pointer in analyzeIndex()");
	if (!(b->left && b->right))
		bugT((Tree) b,
		     "Null subtree pointer in analyzeIndex()");

	lop = b->left;
	rop = b->right;

	/* lop is integral and rop is pointer */
	if ((isIntegralType(lop->type) && isPointerToObjectType(rop)) ||
	    (isIntegralType(rop->type) && isPointerToObjectType(lop))) {

/*
		if (isPointerToObjectType(rop))
			rop = newUnop(ADDR_OF_OP, rop->type, rop);
		else
			lop = newUnop(ADDR_OF_OP, lop->type, lop);
*/

		/* build add tree */
		add = newBinop(ADD_OP, NULL, lop, rop);
		setTreeFileInfo((Tree) b, (Tree) add);
		add = analyzeAdd((BinopTree) add);
		if (isErrorType(add->type))
			return add;

		/* build deref tree */
		deref = newUnop(DEREF_OP, NULL, add);
		setTreeFileInfo((Tree) b, (Tree) deref);
		deref = analyzeUnop((UnopTree) deref);
		if (isErrorType(deref->type))
			return deref;
	}

	else {
		/* illegal index operands */
		errorT((Tree) b, "illegal index operands");
		b->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) b;
	}

	return deref;
}

/**
 * This function does semantic analysis for the '->' operator.
 * ANSI 3.3.3.2
*/

ExpTree analyzePtr(PtrTree p)
{
	ExpTree t;
	ExpTree deref;

	/* check for bugs */
	if (!p)
		bug("Null tree pointer in analyzePtr()");
	if (!p->left || !p->id)
		bugT((Tree) p, "Null subtree pointer in analyzePtr()");

	/* build deref node */
	deref = newUnop(DEREF_OP, NULL, p->left);
	setTreeFileInfo((Tree) p, (Tree) deref);
	deref = analyzeUnop((UnopTree) deref);
	if (isErrorType(deref->type))
		return deref;

	/* build field ref node */
	t = newFieldRef(p->id, NULL, deref);
	setTreeFileInfo((Tree) p, (Tree) t);
	t = analyzeFieldRef((FieldRefTree) t);

	return t;
}

/**
 * Analyzes the '.' operator.
 * ANSI 3.3.2.3
**/

ExpTree analyzeFieldRef(FieldRefTree f)
{
	int offset;		/* the offset of the member */
	Type type;		/* the type of the member */

	/* check for bugs */
	if (!f)
		bugT((Tree) f, "Null tree pointer in analyzeFieldRef");
	if (!f->left || !f->id)
		bugT((Tree) f,
		     "Null subtree pointer in analyzeFieldRef");

	/* left operand must be of struct union type */
	if (!isStructUnionType(f->left->type)) {
		errorT((Tree) f,
		       "operand of '.' must be struct/union type");
		f->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) f;
	}

	/* member must exist in the struct/union type */
	offset = getMemberOffset(f->left->type, f->id, &type);

	/* member not found */
	if (offset < 0) {
		errorT((Tree) f, "illegal struct/union member %s",
		       st_get_id_str(f->id));
		f->expn.type = typeBuildBasic(TYERROR, NO_QUAL);
		return (ExpTree) f;
	}

	/* set the type to be type of member */
	f->expn.type = type;

	/* if the member is derefable, add the deref node */
	if (isDerefable(type)) {
		return newUnop(DEREF_OP, type, (ExpTree) f);
	}

	/* return the expression */
	else {
		return newUnop(NO_OP, type, (ExpTree) f);
	}
}

/**************
 * rel finsih *
**************/
