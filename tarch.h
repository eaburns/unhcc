/**
 * \file tarch.h
 * \brief Includes the give target architecture header file.
 *
 * \note This file includes the more specific header file for the target
 *       architecture.  For example tarch_ia32.h when compiling code for
 *       an IA-32.
 *
 * \author Ethan Burns
 * \date 2007-09-10
 *
 * The following constants must be defined to the size in bytes of the
 * given datatype for the specific target arcitecture:
 * TARCH_SIZEOF_SCHAR   signed char
 * TARCH_SIZEOF_UCHAR   unsigned char
 * TARCH_SIZEOF_SSHORT  signed short
 * TARCH_SIZEOF_USHORT  unsigned short
 * TARCH_SIZEOF_SINT    signed int
 * TARCH_SIZEOF_UINT    unsigned int
 * TARCH_SIZEOF_SLONG   signed long
 * TARCH_SIZEOF_ULONG   unsigned long
 * TARCH_SIZEOF_FLOAT   float
 * TARCH_SIZEOF_DOUBLE  double
 * TARCH_SIZEOF_LDOUBLE long double
 */
#ifndef _TARCH_H_
#define _TARCH_H_

#include "tarch_em64t.h"

#define TARCH_SIZEOF_POINTER	TARCH_SIZEOF_ULONG

void move(REG, REG);
const char *moveString(TypeTag);
const char *registerAs(char, TypeTag);

#endif /* !_TARCH_H_ */
/* vi: set tabstop=8 textwidth=72: */
